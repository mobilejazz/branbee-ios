public struct BrCommunity: Hashable, Codable {
    public let id: String
    public let name: String
    public let picture: String
    public let thumb: String
    public let members: Int
    public let actions: Int
    public let stories: Int
    public let isMember: Bool?
    public let isPrivate: Bool?
    public init(
        id: String,
        name: String,
        picture: String,
        thumb: String,
        members: Int,
        actions: Int,
        stories: Int,
        isMember: Bool?,
        isPrivate: Bool?
    ) {
        self.id = id
        self.name = name
        self.picture = picture
        self.thumb = thumb
        self.members = members
        self.actions = actions
        self.stories = stories
        self.isMember = isMember
        self.isPrivate = isPrivate
    }
}

extension BrCommunity {
    public static var branbeCommunity: Self {
        Self(
            id: "1",
            name: "Branbee Community",
            picture: "https://homepages.cae.wisc.edu/~ece533/images/fruits.png",
            thumb: "https://homepages.cae.wisc.edu/~ece533/images/frymire.png",
            members: 10,
            actions: 87,
            stories: 5,
            isMember: false,
            isPrivate: false
        )
    }
}

extension BrCommunity {
    public static var unicefCommunity: Self {
        Self(
            id: "2",
            name: "Unicef Community",
            picture: "https://homepages.cae.wisc.edu/~ece533/images/fruits.png",
            thumb: "https://homepages.cae.wisc.edu/~ece533/images/frymire.png",
            members: 100,
            actions: 827,
            stories: 10,
            isMember: true,
            isPrivate: true
        )
    }
}
