public struct BrNotification: Equatable {
    public let id: String
    public let text: String
    public let status: Int
    public let date: Date
    public var type: NotificationType
    public init(
        id: String,
        text: String,
        type: NotificationType = .other,
//        projectId: String? = nil,
//        communityId: String? = nil,
//        acceptLink: String? = nil,
//        rejectLink: String? = nil,
     //   url: String,
        status: Int,
        date: Date
    ) {
        self.id = id
        self.text = text
        self.status = status
        self.date = date
        self.type = type
        
//        if let storyId = storyId {
//            self.type = .story(id: storyId)
//        }
//        
//        if let storyId = projectId {
//            self.type = .project(id: storyId)
//        }
//        
//        if let storyId = communityId {
//            self.type = .community(id: storyId)
//        }
//        
//        if let rejectLink = rejectLink, let acceptLink = acceptLink {
//            self.type = .request(acceptLink: acceptLink, rejectLink: rejectLink)
//        }
        
                
    }
    
    public enum NotificationType: Equatable {
        case project(id: String)
        case community(id: String)
        case story(id: String)
        case other
        case request(acceptLink: String, rejectLink: String)
    }
}


extension BrNotification {
    public static var story: Self {
        .init(
            id: "1",
            text: "Majid request something to your action to be linked to",
            type: .story(id: "12"),
            status: 1,
            date: Date()
        )
    }
    public static var project: Self {
        .init(
            id: "1",
            text: "Majid request something to your action to be linked to",
            type: .project(id: "12"),
            status: 1,
            date: Date()
        )
    }
}


