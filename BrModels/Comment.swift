public struct BrComment: Hashable {
    public let creator: BrUser
    public let body: String
    public let date: Date
    public init(
        creator: BrUser,
        body: String,
        date: Date
    ) {
        self.creator = creator
        self.body = body
        self.date = date
    }
    
    public enum ´Type´ {
        case project, story
    }
    
}


let text =
"""
Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
"""

extension BrComment {
    public static var comment: BrComment {
        .init(
            creator: .blob,
            body: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
            date: Date()
        )
    }
    
    public static var shortComment: BrComment {
        .init(
            creator: .blob,
            body: "Lorem Ipsum is simply dummy text of the printing and typesetting",
            date: Date()
        )
    }
    
    public static var customComment: BrComment {
        .init(
            creator: Bool.random() ? .majid: .blob,
            body: String(text.prefix(Int.random(in: 10...100))),
            date: Date(timeIntervalSinceNow: -Double.random(in: 200...1000))
        )
    }
    
}
