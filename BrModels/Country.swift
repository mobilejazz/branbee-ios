public struct BrCountry: Hashable, Codable {
    public let id, name: String
    public init(
        id: String,
        name: String
    ) {
        self.id = id
        self.name = name
    }
}


extension BrCountry {
    public static var spain: BrCountry {
        BrCountry(
            id: "Spain",
            name: "Spain"
        )
    }
    public static var brazil: BrCountry {
        BrCountry(
            id: "brazil",
            name: "Brazil"
        )
    }
}
