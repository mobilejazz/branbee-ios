public struct BrProject: Hashable {
    public let id: String
    public let title: String
    public let body: String
    public let picture: String?
    public let target: Int
    public let pledged: Int
    public let comments: Int
    public let creator: BrUser
    public let community: BrCommunity
    public let backers: Int
    public let date: Date
    public let enDate: Date?
    
    public init(
        id: String,
        title: String,
        body: String,
        picture: String?,
        target: Int,
        pledged: Int,
        comments: Int,
        creator: BrUser,
        community: BrCommunity,
        backers: Int,
        date: Date,
        enDate: Date?
    ) {
        self.id = id
        self.title = title
        self.body = body
        self.picture = picture
        self.target = target
        self.pledged = pledged
        self.comments = comments
        self.creator = creator
        self.community = community
        self.backers = backers
        self.date = date
        self.enDate = enDate
    }    
}

extension BrProject {
    public static var branbee: BrProject {
        BrProject(
            id: "id",
            title: "Branbee Project v1.0",
            body: "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200",
            picture: "https://homepages.cae.wisc.edu/~ece533/images/fruits.png",
            target: 2000,
            pledged: 30,
            comments: 120,
            creator: .blob,
            community: .branbeCommunity,
            backers: 19,
            date: Date(),
            enDate: nil
        )
    }
}


extension BrProject {
    public static var unicef: BrProject {
        BrProject(
            id: "id",
            title: "Unicef Project v1.0",
            body: "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200",
            picture: "https://homepages.cae.wisc.edu/~ece533/images/fruits.png",
            target: 250,
            pledged: 200,
            comments: 19,
            creator: .blob,
            community: .branbeCommunity,
            backers: 19,
            date: Date(),
            enDate: nil
        )
    }
}
