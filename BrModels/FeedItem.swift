public struct BrFeedItem: Hashable {
    public enum `Type` {
        case story
        case project
        case community
    }
    public let type: Type
    public let story: BrStory?
    public let project: BrProject?
    public let community: BrCommunity?
    public init(
        type: Type,
        story: BrStory? = nil,
        project: BrProject? = nil,
        community: BrCommunity? = nil
    ) {
        self.type = type
        self.story = story
        self.project = project
        self.community = community
    }
    
    public var id: UUID = UUID()
        
    
    
    public static func == (lhs: Self, rhs: Self) -> Bool {
        lhs.id == rhs.id
    }
    
    public func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }

}


extension BrFeedItem {
    public static var brStoryFeed: Self {
        BrFeedItem(
            type: .story,
            story: .branbeeStory
        )
    }
    public static var brBookMarkedStoryFeed: Self {
        BrFeedItem(
            type: .story,
            story: .branbeeBookMarkedStory
        )
    }
    public static var brProjectFeed: Self {
        BrFeedItem(
            type: .project,
            project: .branbee
        )
    }
    public static var brUnicefProjectFeed: Self {
        BrFeedItem(
            type: .project,
            project: .unicef
        )
    }
    public static var brCommunityFeed: Self {
        BrFeedItem(
            type: .community,
            community: .branbeCommunity
        )
    }
}

extension BrProject {
    public static var testProject: Self {
        .init(
            id: "5f9c3adba0ff91b514860169",
            title: "Test Project",
            body: "<p>test project</p>",
            picture: "https://branbee.com/undefined",
            target: 1000,
            pledged: 85,
            comments: 0,
            creator: BrUser.init(
                id: "55ef55b0adc11c34263d14ee",
                email: nil,
                username: "Patrick",
                photo: "https://branbee.com/uploads/users/55ef55b0adc11c34263d14ee/1543361239113_kD69vvBH_400x400.jpg",
                thumb: "https://branbee.com/uploads/communities/563535616ab7da08cc42d884/thumb_1543431089630_vlad-tchompalov-219130-unsplash.jpg",
                city: nil,
                country: nil,
                biography: "",
                emailVerified: false
            ),
            community: BrCommunity(
                id: "563535616ab7da08cc42d884",
                name: "Welcome Community",
                picture: "https://branbee.com/uploads/communities/563535616ab7da08cc42d884/1543431089630_vlad-tchompalov-219130-unsplash.jpg",
                thumb: "https://branbee.com/uploads/communities/563535616ab7da08cc42d884/thumb_1543431089630_vlad-tchompalov-219130-unsplash.jpg",
                members: 59,
                actions: 1,
                stories: 1,
                isMember: true,
                isPrivate: false
            ),
            backers: 0,
            date: Date(),
            enDate: nil
        )
    }
}


extension BrFeedItem {
    public static var feed_1: Self {
        .init(
            type: .project,
            story: nil,
            project: .testProject,
            community: nil
        )
    }
}

extension BrStory {
    public static var salma: Self {
        .init(
            id: "5e60e3e0d683998e526b63ff",
            content: [
                BrContent(
                    type: .text,
                    content:
                        "<p style=\"text-align: justify;\"><b>Selma.</b></p><p style=\"text-align: justify;\">This is Selma’s story, little girl who is 11 years-old. She is one the 38% of the population who is living in the rural areas, and of the 26% of girls living there who go to school. She lives in a small village in the heights of the Atlas Mountains, close to Marrakech, a cosmopolite city.</p><p style=\"text-align: justify;\">Selma is very lucky because her parents push her to study, which is far from being the case for all the families living in the rural areas. For many, the “good place” of a woman is still at home. Idea fuelled by cultural norms where the so-called traditional roles of men and women predominate. The Selma’s mother, Janna, is illiterate, she only knows how to write her name. she is one the millions of Moroccans who never enter in a classroom. She got married at 16, the age at which most European girls are in their first year of high school.</p><p style=\"text-align: justify;\">In the village of Selma, there is a school. Sometimes the access conditions to the educational infrastructures are very bad: the closer schools are far from home; or to reach them you have to take dirt roads a bit dangerous. Out of 100 girls in her village, Selma is one of 48 who go on to secondary school. Will she be among the 40.8% to continue after college?</p><p style=\"text-align: justify;\">Also, Selma has the opportunity to speak Berber and Arabic languages thanks to her aunt who was able to leave her village to live in the city. Rural people speak Berber; when Arabic is the most commonly spoken language in the country. The Selma’s neighbour and friend, Yasmine, only speaks Berber so she has to stay at home while Selma is going to school. The classes are taught in Arabic and the teachers know nothing about the Berber language. </p><p style=\"text-align: justify;\">the omnipresence of the school dropout, especially for girls, causes a problem in the rural Morocco. Parents provide for their families, sometimes with great difficulty. Poor living conditions force girls to stay at home to help.</p><p style=\"text-align: justify;\">It has been suggested that education threatens a woman\'s likelihood and ability to become a wife.</p><p style=\"text-align: justify;\">Let us stop defining a woman by her status as a mother or a wife, she is still a woman.</p><p style=\"text-align: justify;\">Too often, the first question a woman is asked, even before knowing her first name, is &#34;do you have children? ». Thus positioning motherhood as the pinnacle of a life. But not all women dream of giving birth, of having a dog and a nice husband with whom she could buy a detached house and go on holiday to a seaside resort. I mean, what could be more reductive than to lock a woman into her sole procreative role, making &#34;abnormal&#34; a woman who does not want children or &#34;shameful&#34; a sterile woman. Why point the finger at this woman who doesn\'t want to be a mother and accuse her of selfishness? Isn\'t it the opposite? Dependent beings are born to fill some kind of emptiness, and sometimes they grow up in conditions that they have not chosen. A binary vision that completely characterizes the human species: male/female; black/white; good/evil; normal/abnormal... And do we need to be more and more human beings on Earth?</p><p style=\"text-align: justify;\">Not all women aspire to the same life, and fortunately, otherwise the world would only be made up of girls dressed in pink who want to become princesses, and boys dressed in blue who want to be dragon hunters and marry the princess.</p><p style=\"text-align: justify;\">The only key to emancipation, to open-mindedness, is education; it is also a fundamental right of every human being. A woman, until proven otherwise, is a human being.</p>"
                )
            ],
            shoulds: 5,
            comments: 0,
            creator: BrUser(
                id: "5e440f8f9ee363012df8ed84",
                email: nil,
                username: "Julie Dnts",
                photo: "https://branbee.com/images/avatar2.png",
                thumb: "https://branbee.com/images/avatar2.png",
                city: nil,
                country: nil,
                biography: "",
                emailVerified: nil
            ),
            community: BrCommunity(
                id: "5e1d9dee9aa98f2e0c27c5cf",
                name: "High Atlas Foundation",
                picture: "https://branbee.com/images/header4.jpg",
                thumb: "https://branbee.com/images/thumb_header4.jpg",
                members: 24,
                actions: 0,
                stories: 56,
                isMember: true,
                isPrivate: false
            ),
            date: Date(),
            remainingBoosts: 0,
            isBookmarked: false
        )
    }
}

extension BrFeedItem {
    public static var story_salma: Self {
        .init(
            type: .story,
            story: .salma,
            project: nil,
            community: nil
        )
    }
}

extension BrStory {
    public static var video: Self {
        .init(
            id: "5e60e1c4d683998e526b63c8",
            content: [
                BrContent(
                    type: .video,
                    content: "https://branbee.com/uploads/stories/5e1d9dee9aa98f2e0c27c5cf/1583407541949_One wish.mp4"
                )],
            shoulds: 0,
            comments: 0,
            creator: BrUser.init(
                id: "5e440f8f9ee363012df8ed84",
                email: nil,
                username: "Julie Donts",
                photo: "https://branbee.com/images/avatar2.png",
                thumb: "https://branbee.com/images/avatar2.png",
                city: nil,
                country: nil,
                biography: "",
                emailVerified: nil
            ),
            community: BrCommunity(
                id: "5e1d9dee9aa98f2e0c27c5cf",
                name: "High Atlas Foundation",
                picture: "https://branbee.com/images/thumb_header4.jpg",
                thumb: "https://branbee.com/images/header4.jpg",
                members: 24,
                actions: 0,
                stories: 56,
                isMember: true,
                isPrivate: false
            ),
            date: Date(),
            remainingBoosts: 0,
            isBookmarked: true
        )
    }
}

extension BrFeedItem {
    public static var story_picture: Self {
        .init(
            type: .story,
            story: .picture,
            project: nil,
            community: nil
        )
    }
}

extension BrStory {
    public static var picture: Self {
        .init(
            id: "5fa5955e63d69072598ea92d",
            content: [
                BrContent.init(type: .text, content: "testing"),
                BrContent.init(type: .picture, content: "https://branbee.com/uploads/1604687197181_ei_16046871739146648398498355397865.jpg"),
            ],
            shoulds: 2,
            comments: 1,
            creator: BrUser.init(
                id: "5bfea5a2c5f576f04aceccb5",
                email: nil,
                username: "Younes Berrada",
                photo: "https://branbee.com/images/avatar3.png",
                thumb: "https://branbee.com/images/avatar3.png",
                city: nil,
                country: nil,
                biography: "",
                emailVerified: nil
            ),
            community: BrCommunity(
                id: "5e1d9dee9aa98f2e0c27c5cf",
                name: "High Atlas Foundation",
                picture: "https://branbee.com/images/thumb_header4.jpg",
                thumb: "https://branbee.com/images/header4.jpg",
                members: 24,
                actions: 0,
                stories: 56,
                isMember: true,
                isPrivate: false
            ),
            date: Date(),
            remainingBoosts: 0,
            isBookmarked: false
        )
    }
}

extension BrFeedItem {
    public static var story_video: Self {
        .init(
            type: .story,
            story: .video,
            project: nil,
            community: nil
        )
    }
}
