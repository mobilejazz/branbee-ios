public struct BrDraft: Hashable, Codable {
    public let userId,
               communityId,
               communityName: String
    public let text: String?
    public let cachedImageFileKey: String?
    public let createdAtTimestampInMillis: Int
    public let updatedAtTimestampInMillis: Int
    public init(
        userId: String,
        communityId: String,
        communityName: String,
        text: String?,
        cachedImageFileKey: String?,
        createdAtTimestampInMillis: Int,
        updatedAtTimestampInMillis: Int
    ) {
        self.userId = userId
        self.communityId = communityId
        self.communityName = communityName
        self.text = text
        self.cachedImageFileKey = cachedImageFileKey
        self.createdAtTimestampInMillis = createdAtTimestampInMillis
        self.updatedAtTimestampInMillis = updatedAtTimestampInMillis
    }
        
}

extension BrDraft {
    public static var draft: Self {
        Self(
            userId: "1",
            communityId: "3",
            communityName: "Unicef",
            text: "text text",
            cachedImageFileKey: "pexels",
            createdAtTimestampInMillis: 1480134638,
            updatedAtTimestampInMillis: 1480134638
        )
    }
    public static var textDraft: Self {
        Self(
            userId: "2",
            communityId: "3",
            communityName: "Unicef",
            text: "text text",
            cachedImageFileKey: nil,
            createdAtTimestampInMillis: 1480134638,
            updatedAtTimestampInMillis: 1480134638
        )
    }
    public static var imageDraft: Self {
        Self(
            userId: "3",
            communityId: "3",
            communityName: "Unicef",
            text: nil,
            cachedImageFileKey: "pexels-1",
            createdAtTimestampInMillis: 1480134638,
            updatedAtTimestampInMillis: 1480134638
        )
    }
}
