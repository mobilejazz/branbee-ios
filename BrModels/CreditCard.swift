public struct CreditCard: Equatable {
    public let cardId: String
    public let holder: String
    public let last4Digits: String
    public let expiryMonth: Int32
    public let expiryYear: Int32
    public init(
        id: String,
        holder: String,
        last4Digits: String,
        expiryMonth: Int32,
        expiryYear: Int32
    ) {
        self.cardId = id
        self.holder = holder
        self.last4Digits = last4Digits
        self.expiryMonth = expiryMonth
        self.expiryYear = expiryYear
    }
}


public struct CardParams: Equatable {
    public var number: String?
    public var currency: String?
    public var name: String?
    public var expMonth: UInt = 0
    public var expYear: UInt = 0
    public var cvc: String?
    public init(
        number: String?,
        currency: String?,
        name: String?,
        expMonth: UInt = 0,
        expYear: UInt = 0,
        cvc: String?
    ) {
        self.number = number
        self.currency = currency
        self.name = name
        self.expMonth = expMonth
        self.expYear = expYear
        self.cvc = cvc
    }
}


extension CreditCard {
    public static var visa: Self {
        .init(
            id: "1",
            holder: "Younes",
            last4Digits: "2231",
            expiryMonth: 12,
            expiryYear: 2022
        )
    }
}
