public struct BrUser: Hashable, Equatable {
    public var id: String
    public var email: String?
    public var username: String
    public var city: String?
    public var country: BrCountry?
    public var biography: String
    public var emailVerified: Bool?
    public var photo: String
    public var thumb: String
        
    public init(
        id: String,
        email: String? = nil,
        username: String,
        photo: String,
        thumb: String,
        city: String? = nil,
        country: BrCountry? = nil,
        biography: String,
        emailVerified: Bool? = nil
    ) {
        self.id = id
        self.email = email
        self.username = username
        self.photo = photo
        self.thumb = thumb
        self.city = city
        self.country = country
        self.biography = biography
        self.emailVerified = emailVerified
    }
    
    
}

extension BrUser: Codable {}

extension BrUser {
    public var _photoURL: URL? {
        return URL(string: photo)
    }
}

extension BrUser {
        public static let blob = BrUser(
        id: "1",
        email: "blob@gmail.com",
        username: "Blob",
        photo: "https://homepages.cae.wisc.edu/~ece533/images/airplane.png",
        thumb: "https://homepages.cae.wisc.edu/~ece533/images/airplane.png",
        city: "Madrid",
        country: .spain,
        biography: "i am blob a new branbee user, i want to help other people make life easier"
    )
}

extension BrUser {
    public static let majid = BrUser(
        id: "2",
        email: "majid@gmail.com",
        username: "majid",
        photo: "https://homepages.cae.wisc.edu/~ece533/images/arctichare.png",
        thumb: "https://homepages.cae.wisc.edu/~ece533/images/airplane.png",
        city: "Brazil",
        country: .brazil,
        biography: "i am blob a new branbee user, i want to help other people make life easier"
    )
}
