public struct BrByteArrayFromFile {
    public let filename: String
    public let data: Data
    public init(
        filename: String,
        data: Data
    ) {
        self.filename = filename
        self.data = data        
    }
}
