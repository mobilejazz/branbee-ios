public struct BrContent: Hashable {    
    public enum `Type` {
        case video
        case text
        case picture
    }
    public let type: Type
    public let content: String
    public init(
        type: Type,
        content: String
    ) {
        self.type = type
        self.content = content
    }
}

extension BrContent {
    public var pictureURL: URL? {
        guard self.type == .picture else { return nil }
        return URL(string: content)
    }
}

extension BrContent {
    public var videoURL: URL? {
        guard self.type == .video else { return nil }
        guard let string = content.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else { return nil }
        return URL(string: string)
    }
}

extension BrContent {
    public var text: String? {
        guard self.type == .text else { return nil }
        return content
    }
}


extension BrContent {
    public static var brPictureContent: Self {
        Self(
            type: .picture,
            content: "https://homepages.cae.wisc.edu/~ece533/images/airplane.png"
        )
    }
    public static var brImageContent: Self {
        Self(
            type: .picture,
            content: "https://homepages.cae.wisc.edu/~ece533/images/arctichare.png"
        )
    }
    public static var brTextContent: Self {
        Self(
            type: .text,
            content: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum"
        )
    }
    public static var brShortTextContent: Self {
        Self(
            type: .text,
            content: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's"
        )
    }
}
