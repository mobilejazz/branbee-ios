import Library
import UIKit
import Validated
import ComposableArchitecture
import ReactiveSwift
import Tagged
import ImageLoader
import BrModels
import ExploreClient


extension Bundle {
    public static var account: Bundle {
        class BundleClass {}
        return Bundle(for: BundleClass.self)
    }
}

public struct AccountInfoState: Equatable {
    public var currentUser: BrUser?
    public var imageURL: URL?
    public var name: Name?
    public var image: UIImage?
    public var email: Email?
    public var password: Password?
    public var country: BrCountry?
    public var countries: [BrCountry]
    public var biography: String?
    public var city: String?
    public var isFormValid: Bool = false
    public var loadingState: LoadingState? = nil
    public var alert: _AlertState<AccountInfoAction>? = nil
    public var actionAlert: _ActionSheetState<AccountInfoAction>?
    public enum PickerType: Equatable {
        case camera, photoLibrary
    }
    public var pickerType: PickerType? = nil
                
    public init(
        currentUser: BrUser?,
        name: Name? = nil,
        image: UIImage? = nil,
        email: Email? = nil,
        password: Password? = nil,
        country: BrCountry? = nil,
        countries: [BrCountry] = [],
        biography: String? = nil,
        city: String? = nil,
        isLoginRequestInFlight: Bool = false,
        isFormValid: Bool = false,
        actionAlert: _ActionSheetState<AccountInfoAction>? = nil,
        alert: _AlertState<AccountInfoAction>? = nil
    ) {
        self.currentUser = currentUser
        self.image = image
        self.name = name
        self.password = password
        self.email = email
        self.country = country
        self.countries = countries
        self.biography = biography
        self.city = city
        self.isFormValid = isFormValid
        self.alert = alert
        self.actionAlert = actionAlert
    }
}


public enum AccountInfoAction: Equatable {
    case viewDidLoad
    case emailTextFieldDidChanged(Email)
    case nameTextFieldDidChanged(Name)
    case cityTextFieldDidChanged(String?)
    case passwordTextFieldDidChanged(Password?)
    case bioTextViewDidChanged(String?)
    case pickedCountry(BrCountry?)
    case pickedImage(UIImage)
    case setStoryImageURL(URL?)
    case viewDismissed
    case viewDidDisppear
    case nextButtonTapped
    case editButtonTapped
    case showPhotos, showCamera
    case retry(RetryAction)
    case event(UpdateUserDelegateEvent)
}

public let accountInfoReducer: Reducer<AccountInfoState, AccountInfoAction, SystemEnvironment<AccountEnvironment>> = .combine(
    Reducer { state, action, environment in
        struct CancelDelegateId: Hashable {}
        switch action {
        case let .emailTextFieldDidChanged(email):
            state.email = email
            return .none
        case let .nameTextFieldDidChanged(name):
            state.name = name
            return .none
        case let .cityTextFieldDidChanged(city):
            state.city = city
            return .none
        case let .passwordTextFieldDidChanged(password):
            state.password = password
            return .none
        case let .bioTextViewDidChanged(bio):
            state.biography = bio
            return .none
        case let .pickedCountry(country):
            state.country = country
            return .none
        case .viewDismissed:
            state.actionAlert = nil
            state.alert = nil
            return .none
        case .nextButtonTapped:
            return environment.updateUserClient
                .updateUser(
                        UserUpdateRequest(
                            email: state.email?.rawValue ?? "",
                            password: state.password?.rawValue,
                            name: state.name?.rawValue ?? "",
                            city: state.city,
                            country: state.country,
                            bio: state.biography,
                            avatarPath: state.imageURL?.path
                        )
                ).fireAndForget()
        case .showPhotos:
            state.pickerType = .photoLibrary
            return .none
        case .showCamera:
            state.pickerType = .camera
            return .none
        case .editButtonTapped:
            state.actionAlert = _ActionSheetState(
                title: "Change Profile Image",
                message: nil,
                buttons: [
                    .default("Select From Photos", send: .showPhotos),
                    .default("Select From Camera", send: .showCamera),
                    .cancel(send: .viewDismissed)
                ]
            )
            return .none
        case let .pickedImage(image):
            state.image = image
            return .none
        case .viewDidLoad:
            state.name = Name(rawValue: state.currentUser?.username ?? "")
            state.email = state.currentUser?.email.map(Email.init(rawValue:))
            state.country = state.currentUser?.country
            state.city = state.currentUser?.city
            state.biography = state.currentUser?.biography
            guard let photoURL = state.currentUser?._photoURL else {
                return .none
            }
            return .merge(
                environment.imageLoader
                    .loadFromURL(photoURL)
                    .compactMap(id)
                    .map(AccountInfoAction.pickedImage),
                environment.updateUserClient
                    .delegate
                    .cancellable(id: CancelDelegateId())
                    .map(AccountInfoAction.event),
                environment.updateUserClient
                    .onViewLoad
                    .fireAndForget()
            )
        case let .retry(action):
            return .fireAndForget(action.run)
        case .event(.didUpdatedSuccessfully):
            state.loadingState = nil
            return .none
        case let .event(.showCountries(countries)):
            state.countries = countries
            return .none
        case .event(.showLoading):
            state.loadingState = .loading
            return .none
        case .event(.hideLoading):
            state.loadingState = nil
            return .none
        case .event(.invalidEmail):
            state.alert = .init(
                title: "Invalid Email",
                dismissButton: .cancel(send: .viewDismissed)
            )
            return .none
        case let .event(.showTransientFailure(failure)):
            state.alert = .init(
                title: "Ooops!",
                message: failure.message,
                dismissButton: .cancel(send: .viewDismissed)
            )
            return .none
        case let .event(.showFullScreenFailure(failure, retry: action)):
            state.loadingState = .failure(failure, action: action)
            return .none
        case .event(.showFieldsRequiredMessage):
            state.alert = .init(title: "Ooops!", message: "Please fill all fields")
            return .none
        case .viewDidDisppear:
            return .cancel(id: CancelDelegateId())
        case let .event(.displayUser(user)):
            state.currentUser = user
            return .none
        case let .setStoryImageURL(imageURL):
            state.imageURL = imageURL
            return .none
        }
    }
)

private let titleLabelStyle: (UILabel) -> Void =
    brLabelDarkStyle
    <> heightAnchor(.br_grid(10))
    <> centerStyle

private let itemLabelStyle: (UILabel) -> Void =
    brLabelDarkBoldStyle <> {
        $0.textAlignment = .left
}

private let profileViewStyle: (UIImageView) -> Void =
    autoLayoutStyle
    <> roundedStyle(cornerRadius: .br_grid(10))
    <> {
        $0.image = UIImage(
            named: "default_avatar",
            in: .account,
            compatibleWith: nil
        )
        $0.contentMode = .scaleAspectFill
    } <> squareStyle(.br_grid(20))
     

private let editButtonStyle: (UIButton) -> Void =
    { (btn: UIButton) in
        btn.setImage(
            UIImage(named: "ic_edit"),
            for: .normal
        )
        btn.backgroundColor = UIColor.white
    } <> autoLayoutStyle
    <> brShadowStyle
    <> squareStyle(.br_grid(10))
    <> roundedStyle(cornerRadius: .br_grid(5))

private let rootStackViewStyle: (UIStackView) -> Void =
    verticalStackStyle(.br_grid(3), alignment: .fill)

let itemStackViewStyle: (UIStackView) -> Void =
    verticalStackStyle(.br_grid(2),
                    distribution: .fill,
                    alignment: .fill
)

let bioStackViewStyle: (UIStackView) -> Void =
    verticalStackStyle(.br_grid(2),
                    distribution: .fillProportionally,
                    alignment: .fill
)

private let topStackViewStyle: (UIStackView) -> Void =
    verticalStackStyle(.br_grid(4),
                       distribution: .fill,
                       alignment: .leading
    )

private let placeholderLabelStyle: (UILabel) -> Void = {
    $0.text = "Write some lines about yourself..."
    $0.sizeToFit()
    $0.frame.origin = CGPoint(
        x: .br_grid(5),
        y: .br_grid(4)
    )
    $0.textColor = UIColor.lightGray
}

private let nextButtonStyle: (LoadingButton) -> Void =
    brMainLoadingButtonStyle
    <> heightAnchor(.br_grid(12))

public class AccountInfoViewController: UIViewController {
                     
    let store: Store<AccountInfoState, AccountInfoAction>
    let viewStore: ViewStore<AccountInfoState, AccountInfoAction>
    
    let placeholderLabel = UILabel()
    let countryPickerView = UIPickerView()
    let scrollView = UIScrollView()
    let rootView = UIView()
    var activeView: UIView?
    
    public init(store: Store<AccountInfoState, AccountInfoAction>) {
        self.store = store
        self.viewStore = ViewStore(store)
        super.init(nibName: nil, bundle: nil)
    }
        
    var countries: [BrCountry] = [] {
        didSet {
            countryPickerView.reloadAllComponents()
        }
    }
        
    required init?(coder: NSCoder) {
        fatalError("unimplemented nscoder")
    }
        
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        viewStore.send(.viewDidLoad)
        
        self.view.backgroundColor = .white
        self.title = "Account"
        
        // scroll setup
        scrollViewStyle(scrollView)
        rootViewStyle(rootView)
        view.addSubview(scrollView)
        scrollView.addSubview(rootView)
        scrollView.constrainEdges(to: view)
        rootView.constrainEdges(to: scrollView)
        
        rootView |> sameWidth(scrollView)
                 <> heightAnchor(.br_grid(200))
               
        // Profile Stack
        let profileImageView = UIImageView()
        profileViewStyle(profileImageView)
        let topStackView = UIStackView(
            arrangedSubviews: [
                profileImageView
            ]
        )
        topStackViewStyle(topStackView)
                                 
        // Name Stack
        let nameLabel = UILabel()
        itemLabelStyle(nameLabel)
        nameLabel.text = "Name"
        let nameTextField = UITextField()
        defaultTextFieldStyle(nameTextField)
        nameTextField.placeholder = "Your Name"
        let nameItemStackView = UIStackView(arrangedSubviews: [
            nameLabel,
            nameTextField
        ])
        itemStackViewStyle(nameItemStackView)
        
        // Email Stack
        let emailLabel = UILabel()
        itemLabelStyle(emailLabel)
        emailLabel.text = "Email"
        let emailTextField = UITextField()
        emailTextFieldStyle(emailTextField)
        let emailItemStackView = UIStackView(arrangedSubviews: [
            emailLabel,
            emailTextField
        ])
        itemStackViewStyle(emailItemStackView)
                                        
        // City Stack
        let cityLabel = UILabel()
        itemLabelStyle(cityLabel)
        cityLabel.text = "City"
        let cityTextField = UITextField()
        defaultTextFieldStyle(cityTextField)
        cityTextField.placeholder = "City"
        let cityItemStackView = UIStackView(arrangedSubviews: [
            cityLabel,
            cityTextField
        ])
        itemStackViewStyle(cityItemStackView)
        
        // Country StackView
        let countryLabel = UILabel()
        itemLabelStyle(countryLabel)
        countryLabel.text = "Country"
        let countryTextField = UITextField()
        countryTextFieldStyle(countryTextField)
        let countryItemStackView = UIStackView(arrangedSubviews: [
            countryLabel,
            countryTextField
        ])
        itemStackViewStyle(countryItemStackView)
        
        // Password StackView
        let passwordLabel = UILabel()
        itemLabelStyle(passwordLabel)
        passwordLabel.text = "Password"
        let passwordTextField = UITextField()
        countryTextFieldStyle(passwordTextField)
        let passwordItemStackView = UIStackView(arrangedSubviews: [
            passwordLabel,
            passwordTextField
        ])
        itemStackViewStyle(passwordItemStackView)
                                
        // Biography StackView
        let bioLabel = UILabel()
        itemLabelStyle(bioLabel)
        bioLabel.text = "Bio"
        let bioTextView = UITextView()
        bioTextView.delegate = self
        bioTextView.addSubview(placeholderLabel)
        placeholderLabelStyle(placeholderLabel)
        placeholderLabel.isHidden = !bioTextView.text.isEmpty
                        
        defaultTextViewStyle(bioTextView)
        let bioItemStackView = UIStackView(arrangedSubviews: [
            bioLabel,
            bioTextView
        ])
        bioStackViewStyle(bioItemStackView)
        let nextButton = LoadingButton()
        nextButton
            .addTarget(self,
                       action: #selector(nextButtonTapped),
                       for: .touchUpInside
        )
        nextButtonStyle(nextButton)
        nextButton.setTitle("Save Changes")
        
        let rootStackView = UIStackView(arrangedSubviews: [
            topStackView,
            nameItemStackView,
            emailItemStackView,
            cityItemStackView,
            countryItemStackView,
            bioItemStackView,
            nextButton,
            UIView()
        ])
        
        //Edit Button
        let editButton = UIButton()
        editButtonStyle(editButton)
        editButton
            .addTarget(self,
                       action: #selector(editButtonTapped),
                       for: .touchUpInside
        )
        topStackView.addSubview(editButton)
        NSLayoutConstraint.activate([
            editButton.bottomAnchor.constraint(equalTo: profileImageView.bottomAnchor, constant: .br_grid(2)),
            editButton.trailingAnchor.constraint(equalTo: profileImageView.trailingAnchor, constant: .br_grid(2))
        ])
        
        rootStackViewStyle(rootStackView)
        rootView.addSubview(rootStackView)
        rootStackView |>
            rootViewStyle(parent: rootView, .square(.br_grid(4)))
                                                        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShowNotification),
            name: UIResponder.keyboardWillShowNotification,
            object: nil
        )

        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillHideNotification),
            name: UIResponder.keyboardWillHideNotification,
            object: nil
        )

        nameTextField
            .addTarget(self,
                       action: #selector(nameTextChanged),
                       for: .editingChanged)
        nameTextField.delegate = self
        
        emailTextField
            .addTarget(self,
                       action: #selector(emailTextChanged),
                       for: .editingChanged)
        emailTextField.delegate = self
                            
        cityTextField
            .addTarget(self,
                       action: #selector(cityTextChanged),
                       for: .editingChanged)
        cityTextField.delegate = self
                
        countryPickerView.delegate = self
        countryPickerView.dataSource = self
        countryTextField.inputView = countryPickerView
                
        tapGestureDismissalStyle(self)
                        
        viewStore.produced.image
            .assign(to: \.image, on: profileImageView)
        
        viewStore.produced.name
            .map{ $0?.rawValue }
            .assign(to: \.text, on: nameTextField)

        viewStore.produced.email
            .map{ $0?.rawValue }
            .assign(to: \.text, on: emailTextField)

        viewStore.produced.city
            .assign(to: \.text, on: cityTextField)

        viewStore.produced.country
            .map{ $0?.name }
            .assign(to: \.text, on: countryTextField)
        
        viewStore.produced.countries
            .assign(to: \.countries, on: self)

        viewStore.produced.biography
            .startWithValues { [weak self] biography in
                bioTextView.text = biography
                self?.placeholderLabel.isHidden = biography != ""
            }

        let loadingView = LoadingView()
        self.view.addSubview(loadingView)
        loadingView.frame = view.frame
        self.view.bringSubviewToFront(loadingView)
        
        viewStore.produced.loadingState
            .startWithValues { state in
                loadingView.state = state
                loadingView.isHidden = state == nil
            }

        viewStore.produced.alert
            .startWithValues { [weak self] (alert) in
                guard let self = self else { return }
                guard let alert = alert else { return }
                let alertController = UIAlertController(
                    title: alert.title,
                    message: alert.message,
                    preferredStyle: .alert
                )
                alertController.addAction(
                    UIAlertAction(
                        title: "Cancel",
                        style: .default,
                        handler: { _ in
                            self.viewStore.send(.viewDismissed)
                    })
                )
                self.present(alertController, animated: true, completion: nil)
        }
        
        viewStore.produced
            .actionAlert
            .startWithValues { [weak self] alert in
                guard let self = self else { return }
                guard let alert = alert else { return }
                let actionController = UIAlertController(
                    title: alert.title,
                    message: alert.message,
                    preferredStyle: .actionSheet
                )
                alert.buttons
                    .map { $0.toUIKit(send: self.viewStore.send) }
                    .forEach(actionController.addAction)
                self.present(actionController,
                             animated: true,
                             completion: nil)
        }
        
        self.viewStore.produced.pickerType
            .startWithValues { [weak self] picker in
                guard let picker = picker else {
                    if let pickerViewController = UIApplication.shared
                        .windows.first?
                        .rootViewController?
                        .presentedViewController as? UIImagePickerController {
                        pickerViewController.dismiss(animated: true, completion: nil)
                    }
                    return
                }
                let pickerController = UIImagePickerController()
                pickerController.sourceType = picker == .camera ? .camera: .photoLibrary
                pickerController.delegate = self
                self?.present(pickerController,
                              animated: false,
                              completion: nil)
            }
                
    }

    @objc
    func emailTextChanged(_ textField: UITextField) {
        guard let email = textField.text
                .map(Email.init(rawValue:)) else { return }
        viewStore.send(.emailTextFieldDidChanged(email))
    }
           
    @objc
    func nameTextChanged(_ textField: UITextField) {
        guard let name = textField.text
                .map(Name.init(rawValue:)) else { return }
        viewStore.send(.nameTextFieldDidChanged(name))
    }
    
    @objc
    func cityTextChanged(_ textField: UITextField) {
        guard let city = textField.text else { return }
        viewStore.send(.cityTextFieldDidChanged(city))
    }
    
    @objc
    func bioTextChanged(_ textView: UITextView) {
        guard let bio = textView.text else { return }
        viewStore.send(.bioTextViewDidChanged(bio))
    }
    
    @objc func keyboardWillShowNotification(_ notification: NSNotification) {
        let height = keyboardHeight(notification)
        scrollView.contentInset = .bottomEdge(height)
        scrollView.scrollIndicatorInsets = .bottomEdge(height)
                                
        var aRect = rootView.frame
        aRect.size.height -= height
        
        if let activeView = activeView as? UITextView {
            guard var bkgndRect = activeView.superview?.frame else { return }
            bkgndRect.size.height += height
            activeView.superview?.frame = bkgndRect
            scrollView.setContentOffset(.init(x: .zero, y: activeView.frame.origin.y+height), animated: true)
        }
        
        if let activeView = activeView as? UITextField {
            if !aRect.contains(activeView.frame.origin) {
                self.scrollView.scrollRectToVisible(
                    activeView.frame,
                    animated: true
                )
            }
        }
              
    }
    
    @objc func keyboardWillHideNotification(_ notification: NSNotification) {
        scrollView.contentInset = .zero
        scrollView.scrollIndicatorInsets = .zero
    }
        
    @objc
    func nextButtonTapped(_ btn: UIButton) {
        viewStore.send(.nextButtonTapped)
    }
    
    @objc
    func editButtonTapped(_ btn: UIButton) {
        viewStore.send(.editButtonTapped)
    }
           
    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)        
        if !isMovingToParent {
            self.viewStore.send(.viewDismissed)
        }
    }
    
}

extension AccountInfoViewController: UIImagePickerControllerDelegate & UINavigationControllerDelegate {
    
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        viewStore.send(.viewDismissed)
    }
    
    public func imagePickerController(
        _ picker: UIImagePickerController,
        didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[.originalImage] as? UIImage,
           let url = info[.imageURL] as? URL {
            viewStore.send(.pickedImage(image))
            viewStore.send(.setStoryImageURL(url))
        }
        picker.dismiss(animated: true)
    }
    
}


extension AccountInfoViewController: UITextViewDelegate, UITextFieldDelegate {
    public func textViewDidChange(_ textView: UITextView) {
        placeholderLabel.isHidden = !textView.text.isEmpty
    }
    
    public func textViewDidBeginEditing(_ textView: UITextView) {
        self.activeView = textView
    }
    
    public func textFieldDidBeginEditing(_ textField: UITextField) {
        self.activeView = textField
    }
    
    public func textFieldDidEndEditing(_ textField: UITextField) {
        self.activeView = nil
    }
    
    public func textViewDidEndEditing(_ textView: UITextView) {
        self.activeView = nil
    }
    
}

let pickerLabelStyle: (UILabel) -> Void = {
    $0.textAlignment = .center
    $0.font = .py_body(size: 16)
    $0.textColor = .secondaryTextColor
}

extension AccountInfoViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    public func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return countries.count
    }
    
    public func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        countries[row].name
    }
    
//    public func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
//        let label = UILabel()
//        label.text = countries[safe: row]?.name
//        pickerLabelStyle(label)
//        return label
//    }    
    
    public func pickerView(
        _ pickerView: UIPickerView,
        didSelectRow row: Int,
        inComponent component: Int) {
        guard let country = countries[safe: component] else { return }
        viewStore.send(.pickedCountry(country))
    }
    
}

extension AccountInfoViewController {
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.tag == 1 {
            //viewStore.send(.setFirstResponder(.password))
        }
        return true
    }
}

private let emailStackViewStyle: (UIStackView) -> Void =  {
    $0.alignment = .fill
    $0.axis = .vertical
    $0.spacing = .br_grid(2)
}

private let emailTextFieldStyle: (UITextField) -> Void =
    brTextFieldStyle <> {
        $0.tag = 2
        $0.autocorrectionType = .no
        $0.autocapitalizationType = .none
        $0.keyboardType = .emailAddress
        $0.placeholder = "Email"
    } <> heightAnchor(.br_grid(14))

private let countryTextFieldStyle: (UITextField) -> Void =
    brTextFieldStyle <> {
        $0.tag = 2
        $0.autocorrectionType = .no
        $0.autocapitalizationType = .none
        $0.keyboardType = .emailAddress
        $0.placeholder = "Select Country"
    } <> arrowDownViewStyle
    <> heightAnchor(.br_grid(14))

private let passwordTextFieldStyle: (UITextField) -> Void =
    brTextFieldStyle <> {
        $0.tag = 2
        $0.autocorrectionType = .no
        $0.autocapitalizationType = .none
        $0.isSecureTextEntry = true
        $0.placeholder = "Select Password"
    } <> heightAnchor(.br_grid(14))
    



private let defaultTextFieldStyle: (UITextField) -> Void =
    brTextFieldStyle <> {
        $0.tag = 1
        $0.autocorrectionType = .yes
        $0.autocapitalizationType = .none
        $0.keyboardType = .default
    } <> heightAnchor(.br_grid(14))

private let defaultTextViewStyle: (UITextView) -> Void =
    brTextViewStyle <> {
        $0.tag = 1
        $0.autocorrectionType = .yes
        $0.autocapitalizationType = .none
        $0.keyboardType = .default
        $0.textContainerInset = .square(.br_grid(4))
        $0.insetsLayoutMarginsFromSafeArea = true
    } <> heightAnchor(.br_grid(38))



private func rootViewStyle(parent: UIView, _ inset: UIEdgeInsets = .zero) -> (UIView) -> Void {
    return {
        NSLayoutConstraint.activate([
            $0.topAnchor.constraint(
                equalTo: parent.topAnchor,
                constant: inset.top),
            $0.leadingAnchor.constraint(
                equalTo: parent.leadingAnchor,
                constant: inset.left),
            $0.trailingAnchor.constraint(
                equalTo: parent.trailingAnchor,
                constant: -inset.right
            )
        ])
    }
}
