import Library
import ComposableArchitecture
import UIKit
import ReactiveSwift
import ExploreClient
import NewStory
import BrModels

public struct PaymentDetailsState: Equatable {
    public init(creditCardState: AddCreditCardState? = nil) {
        self.creditCardState = creditCardState
    }
    var creditCardState: AddCreditCardState?
    var isSubscribed: Bool = false
    var loadingState: LoadingState?
    var creditCard: CreditCard?
    var alert: _AlertState<PaymentDetailsAction>? = nil
}

public enum PaymentDetailsAction: Equatable {
    case viewDidLoad
    case viewDidAppear, viewDismissed
    case viewDidDisappear
    case addButtonTapped
    case deleteButtonTapped, editButtonTapped
    case deleteAction
    case cardResult(Result<CreditCard?, Failure>, cardId: String)
    case event(PaymentsDelegateEvent)
    case addCard(AddCreditCardAction)
}

public struct PaymentDetailsEnvironment {
    public var paymentDetailClient: PaymentDetailsClient
    public var addCardClient: AddCardClient
    public var stripeClient: StripeClient
    
    public init(
        paymentDetailClient: PaymentDetailsClient,
        addCardClient: AddCardClient,
        stripeClient: StripeClient
    ) {
        self.paymentDetailClient = paymentDetailClient
        self.addCardClient = addCardClient
        self.stripeClient = stripeClient
    }
}

public let paymentDetailsReducer:
    Reducer<PaymentDetailsState, PaymentDetailsAction, SystemEnvironment<PaymentDetailsEnvironment>>
    = .combine(
        Reducer { state, action, environment in
            struct CancelDelegateId: Hashable {}
            switch action {
            case .viewDidLoad:
                state.isSubscribed = true
                return .merge(
                    environment.paymentDetailClient
                        .delegate
                        .cancellable(id: CancelDelegateId())
                        .map(PaymentDetailsAction.event),
                    environment.paymentDetailClient
                        .onViewLoad
                        .fireAndForget()
                )
            case .viewDidAppear:
                guard !state.isSubscribed else { return .none }
                return environment.paymentDetailClient
                    .delegate
                    .cancellable(id: CancelDelegateId())
                    .map(PaymentDetailsAction.event)
            case .viewDismissed:
                state.creditCardState = nil
                state.alert = nil
                return .none
            case .addButtonTapped:
                state.creditCardState = AddCreditCardState()
                return .none
            case .addCard:
                return .none
            case let .event(.displayTransientError(error)):
                state.loadingState = nil
                state.alert = .init(
                    title: "Alert",
                    message: error.message,
                    dismissButton: .cancel(send: .viewDismissed)
                )
                return .none
            case .viewDidDisappear:
                state.isSubscribed = false
                return .cancel(id: CancelDelegateId())
            case .event(.onDisplayAddCardWithAExistingOneWarning):
                state.loadingState = nil
                state.alert = .init(
                    title: "Alert",
                    message: "Warning adding card",
                    dismissButton: .cancel(send: .viewDismissed))
                return .none
            case let .event(.onDisplayCard(card)):
                state.creditCard = card
                return .none
            case .event(.showLoading):
                state.loadingState = .loading
                return .none
            case .event(.hideLoading):
                state.loadingState = nil
                return .none
            case .event(.showNoCard):
                state.creditCard = nil
                return .none
            case let .event(.failedObtainCard(failure, retryAction: retryAction)):
                state.loadingState = .failure(
                    failure,
                    action: retryAction
                )
                return .none
            case let .event(.onNotifyStripeToGetCard(id)):
                return environment.stripeClient
                    .paymentMethods(id)
                    .mapError { error in
                        .message(error.localizedDescription)
                    }.catchToEffect()
                    .map {
                        PaymentDetailsAction.cardResult($0, cardId: id)
                    }
            case .deleteButtonTapped:
                state.alert = .init(
                    title: "Alert",
                    message: "Are you sure to delete the card?",
                    primaryButton: .destructive("Delete", send: .deleteAction),
                    secondaryButton: .cancel(send: .viewDismissed))
                return .none
            case .editButtonTapped:
                state.creditCardState = .init()
                return environment.paymentDetailClient
                    .onActionDeleteCard(true)
                    .fireAndForget()
            case .deleteAction:
                return environment.paymentDetailClient
                    .onActionDeleteCard(false)
                    .fireAndForget()
            case .event(.onNotifyNavigateToCardCreation):
                return .none
            case let .cardResult(.success(card), cardId: id):
                guard let creditCard = card else { return .none }
                return environment
                    .paymentDetailClient
                    .onEventStripeSucceedGettingCard(creditCard)
                    .fireAndForget()
            case let .cardResult(.failure(failure), cardId: id):
                return environment
                    .paymentDetailClient
                    .onEventStripeFailedGettingCard(id, 0, failure.message!)
                    .fireAndForget()
            }
        },
        addCreditCardReducer.optional().pullback(
            state: \.creditCardState,
            action: /PaymentDetailsAction.addCard,
            environment: {
                $0.map {
                    .init(
                        stripeClient: $0.stripeClient,
                        addCardClient: $0.addCardClient
                    )
                }
            }
        )
    )

private let iconImageStyle: (UIImageView) -> Void =
    squareStyle(.br_grid(6)) <> {
        $0.image = UIImage(named: "ic_credit_card")
        $0.tintColor = .secondaryTextColor
    }

private let rootStackViewStyle: (UIStackView) -> Void =  verticalStackStyle(
    .br_grid(1)
) <> marginStyle(.square(.br_grid(4)))


private let topStackViewStyle: (UIStackView) -> Void = horizontalStackStyle(.br_grid(2), alignment: .lastBaseline)

private let infoHolderStackViewStyle: (UIStackView) -> Void =
    horizontalStackStyle(.br_grid(3))

private let leadingStackViewStyle: (UIStackView) -> Void =
    verticalStackStyle(.br_grid(2), distribution: .fillEqually)


private let trailingStackViewStyle:
    (UIStackView) -> Void =
    verticalStackStyle(.br_grid(2),
                distribution: .fillEqually,
                alignment: .leading)


private let bottomStackViewStyle:
    (UIStackView) -> Void =
    horizontalStackStyle(.br_grid(4), alignment: .bottom)

private let deleteButtonStyle: (UIButton) -> Void =
    squareStyle(.br_grid(6)) <> {
        $0.setImage(UIImage(named: "ic_delete"))
        $0.tintColor = .systemRed
    }

private let editButtonStyle: (UIButton) -> Void =
    squareStyle(.br_grid(6)) <> {
    $0.setImage(UIImage(named: "ic_edit"))
        $0.tintColor = .darkGray
    }

private let creditCardLabelStyle: (UILabel) -> Void = brLabelDarkStyle <> {
    $0.font = UIFont.py_body(size: 16).bolded
} <> brDarkTextColor

private let titleLabelStyle: (UILabel) -> Void = brLabelDarkStyle <> {
    $0.font = UIFont.py_body(size: 12).bolded
} <> brDarkTextColor

private let contentLabelStyle: (UILabel) -> Void = brLabelDarkStyle <> {
    $0.font = .py_body(size: 12)
} <> brGrayTextColor

private let cellStyle: (UICollectionViewCell) -> Void = roundedStyle(cornerRadius: .br_grid(1))
    <> shadowStyle

class CreditCardCollectionViewCell: UICollectionViewCell {
                
    class var reuseIdentifier: String { "CreditCardCollectionViewCell" }
    
    var deleteAction: (() -> Void)? = nil
    var editAction: (() -> Void)? = nil
    
    func configure(_ creditCard: CreditCard) {
        holderLabelTitle.text = creditCard.holder
        expiresLabelTitle.text = "\(creditCard.expiryMonth)/\(creditCard.expiryYear)"
        numberLabelTitle.text = "XXXX XXXX XXXX XXXX " + creditCard.last4Digits
    }
    
    
    let numberLabelTitle = UILabel()
    let holderLabelTitle = UILabel()
    let expiresLabelTitle = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = .white
        
        let iconImageView = UIImageView()
        iconImageStyle(iconImageView)
        let titleLabel = UILabel()
        titleLabel.text = "Credit Card"
        creditCardLabelStyle(titleLabel)
        let topStackView = UIStackView(arrangedSubviews: [
            iconImageView,
            titleLabel
        ])
        topStackViewStyle(topStackView)
        
        let holderLabel = UILabel()
        titleLabelStyle(holderLabel)
        holderLabel.text = "Holder"
        let numberLabel = UILabel()
        titleLabelStyle(numberLabel)
        numberLabel.text = "Number"
        let expiresLabel = UILabel()
        titleLabelStyle(expiresLabel)
        expiresLabel.text = "Expires"
        
        let leadingStackView = UIStackView(arrangedSubviews: [
            holderLabel,
            numberLabel,
            expiresLabel
        ])
        leadingStackViewStyle(leadingStackView)
        contentLabelStyle(numberLabelTitle)
        contentLabelStyle(holderLabelTitle)
        contentLabelStyle(expiresLabelTitle)
        let trailingStackView = UIStackView(arrangedSubviews: [
            holderLabelTitle,
            numberLabelTitle,
            expiresLabelTitle
        ])
        trailingStackViewStyle(trailingStackView)
                
        let infoHoldeStackView = UIStackView(
            arrangedSubviews: [
                leadingStackView,
                trailingStackView
            ]
        )
        infoHolderStackViewStyle(infoHoldeStackView)
                                
        let deleteButton = UIButton()
        deleteButtonStyle(deleteButton)
        deleteButton.addTarget(self,
                               action: #selector(deleteButtonTapped),
                               for: .touchUpInside)
        let editButton = UIButton()
        editButton.addTarget(self,
                               action: #selector(editButtonTapped),
                               for: .touchUpInside)
        editButtonStyle(editButton)
        let bottomStackView = UIStackView(
            arrangedSubviews: [
                UIView(),
                deleteButton,
                editButton
            ]
        )
        bottomStackViewStyle(bottomStackView)
        
        let rootStackView = UIStackView(arrangedSubviews: [
            topStackView,
            infoHoldeStackView,
            bottomStackView
        ])
        rootStackViewStyle(rootStackView)
        self.addSubview(rootStackView)
        rootStackView.constrainEdges(to: self)
        
        cellStyle(self)
    }
    
    @objc func deleteButtonTapped() {
        deleteAction?()
    }
    
    @objc func editButtonTapped() {
        editAction?()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
                          
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

public class PaymentDetailsViewController: UIViewController {
    
    let store: Store<PaymentDetailsState, PaymentDetailsAction>
    let viewStore: ViewStore<PaymentDetailsState, PaymentDetailsAction>
    
    var collectionView: UICollectionView!
    let emptyStackView = UIStackView()
    
    var datasource: CreditCard? = nil {
        didSet {
            collectionView.reloadData()
        }
    }

    public init(store: Store<PaymentDetailsState, PaymentDetailsAction>) {
        self.store = store
        self.viewStore = ViewStore(store)
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        viewStore.send(.viewDidLoad)
        
        self.title = "Payment details"
        self.view.backgroundColor = .white
        
        let layout = UICollectionViewFlowLayout()
        collectionFlowLayoutStyle(layout)
        layout.itemSize = CGSize(
            width: view.bounds.width * 0.95,
            height: .br_grid(42)
        )
        layout.sectionInsetReference = .fromSafeArea
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionViewStyle(collectionView)
        collectionView.dataSource = self
        collectionView.delegate = self
        
        view.addSubview(collectionView)
        collectionView.constrainEdges(to: view)
        collectionView.isHidden = true
                
        //MARK:  Empty State
        let imageView = UIImageView()
        emptyImageViewStyle(imageView)
        let emptyTitleLabel = UILabel()
        emptyTitleLabel.text = "Add a new Credit Card"
        let emptySubTitleLabel = UILabel()
        emptyTitleLabelStyle(emptySubTitleLabel)
        emptySubTitleLabel.text = "Credit cards are used to pledge in projects or make donations."
        emptySubTitleLabelStyle(emptySubTitleLabel)
        emptyStackView.addArrangedSubview(imageView)
        emptyStackView.addArrangedSubview(emptyTitleLabel)
        emptyStackView.addArrangedSubview(emptySubTitleLabel)
        emptyStackViewStyle(emptyStackView)
        self.view.addSubview(emptyStackView)
        view.centerSubView(emptyStackView)
        
        // Add Card Button
        let addButton = UIButton()
        addButtonStyle(addButton)
        addButton.addTarget(self,
                              action: #selector(addButtonTapped),
                              for: .touchUpInside)
        self.navigationItem
            .rightBarButtonItem = UIBarButtonItem(customView: addButton)
              
        viewStore.produced.creditCard
            .startWithValues {
                self.datasource = $0
                self.emptyStackView.isHidden = $0 != nil
                self.collectionView.isHidden = $0 == nil
            }
        
        let spinner = LoadingView()
        self.view.addSubview(spinner)
        spinner.frame = view.frame
        self.view.bringSubviewToFront(spinner)
        
        viewStore.produced.loadingState
            .startWithValues { state in
                spinner.state = state
                spinner.isHidden = state == nil
            }
                            
        store.scope(
            state: \.creditCardState,
            action: PaymentDetailsAction.addCard
        ).ifLet(then: { [weak self] store in
            guard let self = self else { return }
            self.navigationController?
                .pushViewController(
                    AddCreditCardViewController(store: store),
                    animated: true
                )
        }, else: { [weak self] in
            guard let self = self else { return }
            self.navigationController?
                .popToViewController(self, animated: true)
        })
        
        viewStore.produced.alert
            .startWithValues { [weak self] (alert) in
                guard let self = self else { return }
                guard let alert = alert else { return }
                
                let actionController = UIAlertController(
                    title: alert.title,
                    message: alert.message,
                    preferredStyle: .alert
                )
                alert.primaryButton
                    .map { $0.toUIKit(send: self.viewStore.send) }
                    .map(actionController.addAction)
                
                alert.secondaryButton
                    .map { $0.toUIKit(send: self.viewStore.send) }
                    .map(actionController.addAction)
                
                self.present(actionController,
                             animated: true,
                             completion: nil)
        }
        
        
    }
    
    @objc func addButtonTapped(_ btn: UIButton) {
        viewStore.send(.addButtonTapped)
    }
    
    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.viewStore.send(.viewDidAppear)
        if !isMovingToParent {
            self.viewStore.send(.viewDismissed)
        }
    }
    
    public override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.viewStore.send(.viewDidDisappear)
    }
}

private let addButtonStyle: (UIButton) -> Void = {
    $0.setImage(UIImage(named: "ic_add"))
    $0.tintColor = .white
}

extension PaymentDetailsViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return datasource == nil ? 0: 1
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(
                withReuseIdentifier: CreditCardCollectionViewCell.reuseIdentifier,
                for: indexPath) as? CreditCardCollectionViewCell  else {
            fatalError("cell not registred")
        }
        if let creditCard = datasource {
            cell.configure(creditCard)
        }
        cell.deleteAction = {
            self.viewStore.send(.deleteButtonTapped)
        }
        cell.editAction = {
            self.viewStore.send(.editButtonTapped)
        }                
        return cell
    }
}

private let collectionViewStyle: (UICollectionView) -> Void =
    autoLayoutStyle <> {
        $0.register(CreditCardCollectionViewCell.self,
                    forCellWithReuseIdentifier: CreditCardCollectionViewCell.reuseIdentifier)
        $0.backgroundColor = UIColor(white: 0.95, alpha: 1.0)
        $0.contentInset = .square(.br_grid(4))
    }

private let collectionFlowLayoutStyle: (UICollectionViewFlowLayout) -> Void = {
    $0.scrollDirection = .vertical
    $0.minimumInteritemSpacing = .br_grid(2)
}


// Empty State
internal let emptyImageViewStyle: (UIImageView) -> Void = {
    $0.image = UIImage(
        named: "ic_credit_card"
    )
    $0.contentMode = .scaleAspectFit
    $0.tintColor = .secondaryTextColor
} <> squareStyle(.br_grid(17))

internal let emptyStackViewStyle:
    (UIStackView) -> Void =
    autoLayoutStyle <> {
        $0.axis = .vertical
        $0.alignment = .center
        $0.spacing = .br_grid(5)
        $0.isLayoutMarginsRelativeArrangement = true
        $0.layoutMargins.left = .br_grid(2)
    }

internal let emptyTitleLabelStyle: (UILabel) -> Void =
    autoLayoutStyle
    <> centerStyle
    <> {
        $0.font = UIFont.py_headline(size: .br_grid(6)).bolded
    } <> brDarkTextColor
      

internal let emptySubTitleLabelStyle: (UILabel) -> Void =
    autoLayoutStyle
    <> centerStyle
    <> {
        $0.font = .py_body(size: .br_grid(4))
        $0.numberOfLines = 2
    } <> brGrayTextColor
    <> widthAnchor(.br_grid(80))

internal let emptyInviteButtonStyle: (UIButton) -> Void =  autoLayoutStyle
    <> widthAnchor(.br_grid(50))
    <> heightAnchor(.br_grid(15))
    <> roundedStyle(cornerRadius: .br_grid(4))
<> {
        $0.setTitle("Invite More Friends")
        $0.titleLabel?.font = UIFont.py_headline().bolded
        $0.setTitleColor(.systemPurple, for: .normal)
    }
     

