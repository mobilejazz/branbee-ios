import UIKit
import Library
import BrModels
import ExploreClient

private let headerStackViewStyle: (UIStackView) -> Void = horizontalStackStyle(
    .br_grid(2),
    alignment: .center
) <> {
    $0.isLayoutMarginsRelativeArrangement = true
    $0.layoutMargins = .square(.br_grid(4))
}

private let accountImageStyle: (UIImageView) -> Void = profileViewStyle <> {
    $0.backgroundColor  = UIColor(white: 0.98, alpha: 1.0)
}

private let tableViewStyle: (UITableView) -> Void = {
    $0.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
    $0.separatorStyle = .singleLine
    $0.separatorColor = UIColor(white: 0.97, alpha: 0.9)
}

public struct AccountSettingState: Equatable {
    public var currentUser: BrUser?
    public var accountTypes: [AccountSectionType]
    public var profileImage: UIImage?
    
    public init(
        currentUser: BrUser? = nil,
        accountTypes: [AccountSectionType] = AccountSectionType.allCases,
        profileImage: UIImage? = nil,
        accountInfo: AccountInfoState? = nil,
        paymentDetails: PaymentDetailsState? = nil
    ) {
        self.currentUser = currentUser
        self.accountTypes = accountTypes
        self.profileImage = profileImage
        self.accountInfo = accountInfo
        self.paymentDetails = paymentDetails
        
    }
    
    public var accountInfo: AccountInfoState?
    public var paymentDetails: PaymentDetailsState?
    
}

public enum AccountSettingAction: Equatable {
    case viewDidLoad
    case viewDismissed
    case destroy
    case loadImage(UIImage?)
    case didCellTapped(AccountCellType)
    case accountInfo(AccountInfoAction)
    case details(PaymentDetailsAction)
    case event(AccountDelegateEvent)
}

import ComposableArchitecture
import ReactiveSwift
import ImageLoader

public struct AccountEnvironment {
    public var imageLoader: ImageLoader
    public var accountClient: AccountClient
    public var updateUserClient: UpdateUserClient
    public var addCardClient: AddCardClient
    public var paymentDetailsClient: PaymentDetailsClient
    public var stripeClient: StripeClient
    public init(
        imageLoader: ImageLoader = .empty,
        accountClient: AccountClient = .empty,
        updateUserClient: UpdateUserClient,
        addCardClient: AddCardClient = .empty,
        paymentDetailsClient: PaymentDetailsClient = .empty,
        stripeClient: StripeClient = .empty
    ) {        
        self.imageLoader = imageLoader
        self.accountClient = accountClient
        self.updateUserClient = updateUserClient
        self.addCardClient = addCardClient
        self.paymentDetailsClient = paymentDetailsClient
        self.stripeClient = stripeClient
    }
}

extension AccountEnvironment {
    public static var empty: Self {
        AccountEnvironment(
            imageLoader: .empty,
            accountClient: .empty,
            updateUserClient: .mock(.empty),
            addCardClient: .empty,
            paymentDetailsClient: .empty,
            stripeClient: .empty
        )
    }
}


public let accountReducer: Reducer<AccountSettingState, AccountSettingAction, SystemEnvironment<AccountEnvironment>> = .combine(
    Reducer { state, action, environement in
        struct CancelDelegateId: Hashable {}
        switch action {
        case .viewDidLoad:
            return .merge(
                environement.accountClient
                    .delegate
                    .cancellable(id: CancelDelegateId())
                    .map(AccountSettingAction.event),
                environement.accountClient
                    .onViewLoad
                    .fireAndForget()                     
            )
        case let .loadImage(image):
            state.profileImage = image
            return .none
        case let .didCellTapped(cell):
            switch cell {
            case .account:
                state.accountInfo = AccountInfoState(currentUser: state.currentUser)
                return .none
            case .paymentDetails:
                state.paymentDetails = PaymentDetailsState()
                return .none
            case .changePassword:
                return .none
            case .logout:
                return environement
                        .accountClient
                        .logOut
                        .fireAndForget()
            }
        case .accountInfo, .details:
            return .none
        case let .event(.displayInfo(user)):
            state.currentUser = user
            guard let photoURL = user._photoURL else { return .none }
            return environement
                .imageLoader
                .loadFromURL(photoURL)
                .start(on: environement.globalQueue())
                .observe(on: environement.mainQueue())
                .map(AccountSettingAction.loadImage)
        case let .event(.didDisabledFCM(userId: userId)):
            return .none
        case .event(.logOutSuccessfully):
            return .none
        case .destroy:
            return .cancel(id: CancelDelegateId())
        case .viewDismissed:
            state.accountInfo = nil
            state.paymentDetails = nil
            return .none
        }
    },
    accountInfoReducer.optional().pullback(
        state: \.accountInfo,
        action: /AccountSettingAction.accountInfo,
        environment: { $0 }
    ),
    paymentDetailsReducer.optional().pullback(
        state: \.paymentDetails,
        action: /AccountSettingAction.details,
        environment: { $0.map(\.paymentDetails) }
    )
)

extension AccountEnvironment {
    public var paymentDetails: PaymentDetailsEnvironment {
        PaymentDetailsEnvironment(
            paymentDetailClient: paymentDetailsClient,
            addCardClient: addCardClient,
            stripeClient: stripeClient
        )
    }
}

public class AccountViewController: UIViewController {

    public var dataSource: [AccountSectionType] = [] {
        didSet {
            self.tableview.reloadData()
        }
    }
    
    let store: Store<AccountSettingState, AccountSettingAction>
    let viewStore: ViewStore<AccountSettingState, AccountSettingAction>
    
    public init(store: Store<AccountSettingState, AccountSettingAction>) {
        self.store = store
        self.viewStore = ViewStore(store)
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    var tableview: UITableView!
    
    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
       // UITableViewCell.appearance().selectionStyle = .none        
        if !isMovingToParent {
            self.viewStore.send(.viewDismissed)
        }
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
                        
        self.title = "Account"
                
        self.viewStore.send(.viewDidLoad)        
        
        if #available(iOS 13.0, *) {
            tableview = UITableView(frame: .zero, style: .insetGrouped)
        } else {
            tableview = UITableView(frame: .zero, style: .grouped)
        }
                
        tableview.delegate = self
        tableview.dataSource = self
        tableViewStyle(tableview)
        
        let rootStackView =
        UIStackView(arrangedSubviews: [
            tableview
        ])
        
        rootStackView |>
            verticalStackStyle()
        
        view.addSubview(rootStackView)
        rootStackView.constrainEdges(to: view)
                        
        viewStore.produced
            .accountTypes
            .assign(to: \.dataSource, on: self)
        
        store.scope(
            state: \.accountInfo,
            action: AccountSettingAction.accountInfo
        ).ifLet(then: { [weak self] store in
            guard let self = self else { return }
            self.navigationController?
                        .pushViewController(
                            AccountInfoViewController(store: store),
                            animated: true
                        )
        }, else: { [weak self] in
            guard let self = self else { return }
            self.navigationController?
                .popToViewController(self, animated: true)
        })
        
        store.scope(
            state: \.paymentDetails,
            action: AccountSettingAction.details
        ).ifLet(then: { [weak self] store in
            guard let self = self else { return }
            self.navigationController?
                        .pushViewController(
                            PaymentDetailsViewController(store: store),
                            animated: true
                        )
        }, else: { [weak self] in
            guard let self = self else { return }
            self.navigationController?
                .popToViewController(self, animated: true)
        })                       
        
    }

}

private let cellLabelStyle: (UILabel) -> Void = {
    $0.font = .py_body(size: 16)
} <> brDarkTextColor

extension AccountViewController: UITableViewDelegate, UITableViewDataSource {
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource[section].cellRowsForSection.count
    }
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        dataSource.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let type = dataSource[indexPath.section]
            .cellRowsForSection[indexPath.item]
        cell.accessoryType = .disclosureIndicator
        cell.accessoryView = type.accessoryView
        cell.textLabel?.text = type.title
        cellLabelStyle(cell.textLabel!)
        return cell
    }
    
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard section == .zero else { return nil }
        let imageView = UIImageView()
        viewStore.produced
            .profileImage
            .assign(to: \.image, on: imageView)
        accountImageStyle(imageView)
        let titleLabel = UILabel()
        viewStore.produced
            .currentUser
            .skipNil()
            .map { $0.username }
            .assign(to: \.text, on: titleLabel)
        brLabelDarkBoldStyle(titleLabel)
        let stackview = UIStackView(arrangedSubviews: [
            imageView,
            titleLabel
        ])
        headerStackViewStyle(stackview)
        return stackview
    }
    
    public func tableView(
        _ tableView: UITableView,
        heightForHeaderInSection section: Int
    ) -> CGFloat {
        section == .zero
            ? AccountSectionType.sectionHeaderHeight
            : .zero
    }
    
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewStore.send(
            .didCellTapped(dataSource[indexPath.section]
                            .cellRowsForSection[indexPath.item])
        )
    }
    
    
}
