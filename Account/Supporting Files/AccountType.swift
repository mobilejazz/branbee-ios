import UIKit
import Library

public protocol AccountCellTypeProtocol {
  var accessibilityTraits: UIAccessibilityTraits { get }
  var showArrowImageView: Bool { get }
  var textColor: UIColor { get }
  var title: String { get }
}

//private let accessoryViewStyle: (UIView) -> Void =
//     squareStyle(.br_grid(8))

public enum AccountCellType: AccountCellTypeProtocol {
    case account
    case paymentDetails
    case changePassword
    case logout
    
    public var accessibilityTraits: UIAccessibilityTraits {
        return .button
    }
    
    public var title: String {
        switch self {
        case .account:
            return "Account Information"
        case .paymentDetails:
            return "Payment Details"
        case .changePassword:
            return "Change Password"
        case .logout:
            return "Log out"
        }
    }
    
    public var showArrowImageView: Bool {
        switch self {
        case .logout:
            return false
        default:
            return true
        }
    }
    
    public var textColor: UIColor {
        switch self {
        case .logout:
            return UIColor.blueColor
        default:
            return .darkText
        }
    }
    
    public var accessoryView: UIView? {
        switch self {
        case .logout:
            let imageView = UIImageView(image: UIImage(named: "ic_logout"))
            //accessoryViewStyle(imageView)
            return imageView
        default:
            return nil
        }
    }
    
}

public enum AccountSectionType: Int, CaseIterable {
    case account
    case logOut
    
    public static var sectionHeaderHeight: CGFloat {
        return .br_grid(30)
    }
    
    public var cellRowsForSection: [AccountCellType] {
        switch self {
        case .account:
            return [.account, .paymentDetails]
        case .logOut:
            return [.logout]
        }
    }
    
    public var hasSectionFooter: Bool {
        false
    }
    
    public var footerText: String? {
        return nil
    }
    
}
