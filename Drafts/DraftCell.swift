import Library
import UIKit
import BrModels
import ComposableArchitecture

private let dateLabelStyle: (UILabel) -> Void = brGrayTextColor <> {
    $0.font = .py_caption1()
}

private let bottomStackViewStyle: (UIStackView) -> Void = horizontalStackStyle(.br_grid(3), alignment: .center)
<> marginStyle()

private let storyLabelStyle: (EdgeLabel) -> Void =
    brDarkTextColor <> {
        $0.font = .py_body(size: 14)
        $0.numberOfLines = .zero
    }

private let draftStoryImageViewStyle: (UIImageView) -> Void = {
    $0.image = UIImage(.systemRed)
} <> heightAnchor(.br_grid(48))

private let draftRootStackViewStyle: (UIStackView) -> Void = verticalStackStyle(.br_grid(1))

private let editButtonStyle: (UIButton) -> Void = squareStyle(.br_grid(6))
    <> {
        $0.setImage(UIImage(named: "ic_edit"))
        $0.tintColor = .black
    }

private let deleteButtonStyle: (UIButton) -> Void =
    squareStyle(.br_grid(6))
    <> {
        $0.setImage(UIImage(named: "ic_delete"))
        $0.tintColor = .systemRed
    }

extension BrDraft {
    var hasText: Bool {
        text != nil
    }
    
    var hasImage: Bool {
        cachedImageFileKey != nil
    }
    
    var hasBoth: Bool {
        hasImage && hasText
    }
}

public struct DraftCellState: Equatable {
    var draft: BrDraft
    var picture: UIImage?
    public init(
        draft: BrDraft,
        picture: UIImage? = nil
    ) {
        self.draft = draft
        self.picture = picture
    }    
}

public enum DraftCellAction: Equatable {
    case load
    case image(UIImage?)
    case editButtonTapped
    case deleteButtonTapped
}

public class DraftViewCell: UICollectionViewCell {
        
    public class var reuseIdentifier: String {
        "DraftViewCell"
    }
    
    var viewStore: ViewStore<BrDraft, DraftCellAction>!
    func configure(_ store: Store<DraftCellState, DraftCellAction>) {
        viewStore = ViewStore(store.scope(state: { $0.draft }))
        
        viewStore.send(.load)
        
        viewStore.produced.text
            .startWithValues { text in
                self.storyLabel.text = text
                self.storyLabel.isHidden = text == nil
            }
        
        viewStore.produced.cachedImageFileKey
            .startWithValues { key in
                self.storyImageView.isHidden = key == nil
            }
        
        ViewStore(store).produced.picture
            .assign(to: \.image, on: storyImageView)
        
        viewStore.produced.createdAtTimestampInMillis
            .map(Double.init)
            .map(Date.init(timeIntervalSince1970:))
            .map(brDateFormatter().string(from:))
            .startWithValues { date in
                self.dateLabel.text = date
            }

    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        draftCellStyle(self)
    }
    
    let storyImageView = UIImageView()
    let storyLabel = EdgeLabel(edge: .square(.br_grid(4)))
    let dateLabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
                                        
        draftStoryImageViewStyle(storyImageView)
        storyLabelStyle(storyLabel)
        dateLabelStyle(dateLabel)
        
        let editButton = UIButton()
        editButton
            .addTarget(self,
                       action: #selector(editButtonTapped),
                       for: .touchUpInside)
        editButtonStyle(editButton)
        
        let deleteButton = UIButton()
        deleteButton
            .addTarget(self,
                       action: #selector(deleteButtonTapped),
                       for: .touchUpInside)
        deleteButtonStyle(deleteButton)
        
        let bottomStackView = UIStackView(arrangedSubviews: [
            dateLabel,
            deleteButton,
            editButton,
        ])
        bottomStackViewStyle(bottomStackView)
        
        let draftStackView = UIStackView(arrangedSubviews: [
            storyImageView,
            storyLabel,
            bottomStackView
        ])
        draftStackView |>
            draftRootStackViewStyle
        self.addSubview(draftStackView)
        draftStackView.constrainEdges(to: self)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public override func prepareForReuse() {
        super.prepareForReuse()
    }
    
    @objc
    func editButtonTapped(_ btn: UIButton) {
        viewStore.send(.editButtonTapped)
    }
    
    @objc
    func deleteButtonTapped(_ btn: UIButton) {
        viewStore.send(.deleteButtonTapped)
    }
    
}


let draftCellStyle: (DraftViewCell) -> Void =
    {
        $0.backgroundColor = .white
    } <> roundedStyle(cornerRadius: .br_grid(2))
    <> brShadowStyle

private let flexibleCellHeight: (String, CGFloat) -> CGFloat = {
    NSAttributedString(
        string: $0,
        attributes: [.font: UIFont.py_body(size: 14)])
            .boundingRect(with: .init(
                        width: $1,
                        height: .greatestFiniteMagnitude),
                      options: .usesLineFragmentOrigin, context: nil).height
}
