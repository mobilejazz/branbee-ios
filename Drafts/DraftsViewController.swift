import Library
import UIKit
import ReactiveSwift
import BrModels
import ComposableArchitecture
import ImageLoader
import ExploreClient
import NewStory

public struct DraftsState: Equatable {
    var states: [DraftCellState]
    var newStory: NewStoryState?
    var picture: UIImage?
    var alert: _AlertState<DraftsAction>?
    var loadingState: LoadingState? = nil
    public init(
        states: [DraftCellState] = [],
        newStory: NewStoryState? = nil,
        picture: UIImage? = nil,
        alert: _AlertState<DraftsAction>? = nil
    ) {
        self.states = states
        self.newStory = newStory
        self.picture = picture
        self.alert = alert
    }
}

public enum DraftsAction: Equatable {
    case viewDidLoad
    case viewDidAppear
    case viewDismissed
    case destroy
    case deleteDraft(BrDraft)
    case response(DraftsDelegateEvent)
    case cell(index: Int, action: DraftCellAction)
    case newStory(NewStoryAction)
}

public struct DraftsEnvironement {
    public var imageLoader: ImageLoader
    public var draftsClient: DraftsClient
    public let selectCommunity: SelectCommunityClient
    public let newStoryClient: (String?) -> NewStoryClient
    public init(
        loader: ImageLoader,
        draftsClient: DraftsClient,
        selectCommunity: SelectCommunityClient,
        newStoryClient: @escaping (String?) -> NewStoryClient
    ) {        
        self.imageLoader = loader       
        self.draftsClient = draftsClient
        self.newStoryClient = newStoryClient
        self.selectCommunity = selectCommunity
    }
}

let draftState: (BrDraft) -> DraftCellState = {
    DraftCellState(draft: $0)
}

public let draftsReducer: Reducer<DraftsState, DraftsAction, SystemEnvironment<DraftsEnvironement>> = .combine(
    Reducer { state, action, environment in
        struct CancelDelegateId: Hashable {}
        switch action {
        case .viewDidLoad:            
            return environment.draftsClient
                .delegate
                .cancellable(id: CancelDelegateId())
                .observe(on: environment.mainQueue())
                .map(DraftsAction.response)
        case .viewDidAppear:
            return environment.draftsClient
                .refresh
                .fireAndForget()
        case let .response(.showDrafts(drafts)):
            state.loadingState = drafts.isEmpty
                ? .message("No Drafts")
                : nil
            state.states = drafts.map(draftState)
            return .none
        case let .cell(index: index, action: .deleteButtonTapped):
            let draft = state.states[index].draft
            state.alert = _AlertState(
                title: "Alert",
                message: "Are you sure want to delete \(draft.text?.prefix(5) ?? "")...",
                primaryButton: .destructive("Delete", send: .deleteDraft(draft)),
                secondaryButton: .cancel(send: .viewDismissed)
            )
            return .none
        case .cell(index: let index, action: .editButtonTapped):
            let draft = state.states[index].draft
            state.newStory = NewStoryState(
                communityName: draft.communityName,
                communityId: draft.communityId
            )
            return .none
        case .cell, .newStory:
            return .none
        case .viewDismissed:
            state.newStory = nil
            state.alert = nil
            return .none
        case let .deleteDraft(draft):
            return environment
                .draftsClient
                .deleteStory(draft)
                .fireAndForget()
        case .destroy:
            return .cancel(id: CancelDelegateId())
        }
    }, draftCellReducer.forEach(
        state: \.states,
        action: /DraftsAction.cell(index:action:),
        environment: \.imageLoader
    ),
    newStoryReducer.optional().pullback(
        state: \.newStory,
        action: /DraftsAction.newStory,
        environment: {
            $0.map {
            NewStoryEnvironment(
                newStory: $0.newStoryClient,
                selectCommunity: $0.selectCommunity,
                imageLoader: $0.imageLoader
            )
            }
        }
    )
)

let draftCellReducer = Reducer<DraftCellState, DraftCellAction, ImageLoader> { state, action, loader in
    switch action {
    case .load:
        return .none //FIXME:- Fix caching
        return state.draft.cachedImageFileKey
            .map(loader.get)?
            .compactMap { $0.map(UIImage.init(contentsOfFile:)) }
            .compactMap(DraftCellAction.image) ?? .none
    case .editButtonTapped:
        return .none
    case .deleteButtonTapped:
        return .none
    case let .image(picture):
        state.picture = picture
        return .none
    }
}

import ComposableArchitecture
public class DraftsViewController: UIViewController {
    
    let store: Store<DraftsState, DraftsAction>
    let viewStore: ViewStore<DraftsState, DraftsAction>
    
    public init(store: Store<DraftsState, DraftsAction>) {
        self.store = store
        self.viewStore = ViewStore(store)
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var collectionView: UICollectionView!
    var datasource: [DraftCellState] = [.init(draft: .draft)] {
        didSet {
            collectionView.reloadData()
        }
    }
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Drafts"
        
        viewStore.send(.viewDidLoad)
                                
        let layout = UICollectionViewFlowLayout()
        collectionFlowLayoutStyle(layout)
        collectionView = UICollectionView(
            frame: .zero,
            collectionViewLayout: layout
        )
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionViewStyle(collectionView)
        self.view.addSubview(collectionView)
        collectionView.constrainEdges(to: view)
        
        
        let spinner = LoadingView()
        self.view.addSubview(spinner)
        spinner.frame = view.frame
        self.view.bringSubviewToFront(spinner)
        
        viewStore.produced.loadingState
            .startWithValues { state in
                spinner.state = state
                spinner.isHidden = state == nil
            }
        
                                              
        viewStore.produced.states
            .assign(to: \.datasource, on: self)
        
        // New Story
        store.scope(
            state: \.newStory,
            action: DraftsAction.newStory
        ).ifLet(then: { [weak self] store in
            guard let self = self else { return }
            self.navigationController?.pushViewController(
                NewStoryViewController(store: store), animated: true)
        }) {
            self.navigationController?.popToViewController(self, animated: true)
        }
        
        // Alert
        viewStore.produced.alert
            .startWithValues { [weak self] alert in
                guard let self = self else { return }
                guard let alert = alert else { return }
                
                let actionController = UIAlertController(
                    title: alert.title,
                    message: alert.message,
                    preferredStyle: .alert
                )
                
                alert.primaryButton
                    .map { $0.toUIKit(send: self.viewStore.send) }
                    .map(actionController.addAction)
                
                alert.secondaryButton
                    .map { $0.toUIKit(send: self.viewStore.send) }
                    .map(actionController.addAction)
                
                self.present(actionController,
                             animated: true,
                             completion: nil)
        }
        
        
        
    }
    
    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewStore.send(.viewDidAppear)
        if !self.isMovingToParent {
            viewStore.send(.viewDismissed)
        }
        
        
    }
}

let progressTitleLabelStyle: (UILabel) -> Void = {
    $0.font = .py_subhead(size: 14)
    $0.textAlignment = .center
    $0.textColor = .blueSecondColor
}

let progressStackViewStyle: (UIStackView) -> Void = autoLayoutStyle <> verticalStackStyle(.br_grid(2),
                    distribution: .fillProportionally,
                    alignment: .fill)


let contentViewStyle: (UIView) -> Void = autoLayoutStyle <> {
    $0.backgroundColor = .white
}


private let collectionViewStyle: (UICollectionView) -> Void =
    autoLayoutStyle <> {
        $0.register(DraftViewCell.self,
                    forCellWithReuseIdentifier: DraftViewCell.reuseIdentifier)
        $0.backgroundColor = .white
        //$0.contentInset = .horizontal(.br_grid(4))
    }

private let collectionFlowLayoutStyle: (UICollectionViewFlowLayout) -> Void = {
    $0.scrollDirection = .vertical
    $0.sectionHeadersPinToVisibleBounds = true
    $0.minimumLineSpacing = .br_grid(4)
    $0.minimumInteritemSpacing = .br_grid(4)
    $0.sectionInsetReference = .fromContentInset
    $0.sectionInset = .topHorizontalEdge(.br_grid(4))
}

extension DraftsViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        return datasource.count
    }
            
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let draftCell = collectionView.dequeueReusableCell(
            withReuseIdentifier: DraftViewCell.reuseIdentifier,
            for: indexPath
        ) as? DraftViewCell else {
            fatalError()
        }
        //exploreCell.isReading = viewStore.focusedIndexPath == indexPath
        draftCell.configure(store.scope(
            state:  { $0.states[safe: indexPath.section] ?? .init(draft: .draft) },
            action: { .cell(index: indexPath.section, action: $0) }
        ))
        
        return draftCell
    }
}

private let flexibleCellHeight: (String, CGFloat) -> CGFloat = {
    NSAttributedString(
        string: $0,
        attributes: [.font: UIFont.py_caption2(size: 14)])
            .boundingRect(with: .init(
                        width: $1,
                        height: .greatestFiniteMagnitude),
                      options: .usesLineFragmentOrigin, context: nil).height
}

extension DraftsViewController: UICollectionViewDelegateFlowLayout {
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let cellWidth = collectionView.bounds.width - .br_grid(8)
        
        guard let draft = viewStore.states[safe: indexPath.section]?.draft else { return .zero }
                
        if draft.hasBoth {
            return CGSize(
                width: cellWidth,
                height: flexibleCellHeight(draft.text!, cellWidth) + .br_grid(70)
            )
        }
        
        if draft.hasText {
            return CGSize(
                width: cellWidth,
                height: flexibleCellHeight(draft.text!, cellWidth) + .br_grid(20)
            )
        }
        
        if draft.hasImage {
            return CGSize(
                width: cellWidth,
                height: .br_grid(65)
            )
        }
                
        return .zero
        
    }
    
}
