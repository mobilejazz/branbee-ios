import Library
import UIKit
import ReactiveSwift
import BrModels
import ComposableArchitecture
import ImageLoader
import ExploreClient
import NewStory
import SDWebImage

public struct SavedStoriesState: Equatable {
    var stories: [BrStory]
    var focusedIndexPath: IndexPath?
    var comments: CommentsState?
    public var storyDetails: StoryDetailState?
    
    var detail: StoryDetailStateCopy? {
        get {
            storyDetails.map(StoryDetailStateCopy.init)
        }
        set {
            storyDetails = newValue?.value
        }
    }
    
    
    var loadingState: LoadingState? = nil
    public init(
        stories: [BrStory] = [],
        focusedIndexPath: IndexPath? = nil,
        comments: CommentsState? = nil,
        storyDetails: StoryDetailState? = nil
    ) {
        self.stories = stories
        self.focusedIndexPath = focusedIndexPath
        self.comments = comments
        self.storyDetails = storyDetails
    }
}

public enum SavedStoriesAction: Equatable {
    case viewDidLoad, viewDidAppear
    case viewDismissed
    case destroy
    case details(StoryDetailAction)
    case viewDetail(BrStory)
    case viewComments(BrStory)
    case comments(CommentsAction)
    case response(SavedStoriesDelegateEvent)
    case cell(index: Int, action: SavedStoryCellAction)
}


public struct SavedStoriesEnvironement {
    public var loader: ImageLoader
    public var savedClient: SavedStoriesClient
    public var commentsClient: (String, BrComment.´Type´) -> CommentsClient
    var storyDetailsClient: (String) -> StoryDetailClient
    var linkProjectClient: (String, String) -> LinkProjectClient
    var projectDetailClient: (String) -> ProjectDetailsClient
    var donateClient: (_ projectId: String, _ projectName: String) -> DonateClient
    let addCardClient: AddCardClient
    var linkStoryClient: (String, String) -> LinkStoryClient
    let stripeClient: StripeClient
    public init(
        loader: ImageLoader,
        savedClient: SavedStoriesClient,
        commentsClient: @escaping (String, BrComment.´Type´) -> CommentsClient = { _,_ in .empty },
        storyDetailsClient: @escaping (String) -> StoryDetailClient = { _ in .empty },
        linkProjectClient: @escaping (String, String) -> LinkProjectClient = { _,_ in .empty },
        projectDetailClient: @escaping (String) -> ProjectDetailsClient = { _ in .empty },
        donateClient: @escaping (_ projectId: String, _ projectName: String) -> DonateClient = { _,_ in .empty },
        addCardClient: AddCardClient = .empty,
        linkStoryClient: @escaping (String, String) -> LinkStoryClient = { _,_ in .empty },
        stripeClient: StripeClient
    ) {
        self.loader = loader
        self.storyDetailsClient = storyDetailsClient
        self.linkProjectClient = linkProjectClient
        self.savedClient = savedClient
        self.commentsClient = commentsClient
        self.projectDetailClient = projectDetailClient
        self.donateClient = donateClient
        self.addCardClient = addCardClient
        self.linkStoryClient = linkStoryClient
        self.stripeClient = stripeClient
    }
}

extension SavedStoriesEnvironement {
    var storyDetail: StoryDetailEnvironment {
        .init(storyDetailClient: storyDetailsClient,
              linkProjectClient: linkProjectClient,
              commentsClient: commentsClient,
              projectDetailClient: projectDetailClient,
              donateClient: donateClient,
              linkStoryClient: linkStoryClient,
              addCardClient: addCardClient,
              imageLoader: loader,
              stripeClient: stripeClient
        )
    }
}



public let savedStoriesReducer: Reducer<SavedStoriesState, SavedStoriesAction, SystemEnvironment<SavedStoriesEnvironement>> = .combine(
    Reducer { state, action, environment in
        struct CancelDelegateId: Hashable {}
        switch action {
        case .viewDidLoad:
            return
                environment.savedClient
                    .delegate
                    .cancellable(id: CancelDelegateId())
                    .observe(on: environment.mainQueue())
                    .map(SavedStoriesAction.response)
        case .viewDidAppear:
            return environment.savedClient
                .refresh
                .fireAndForget()
        case .viewDismissed:
            state.storyDetails = nil
            state.comments = nil
            return .none
        case let .cell(index: index, action: .readMoreTapped):
            state.focusedIndexPath = IndexPath(item: 0, section: index)
//            for indice in state.stories.indices {
//                state.stories[indice].isFocused = index == indice
//            }
            return .none
        case let .cell(index: index, action: .commentsButtonTapped):
            return Effect(value: .viewComments(state.stories[index]))
        case let .cell(index: index, action: .boostButtonTapped):
            return Effect(value: .viewDetail(state.stories[index]))
        case let .response(.showStories(stories)):
            state.loadingState = nil
            state.stories = stories
            return .none
        case .response(.showNoBookMaredStories):
            state.loadingState = .message("No BookMard Stories")
            return .none
        case let .viewDetail(story):
            state.storyDetails = StoryDetailState(story: story)
            return .none
        case let .viewComments(story):
            state.comments = CommentsState(
                parentId: story.id,
                parentType: .story
            )
            return .none
        case .destroy:
            return .cancel(id: CancelDelegateId())
        case .details, .cell, .comments:
            return .none
        }
    },
    storyDetailReducer.optional().pullback(
        state: \.detail,
        action: /SavedStoriesAction.details,
        environment: { $0.map(\.storyDetail) }
    ),
    savedStoryCellReducer.forEach(
        state: \.stories,
        action: /SavedStoriesAction.cell(index:action:),
        environment: { $0 }
    ),
    commentsReducer.optional().pullback(
        state: \.comments,
        action: /SavedStoriesAction.comments,
        environment: { $0.map {
            CommentsEnvironment(
                commentsClient: $0.commentsClient,
                imageLoader: $0.loader
            )
            }
        }
    )
)

let savedStoryCellReducer = Reducer<BrStory, SavedStoryCellAction, SystemEnvironment<SavedStoriesEnvironement>> { state, action, env in
    switch action {
    case .readMoreTapped:
        return .none
    case .bookmarkButtonTapped:
        //FIXME:: Navigate
        return .none
    case .boostButtonTapped:
        //FIXME:: Navigate
        return .none
    case .commentsButtonTapped:
        return .none
    }
}

import ComposableArchitecture
public class SavedStoriesViewController: UIViewController {
    
    let store: Store<SavedStoriesState, SavedStoriesAction>
    let viewStore: ViewStore<SavedStoriesState, SavedStoriesAction>
    
    public init(store: Store<SavedStoriesState, SavedStoriesAction>) {
        self.store = store
        self.viewStore = ViewStore(store)
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var collectionView: UICollectionView!
    var datasource: [BrStory] = [] {
        didSet {
            collectionView.reloadData()
        }
    }
            
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Saved Stories"
    
        self.viewStore.send(.viewDidLoad)
                                        
        let layout = UICollectionViewFlowLayout()
        collectionFlowLayoutStyle(layout)
        collectionView = UICollectionView(
            frame: .zero,
            collectionViewLayout: layout
        )
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionViewStyle(collectionView)
        self.view.addSubview(collectionView)
        collectionView.constrainEdges(to: view)
                
        let spinner = LoadingView()
        self.view.addSubview(spinner)
        spinner.frame = view.frame
        self.view.bringSubviewToFront(spinner)
        
        viewStore.produced.loadingState
            .startWithValues { state in
                spinner.state = state
                spinner.isHidden = state == nil
            }
                            
        viewStore.produced.focusedIndexPath
            .skipNil()
            .startWithValues { [weak self] indexPath in
                self?.collectionView.performBatchUpdates {
                    self?.collectionView.reloadItems(at: [indexPath])
                }
            }
                        
        viewStore.produced.stories
            .observe(on: QueueScheduler.main)
            .assign(to: \.datasource, on: self)
        
        // Comments
        store.scope(
            state: \.comments,
            action: SavedStoriesAction.comments
        ).ifLet(then: { [weak self] store in
            guard let self = self else { return }
            self.navigationController?.pushViewController(CommentsViewController(store: store), animated: true)
        }) {
            self.navigationController?.popToViewController(self, animated: true)
        }
        
        // Details
        store.scope(
            state: \.storyDetails,
            action: SavedStoriesAction.details
        ).ifLet(then: { [weak self] store in
            guard let self = self else { return }
            self.navigationController?.pushViewController(StoryDetailViewController(store: store), animated: true)
        }) {
            self.navigationController?.popToViewController(self, animated: true)
        }
                    
    }
    
    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewStore.send(.viewDidAppear)
        if !isMovingToParent {
            viewStore.send(.viewDismissed)
        }
    }
}

let progressTitleLabelStyle: (UILabel) -> Void = {
    $0.font = .py_subhead(size: 14)
    $0.textAlignment = .center
    $0.textColor = .blueSecondColor
}

let progressStackViewStyle: (UIStackView) -> Void = autoLayoutStyle <> verticalStackStyle(.br_grid(2),
                    distribution: .fillProportionally,
                    alignment: .fill)


let contentViewStyle: (UIView) -> Void = autoLayoutStyle <> {
    $0.backgroundColor = .white
}


private let collectionViewStyle: (UICollectionView) -> Void =
    autoLayoutStyle <> {
        $0.register(StoryViewCell.self,
                    forCellWithReuseIdentifier: StoryViewCell.reuseIdentifier)
        $0.backgroundColor = .white
        //$0.contentInset = .horizontal(.br_grid(4))
    }

private let collectionFlowLayoutStyle: (UICollectionViewFlowLayout) -> Void = {
    $0.scrollDirection = .vertical
    $0.sectionHeadersPinToVisibleBounds = true
    $0.minimumLineSpacing = .br_grid(4)
    $0.minimumInteritemSpacing = .br_grid(4)
    $0.sectionInsetReference = .fromContentInset
    $0.sectionInset = .topHorizontalEdge(.br_grid(4))
}

extension SavedStoriesViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        return datasource.count
    }
    
    public func collectionView(
        _ collectionView: UICollectionView,
        didSelectItemAt indexPath: IndexPath) {
        guard let story = datasource[safe: indexPath.section] else { return }
        viewStore.send(.viewDetail(story))
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let storyCell = collectionView.dequeueReusableCell(
            withReuseIdentifier: StoryViewCell.reuseIdentifier,
            for: indexPath
        ) as? StoryViewCell else {
            fatalError()
        }        
        storyCell.configure(store.scope(
            state:  { $0.stories[safe:indexPath.section] ??  BrStory.branbeeStory },
            action: { .cell(index: indexPath.section, action: $0) }
        ))
        return storyCell
    }
}


extension SavedStoriesViewController: UICollectionViewDelegateFlowLayout {
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let cellWidth = collectionView.bounds.width - .br_grid(8)
        let focusStory = datasource[safe: viewStore.focusedIndexPath?.section ?? .zero]
        
        let text = focusStory?.content.first(where: { $0.type == .text })?.content ?? ""
        
        return CGSize(
            width: cellWidth,
            height: indexPath == viewStore.focusedIndexPath
                ? heightForText(text, width: cellWidth) + .br_grid(80)
                : .br_grid(100)
        )
        
    }
    
}
