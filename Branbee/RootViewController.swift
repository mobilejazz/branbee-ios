import Library
import Account
import ComposableArchitecture
import ReactiveSwift
import ImageLoader
import UIKit
import BrModels
import ExploreClient
import Explore
import SavedStories
import Drafts

public enum Navigation: Equatable {
    
    case tab(Tab)
    
    public enum Tab: Int, Equatable {
        case explore = 0
        case savedStories
        case drafts
        case account
        var string: String {
            switch self {
            case .account:
                return "account"
            case .explore:
                return "explore"
            case .savedStories:
                return "savedStories"
            case .drafts:
                return "drafts"
            }
        }
    }
            
}

public struct TabBar: Equatable {
    let tab: Navigation.Tab
    var badgeValue: Int
    public init(
        tab: Navigation.Tab,
        badgeValue: Int = .zero
    ) {
        self.tab = tab
        self.badgeValue = badgeValue
    }
}

public struct TabBarState: Equatable {    
    public let bars: [TabBar] = [
        TabBar(tab: .explore),
        TabBar(tab: .savedStories),
        TabBar(tab: .drafts),
        TabBar(tab: .account),
    ]
    public var selectedBar: TabBar? = nil
    public var account: AccountSettingState
    public var explore: ExploreState
    public var savedStories: SavedStoriesState
    public var drafts: DraftsState
    
    public init(
        selectedBar: TabBar? = nil
    ) {
        self.selectedBar = selectedBar        
        self.account = .init()
        self.explore = .init()
        self.savedStories = .init()
        self.drafts = .init()
    }
}

public enum TabBarAction: Equatable {
    case account(AccountSettingAction)
    case explore(ExploreAction)
    case savedStories(SavedStoriesAction)
    case drafts(DraftsAction)
}

public struct TabBarEnvironment {
    let imageLoader: ImageLoader
    //let userDefaults: KeyValueStoreType
    let stripeClient: StripeClient
    let addCardClient: AddCardClient
    let updateUserClient: UpdateUserClient
    let commentsClient: (String, BrComment.´Type´) -> CommentsClient
    let storyDetailClient: (String) -> StoryDetailClient
    let newStoryClient: (String?) -> NewStoryClient
    let projectDetailClient: (String) -> ProjectDetailsClient
    let linkProjectClient: (String, String) -> LinkProjectClient
    let linkStoryClient: (String, String) -> LinkStoryClient
    let accountClient: AccountClient
    let exploreClient: ExploreClient
    let savedClient: SavedStoriesClient
    let searchClient: SearchClient
    let draftsClient: DraftsClient
    let selectCommunityClient: SelectCommunityClient
    let notificationsClient: NotificationsClient
    let paymentDetailsClient: PaymentDetailsClient
    var communityDetailClient: (_ communityId: String) -> CommunityDetailClient
    var donateClient: (_ projectId: String, _ projectName: String) -> DonateClient
}

public let tabBarReducer = Reducer<TabBarState, TabBarAction, SystemEnvironment<TabBarEnvironment>>.combine(
    Reducer { state, action, env in
        switch action {
        case .account, .explore, .savedStories, .drafts:
            return .none
        }
    }, accountReducer.pullback(
        state: \.account,
        action: /TabBarAction.account,
        environment: { $0.map(\.account) }
    ),
    exploreReducer.pullback(
        state: \.explore,
        action: /TabBarAction.explore,
        environment: { $0.map(\.explore) }
    ),
    savedStoriesReducer.pullback(
        state: \.savedStories,
        action: /TabBarAction.savedStories,
        environment: { $0.map(\.savedStories) }
    ),
    draftsReducer.pullback(
        state: \.drafts,
        action: /TabBarAction.drafts,
        environment: { $0.map(\.drafts) }
    )
)

extension TabBarEnvironment {
    var account: AccountEnvironment {
        AccountEnvironment(
            imageLoader: imageLoader,
            accountClient: accountClient,
            updateUserClient: updateUserClient,
            addCardClient: addCardClient,
            paymentDetailsClient: paymentDetailsClient,
            stripeClient: stripeClient
        )
    }
}

extension TabBarEnvironment {
    var explore: ExploreEnvironment {
        ExploreEnvironment(
            client: exploreClient,
            stripeClient: stripeClient,
            commentsClient: commentsClient,
            storyDetailsClient: storyDetailClient,
            projectDetailsClient: projectDetailClient,
            linkProjectClient: linkProjectClient,
            linkStoryClient: linkStoryClient,
            searchClient: searchClient,
            donateClient: donateClient,
            newStoryClient: newStoryClient,
            addCardClient: addCardClient,
            notificationsClient: notificationsClient,
            selectCommunity: selectCommunityClient,
            communityDetailClient: communityDetailClient,
            imageLoader: imageLoader
        )
    }
}

extension TabBarEnvironment {
    var savedStories: SavedStoriesEnvironement {
        SavedStoriesEnvironement(
            loader: imageLoader,
            savedClient: savedClient,
            commentsClient: commentsClient,
            storyDetailsClient: storyDetailClient,
            linkProjectClient: linkProjectClient,
            projectDetailClient: projectDetailClient,
            donateClient: donateClient,
            addCardClient: addCardClient,
            linkStoryClient: linkStoryClient,
            stripeClient: stripeClient
        
        )
    }
}

extension TabBarEnvironment {
    var drafts: DraftsEnvironement {
        DraftsEnvironement(
            loader: imageLoader,            
            draftsClient: draftsClient,
            selectCommunity: selectCommunityClient,
            newStoryClient: newStoryClient
        )
    }
}


public class TabBarViewController: UITabBarController, UITabBarControllerDelegate {

    let store: Store<TabBarState, TabBarAction>
    let viewStore: ViewStore<TabBarState, TabBarAction>

    init(store: Store<TabBarState, TabBarAction>) {
        self.store = store
        self.viewStore = ViewStore(store)
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    public override func viewDidLoad() {
        super.viewDidLoad()
                 
        delegate = self
                                        
        self.tabBar.tintColor = .systemPurple
                        
        let exploreViewController = UINavigationController(
            rootViewController:
                ExploreViewController(store: store.scope(
                    state: \.explore,
                    action: TabBarAction.explore
                ))
        )
        
        exploreViewController.tabBarItem = UITabBarItem(
            title: "Explore",
            image: nil,
            tag: Tag(tab: .explore).id
        )
        exploreTabBarStyle(exploreViewController.tabBarItem)
        
        let savedStoriesViewController = UINavigationController(
            rootViewController: SavedStoriesViewController(store: store.scope(
                state: \.savedStories,
                action: TabBarAction.savedStories
            ))
        )
        
        savedStoriesViewController.tabBarItem = UITabBarItem(
            title: "Saved Stories",
            image: nil,
            tag: Tag(tab: .savedStories).id
        )
        
        savedStoriesTabBarStyle(savedStoriesViewController.tabBarItem)

                
        let draftsViewController = UINavigationController(
            rootViewController: DraftsViewController(
                store: store.scope(
                    state: \.drafts,
                    action: TabBarAction.drafts
                ))
        )
        
        draftsViewController.tabBarItem = UITabBarItem(
            title: "Drafts",
            image: nil,
            tag: Tag(tab: .drafts).id
        )
        drafsTabBarStyle(draftsViewController.tabBarItem)

        let accountViewController = UINavigationController(
            rootViewController: AccountViewController(
            store: store.scope(
                state: \.account,
                action: TabBarAction.account
            ))
        )
        accountViewController.tabBarItem = UITabBarItem(
            title: "Account",
            image: nil,
            tag: Tag(tab: .account).id
        )
        accountTabBarStyle(accountViewController.tabBarItem)

        let controllers: [String: UIViewController] = [
            "explore": exploreViewController,
            "savedStories": savedStoriesViewController,
            "drafts": draftsViewController,
            "account": accountViewController,
        ]
                
                        
        viewStore.produced.bars
            .startWithValues { [weak self] (bars) in
                guard let self = self else { return }
                self.viewControllers = bars
                    .map(\.tab)
                    .compactMap { controllers[$0.string] }

                bars.forEach { bar in
                    controllers[bar.tab.string]?.tabBarItem
                        .badgeValue = bar.badgeValue == .zero
                            ? nil
                            : String(bar.badgeValue)
                }
        }

        viewStore.produced.selectedBar
            .compactMap(id)
            .startWithValues { bar in
                self.selectedIndex = self.viewStore
                    .bars
                    .firstIndex(where: { $0.tab == bar.tab }) ?? .zero
                controllers[bar.tab.string]?.tabBarItem.badgeValue = nil
        }
        
        
        if #available(iOS 13.0, *) {
            UITabBarAppearance().backgroundColor = .systemRed
        } else {
            // Fallback on earlier versions
        }
        
    }
}

struct Tag {
    let id: Int
    init(tab: Navigation.Tab) {
        switch tab {
        case .explore:
            id = 0
        case .savedStories:
            id = 1
        case .drafts:
            id = 2
        case .account:
            id = 3
        }
    }
}

extension TabBarViewController {
    public func tabBarController(_ tabBarController: UITabBarController, animationControllerForTransitionFrom fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return CrossDissolveTransitionAnimator()
    }
}

private let exploreTabBarStyle: (UITabBarItem) -> Void = {
    $0.titlePositionAdjustment = .init(horizontal: 5, vertical: 0)
    $0.setBadgeTextAttributes([.font: UIFont.boldSystemFont(ofSize: 13)], for: .normal)
    $0.image = UIImage(named: "ic_explore")
} <> badgeColor(.systemBlue)

private let accountTabBarStyle: (UITabBarItem) -> Void = {
    $0.titlePositionAdjustment = .init(horizontal: 5, vertical: 0)
    $0.setBadgeTextAttributes([.font: UIFont.boldSystemFont(ofSize: 13)], for: .normal)
    $0.image = UIImage(named: "ic_user")
} <> badgeColor(.systemRed)

private let savedStoriesTabBarStyle: (UITabBarItem) -> Void = {
    $0.titlePositionAdjustment = .init(horizontal: 5, vertical: 0)
    $0.setBadgeTextAttributes([.font: UIFont.boldSystemFont(ofSize: 13)], for: .normal)
    $0.image = UIImage(named: "ic_bookmark")
} <> badgeColor(.systemRed)

private let drafsTabBarStyle: (UITabBarItem) -> Void = {
    $0.titlePositionAdjustment = .init(horizontal: 5, vertical: 0)
    $0.setBadgeTextAttributes([.font: UIFont.boldSystemFont(ofSize: 13)], for: .normal)
    $0.image = UIImage(named: "ic_edit")
} <> badgeColor(.systemBlue)

private func badgeColor(_ color: UIColor) -> (UITabBarItem) -> Void {
    return {
        $0.badgeColor = color
    }
}

extension Array where Element == TabBar {
    subscript(_ bar: Navigation.Tab) -> TabBar? {
        get {
            self.first { $0.tab == bar }
        }
        set {
            guard let newValue = newValue else { return }
            if let index = self.firstIndex(where: { $0.tab == newValue.tab }) {
                self[index] = newValue
                return
            }
            self.append(newValue)
        }
    }
}

