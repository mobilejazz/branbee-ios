import core
import ComposableArchitecture


/**
 * TODO: @ruben here you can write the iOS Resolution
 */
class IOSUnauthorizedResolution: Harmony_kotlinUnauthorizedResolution {
    public func resolve() -> Bool {
        ViewStore(appStore).send(.launch)
        return true
    }
}
