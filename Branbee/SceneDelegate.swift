import UIKit
import Authentication
import AuthenticationClient
import ComposableArchitecture
import ReactiveSwift
import Library
import Account
import Explore
import ExploreClient
import BrModels
import NewStory
import SavedStories
import Stripe
import Drafts

//21.3.1
let navigationStyle: () -> Void = {
    let appearance = UINavigationBar.appearance()
    appearance.setBackgroundImage(nil, for: .default)
    appearance.shadowImage = nil
    appearance.isTranslucent = false
    appearance.backgroundColor = .primaryColor
    appearance.titleTextAttributes = [
        .foregroundColor: UIColor.white,
        .font: UIFont.py_title3().bolded
    ]
    appearance.tintColor = .white
    appearance.barTintColor = .primaryColor
}

public let appStore = Store(
    initialState: AppState(),
    reducer: appReducer.debug(),
    //environment: .live(environment: .mock(caseStudy: .success))
    environment: .live(environment: .mock(caseStudy: .success))
)

@available(iOS 13.0, *)
class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
                   
        navigationStyle()
        
        guard let windowScene = (scene as? UIWindowScene) else { return }
        window = UIWindow(windowScene: windowScene)
        
        window?.rootViewController =
            UINavigationController(rootViewController:
                    DebugTableViewController(nibName: nil, bundle: nil)
            )
                       
        
            
//            UINavigationController.init(
//                rootViewController:
//                    ExploreViewController(
//                        store: .init(
//                            initialState: .init(),
//                            reducer: exploreReducer.debug(),
//                            environment: .live(
//                                environment: .init(
//                                    client: .mock(.success),
//                                    stripeClient: .empty,
//                                    imageLoader: .placeholder
//                                )
//                            )
//                        ))
//            )

            
            
            
//            AccountInfoViewController(
//                store: .init(
//                    initialState: .init(),
//                    reducer: accountInfoReducer,
//                    environment: .live(
//                        environment: .init(
//                            updateUserClient: .mock(.success)
//                        )
//                    )
//                ))
            
//            DonateViewController(
//                store: .init(
//                    initialState: .init(project: .branbee),
//                    reducer: donationReducer,
//                    environment: .live(
//                        environment: DonateEnvironment(
//                            donateClient: {_,_ in .mock(.success) },
//                            addCardClient: .empty,
//                            stripeClient: .mock(caseStudy: .success)
//                        )
//                    ))
//            )
            
            
        self.window?.makeKeyAndVisible()
        
        ViewStore(appStore).send(.launch)
    }

    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not necessarily discarded (see `application:didDiscardSceneSessions` instead).
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }

    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }

    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
       
    }


}

