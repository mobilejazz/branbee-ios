import UIKit
import Library
import ComposableArchitecture
import Authentication

let brNavBarStyle: (UINavigationBar) -> Void = {
    $0.setBackgroundImage(nil, for: .default)
    $0.shadowImage = nil
    $0.isTranslucent = false
    $0.backgroundColor = .primaryColor
    $0.titleTextAttributes = [
        .foregroundColor: UIColor.white,
        .font: UIFont.py_title3().bolded
    ]
    $0.barTintColor = .primaryColor
    $0.tintColor = .white
}

class ViewController: UINavigationController {

    let store: Store<AppState, AppAction>
    let viewStore: ViewStore<AppState, AppAction>

    init(store: Store<AppState, AppAction>) {
        self.store = store
        self.viewStore = ViewStore(store)
        super.init(nibName: nil, bundle: nil)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .lightContent
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = .white
        
        viewStore.send(.launch)
        
        
        brNavBarStyle(UINavigationBar.appearance())
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        }
                                                                   
        store.scope(
            state: \.login,
            action: AppAction.login
        ).ifLet(then: { [weak self] loginStore in
            guard let self = self else { return }
            self.navigationBar.isHidden = false
            //self.popToRootViewController(animated: false)
            self.setViewControllers([
                LoginViewController(store: loginStore)
            ], animated: false)
        }) {
            
        }
                        
        store.scope(
            state: \.tabBar,
            action: AppAction.tab
        ).ifLet(then: { [weak self] tabStore in
            guard let self = self else { return }
            self.navigationBar.isHidden = true
            //self.popToRootViewController(animated: false)
            self.setViewControllers([
                TabBarViewController(store: tabStore)
            ], animated: false)
        })
                                       
                
    }

}
