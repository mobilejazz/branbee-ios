import Foundation
import ComposableArchitecture
import core

public let branbeeShared = BranbeeSDK.shared

private struct BranbeeSDK {
    
    #if DEBUG
    static let isDebug = true
    #else
    static let isDebug = false
    #endif
    
    public static let shared = ApplicationDefaultModule(
        imageCache: ImageCache(),
        cacheSQLConfiguration: DatabaseIosBridgeProvider().db() ,
        coreLogger: BugfenderIOSLogger(applicationKey: "4DzEMWT8o5IpVU6d9UUOLLsrhNSN5kvt"),
        unauthorizedResolution: IOSUnauthorizedResolution(),
        debug: isDebug
    )
}

