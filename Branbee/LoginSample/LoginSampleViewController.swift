//
//  LoginSampleViewController.swift
//  Branbee
//
//  Created by Rubén Vázquez Otero on 18/01/2021.
//

import UIKit
import ReactiveSwift
import ComposableArchitecture
import BranbeeSDK
import core

class LoginSampleViewController: UIViewController {

    lazy var presenter = branbeeShared.presenterComponent.loginPresenter(view: self)
    
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var button: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }


    @IBAction func btLoginPressed(_ sender: Any) {
        self.presenter.onActionLogin(email: "montiel@mobilejazz.com", password: "aaa123")
    }
    
}

extension LoginSampleViewController: LoginPresenterView {
    func onDisplayErrorLogin(error: KotlinException) {
        label.text = error.message
    }
    
    func onNotifyNavigateToForgotPassword() {
        print("Should navigate to forgot password")
    }
    
    func onNotifyNavigateToMain() {
        label.text = "Login succeeded"
    }
    
    func onNotifyNavigateToSignUp() {
        print("Should navigate to signup")
    }
    
    
}
