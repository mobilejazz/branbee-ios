import ComposableArchitecture
import Authentication
import ReactiveSwift
import AuthenticationClient
import LiveAuthenticationClient
import ImageLoader
import Library
import BrModels
import LiveImageLoader
import ExploreClient
import LiveExploreClient

public struct BranbeeClient {
    let imageLoader: ImageLoader
    let loginClient: LoginClient
    let updateUserClient: UpdateUserClient
    let signUpClient: SignUpClient
    let forgotPassword: ForgotPasswordClient
    let addCardClient: AddCardClient
    let commentsClient: (String, BrComment.´Type´) -> CommentsClient
    let accountClient: AccountClient
    let exploreClient: ExploreClient
    let searchClient: SearchClient
    let savedStoriesClient: SavedStoriesClient
    let draftsClient: DraftsClient
    let selectCommunityClient: SelectCommunityClient
    let communityDetailClient: (_ communityId: String) -> CommunityDetailClient
    let notificationsClient: NotificationsClient
    let paymentDetailsClient: PaymentDetailsClient
    let linkProjectClient: (String, String) -> LinkProjectClient
    let linkStoryClient: (String, String) -> LinkStoryClient
    let storyDetailClient: (String) -> StoryDetailClient
    let projectDetailClient: (String) -> ProjectDetailsClient
    let newStoryClient: (String?) -> NewStoryClient
    var donateClient: (_ projectId: String, _ projectName: String) -> DonateClient
    let stripeClient: StripeClient
    public init(
        imageLoader: ImageLoader,
        loginClient: LoginClient,
        updateUserClient: UpdateUserClient,
        signUpClient: SignUpClient,
        forgotPassword: ForgotPasswordClient,
        facebookClient: FacebookClient,
        userDefaults: KeyValueStoreType,
        addCardClient: AddCardClient,
        accountClient: AccountClient,
        searchClient: SearchClient,
        exploreClient: ExploreClient,
        selectCommunityClient: SelectCommunityClient,
        savedStoriesClient: SavedStoriesClient,
        notificationsClient: NotificationsClient,
        draftsClient: DraftsClient,
        paymentDetailsClient: PaymentDetailsClient,
        linkProjectClient: @escaping (String, String) -> LinkProjectClient,
        linkStoryClient: @escaping (String, String) -> LinkStoryClient,
        commentsClient: @escaping (String, BrComment.´Type´) -> CommentsClient,
        storyDetailClient: @escaping (String) -> StoryDetailClient,
        projectDetailClient: @escaping (String) -> ProjectDetailsClient,
        newStoryClient: @escaping (String?) -> NewStoryClient,
        donateClient: @escaping (_ projectId: String, _ projectName: String) -> DonateClient,
        communityDetailClient: @escaping (_ communityId: String) -> CommunityDetailClient,
        stripeClient: StripeClient
    ) {
        self.updateUserClient = updateUserClient
        self.communityDetailClient = communityDetailClient
        self.loginClient = loginClient
        self.signUpClient = signUpClient
        self.forgotPassword = forgotPassword
        self.addCardClient = addCardClient
        self.exploreClient = exploreClient
        self.accountClient = accountClient
        self.savedStoriesClient = savedStoriesClient
        self.draftsClient = draftsClient
        self.storyDetailClient = storyDetailClient
        self.newStoryClient = newStoryClient
        self.commentsClient = commentsClient
        self.searchClient = searchClient
        self.notificationsClient = notificationsClient
        self.projectDetailClient = projectDetailClient
        self.linkProjectClient = linkProjectClient
        self.linkStoryClient = linkStoryClient
        self.selectCommunityClient = selectCommunityClient
        self.paymentDetailsClient = paymentDetailsClient
        self.imageLoader = imageLoader
        self.donateClient = donateClient
        self.stripeClient = stripeClient
    }
}
