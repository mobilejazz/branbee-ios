import AuthenticationClient
import ImageLoader
import ExploreClient

extension BranbeeClient {
    static func mock(caseStudy: CaseStudy) -> Self {
        .init(
            imageLoader: .live,
            loginClient: .mock(caseStudy == .success),
            updateUserClient: .mock(caseStudy),
            signUpClient: .mock(caseStudy == .success),
            forgotPassword: .mock(caseStudy == .success),
            facebookClient: .empty,
            userDefaults: UserDefaults(),
            addCardClient: .mock(caseStudy),
            accountClient: .mock(caseStudy),
            searchClient: .mock(caseStudy),
            exploreClient: .mock(caseStudy),
            selectCommunityClient: .mock(caseStudy: caseStudy),
            savedStoriesClient: .mock(caseStudy),
            notificationsClient: .mock(caseStudy),
            draftsClient: .mock(caseStudy: caseStudy),
            paymentDetailsClient: .mock(caseStudy),
            linkProjectClient: { _,_ in  .mock(caseStudy) },
            linkStoryClient: { _,_ in  .mock(caseStudy) },
            commentsClient: { _,_  in .mock(caseStudy) },
            storyDetailClient: { _ in .mock(caseStudy: caseStudy) },
            projectDetailClient: { _ in .mock(caseStudy) },
            newStoryClient: NewStoryClient
                .mock(caseStudy == .success
                                ? .success
                                : .error),
            donateClient: { _,_  in .mock(caseStudy) },
            communityDetailClient: CommunityDetailClient.mock(caseStudy),
            stripeClient: .mock(caseStudy: caseStudy)
        )
    }
}
