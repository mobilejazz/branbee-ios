import ExploreClient
import LiveExploreClient

extension BranbeeClient {
    public static var live: BranbeeClient {
        BranbeeClient(
            imageLoader: .live,
            loginClient: .live,
            updateUserClient: .live,
            signUpClient: .live,
            forgotPassword: .live,
            facebookClient: .empty,
            userDefaults: UserDefaults.standard,
            addCardClient: .live,
            accountClient: .live,
            searchClient: .live,
            exploreClient: .live,
            selectCommunityClient: .live,
            savedStoriesClient: .live,
            notificationsClient: .live,
            draftsClient: .live,
            paymentDetailsClient: .live,
            linkProjectClient: LinkProjectClient.live,
            linkStoryClient: LinkStoryClient.live,
            commentsClient: CommentsClient.live,
            storyDetailClient: StoryDetailClient.live,
            projectDetailClient: ProjectDetailsClient.live,
            newStoryClient: NewStoryClient.live,
            donateClient: DonateClient.live,
            communityDetailClient: CommunityDetailClient.live,
            stripeClient: .live
        )
    }
}
