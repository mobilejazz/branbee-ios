import UIKit
import ComposableArchitecture

enum DebugCase: CaseIterable {
    case live
    case success
    case error
    case empty
    
    var title: String {
        switch self {
        case .live:
            return "Live"
        case .success:
            return "Success"
        case .error:
            return "Error"
        case .empty:
            return "Empty"
        }
    }
    
    var description: String {
        switch self {
        case .live:
            return "This linked directly to branbee SDK"
        case .success:
            return "Mock responses, that alwayas succeed"
        case .error:
            return "Mock Responses, This will help test the Retry buttons, please use test@br.com/test to connect"
        case .empty:
            return "Mocks responses, show empty states most views that have empty case "
        }
    }
}


class DebugTableViewController: UITableViewController {

    
    var datasource: [DebugCase] = [] {
        didSet {
            tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Debug "
        tableView.estimatedRowHeight = UITableView.automaticDimension
        datasource = DebugCase.allCases
    }
   
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datasource.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: "cell")
        
        let debugCase = datasource[indexPath.row]
        
        cell.textLabel?.text = debugCase.title
        cell.detailTextLabel?.text = debugCase.description
        cell.detailTextLabel?.numberOfLines = 0

        return cell
    }


    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let appStore = Store(
            initialState: AppState(),
            reducer: appReducer.debug(),
            environment: .live(environment:
                                datasource[indexPath.row] == .live
                                ? .live
                                : datasource[indexPath.row] == .success
                                ? .mock(caseStudy: .success)
                                : datasource[indexPath.row] == .error
                                ? .mock(caseStudy: .error)
                                : .mock(caseStudy: .empty)
            )
        )
        
        self.show(ViewController(store: appStore), sender: nil)
        
        //ViewStore(appStore).send(.launch)
    }
    
    

}
