import ComposableArchitecture
import Authentication

public enum AppAction: Equatable {
    case launch
    case openLogin
    case openHome
    case login(LoginAction)
    case tab(TabBarAction)    
}
