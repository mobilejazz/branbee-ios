import ComposableArchitecture
import Authentication
import ReactiveSwift
import AuthenticationClient
import LiveAuthenticationClient
import ImageLoader
import Library
import BrModels
import LiveImageLoader
import ExploreClient
import LiveExploreClient



extension BranbeeClient {
    var login: LoginEnvironment {
        LoginEnvironment(
            login: loginClient,
            signUp: signUpClient,
            forgotPassword: forgotPassword
        )
    }
}


extension BranbeeClient {
    var tabBar: TabBarEnvironment {
        TabBarEnvironment(
            imageLoader: imageLoader,
            stripeClient: stripeClient,
            addCardClient: addCardClient,
            updateUserClient: updateUserClient,
            commentsClient: commentsClient,
            storyDetailClient: storyDetailClient,
            newStoryClient: newStoryClient,
            projectDetailClient: projectDetailClient,
            linkProjectClient: linkProjectClient,
            linkStoryClient: linkStoryClient,
            accountClient: accountClient,
            exploreClient: exploreClient,
            savedClient: savedStoriesClient,
            searchClient: searchClient,
            draftsClient: draftsClient,
            selectCommunityClient: selectCommunityClient,
            notificationsClient: notificationsClient,
            paymentDetailsClient: paymentDetailsClient,
            communityDetailClient: communityDetailClient,
            donateClient: donateClient
        )
    }
}


public let appReducer: Reducer<AppState, AppAction, SystemEnvironment<BranbeeClient>> =
    Reducer.combine(
        Reducer { state, action, environment in
            switch action {
            case .launch:
                return Effect(value: .openLogin)
            case .openLogin:
                state.tabBar = nil
                state.login = LoginState()
                return .none
            case .openHome:
                state.tabBar = TabBarState()
                state.login = nil
                return .none
            case .tab, .login:
                return .none
            }
        }.connectReducer,
        loginReducer.optional().pullback(
            state: \.login,
            action: /AppAction.login,
            environment: {
                $0.map(\.login)
            }
        ),
        tabBarReducer.optional().pullback(
            state: \.tabBar,
            action: /AppAction.tab,
            environment: { $0.map(\.tabBar) }
        )
    )

//Login
extension Reducer where
    State == AppState,
    Action == AppAction,
    Environment == SystemEnvironment<BranbeeClient> {
    public var connectReducer: Self {
        Self { state, action, environment in
            let effects = self(&state, action, environment)
            switch action {
            case .tab(.account(.event(.logOutSuccessfully))):
                return .concatenate(
                    Effect(value: .tab(.account(.destroy))),
                    Effect(value: .tab(.explore(.destroy))),
                    Effect(value: .tab(.drafts(.destroy))),
                    Effect(value: .tab(.account(.destroy))),
                    Effect(value: .openLogin)
                )
            case .login(.event(.loginSuccess)):
                return .concatenate(
                    Effect(value: .login(.destroy)),
                    Effect(value: .openHome)
                )                    
            case .login(.signUpName(.signUp(.event(.successRegistration)))):
                return Effect(value: .openHome)
            default:
                return effects
            }
        }
    }
}


