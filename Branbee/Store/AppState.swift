import ComposableArchitecture
import Authentication
import Library
import BrModels

public struct AppState: Equatable {
    public var currentUser: BrUser?
    public var login: LoginState?
    public var tabBar: TabBarState?    
    public init(
        currentUser: BrUser? = nil,
        login: LoginState? = nil,
        tabBar: TabBarState? = nil
    ) {
        self.currentUser = currentUser
        self.login = login
        self.tabBar = tabBar
    }
}
