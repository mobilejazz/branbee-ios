import Validated

public enum ValidationError: String {
    case name = "name should have at least 2 letters & no special letter"
    case password = "password should have one big letter, one special character and 6 digits"
    case email = "email your enter is not valid"
    case phone = "phone your entered is not valid"
}

public enum ValidationResponse: String {
    case success
}

/// Validation for Username
public func validate(name: String) -> Validated<String, ValidationError> {
    NSPredicate(format: "SELF MATCHES %@", "^(([^ ]?)(^[a-zA-Z].*[a-zA-Z]$)([^ ]?))$")
        .evaluate(with: name)
        ? .valid(name)
        : .error(.name)
}

/// Validation for Password
public func validate(password: String) -> Validated<String, ValidationError> {
    NSPredicate(format: "SELF MATCHES %@", "^(?=.*[a-z])(?=.*[$@$#!%*?&])(?=.*[A-Z]).{6,}$")
        .evaluate(with: password)
        ? .valid(password)
        : .error(.password)
}

/// Validation for Phone
public func validate(phone: String, countryCode: String = "") -> Validated<String, ValidationError> {
     NSPredicate(format: "SELF MATCHES %@", "^((\\+)|(00))[0-9]{6,14}$")
        .evaluate(with:"+" + countryCode + phone)
        ? .valid(phone)
        : .error(.phone)
}

/// Validation for Email
private let pattern =
"(?:[a-zA-Z0-9!#$%\\&‘*+/=?\\^_`{|}~-]+(?:\\.[a-zA-Z0-9!#$%\\&'*+/=?\\^_`{|}"  +
"~-]+)*|\\“(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\" +
"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\\“)@(?:(?:[a-z0-9](?:[a-" +
"z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5" +
"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-" +
"9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21" +
"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"
public func validate(email: String) -> Validated<String, ValidationError> {
  let predicate = NSPredicate(format: "SELF MATCHES %@", pattern)
  return predicate.evaluate(with: email)
    ? .valid(email)
    : .error(.email)
}
