import Library
import UIKit
import Validated
import ComposableArchitecture
import AuthenticationClient
import ReactiveSwift
import Tagged
import BrModels

public struct SignUpState: Equatable {
    public enum PickerType: Equatable {
        case camera, photoLibrary
    }
    public var name: Name
    public var imageURL: URL?
    public var image: UIImage?
    public var email: Email
    public var password: Password
    public var countries: [BrCountry]
    public var country: BrCountry?
    public var biography: String?
    public var city: String?
    public var pickerType: PickerType?
    public var isFormValid: Bool = false
    public var alert: _AlertState<SignUpAction>? = nil
    public var loadingState: LoadingState? = nil
    public var actionAlert: _ActionSheetState<SignUpAction>?
        
    public init(
        name: Name = "",
        imageURL: URL? = nil,
        email: Email = "",
        password: Password = "",
        countries: [BrCountry] = [],
        country: BrCountry? = nil,
        biography: String? = nil,
        city: String? = nil,
        isLoginRequestInFlight: Bool = false,
        isFormValid: Bool = false,
        pickerType: PickerType? = nil,
        alert: _AlertState<SignUpAction>? = nil
    ) {
        self.imageURL = imageURL
        self.name = name
        self.email = email
        self.password = password
        self.country = country
        self.biography = biography
        self.city = city
        self.isFormValid = isFormValid
        self.alert = alert        
        self.countries = countries
    }
        
}

public enum SignUpAction: Equatable {
    case emailTextFieldDidChanged(Email)
    case nameTextFieldDidChanged(Name)
    case passwordTextFieldDidChanged(Password)
    case cityTextFieldDidChanged(String?)
    case bioTextViewDidChanged(String?)
    case pickedCountry(BrCountry?)
    case pickedImage(UIImage?, withURL: URL?)
    case viewDismissed, viewDidLoad
    case viewDidDisppear
    case nextButtonTapped
    case editButtonTapped
    case showPhotos, showCamera
    case event(SignUpDelegateEvent)
}

public let registerReducer: Reducer<SignUpState, SignUpAction, SystemEnvironment<SignUpClient>> = Reducer.combine(
Reducer { state, action, environment in
    struct CancelDelegateId: Hashable {}
    switch action {
    case .viewDidLoad:
        return .merge(
            environment
                .delegate
                .cancellable(id: CancelDelegateId())
                .map(SignUpAction.event),
            environment
                .onViewLoaded
                .fireAndForget()
        )
    case let .emailTextFieldDidChanged(email):
        state.email = email
        return .none
    case let .nameTextFieldDidChanged(name):
        state.name = name
        return .none
    case let .passwordTextFieldDidChanged(password):
        state.password = password
        return .none
    case let .cityTextFieldDidChanged(city):
        state.city = city
        return .none
    case let .bioTextViewDidChanged(bio):
        state.biography = bio
        return .none
    case let .pickedCountry(country):
        state.country = country
        return .none
    case .viewDismissed:
        state.actionAlert = nil
        state.alert = nil
        return .none
    case .nextButtonTapped:
        return environment            
            .register(
                RegisterRequest(
                    name: state.name.rawValue,
                    email: state.email.rawValue,
                    password: state.password.rawValue,
                    avatarPath: state.imageURL?.path,
                    city: state.city,
                    country: state.country,
                    biography: state.biography
                )
            ).fireAndForget()
    case .editButtonTapped:
        state.actionAlert = _ActionSheetState(
            title: "Change Profile Image",
            message: nil,
            buttons: [
                .default("Select From Photos", send: .showPhotos),
                .default("Select From Camera", send: .showCamera),
                .cancel(send: .viewDismissed)
            ]
        )
        return .none
    case let .event(.onDisplayCountries(countries)):
        state.country = countries.first
        state.countries = countries
        return .none
    case .event(.onFailedToValidateEmail):
        state.loadingState = nil
        state.alert = _AlertState(
            title: "Ooops!",
            message: "Failed to validate email",
            dismissButton: .cancel(send: .viewDismissed)
        )
        return .none
    case let .event(.onFailedToRegister(failure)):
        state.loadingState = nil
        state.alert = _AlertState(
            title: "Ooops!",
            message: failure.message,
            dismissButton: .cancel(send: .viewDismissed)
        )
        return .none
    case let .event(.onDisplayFullScreenError(error, action: action)):
        state.loadingState = .failure(error, action: action)
        return .none
    case let .pickedImage(image, withURL: imageURL):
        state.imageURL = imageURL
        state.image = image
        return .none
    case .showPhotos:
        state.pickerType = .photoLibrary
        return .none
    case .showCamera:
        state.pickerType = .camera
        return .none
    case .event(.onDisplayFillRequiredFields):
        state.loadingState = nil
        state.alert = _AlertState(
            title: "Ooops!",
            message: "Fill Required Fields",
            dismissButton: .cancel(send: .viewDismissed)
        )
        return .none
    case .event(.successRegistration):
        state.loadingState = nil
        return .none
    case .viewDidDisppear:
        return .cancel(id: CancelDelegateId())
    case .event(.showLoading):
        state.loadingState = .loading
        return .none
    case .event(.hideLoading):
        state.loadingState = nil
        return .none
    }
})

private let titleLabelStyle: (UILabel) -> Void =
    brLabelDarkStyle
    <> heightAnchor(.br_grid(10))
    <> centerStyle

private let itemLabelStyle: (UILabel) -> Void =
    brLabelDarkBoldStyle <> {
        $0.textAlignment = .left
}

private let profileImageViewStyle: (UIImageView) -> Void =
    squareStyle(.br_grid(20))
    <> roundedStyle(cornerRadius: .br_grid(10))
    <> autoLayoutStyle
    <> { (im: UIImageView) in
        im.image = UIImage(
           named: "default_avatar",
           in: .authentication,
           compatibleWith: nil
       )
        im.contentMode = .scaleAspectFill
    }
     
let editButtonStyle: (UIButton) -> Void =
    squareStyle(.br_grid(10))
    <> { (btn: UIButton) in
        btn.setImage(UIImage(named: "ic_edit"), for: .normal)
        btn.backgroundColor = UIColor(white: 0.97, alpha: 1.0)
    } <> brShadowStyle
    <> roundedStyle(cornerRadius: .br_grid(5))
    <> autoLayoutStyle
    
private let rootStackViewStyle:
    (UIStackView) -> Void =
    verticalStackStyle(.br_grid(3), alignment: .fill)

let itemStackViewStyle:
    (UIStackView) -> Void =
    verticalStackStyle(.br_grid(2),
                   distribution: .fill,
                   alignment: .fill)

let bioStackViewStyle: (UIStackView) -> Void =
    verticalStackStyle(.br_grid(2),
                    distribution: .fillProportionally,
                    alignment: .fill
)

private let topStackViewStyle: (UIStackView) -> Void =
    verticalStackStyle(.br_grid(4),
                       distribution: .fill,
                       alignment: .center
    )

private let nextButtonStyle: (LoadingButton) -> Void =
    brMainLoadingButtonStyle
    <> {
        $0.setTitle("Next")
    } <> heightAnchor(.br_grid(12))

private let signUpControllerStyle: (UIViewController) -> Void = {
    $0.view.backgroundColor = .white
}

public class SignUpViewController: UIViewController {
                     
    let store: Store<SignUpState, SignUpAction>
    let viewStore: ViewStore<SignUpState, SignUpAction>
    var constraintLayout: NSLayoutConstraint!
    let placeholderLabel = UILabel()
    let countryPickerView = UIPickerView()
    let scrollView = UIScrollView()
    let rootView = UIView()
    var activeView: UIView? = nil
    let nextButton = LoadingButton()
    
    public init(store: Store<SignUpState, SignUpAction>) {
        self.store = store
        self.viewStore = ViewStore(store)
        super.init(nibName: nil, bundle: nil)
    }
        
    required init?(coder: NSCoder) {
        fatalError("unimplemented nscoder")
    }
    
    var countries: [BrCountry] = [] {
        didSet {
            countryPickerView.reloadAllComponents()
        }
    }
               
    public override func viewDidLoad() {
        super.viewDidLoad()
        viewStore.send(.viewDidLoad)
        signUpControllerStyle(self)
        // scroll setup
        scrollViewStyle(scrollView)
        rootViewStyle(rootView)
        view.addSubview(scrollView)
        scrollView.addSubview(rootView)
        scrollView.constrainEdges(to: view)
        rootView.constrainEdges(to: scrollView)
        
        rootView |> sameWidth(scrollView)
                 <> heightAnchor(.br_grid(250))
               
        // Top Stack
        let profileImageView = UIImageView()
        profileImageViewStyle(profileImageView)
        let accountInfoLabel = UILabel()
        accountInfoLabel.text = "Account Infomation"
        titleLabelStyle(accountInfoLabel)
        
        let topStackView = UIStackView(
            arrangedSubviews: [
                profileImageView,
                accountInfoLabel
            ]
        )
        topStackViewStyle(topStackView)
                                 
        // Name Stack
        let nameLabel = UILabel()
        itemLabelStyle(nameLabel)
        nameLabel.text = "Name"
        let nameTextField = UITextField()
        defaultTextFieldStyle(nameTextField)
        nameTextField.placeholder = "Your Name"
        let nameItemStackView = UIStackView(arrangedSubviews: [
            nameLabel,
            nameTextField
        ])
        itemStackViewStyle(nameItemStackView)
        
        // Item Stack
        let emailLabel = UILabel()
        itemLabelStyle(emailLabel)
        emailLabel.text = "Email"
        let emailTextField = UITextField()
        emailTextFieldStyle(emailTextField)
        let emailItemStackView = UIStackView(arrangedSubviews: [
            emailLabel,
            emailTextField
        ])
        itemStackViewStyle(emailItemStackView)
        
        // password Stack
        let passwordLabel = UILabel()
        itemLabelStyle(passwordLabel)
        passwordLabel.text = "Password"
        let passwordTextField = UITextField()
        passwordTextFieldStyle(passwordTextField)
        let passwordItemStackView = UIStackView(arrangedSubviews: [
            passwordLabel,
            passwordTextField
        ])
        itemStackViewStyle(passwordItemStackView)
        
        let locationBioLabel = UILabel()
        titleLabelStyle(locationBioLabel)
        locationBioLabel.text = "Location and Bio (Optional)"
        
        // Bottom Stack
        let cityLabel = UILabel()
        itemLabelStyle(cityLabel)
        cityLabel.text = "City"
        let cityTextField = UITextField()
        defaultTextFieldStyle(cityTextField)
        cityTextField.placeholder = "City"
        let cityItemStackView = UIStackView(arrangedSubviews: [
            cityLabel,
            cityTextField
        ])
        itemStackViewStyle(cityItemStackView)
        
        // Bottom Stack
        let countryLabel = UILabel()
        itemLabelStyle(countryLabel)
        countryLabel.text = "Country"
        let countryTextField = UITextField()
        countryTextFieldStyle(countryTextField)
        countryTextField.placeholder = "Select Country"
        let countryItemStackView = UIStackView(arrangedSubviews: [
            countryLabel,
            countryTextField
        ])
        itemStackViewStyle(countryItemStackView)
                
        let bioLabel = UILabel()
        itemLabelStyle(bioLabel)
        bioLabel.text = "Bio"
        let bioTextView = UITextView()
        bioTextView.delegate = self
        
        placeholderLabel.text = "Write some lines about yourself..."
        placeholderLabel.sizeToFit()
        bioTextView.addSubview(placeholderLabel)
        placeholderLabel.frame.origin = CGPoint(
            x: .br_grid(5),
            y: .br_grid(4)
        )
        placeholderLabel.textColor = .lightGray
        placeholderLabel.isHidden = !bioTextView.text.isEmpty
        
        defaultTextViewStyle(bioTextView)
        let bioItemStackView = UIStackView(arrangedSubviews: [
            bioLabel,
            bioTextView
        ])
        bioStackViewStyle(bioItemStackView)
        
        nextButton
            .addTarget(self,
                       action: #selector(nextButtonTapped),
                       for: .touchUpInside
        )
        nextButtonStyle(nextButton)
        
        let rootStackView = UIStackView(arrangedSubviews: [
            topStackView,
            nameItemStackView,
            emailItemStackView,
            passwordItemStackView,
            locationBioLabel,
            cityItemStackView,
            countryItemStackView,
            bioItemStackView,
            nextButton,
            UIView()
        ])
        
        //Edit Button
        let editButton = UIButton()
        editButtonStyle(editButton)
        editButton
            .addTarget(self,
                       action: #selector(editButtonTapped),
                       for: .touchUpInside)
        
        topStackView.addSubview(editButton)
        NSLayoutConstraint.activate([
            editButton.bottomAnchor.constraint(equalTo: profileImageView.bottomAnchor, constant: .br_grid(2)),
            editButton.trailingAnchor.constraint(
                equalTo: profileImageView.trailingAnchor,
                constant: .br_grid(2)
            )
        ])
        
        rootStackViewStyle(rootStackView)
        rootView.addSubview(rootStackView)
        
        rootStackView |>
            rootViewStyle(parent: rootView, .square(.br_grid(4)))
        
        nameTextField
            .addTarget(self,
                       action: #selector(nameTextChanged),
                       for: .editingChanged)
        nameTextField.delegate = self
        
        emailTextField
            .addTarget(self,
                       action: #selector(emailTextChanged),
                       for: .editingChanged)
        emailTextField.delegate = self
        
        passwordTextField
            .addTarget(self,
                       action: #selector(passwordTextChanged),
                       for: .editingChanged)
        passwordTextField.delegate = self
        
        
        cityTextField
            .addTarget(self,
                       action: #selector(cityTextChanged),
                       for: .editingChanged)
        cityTextField.delegate = self
                
        countryPickerView.delegate = self
        countryPickerView.dataSource = self
        countryTextField.inputView = countryPickerView
                
        tapGestureDismissalStyle(self)
        
        viewStore.produced.image
            .skipNil()
            .startWithValues {
                profileImageView.image = $0
            }
        
        viewStore.produced.name
            .map{ $0.rawValue }
            .assign(to: \.text, on: nameTextField)
        
        viewStore.produced.email
            .map{ $0.rawValue }
            .assign(to: \.text, on: emailTextField)
        
        viewStore.produced.password
            .map{ $0.rawValue }
            .assign(to: \.text, on: passwordTextField)
        
        viewStore.produced.city
            .assign(to: \.text, on: cityTextField)
        
        viewStore.produced.country
            .map { $0?.name }
            .assign(to: \.text, on: countryTextField)
        
        viewStore.produced.countries
            .assign(to: \.countries, on: self)
        
        viewStore.produced.biography
            .assign(to: \.text, on: bioTextView)
                                                                
        viewStore.produced.alert
            .startWithValues { [weak self] (alert) in
                guard let self = self else { return }
                guard let alert = alert else { return }
                
                let actionController = UIAlertController(
                    title: alert.title,
                    message: alert.message,
                    preferredStyle: .alert
                )
                alert.primaryButton
                    .map { $0.toUIKit(send: self.viewStore.send) }
                    .map(actionController.addAction)
                
                alert.secondaryButton
                    .map { $0.toUIKit(send: self.viewStore.send) }
                    .map(actionController.addAction)
                
                self.present(actionController,
                             animated: true,
                             completion: nil)                
        }
        
        viewStore.produced
            .actionAlert
            .startWithValues { [weak self] alert in
                guard let self = self else { return }
                guard let alert = alert else { return }
                let actionController = UIAlertController(
                    title: alert.title,
                    message: alert.message,
                    preferredStyle: .actionSheet
                )
                alert.buttons
                    .map { $0.toUIKit(send: self.viewStore.send) }
                    .forEach(actionController.addAction)
                self.present(actionController,
                             animated: true,
                             completion: nil)
        }
        
        self.viewStore.produced.pickerType
            .startWithValues { [weak self] picker in
                guard let picker = picker else {
                    if let pickerViewController = UIApplication.shared
                        .windows.first?
                        .rootViewController?
                        .presentedViewController as? UIImagePickerController {
                        pickerViewController.dismiss(animated: true, completion: nil)
                    }
                    return
                }
                let pickerController = UIImagePickerController()
                pickerController.sourceType = picker == .camera ? .camera: .photoLibrary
                pickerController.delegate = self
                self?.present(pickerController,
                              animated: false,
                              completion: nil)
            }
        
        let spinner = LoadingView()
        self.view.addSubview(spinner)
        spinner.frame = view.frame
        self.view.bringSubviewToFront(spinner)
        
        viewStore.produced.loadingState
            .startWithValues { state in
                spinner.state = state
                spinner.isHidden = state == nil
            }
        
                
    }
    
    @objc
    func emailTextChanged(_ textField: UITextField) {
        guard let email = textField.text
                .map(Email.init(rawValue:)) else { return }
        viewStore.send(.emailTextFieldDidChanged(email))
    }
    
    @objc
    func passwordTextChanged(_ textField: UITextField) {
        guard let password = textField.text
                .map(Password.init(rawValue:)) else { return }
        viewStore.send(.passwordTextFieldDidChanged(password))
    }
    
    @objc
    func nameTextChanged(_ textField: UITextField) {
        guard let name = textField.text
                .map(Name.init(rawValue:)) else { return }
        viewStore.send(.nameTextFieldDidChanged(name))
    }
    
    @objc
    func cityTextChanged(_ textField: UITextField) {
        guard let city = textField.text else { return }
        viewStore.send(.cityTextFieldDidChanged(city))
    }
    
    @objc
    func bioTextChanged(_ textView: UITextView) {
        guard let bio = textView.text else { return }
        viewStore.send(.bioTextViewDidChanged(bio))
    }
    
    @objc func keyboardWillShowNotification(_ notification: NSNotification) {
        let height = keyboardHeight(notification)
        scrollView.contentInset = .bottomEdge(height)
        scrollView.scrollIndicatorInsets = .bottomEdge(height)
                                
        var aRect = rootView.frame
        aRect.size.height -= height
        
        if let activeView = activeView as? UITextView {
            guard var bkgndRect = activeView.superview?.frame else { return }
            bkgndRect.size.height += height
            activeView.superview?.frame = bkgndRect
            scrollView.setContentOffset(.init(x: .zero, y: activeView.frame.origin.y+height), animated: true)
        }
        
        if let activeView = activeView as? UITextField {
            if !aRect.contains(activeView.frame.origin) {
                self.scrollView.scrollRectToVisible(
                    activeView.frame,
                    animated: true
                )
            }
        }
              
    }
    
    @objc func keyboardWillHideNotification(_ notification: NSNotification) {
        scrollView.contentInset = .zero
        scrollView.scrollIndicatorInsets = .zero
    }
        
    @objc
    func nextButtonTapped(_ btn: UIButton) {
        viewStore.send(.nextButtonTapped)
    }
    
    @objc
    func editButtonTapped(_ btn: UIButton) {
        viewStore.send(.editButtonTapped)
    }
           
    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if !isMovingToParent {
            self.viewStore.send(.viewDismissed)
        }
    }
    
}

extension SignUpViewController {
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = false
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShowNotification),
            name: UIResponder.keyboardDidShowNotification,
            object: nil
        )

        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillHideNotification),
            name: UIResponder.keyboardWillHideNotification,
            object: nil
        )
    }
    
    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(
            self,
            name: UIResponder.keyboardDidShowNotification,
            object: nil
        )
        NotificationCenter.default.removeObserver(
            self,
            name: UIResponder.keyboardWillHideNotification,
            object: nil
        )
    }
    
}

extension SignUpViewController: UITextViewDelegate, UITextFieldDelegate {
    
    public func textViewDidChange(_ textView: UITextView) {
        placeholderLabel.isHidden = !textView.text.isEmpty
    }
    
    public func textViewDidBeginEditing(_ textView: UITextView) {
        self.activeView = textView
    }
    
    public func textViewDidEndEditing(_ textView: UITextView) {
        self.activeView = nil
    }
    
    public func textFieldDidBeginEditing(_ textField: UITextField) {
        self.activeView = textField
    }
    
    public func textFieldDidEndEditing(_ textField: UITextField) {
        self.activeView = nil
    }
    
}

let pickerLabelStyle: (UILabel) -> Void = {
    $0.textAlignment = .center
    $0.font = .py_body(size: 16)
    $0.textColor = .secondaryTextColor
}

extension SignUpViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    public func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return countries.count
    }
    
    public func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let label = UILabel()
        label.text = countries[safe: row]?.name
        pickerLabelStyle(label)
        return label
    }
    
    public func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        .br_grid(10)
    }
    
    public func pickerView(
        _ pickerView: UIPickerView,
        didSelectRow row: Int,
        inComponent component: Int) {
        guard let country = countries[safe: row] else { return }
        viewStore.send(.pickedCountry(country))
    }
    
}

extension SignUpViewController: UIImagePickerControllerDelegate & UINavigationControllerDelegate {
    
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        viewStore.send(.viewDismissed)
    }
    
    public func imagePickerController(
        _ picker: UIImagePickerController,
        didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[.originalImage] as? UIImage,
           let url = info[.imageURL] as? URL {
            viewStore.send(.pickedImage(image, withURL: url))
        }
        
        picker.dismiss(animated: true)
    }
    
}

extension SignUpViewController {
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.tag == 1 {
            //viewStore.send(.setFirstResponder(.password))
        }
        return true
    }
}

private let emailStackViewStyle: (UIStackView) -> Void =  {
    $0.alignment = .fill
    $0.axis = .vertical
    $0.spacing = .br_grid(2)
}

private let emailTextFieldStyle: (UITextField) -> Void =
    brTextFieldStyle <> {
        $0.tag = 2
        $0.autocorrectionType = .no
        $0.autocapitalizationType = .none
        $0.keyboardType = .emailAddress
        $0.placeholder = Strings.authentication_email_textfield
    } <> heightAnchor(.br_grid(14))

private let countryTextFieldStyle: (UITextField) -> Void =
    brTextFieldStyle <> {
        $0.tag = 2
        $0.autocorrectionType = .no
        $0.autocapitalizationType = .none
        $0.keyboardType = .emailAddress
        $0.placeholder = Strings.authentication_email_textfield
    } <> arrowDownViewStyle
    <> heightAnchor(.br_grid(14))

private let defaultTextFieldStyle: (UITextField) -> Void =
    brTextFieldStyle <> {
        $0.tag = 1
        $0.autocorrectionType = .yes
        $0.autocapitalizationType = .none
        $0.keyboardType = .default
    } <> heightAnchor(.br_grid(14))

private let defaultTextViewStyle: (UITextView) -> Void =
    brTextViewStyle <> {
        $0.tag = 1
        $0.autocorrectionType = .yes
        $0.autocapitalizationType = .none
        $0.keyboardType = .default
        $0.textContainerInset = .square(.br_grid(4))
        $0.insetsLayoutMarginsFromSafeArea = true
    } <> heightAnchor(.br_grid(38))

private let passwordTextFieldStyle: (UITextField) -> Void =
    brTextFieldStyle <> {
        $0.tag = 3
        $0.autocorrectionType = .no
        $0.autocapitalizationType = .none
        $0.isSecureTextEntry = true
        $0.placeholder = Strings.authentication_password_textfield
    } <> heightAnchor(.br_grid(14))

public let arrowDownViewStyle: (UITextField) -> Void = {
    let iconView = UIImageView(
        frame: CGRect(x: 10, y: 5, width: 20, height: 20)
    )
    iconView.contentMode = .center
    iconView.image = UIImage(
        named: "ic_arrow_drop_down"        
    )
    let iconContainerView = UIView(frame: CGRect(x: 20, y: 0, width: 40, height: 30))
    iconContainerView.addSubview(iconView)
    $0.rightView = iconContainerView
    $0.rightViewMode = .always
}

private func rootViewStyle(parent: UIView, _ inset: UIEdgeInsets) -> (UIView) -> Void {
    return {
        NSLayoutConstraint.activate([
            $0.topAnchor.constraint(
                equalTo: parent.topAnchor,
                constant: inset.top),
            $0.leadingAnchor.constraint(
                equalTo: parent.leadingAnchor,
                constant: inset.left),
            $0.trailingAnchor.constraint(
                equalTo: parent.trailingAnchor,
                constant: -inset.right
            )
        ])
    }
}

    
