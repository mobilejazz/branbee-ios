import Library
import UIKit
import Validated
import ComposableArchitecture
import AuthenticationClient
import ReactiveSwift
import Tagged
import BrModels

public struct LoginState: Equatable {
    public var email: Email
    public var password: Password
    public var isFormValid: Bool
    public var isLoginRequestInFlight: Bool = false
    public var alert: _AlertState<LoginAction>? = nil
    public var isSubscribed: Bool = false
    public init(
        email: Email = "",
        password: Password = "",
        isLoginRequestInFlight: Bool = false,
        isFormValid: Bool = true,
        signUpState: SignUpNameState? = nil,
        forgotPasswordState: ForgotPasswordState? = nil,
        alert: _AlertState<LoginAction>? = nil
    ) {
        self.email = email
        self.password = password
        self.isLoginRequestInFlight = isLoginRequestInFlight
        self.isFormValid = isFormValid        
        self.signUpState = signUpState
        self.forgotPassword = forgotPasswordState
        self.alert = alert
    }
          
    public var signUpState: SignUpNameState?
    public var forgotPassword: ForgotPasswordState?
    public var currentUser: BrUser?
}


public enum LoginAction: Equatable {
    case emailDidChanged(Email)
    case passwordDidChanged(Password)
    case viewDismissed
    case viewDidLoad, viewDidAppear
    case destroy
    case facebookLoginButtonTapped
    case loginButtonTapped
    case signUpButtonTapped
    case forgotPasswordButtonTapped
    case event(LoginDelegateEvent)
    case validatePassword(Password)
    case validateEmail(Email)
    case validation(Validated<ValidationResponse, ValidationError>)
    case forgotPassword(ForgotPasswordAction)
    case signUpName(SignUpNameAction)
}




public struct LoginEnvironment {
    public var loginClient: LoginClient
    public var signUpClient: SignUpClient
    public var forgotPasswordClient: ForgotPasswordClient
    public init(
        login: LoginClient,
        signUp: SignUpClient,
        forgotPassword: ForgotPasswordClient
    ) {
        self.loginClient = login
        self.signUpClient = signUp
        self.forgotPasswordClient = forgotPassword
    }
}

public let loginReducer: Reducer<LoginState, LoginAction, SystemEnvironment<LoginEnvironment>> = .combine(
    Reducer { state, action, environment in
        struct EventId: Hashable {}
        struct CancelDelegateId: Hashable {}
        switch action {
        case let .emailDidChanged(email):
            return Effect(value: .validateEmail(email))
        case let .passwordDidChanged(pwd):
            return Effect(value: .validatePassword(pwd))
        case .loginButtonTapped:
            guard !state.isLoginRequestInFlight else {
                state.isLoginRequestInFlight = false
                return .cancel(id: EventId())
            }
            state.isLoginRequestInFlight = true
            //return Effect(value: .event(.loginSuccess))
            return environment.loginClient(
                LoginRequest(
                    email: state.email,
                    password: state.password
                )
            )
            .cancellable(id: EventId())
            .fireAndForget()
        case .event(.loginSuccess):
            state.isLoginRequestInFlight = false
            return .none
        case let .event(.onDisplayErrorLogin(failure)):
            state.isLoginRequestInFlight = false
            state.alert = _AlertState(
                title: "Ooops!",
                message: failure.message,
                dismissButton: .cancel(send: .viewDismissed)
            )
            return .none
        case  .validation(.valid):
            state.isFormValid = true
            return .none
        case let .validation(.invalid(errors)):
            state.isFormValid = false
            return .none
        case let .validateEmail(email):
            state.email = email
            return .none
        case let .validatePassword(password):
            state.password = password
            return .none
//                Effect(value: .validation(
//                zip(
//                    validate(password: state.password.rawValue),
//                    validate(email: state.email.rawValue)
//                ).map { _ in .success }
//            ))
        case .forgotPasswordButtonTapped:
            state.forgotPassword = ForgotPasswordState(email: state.email)
            return .none
        case .signUpButtonTapped:
            state.signUpState = SignUpNameState()
            return .none
        case .viewDismissed:
            state.signUpState = nil
            state.forgotPassword = nil
            state.alert = nil
            return .none
        case .forgotPassword, .signUpName:
            return .none
        case .viewDidLoad:
            
            #if DEBUG
            state.email = .init(rawValue: "younesbe@gmail.com")
            state.password = .init(rawValue: "swapsee29")
            #endif
            
            state.isSubscribed = true
            return environment
                .loginClient
                .delegate
                .cancellable(id: CancelDelegateId())
                .map(LoginAction.event)
        case .facebookLoginButtonTapped:
            return .none
        case .viewDidAppear:
            guard !state.isSubscribed else { return .none }
            return environment
                .loginClient
                .delegate
                .cancellable(id: CancelDelegateId())
                .map(LoginAction.event)
        case .destroy:
            state.isSubscribed = false
            return Effect.cancel(id: CancelDelegateId())
        }
    },
    forgotPasswordReducer.optional().pullback(
        state: \.forgotPassword,
        action: /LoginAction.forgotPassword,
        environment: {
            $0.map(\.forgotPasswordClient)
        }
    ),
    signUpNameReducer.optional().pullback(
        state: \.signUpState,
        action: /LoginAction.signUpName,
        environment: {
            $0.map(\.signUpClient)
        }
    )
)


extension Bundle {
    class BundleClass {}
    public static var authentication: Bundle {
        Bundle(for: BundleClass.self)
    }
}

public let backgroundImageStyle: (UIImageView) -> Void =
    autoLayoutStyle <> {
    $0.image = UIImage(
        named: "background_screen",
        in: .authentication,
        compatibleWith: nil
    )
}

public let branbeeLogoViewStyle: (UIImageView) -> Void =
    autoLayoutStyle <> {
        $0.image = UIImage(
            named: "branbee_logo",
            in: .authentication,
            compatibleWith: nil
        )
        $0.contentMode = .scaleAspectFit
    }

private let topStackViewStyle: (UIStackView) -> Void =
    verticalStackStyle(.br_grid(2), alignment: .center)

private let titleLabelStyle: (UILabel) -> Void =
    brLabelStyle <> {
        $0.text = Strings.authentication_join_community_label
        $0.numberOfLines = 2
    }

private let orLabelStyle: (UILabel) -> Void =
    brLabelStyle <>
    centerStyle <> {
        $0.text = Strings.authentication_or_label
}


private let haveAccountLabelStyle: (UILabel) -> Void =
    brLabelStyle <>
    centerStyle <> {
        $0.text = Strings.authentication_no_account_label
    }

private let facebookLoginButtonStyle: (LoadingButton) -> Void =
    branbeeButtonStyle <>
    viewBorderColor(.blueSecondColor,
                    borderWidth: 1,
                    cornerRadius: 2)
    

private let centerStackViewStyle: (UIStackView) -> Void = verticalStackStyle(distribution: .fillEqually)

let bottomStackViewStyle: (UIStackView) -> Void = verticalStackStyle(.br_grid(1), distribution: .fillEqually)

private
func stackRootViewStyle(parent: UIView, _ inset: UIEdgeInsets) -> (UIView) -> Void {{
    NSLayoutConstraint.activate([
        $0.topAnchor.constraint(
            equalTo: parent.safeAreaLayoutGuide.topAnchor,
            constant: inset.top),
        $0.leadingAnchor.constraint(
            equalTo: parent.safeAreaLayoutGuide.leadingAnchor,
            constant: inset.left),
        $0.trailingAnchor.constraint(
            equalTo: parent.safeAreaLayoutGuide.trailingAnchor,
            constant: -inset.right
        )
    ])
}}


private let loginControllerStyle: (UIViewController) -> Void = {
    $0.view.backgroundColor = .white
    $0.navigationItem.title = String()
    $0.navigationController?.navigationBar.tintColor = .white
    $0.navigationController?.navigationBar.backgroundColor = .blueColor
}

let clearNavBarStyle: (UINavigationBar) -> Void = {
    $0.setBackgroundImage(UIImage(), for: .default)
    $0.shadowImage = UIImage()
    $0.isTranslucent = true
    $0.backgroundColor = .clear
}

let fillNavBarStyle: (UINavigationBar?) -> Void = {
    $0?.setBackgroundImage(nil, for: .default)
    $0?.shadowImage = nil
    $0?.isTranslucent = false
    $0?.backgroundColor = .primaryColor
    $0?.titleTextAttributes = [
        .foregroundColor: UIColor.white,
        .font: UIFont.py_title3().bolded
    ]
    $0?.barTintColor = .primaryColor
}

public class LoginViewController: UIViewController {
                     
    let store: Store<LoginState, LoginAction>
    let viewStore: ViewStore<LoginState, LoginAction>
    
    public init(store: Store<LoginState, LoginAction>) {
        self.store = store
        self.viewStore = ViewStore(store)
        super.init(nibName: nil, bundle: nil)
    }
        
    required init?(coder: NSCoder) {
        fatalError("unimplemented nscoder")
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
        clearNavBarStyle(self.navigationController!.navigationBar)
    }
        
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        viewStore.send(.viewDidLoad)
        
        loginControllerStyle(self)
        
        let scrollView = UIScrollView()
        scrollViewStyle(scrollView)
        let backgroundImage = UIImageView()
        backgroundImageStyle(backgroundImage)
        let rootView = UIView()
        rootViewStyle(rootView)
        view.addSubview(scrollView)
        scrollView.addSubview(backgroundImage)
        scrollView.addSubview(rootView)
        scrollView.constrainEdges(to: view)
        backgroundImage.constrainEdges(to: view)
        rootView.constrainEdges(to: scrollView)
        
        backgroundImage |>
            fillContent(scrollView)
        rootView |>
            fillContent(scrollView)
               
        // Top Stack
        let branbeeLogoImageView = UIImageView()
        branbeeLogoViewStyle(branbeeLogoImageView)
        let titleLabel = UILabel()
        titleLabelStyle(titleLabel)
        
        let topStackView = UIStackView(
            arrangedSubviews: [
                branbeeLogoImageView,
                titleLabel
            ]
        )
                                
        rootView.addSubview(topStackView)
        topStackView
            |> topStackViewStyle
            <> stackRootViewStyle(parent: rootView, .vertical(.br_grid(4)))
        
        let facebookLoginButton = LoadingButton(
            icon: UIImage(named: "ic_facebook"),
            text: Strings.authentication_facebook_login_button,
            font: .py_headline(),
            bgColor: .blueColor,
            cornerRadius: 2,
            withShadow: false
        )
        facebookLoginButtonStyle(facebookLoginButton)
        facebookLoginButton
            .addTarget(
                self,
                action: #selector(facebookButtonTapped),
                for: .touchUpInside
            )
        
        let orLabel = UILabel()
        orLabelStyle(orLabel)
        let emailTextField = UITextField()
        emailTextFieldStyle(emailTextField)
        let passwordTextField = UITextField()
        passwordTextFieldStyle(passwordTextField)
        let forgetPasswordButton = UIButton(
            frame:
                CGRect(
                    x: .zero,
                    y: .zero,
                    width: .br_grid(16),
                    height: passwordTextField.frame.height
                ))
        
        forgetPasswordButton
            |> autoLayoutStyle
            <> widthAnchor(.br_grid(20))
        
        forgetPasswordButtonStyle(forgetPasswordButton)
        forgetPasswordButton
            .addTarget(
                self,
                action: #selector(forgotPasswordButtonTapped),
                for: .touchUpInside
            )
        let logInButton = LoadingButton()
        brMainLoadingButtonStyle(logInButton)
        logInButton.setTitle("LOG IN")
        logInButton
            .addTarget(self,
                       action: #selector(logInButtonTapped),
                       for: .touchUpInside)
        
        passwordTextField.rightView = forgetPasswordButton
        passwordTextField.rightViewMode = .always
        passwordTextField.insetsLayoutMarginsFromSafeArea = true
        passwordTextField.layoutMargins = .trailing(.br_grid(2))
        
        let centerStackView = UIStackView(arrangedSubviews: [
            facebookLoginButton,
            orLabel,
            emailTextField,
            passwordTextField,
            logInButton
        ])
        
        centerStackViewStyle(centerStackView)
        rootView.addSubview(centerStackView)
        centerStackView
            |> horizontalPadding(parent: rootView)
        
        NSLayoutConstraint.activate([
            centerStackView.topAnchor.constraint(
                equalTo: topStackView.bottomAnchor,
                constant: .br_grid(15)
            )
        ])
                
        // Botton Stack
        let haveAccountLabel = UILabel()
        haveAccountLabelStyle(haveAccountLabel)
        let signUpButton = UIButton()
        signUpButton
            .addTarget(self,
                       action: #selector(signUpButtonTapped),
                       for: .touchUpInside
        )
        brSecondaryButtonStyle(signUpButton)
        signUpButton.setTitle("SIGN UP")
                
        let bottomStackView = UIStackView(
            arrangedSubviews: [
                haveAccountLabel,
                signUpButton
            ])
        
        bottomStackViewStyle(bottomStackView)
        rootView.addSubview(bottomStackView)
        bottomStackView
            |> horizontalPadding(parent: rootView)
        
        NSLayoutConstraint.activate([
            bottomStackView.topAnchor.constraint(
                equalTo: centerStackView.bottomAnchor,
                constant: .br_grid(3)
            )          
        ])                                                                        
        emailTextField
            .addTarget(self,
                       action: #selector(emailTextChanged),
                       for: .editingChanged)
        emailTextField.delegate = self
        
        passwordTextField
            .addTarget(self,
                       action: #selector(passwordTextChanged),
                       for: .editingChanged)
        
        passwordTextField.delegate = self
        
        tapGestureDismissalStyle(self)
        
        viewStore.produced.isFormValid
            .assign(to: \.isEnabled, on: logInButton)
                        
        viewStore.produced.email
            .map(\.rawValue)
            .map(Optional.init)
            .assign(to: \.text, on: emailTextField)
        
        viewStore.produced.password
            .map(\.rawValue)
            .map(Optional.init)
            .assign(to: \.text, on: passwordTextField)
           
        emailTextField.becomeFirstResponder()
        
        viewStore.produced
            .isLoginRequestInFlight
            .startWithValues {
                $0
                ? logInButton.showLoader(userInteraction: true)
                : logInButton.hideLoader()
        }
                                                                    
        store.scope(
            state: \.forgotPassword,
            action: LoginAction.forgotPassword
        ).ifLet(then: { [weak self] store in
            guard let self = self else { return }
            self.navigationController?
                        .pushViewController(
                            ForgetPasswordViewController(store: store),
                            animated: true
                        )
        }, else: { [weak self] in
            guard let self = self else { return }
            self.navigationController?
                .popToViewController(self, animated: true)
        })
        
        store.scope(
            state: \.signUpState,
            action: LoginAction.signUpName
        ).ifLet(then: { [weak self] store in
            guard let self = self else { return }
            self.navigationController?
                .pushViewController(
                    SignUpNameViewController(store: store),
                    animated: true
                )
        }, else: { [weak self] in
            guard let self = self else { return }
            self.navigationController?
                .popToViewController(self, animated: true)
        })

        viewStore.produced.alert
            .startWithValues { [weak self] (alert) in
                guard let self = self else { return }
                guard let alert = alert else { return }
                
                let alertController = UIAlertController(
                    title: alert.title,
                    message: alert.message ?? "Message",
                    preferredStyle: .alert
                )
                alertController.addAction(
                    UIAlertAction(
                        title: "Cancel",
                        style: .default,
                        handler: { _ in
                            self.viewStore.send(.viewDismissed)
                    })
                )
                self.present(alertController, animated: true, completion: nil)
        }
                
    }
    
    @objc
    func emailTextChanged(_ textField: UITextField) {
        guard let email = textField.text
                .map(Email.init(rawValue:)) else { return }
        viewStore.send(.emailDidChanged(email))
    }
    
    @objc
    func passwordTextChanged(_ textField: UITextField) {
        guard let password = textField.text
                .map(Password.init(rawValue:)) else { return }
        viewStore.send(.passwordDidChanged(password))
    }
        
    @objc
    func facebookButtonTapped(_ button: LoadingButton) {
        viewStore.send(.facebookLoginButtonTapped)
    }
    
    @objc
    func forgotPasswordButtonTapped(_ button: LoadingButton) {
        viewStore.send(.forgotPasswordButtonTapped)
    }
    
    @objc
    func logInButtonTapped(_ btn: LoadingButton) {
        if #available(iOS 13.0, *) {
            DispatchQueue.main.schedule {
                self.viewStore.send(.loginButtonTapped)
            }
        } else {
            
        }
    }
    
    @objc
    func signUpButtonTapped(_ btn: UIButton) {
        viewStore.send(.signUpButtonTapped)
    }
           
    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewStore.send(.viewDidAppear)
        if !isMovingToParent {
            self.viewStore.send(.viewDismissed)
        }
    }
           
}

//TODO:- Improve Later
extension LoginViewController: UITextFieldDelegate {
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.tag == 1 {
            //viewStore.send(.setFirstResponder(.password))
        }
        return true
    }
}


func id<A>(_ a: A?) -> A? { a }

let forgetPasswordButtonStyle: (UIButton) -> Void = {
    $0.setTitle("Forgot?", for: .normal)
    $0.setTitleColor(.blueColor, for: .normal)
    $0.titleLabel?.font = UIFont.py_footnote(size: 14).bolded
}

private let emailStackViewStyle: (UIStackView) -> Void =  {
    $0.alignment = .fill
    $0.axis = .vertical
    $0.spacing = .br_grid(2)
}

private let emailTextFieldStyle: (UITextField) -> Void =
    brTextFieldStyle <> {
        $0.tag = 1
        $0.autocorrectionType = .no
        $0.autocapitalizationType = .none
        $0.keyboardType = .emailAddress
        $0.placeholder = Strings.authentication_email_textfield
    }

private let passwordTextFieldStyle: (UITextField) -> Void =
    brTextFieldStyle <> {
        $0.tag = 2
        $0.autocorrectionType = .no
        $0.autocapitalizationType = .none
        $0.isSecureTextEntry = true
        $0.placeholder = Strings.authentication_password_textfield
    }
