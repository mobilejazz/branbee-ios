import ReactiveSwift
import Library
import AuthenticationClient
import Validated
import ComposableArchitecture
import UIKit
import Tagged




public struct SignUpNameState: Equatable {
    var name: Name?
    var isFieldValid: Bool
    var error: String?
    var hasSuccessRequest = false
    
    public init(
        name: Name? = nil,
        isFieldValid: Bool = false,
        error: String? = nil
    ) {
        self.name = name
        self.isFieldValid = isFieldValid
        self.error = error
    }
    
    var signUpState: SignUpState? = nil
            
}

public enum SignUpNameAction: Equatable {
    case didChangeName(Name?)
    case nextButtonTapped
    case viewDidLoad
    case viewDismissed
    case viewDidDisppear
    case validation(Validated<Name, ValidationError>)
    case signUp(SignUpAction)
}

public struct SignUpNameEnvironment {
    public let mainQueue: DateScheduler
    public let globalQueue: DateScheduler
    public let authClient: LoginClient
    public init(
        mainQueue: DateScheduler,
        globalQueue: DateScheduler,
        authClient: LoginClient
    ) {
        self.mainQueue = mainQueue
        self.globalQueue = globalQueue
        self.authClient = authClient
    }
    
}

public let signUpNameReducer: Reducer<SignUpNameState, SignUpNameAction, SystemEnvironment<SignUpClient>> = .combine(
    Reducer { state, action, environment in
        struct EventId: Hashable {}
        struct CancelDelegateId: Hashable {}
        switch action {
        case let .didChangeName(name):
            state.name = name
            return .none
        case .nextButtonTapped:
            state.signUpState = SignUpState(name: state.name ?? "")
            return .none
        case let .validation(.valid(name)):
            state.name = name
            state.isFieldValid = true
            state.error = nil
            return .none
        case .validation(.invalid(let value)):
            state.isFieldValid = false
            state.error = value.first.rawValue
            return .none
        case .viewDismissed:
            state.signUpState = nil
            return .none
        case .viewDidLoad:
            return .none
        case .signUp:
            return .none
        case .viewDidDisppear:
            return .none
        }
    },
    registerReducer.optional().pullback(
        state: \.signUpState,
        action: /SignUpNameAction.signUp,
        environment: { $0 }
    )
)

private let titleLabelStyle: (UILabel) -> Void =
    brLabelDarkStyle <> {
    $0.text = Strings.signUp_title_label
    $0.font = UIFont.py_title1(size: .br_grid(8)).bolded
}

private let subTitleLabelStyle: (UILabel) -> Void =
    brLabelDarkStyle <>
    centerStyle <> {
    $0.text = Strings.signUp_subtitle_label
    $0.font = UIFont.py_body(size: 18)
    $0.numberOfLines = 3
}

public let logoViewStyle: (UIImageView) -> Void = {
    $0.image = UIImage(
        named: "branbee_icon",
        in: .authentication,
        compatibleWith: nil
    )
}

private let topStackViewStyle: (UIStackView) -> Void =
    verticalStackStyle(.br_grid(2), alignment: .center)
    //<> marginStyle(.horizontal(.br_grid(4)))

let signUpNameStyle: (UIViewController) -> Void = {
    $0.title = "Sign Up"
    $0.view.backgroundColor = .white
}

public class SignUpNameViewController: UIViewController {

    let store: Store<SignUpNameState, SignUpNameAction>
    let viewStore: ViewStore<SignUpNameState, SignUpNameAction>
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fillNavBarStyle(self.navigationController?.navigationBar)
        self.navigationController?.navigationBar.isHidden = false
    }
    
    public init(store: Store<SignUpNameState, SignUpNameAction>) {
        self.store = store
        self.viewStore = ViewStore(store)
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("Require to call other initilizer")
    }

    public override func viewDidLoad() {
        super.viewDidLoad()
        
        viewStore.send(.viewDidLoad)
        signUpNameStyle(self)
                                        
        // Top Stack
        let imageView = UIImageView()
        logoViewStyle(imageView)
        let titleLabel = UILabel()
        titleLabelStyle(titleLabel)
        let subTitleLabel = UILabel()
        subTitleLabelStyle(subTitleLabel)
        
        let topStackView = UIStackView(
            arrangedSubviews: [
                imageView,
                titleLabel,
                subTitleLabel
            ]
        )
                                
        view.addSubview(topStackView)
        topStackView |>
            horizontalPadding(parent: view) <>
            topStackViewStyle
        
        NSLayoutConstraint.activate([
            topStackView.topAnchor.constraint(
                equalTo: view.safeAreaLayoutGuide.topAnchor,
                constant: .br_grid(20)
            )
        ])
                
        // Root Stack
        let nameTextField = UITextField()
        nameTextFieldStyle(nameTextField)
        nameTextField.addTarget(
            self,
            action: #selector(emailTextChanged),
            for: .editingChanged
        )
        nameTextField.becomeFirstResponder()
        
        let actionButton = LoadingButton()
        actionButton
            .addTarget(self,
                       action: #selector(actionButtonTapped),
                       for: .touchUpInside)
       
        actionButtonStyle(actionButton)
                
        let rootStackView = UIStackView(
            arrangedSubviews: [
                nameTextField,
                actionButton
        ])
                 
        rootStackViewStyle(rootStackView)
        view.addSubview(rootStackView)
        
        rootStackView |>
            horizontalPadding(parent: view)
        
        NSLayoutConstraint.activate([
            rootStackView.topAnchor.constraint(
                equalTo: topStackView.bottomAnchor,
                constant: .br_grid(8)
            )
        ])
        
        viewStore.produced.name
            .skipNil()
            .map { $0.rawValue }
            .assign(to: \.text, on: nameTextField)
        
//        viewStore.produced.isFieldValid
//            .assign(to: \.isEnabled, on: actionButton)
       
        self.viewStore.produced
            .hasSuccessRequest
            .startWithValues { hasSuccessRequest in
                guard hasSuccessRequest else { return }
                subTitleLabel.text = Strings
                    .authentication_forgotPassword_subtitle_label_success
                nameTextField.isHidden = true
                actionButton.setTitle(Strings.accept_button, for: .normal)
        }
        
        store.scope(
            state: \.signUpState,
            action: SignUpNameAction.signUp
        ).ifLet(then: { [weak self] store in
            guard let self = self else { return }
            self.navigationController?
                .pushViewController(
                    SignUpViewController(store: store),
                    animated: true)
        }, else: { [weak self] in
            guard let self = self else { return }
            self.navigationController?
                .popToViewController(self, animated: true)
        })

        tapGestureDismissalStyle(self)
    }

    @objc
    func actionButtonTapped(_ btn: UIButton) {
        viewStore.send(.nextButtonTapped)
    }
       
    @objc
    func emailTextChanged(_ textField: UITextField) {
        viewStore.send(
            .didChangeName(
                textField.text.map(Name.init(rawValue:))
            )
        )
    }
    
    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if !self.isMovingToParent {
            viewStore.send(.viewDismissed)
        }
    }
}

private let rootStackViewStyle: (UIStackView) -> Void =
    verticalStackStyle(.br_grid(4))

private let nameTextFieldStyle: (UITextField) -> Void =
    brTextFieldStyle
        <> {
            $0.autocorrectionType = .no
            $0.autocapitalizationType = .words
            $0.font = UIFont.py_body()
            $0.textAlignment = .left
            $0.placeholder = Strings.signUp_name_textfield_placeholder
        } <> heightAnchor(.br_grid(12))

private let actionButtonStyle: (LoadingButton) -> Void =
    brMainLoadingButtonStyle
    <> {
        $0.setTitle(Strings.next_button.uppercased(), for: .normal)
    } <> heightAnchor(.br_grid(12))

