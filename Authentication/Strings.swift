enum Strings {
    public static var authentication_password_textfield: String {
        return "Password"
    }
    public static var authentication_email_textfield: String {
        return "Your Email"
    }
    public static var authentication_facebook_login_button: String {
        return "Log in with Facebook"
    }
    public static var authentication_login_button: String {
        return "Log in"
    }
    public static var authentication_no_account_label: String {
        return "Don’t have an account?"
    }
    public static var authentication_sign_up_button: String {
        return "Sign Up"
    }
    public static var authentication_or_label: String {
        return "Or"
    }
    public static var authentication_join_community_label: String {
        return "Join your community and do great things together"
    }
    
    //
    
    public static var authentication_forgotPassword_title_label: String {
        return "Forgot password"
    }
    
    public static var authentication_forgotPassword_subtitle_label: String {
        return "Introduce your email and we will send you a recovery link where you can set a new password."
    }
    
    public static var authentication_forgotPassword_sendRecovery_button: String {
        return "SEND ME A RECOVERY LINK"
    }
    
    public static var authentication_forgotPassword_subtitle_label_success: String {
        return "We have sent a recovery link to your email."
    }
    
    public static var accept_button: String {
        return "Accept"
    }
    
    public static var next_button: String {
        return "Next"
    }
    
    public static var signUp_title_label: String {
        return "Let's get started"
    }
    
    public static var signUp_subtitle_label: String {
        return "What’s your name?"
    }
    
    public static var signUp_name_textfield_placeholder: String {
        return "Your Name"
    }
    
}
