import ReactiveSwift
import Library
import AuthenticationClient
import Validated
import ComposableArchitecture
import UIKit

public struct ForgotPasswordState: Equatable {
    public var email: Email
    public var isFieldValid: Bool
    public var isRequestInFlight: Bool
    public var isTextFieldEnabled: Bool
    public var error: String?
    public var hasSuccessRequest: Bool
    
    public init(
        email: Email = "",
        isFieldValid: Bool = false,
        isRequestInFlight: Bool = false ,
        isTextFieldEnabled: Bool = true,
        error: String? = nil,
        hasSuccessRequest: Bool = false
    ) {
        self.email = email
        self.isFieldValid = isFieldValid
        self.isRequestInFlight = isRequestInFlight
        self.isTextFieldEnabled = isTextFieldEnabled
        self.error = error
        self.hasSuccessRequest = hasSuccessRequest
    }
            
}

public enum ForgotPasswordAction: Equatable {
    case didChangeText(Email)
    case actionButtonTapped
    case cancelRequest
    case sendRequest
    case viewDidLoad
    case viewDismissed
    case validation(Validated<Email, ValidationError>)
    case event(ForgotPasswordDelegateEvent)
}

public struct ForgotPasswordEnvironment {
    public let mainQueue: DateScheduler
    public let globalQueue: DateScheduler
    public let forgotPassword: ForgotPasswordClient
    public init(
        mainQueue: DateScheduler,
        globalQueue: DateScheduler,
        forgotPassword: ForgotPasswordClient
    ) {
        self.mainQueue = mainQueue
        self.globalQueue = globalQueue
        self.forgotPassword = forgotPassword
    }
    
}

public let forgotPasswordReducer:
Reducer<ForgotPasswordState, ForgotPasswordAction, SystemEnvironment<ForgotPasswordClient>> = .combine(
    Reducer { state, action, environment in
        struct EventId: Hashable {}
        struct CancelDelegateId: Hashable {}
        switch action {
        case let .didChangeText(email):
            state.email = email
            return .none
        case .actionButtonTapped:
            guard !state.hasSuccessRequest else { return .none }
            return state.isRequestInFlight
                ? Effect(value: .cancelRequest)
                : Effect(value: .sendRequest)
        case .cancelRequest:
            state.isRequestInFlight = false
            return Effect.cancel(id: EventId())
        case .sendRequest:
            state.isRequestInFlight = true
            return environment.environment
                .forgotPassword(state.email)
                .cancellable(id: EventId())
                .fireAndForget()
        case let .validation(.valid(email)):
            state.email = email
            state.isFieldValid = true
            state.error = nil
            return .none
        case .validation(.invalid(let value)):
            state.isFieldValid = false
            state.error = value.first.rawValue
            return .none
        case .viewDismissed:
            state.hasSuccessRequest = false
            state.error = nil
            return .none
        case .viewDidLoad:
            return .concatenate(
                Effect(value: .didChangeText(state.email)),
                environment.environment
                    .delegate
                    .cancellable(id: CancelDelegateId())
                    .map(ForgotPasswordAction.event)
            )
        case .event(.failedToValidateEmail):
            state.isRequestInFlight = false
            state.error = "failed to validate email"
            return .empty
        case .event(.requestedSuccessfully):
            state.hasSuccessRequest = true
            state.isRequestInFlight = false
            return .empty
        case let .event(.failedToRecoverPassword(failure)):
            state.isRequestInFlight = false
            state.error = failure.message
            return .empty
        }
    }
)


private let titleLabelStyle: (UILabel) -> Void = brLabelStyle <> {
    $0.text = Strings.authentication_forgotPassword_title_label
    $0.font = UIFont.py_title1(size: .br_grid(8)).bolded
}

private let subTitleLabelStyle: (UILabel) -> Void =
    brLabelStyle <>
    centerStyle <> {
        $0.text = Strings.authentication_forgotPassword_subtitle_label
        $0.font = UIFont.py_body(size: 18)
        $0.numberOfLines = 3
    }

private let topStackViewStyle: (UIStackView) -> Void =
    verticalStackStyle(.br_grid(2), alignment: .center)

private let forgotPasswordControllerStyle: (UIViewController) -> Void = {
    $0.navigationItem.backBarButtonItem?.title = ""
    $0.navigationController?.navigationBar.isHidden = false
}

public class ForgetPasswordViewController: UIViewController {

    let store: Store<ForgotPasswordState, ForgotPasswordAction>
    let viewStore: ViewStore<ForgotPasswordState, ForgotPasswordAction>
    
    public init(store: Store<ForgotPasswordState, ForgotPasswordAction>) {
        self.store = store
        self.viewStore = ViewStore(store)
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("Require to call other initilizer")
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    public override func viewDidLoad() {
        super.viewDidLoad()
        
        forgotPasswordControllerStyle(self)
                        
        viewStore.send(.viewDidLoad)
        
        let scrollView = UIScrollView()
        scrollViewStyle(scrollView)
        let backgroundImage = UIImageView()
        backgroundImageStyle(backgroundImage)
        let rootView = UIView()
        rootViewStyle(rootView)
        view.addSubview(scrollView)
        scrollView.addSubview(backgroundImage)
        scrollView.addSubview(rootView)
        
        scrollView.constrainEdges(to: view)
        rootView.constrainEdges(to: scrollView)
        backgroundImage.constrainEdges(to: view)
        
        backgroundImage |>
            fillContent(scrollView)
        rootView |>
            fillContent(scrollView)
                
        // Top Stack
        let titleLabel = UILabel()
        titleLabelStyle(titleLabel)
        let subTitleLabel = UILabel()
        subTitleLabelStyle(subTitleLabel)
        
        let topStackView = UIStackView(
            arrangedSubviews: [
                titleLabel,
                subTitleLabel
            ]
        )
                                
        rootView.addSubview(topStackView)
        topStackView |>
            horizontalPadding(parent: rootView)
            <> topStackViewStyle
        
        NSLayoutConstraint.activate([
            topStackView.topAnchor.constraint(
                equalTo: rootView.safeAreaLayoutGuide.topAnchor,
                constant: .br_grid(20)
            )
        ])
                
        // Root Stack
        let emailTextField = UITextField()
        emailTextFieldStyle(emailTextField)
        emailTextField.addTarget(
            self,
            action: #selector(emailTextChanged),
            for: .editingChanged
        )
        emailTextField.becomeFirstResponder()
        
        let actionButton = LoadingButton(
            text: Strings.authentication_forgotPassword_sendRecovery_button,
            textColor: .white,
            font: .py_headline(),
            bgColor: .mainColor,
            cornerRadius: 2,
            withShadow: false
        )
        
        actionButton
            .addTarget(self,
                       action: #selector(actionButtonTapped),
                       for: .touchUpInside)
       
        actionButtonStyle(actionButton)
                
        let rootStackView = UIStackView(arrangedSubviews: [
            emailTextField,
            actionButton
        ])
                 
        rootStackViewStyle(rootStackView)
        rootView.addSubview(rootStackView)
        
        rootStackView |>
            horizontalPadding(parent: rootView)
        
        NSLayoutConstraint.activate([
            rootStackView.topAnchor.constraint(
                equalTo: topStackView.bottomAnchor,
                constant: .br_grid(8)
            )
        ])
        
        viewStore.produced.email
            .map { $0.rawValue }
            .assign(to: \.text, on: emailTextField)
                
//        viewStore.produced.isFieldValid
//            .assign(to: \.isEnabled, on: actionButton)

        viewStore.produced
            .isRequestInFlight
            .startWithValues {
                if $0 {
                    actionButton.showLoader(userInteraction: true)
                    self.dismissKeyboard()
                } else {
                    actionButton.hideLoader()
                }
        }
              
        self.viewStore.produced
            .hasSuccessRequest
            .startWithValues { hasSuccessRequest in
                guard hasSuccessRequest else { return }
                subTitleLabel.text = Strings
                    .authentication_forgotPassword_subtitle_label_success
                emailTextField.isHidden = true
                actionButton.setTitle(Strings.accept_button, for: .normal)
        }
        
        viewStore.produced.error
            .startWithValues { [weak self] error in
                guard let self = self else { return }
                guard let error = error else { return }
                
                let alertController = UIAlertController(
                    title: "Ooops",
                    message: error,
                    preferredStyle: .alert
                )
                alertController.addAction(
                    UIAlertAction(
                        title: "Cancel",
                        style: .default,
                        handler: { _ in
                            self.viewStore.send(.viewDismissed)
                    })
                )
                self.present(alertController, animated: true, completion: nil)
        }
        

        tapGestureDismissalStyle(self)
    }

    @objc
    func actionButtonTapped(_ btn: UIButton) {
        viewStore.send(.actionButtonTapped)
    }
       
    @objc
    func emailTextChanged(_ textField: UITextField) {
        viewStore.send(.didChangeText(
            Email(rawValue: textField.text ?? "")
        ))
    }
    
    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if !self.isMovingToParent {
            viewStore.send(.viewDismissed)
        }
    }
}

private let rootStackViewStyle: (UIStackView) -> Void =
    verticalStackStyle(.br_grid(2))

private let emailTextFieldStyle: (UITextField) -> Void =
    brTextFieldStyle
        <> {
            $0.autocorrectionType = .no
            $0.autocapitalizationType = .none
            $0.font = UIFont.py_body()
            $0.textAlignment = .left
            $0.placeholder = Strings.authentication_email_textfield
        } <> heightAnchor(.br_grid(12))


private let actionButtonStyle: (LoadingButton) -> Void =
    brMainLoadingButtonStyle
<> heightAnchor(.br_grid(12))
   
