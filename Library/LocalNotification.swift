import UIKit
import UserNotifications
import ReactiveSwift

public class LocalNotification: NSObject {
    let identifier: String = UUID().uuidString
    let title: String
    let subtitle: String
    let message: String
    let badge: Int?
    
    public init(title: String, subtitle: String, message: String, badge: Int? = nil) {
        self.title = title
        self.subtitle = subtitle
        self.message = message
        self.badge = badge
    }
}


extension Date {
    static func - (lhs: Date, rhs: Date) -> TimeInterval {
        return lhs.timeIntervalSince1970 - rhs.timeIntervalSince1970
    }
}


import UserNotifications

func dismiss() {
    UNUserNotificationCenter
        .current()
        .requestAuthorization(options: [.alert, .badge, .sound]) { (bool, error) in
    }
}


public struct NotificationRequest: Equatable {
    let notification: LocalNotification
    let date: Date
    public init(
        notification: LocalNotification,
        date: Date
    ) {
        self.notification = notification
        self.date = date
    }
}

import Combine
public struct NotificationClient {
    
    public enum Response: Equatable {
        case notification(LocalNotification)
    }
    
    public enum Failure: LocalizedError {
        case notAuthorized
        case custom(String)
    }
    
    public enum AuthorizationStatus: Equatable {
        case allowed
        case denied
        case notDetermined
        case provisional
        case ephemeral
    }
    
    public let authorizationStatus: SignalProducer<AuthorizationStatus, Failure>
    public let send: (NotificationRequest) -> SignalProducer<Response, Failure>
    public let requestAuthorization: SignalProducer<AuthorizationStatus, Failure>
    
    public init(
        requestAuthorization: SignalProducer<AuthorizationStatus, Failure>,
        authorizationStatus: SignalProducer<AuthorizationStatus, Failure>,
        send: @escaping (NotificationRequest) -> SignalProducer<Response, Failure>
    ) {
        self.requestAuthorization = requestAuthorization
        self.authorizationStatus = authorizationStatus
        self.send = send
    }
}

extension NotificationClient {
    public static var empty: Self {
        Self(
            requestAuthorization: SignalProducer(value: .allowed),
            authorizationStatus:
                SignalProducer(value: .notDetermined),
            send: { _ in
                SignalProducer(
                    value:
                    .notification(
                        LocalNotification(title: "", subtitle: "", message: "")
                    )
                )
            }
        )
    }
}
 
// Live
extension NotificationClient {
    public static var live: Self {
        .init(
            requestAuthorization: .future { promise in
                UNUserNotificationCenter
                    .current()
                    .requestAuthorization(
                        options: [.alert, .sound],
                        completionHandler: { granted, error in
                            if let error = error {
                                promise(.failure(.custom(error.localizedDescription)))
                            }
                            promise(.success(granted ? .allowed: .denied))
                        }
                    )
            },
            authorizationStatus: SignalProducer<AuthorizationStatus, Failure> { observer, _ in
                UNUserNotificationCenter.current().getNotificationSettings(completionHandler: { (settings) in
                    switch settings.authorizationStatus {
                    case .notDetermined:
                        observer.send(value: .notDetermined)
                    case .denied:
                        observer.send(value: .denied)
                    case .authorized:
                        observer.send(value: .allowed)
                    case .provisional:
                        observer.send(value: .provisional)
                    case .ephemeral:
                        observer.send(value: .ephemeral)
                    @unknown default:
                        fatalError()
                    }
                    observer.sendCompleted()
                })
            },
            send: { request in
                .future { promise in
                    
                    let trigger = UNTimeIntervalNotificationTrigger(
                        timeInterval: request.date - Date(),
                        repeats: false
                    )
                    
                    let notificationRequest = UNNotificationRequest(
                        identifier: UUID().uuidString,
                        content: request.notification.content,
                        trigger: trigger
                    )
                    
                    UNUserNotificationCenter.current().add(notificationRequest) { error in
                        if let error = error {
                            promise(.failure(.custom(error.localizedDescription)))
                        } else {
                            promise(.success(.notification(request.notification)))
                        }
                    }
                }
            }
        )
    }
}


extension LocalNotification {
    var content: UNMutableNotificationContent {
        let content = UNMutableNotificationContent()
        content.title = self.title
        content.subtitle = self.subtitle
        content.body = self.message
        content.badge = self.badge == nil ? nil : NSNumber(value: self.badge!)
        return content
    }
}
