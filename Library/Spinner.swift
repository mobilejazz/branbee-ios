import UIKit
import ComposableArchitecture

public let indStyle: (UIView) -> Void = {
    $0.backgroundColor = UIColor(white: 0.98, alpha: 1.0)
    $0.layer.masksToBounds = false
    $0.layer.shadowColor = UIColor.gray.cgColor
    $0.layer.shadowOpacity = 0.5
    $0.layer.shadowOffset =  CGSize(width: -1, height: 1)
    $0.layer.shadowRadius = 1
    $0.layer.shadowPath = UIBezierPath(rect: $0.bounds).cgPath
} <> roundedStyle(cornerRadius: .br_grid(4))
  <> squareStyle(.br_grid(30))

let retryButtonStyle: (UIButton) -> Void =
    brMainButtonStyle <> {
        $0.setTitle("Retry")
    } <> rectangleStyle(
        horizontal: .br_grid(40),
        vertical: .br_grid(12)
    )

let spinnerStackViewStyle: (UIStackView) -> Void =
     verticalStackStyle(
        .br_grid(4),
        distribution: .fill,
        alignment: .center
    )

private let messageLabelStyle: (UILabel) -> Void = {
    $0.textColor = .secondaryTextColor
    $0.font = UIFont.py_body(size: .br_grid(4)).bolded
    $0.numberOfLines = 0
    $0.textAlignment = .center
} <> widthAnchor(.br_grid(80))


public enum LoadingState: Equatable {
    
    case loading
    case failure(Failure?, action: RetryAction?)
    case message(String)
}


public class LoadingView: UIView {
            
    public enum Style {
        case indicator
        case material
    }
                
    private var activityIndicator: UIView!
    private let retryButton = UIButton()
    private let messageLabel = UILabel()
    public var state: LoadingState? {
        didSet {
            guard let state = state else { return }
                    
            switch state {
            case .loading:
                retryButton.isHidden = true
                messageLabel.isHidden = true
                self.startAnimating()
            case let .failure(error, action: action):
                self.stopAnimating()
                messageLabel.text = error?.message
                retryButton.isHidden = action == nil
                messageLabel.isHidden = error == nil
            case let .message(message):
                self.stopAnimating()
                messageLabel.text = message
                retryButton.isHidden = true
                messageLabel.isHidden = false
            }
                                   
            
            
        }
    }
    
    public init() {
        super.init(frame: .zero)
               
        self.activityIndicator =
            MaterialLoadingIndicator(color: .mainColor)
        indStyle(activityIndicator)
               
        let container = UIVisualEffectView(
            effect: UIBlurEffect(style: .light)
        )
        container.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(container)
        container.constrainEdges(to: self)
        retryButtonStyle(retryButton)
        retryButton.addTarget(self,
                              action: #selector(retryButtonTapped(_:)),
                              for: .touchUpInside)
        messageLabelStyle(messageLabel)
        let spinnerStackView = UIStackView(arrangedSubviews: [
            activityIndicator,
            messageLabel,
            retryButton
        ])
        spinnerStackViewStyle(spinnerStackView)
        self.addSubview(spinnerStackView)        
        spinnerStackView |> centerAutoLayoutStyle(self)
        self.bringSubviewToFront(spinnerStackView)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func retryButtonTapped(_ button: UIButton) {
        extract(case: LoadingState.failure, from: state)?.1?.run()
    }
    
    func startAnimating() {
        (activityIndicator as? MaterialLoadingIndicator)?.startAnimating()
        (activityIndicator as? UIActivityIndicatorView)?.startAnimating()
    }
    
    func stopAnimating() {
        (activityIndicator as? MaterialLoadingIndicator)?.stopAnimating()
        (activityIndicator as? UIActivityIndicatorView)?.stopAnimating()
    }
    
}

