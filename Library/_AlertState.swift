public struct _AlertState<Action> {
  public let id = UUID()
  public var message: String?
  public var primaryButton: Button?
  public var secondaryButton: Button?
  public var title: String

  public init(
    title: String,
    message: String? = nil,
    dismissButton: Button? = nil
  ) {
    self.title = title
    self.message = message
    self.primaryButton = dismissButton
  }

  public init(
    title: String,
    message: String? = nil,
    primaryButton: Button,
    secondaryButton: Button
  ) {
    self.title = title
    self.message = message
    self.primaryButton = primaryButton
    self.secondaryButton = secondaryButton
  }

  public struct Button {
    public var action: Action?
    public var type: `Type`

    public static func cancel(
      _ label: String,
      send action: Action? = nil
    ) -> Self {
      Self(action: action, type: .cancel(label: label))
    }

    public static func cancel(
      send action: Action? = nil
    ) -> Self {
      Self(action: action, type: .cancel(label: nil))
    }

    public static func `default`(
      _ label: String,
      send action: Action? = nil
    ) -> Self {
      Self(action: action, type: .default(label: label))
    }

    public static func destructive(
      _ label: String,
      send action: Action? = nil
    ) -> Self {
      Self(action: action, type: .destructive(label: label))
    }

    public enum `Type` {
      case cancel(label: String?)
      case `default`(label: String)
      case destructive(label: String)
    }
  }
}




extension _AlertState: Equatable where Action: Equatable {
  public static func == (lhs: Self, rhs: Self) -> Bool {
    lhs.title == rhs.title
      && lhs.message == rhs.message
      && lhs.primaryButton == rhs.primaryButton
      && lhs.secondaryButton == rhs.secondaryButton
  }
}

extension _AlertState: Hashable where Action: Hashable {
  public func hash(into hasher: inout Hasher) {
    hasher.combine(self.title)
    hasher.combine(self.message)
    hasher.combine(self.primaryButton)
    hasher.combine(self.secondaryButton)
  }
}



extension _AlertState.Button.`Type`: Equatable {
  public static func == (lhs: Self, rhs: Self) -> Bool {
    switch (lhs, rhs) {
    case let (.cancel(lhs), .cancel(rhs)):
      return lhs == rhs
    case let (.default(lhs), .default(rhs)), let (.destructive(lhs), .destructive(rhs)):
      return lhs == rhs
    default:
      return false
    }
  }
}


extension _AlertState.Button: Equatable where Action: Equatable {
  public static func == (lhs: Self, rhs: Self) -> Bool {
    return lhs.action == rhs.action && lhs.type == rhs.type
  }
}


extension _AlertState.Button.`Type`: Hashable {
  public func hash(into hasher: inout Hasher) {
    switch self {
    case let .cancel(label):
      hasher.combine(label)
    case let .default(label), let .destructive(label):
      hasher.combine(label)
    }
  }
}


extension _AlertState.Button: Hashable where Action: Hashable {
  public func hash(into hasher: inout Hasher) {
    hasher.combine(self.action)
    hasher.combine(self.type)
  }
}

import UIKit
extension _AlertState.Button {
    public func toUIKit(send: @escaping (Action) -> Void) -> UIAlertAction {
    let action = { if let action = self.action { send(action) } }
    switch self.type {
    case let .cancel(.some(label)):
        return UIAlertAction(
            title: label,
            style: .cancel,
            handler: { _ in action() }
        )
    case .cancel(.none):
      return UIAlertAction(
          title: "Cancel",
          style: .cancel,
          handler: { _ in action() }
      )
    case let .default(label):
      return UIAlertAction(
          title: label,
          style: .default,
          handler: { _ in action() }
      )
    case let .destructive(label):
      return UIAlertAction(
          title: label,
          style: .destructive,
          handler: { _ in action() }
      )
    }
  }
}



extension _AlertState {
    public  func toUIKit(send: @escaping (Action) -> Void) -> UIAlertController {
        
        if let primaryButton = self.primaryButton, let secondaryButton = self.secondaryButton {
            let alertController = UIAlertController(
                title: title,
                message: message,
                preferredStyle: .alert
            )
            
            alertController
                .addAction(primaryButton.toUIKit(send: send))
            
            alertController
                .addAction(secondaryButton.toUIKit(send: send))
            
            return alertController
        } else {
            let alertController =  UIAlertController(
                title: title,
                message: message,
                preferredStyle: .alert
            )
            
            if let alertaction = primaryButton?.toUIKit(send: send) {
                alertController.addAction(alertaction)
            }
            
            return alertController
        }
    }
}



public struct _ActionSheetState<Action: Equatable>: Equatable {
  public let id = UUID()
  public var buttons: [Button]
  public var message: String?
  public var title: String

  public init(
    title: String,
    message: String? = nil,
    buttons: [Button]
  ) {
    self.buttons = buttons
    self.message = message
    self.title = title
  }

  public typealias Button = _AlertState<Action>.Button
}
