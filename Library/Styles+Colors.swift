import UIKit

extension UIColor {
    
    public class var mainColor: UIColor {
        return UIColor(red: 254.0 / 255.0, green: 63.0 / 255.0, blue: 85.0 / 255.0, alpha: 1.0)
    }
    
    public class var primaryColor: UIColor {
        return UIColor(red: 17.0 / 255.0, green: 46.0 / 255.0, blue: 74.0 / 255.0, alpha: 1.0)
      }
    
    public class var secondaryTextColor: UIColor {
        return UIColor(red: 17.0 / 255.0, green: 46.0 / 255.0, blue: 74.0 / 255.0, alpha: 1.0)
      }
    
    public class var blueColor: UIColor {
        return UIColor(red: 59.0 / 255.0, green: 89.0 / 255.0, blue: 152.0 / 255.0, alpha: 1.0)
    }
    
    public class var blueSecondColor: UIColor {
        return UIColor(red: 87.0 / 255.0, green: 123.0 / 255.0, blue: 196.0 / 255.0, alpha: 1.0)
    }
    
    public class var placeholderColor: UIColor {
        return UIColor(white: 0.0, alpha: 0.3)
    }
    
    public class var whiteBorderColor: UIColor {
        return UIColor(white: 201.0 / 255.0, alpha: 1.0)
      }
    
    public class var brGrayColor: UIColor {
        return UIColor(white: 129.0 / 255.0, alpha: 1.0)
    }
    
    
}
