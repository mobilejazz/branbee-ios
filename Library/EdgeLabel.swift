import UIKit
public class EdgeLabel: UILabel {

    private let edge: UIEdgeInsets
    
    public init(edge: UIEdgeInsets = .square(.br_grid(2))) {
        self.edge = edge
        super.init(frame: .zero)
    }

    required init?(coder: NSCoder) {
        fatalError("Unimplemented")
    }
    
    public override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets(
            top: edge.top,
            left: edge.left,
            bottom: edge.bottom,
            right: edge.right
        )
        super.drawText(in: rect.inset(by: insets))
    }

    public override var intrinsicContentSize: CGSize {
        let size = super.intrinsicContentSize
        return CGSize(
            width: size.width + edge.left + edge.right,
            height: size.height + edge.top + edge.bottom
        )
    }

    public override var bounds: CGRect {
        didSet {
            preferredMaxLayoutWidth = bounds.width - (edge.left + edge.right)
        }
    }
}
