import UIKit.UIActivityIndicatorView
public let indicatorStyle: (UIActivityIndicatorView) -> Void = {
    $0.frame = CGRect(x: 0, y: 0, width: .br_grid(25), height: .br_grid(25))
    $0.backgroundColor = UIColor(white: 0.98, alpha: 1.0)
    $0.layer.masksToBounds = false
    $0.layer.shadowColor = UIColor.gray.cgColor
    $0.layer.shadowOpacity = 0.5
    $0.layer.shadowOffset =  CGSize(width: -1, height: 1)
    $0.layer.shadowRadius = 1
    $0.layer.shadowPath = UIBezierPath(rect: $0.bounds).cgPath
} <> roundedStyle(cornerRadius: .br_grid(4))
