import UIKit.UIImage
extension UIImage {
    public func downSample(
        to pointSize: CGSize,
        scale: CGFloat) -> UIImage {
        let imageData = jpegData(compressionQuality: 1)!
        let imageSourceOptions = [kCGImageSourceShouldCache: false] as CFDictionary
        let imageSource = CGImageSourceCreateWithData(imageData as CFData, imageSourceOptions)!
        let maxDimensionInPixels = max(pointSize.width, pointSize.height) * scale
        let downsampleOptions =
            [kCGImageSourceCreateThumbnailFromImageAlways: true,
             kCGImageSourceShouldCacheImmediately: true,
             kCGImageSourceCreateThumbnailWithTransform: true,
             kCGImageSourceThumbnailMaxPixelSize: maxDimensionInPixels] as CFDictionary
        return  UIImage(cgImage: CGImageSourceCreateThumbnailAtIndex(imageSource, 0, downsampleOptions)!)
    }
}

extension CGSize {
    public static func square(_ a: CGFloat) -> CGSize {
        CGSize(width: a, height: a)
    }
    public static func rectange(_ v: CGFloat, _ h: CGFloat) -> CGSize {
        CGSize(width: h, height: v)
    }
}

extension UIImageView {
    public func label(_ text: String)  {
        let label = UILabel()
        labelStyle(label)
        label.text = text

        UIGraphicsBeginImageContext(label.frame.size)
        label.layer.render(in: UIGraphicsGetCurrentContext()!)
        self.image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
    }
}

let labelStyle: (UILabel) -> Void = {
    $0.frame.size = .square(.br_grid(25))
    $0.textColor = .darkGray
    $0.font = UIFont.py_subhead(size: .br_grid(7)).bolded
    $0.textAlignment = .center
    $0.layer.cornerRadius = 50.0
}


