import Tagged

public class NameType {}
public typealias Name = Tagged<NameType, String>

public class EmailType {}
public typealias Email = Tagged<EmailType, String>

public class PasswordType {}
public typealias Password = Tagged<PasswordType, String>
