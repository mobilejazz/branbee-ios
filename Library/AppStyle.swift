import UIKit

public let brLabelStyle: (UILabel) -> Void =
    autoLayoutStyle <> {
        $0.font = .py_body(size: 16)
        $0.textColor = .white
    }

public let brDarkTextColor: (UILabel) -> Void = {
    $0.textColor = .secondaryTextColor
}

public let brGrayTextColor: (UILabel) -> Void = {
    $0.textColor = .brGrayColor
}


public let brLabelDarkStyle: (UILabel) -> Void =
    {
        $0.font = .py_body(size: 16)
    } <> brDarkTextColor
    <> autoLayoutStyle

public let brLabelDarkBoldStyle: (UILabel) -> Void =
    {
        $0.font = UIFont.py_body(size: 16).bolded
    } <> brDarkTextColor
<> autoLayoutStyle

public let branbeeButtonStyle: (UIButton) -> Void =
    autoLayoutStyle
    <> roundedStyle(cornerRadius: .br_grid(1))
    <> {
        $0.titleLabel?.font = UIFont.py_headline(size: 14).bolded
        $0.titleLabel?.textColor = .white
    } <> heightAnchor(.br_grid(12))

public let brButtonStyle: (UIButton) -> Void =
    autoLayoutStyle
    <> roundedStyle(cornerRadius: .br_grid(1))
    <> {
        $0.titleLabel?.font = UIFont.py_headline(size: 14).bolded
        $0.titleLabel?.textColor = .white
    }

public let brMainButtonStyle: (UIButton) -> Void =
    brButtonStyle <> {
        $0.backgroundColor = .mainColor        
    }

public let brMainLoadingButtonStyle: (LoadingButton) -> Void =
    brMainButtonStyle <> {
        $0.bgColor = .mainColor
        $0.shadowAdded = true
        $0.setBackgroundImage(UIImage(.mainColor), for: .disabled)
    }  <> {
        $0.indicator = indicator()
        $0.indicator.tintColor = .white
    }

private let indicator: () -> UIActivityIndicatorView = {
    UIActivityIndicatorView(style: .white)
}


public let brSecondaryButtonStyle: (UIButton) -> Void =
    branbeeButtonStyle <> {
        $0.backgroundColor = .primaryColor
    }

public let brTextFieldStyle: (UITextField) -> Void =
    autoLayoutStyle
    <> leftPadding()
    <>  {
        $0.tintColor = UIColor.placeholderColor
        $0.backgroundColor = .white
        $0.font = .py_body(size: 16)
    } <> viewBorderColor(.whiteBorderColor,
                         borderWidth: 1,
                         cornerRadius: .br_grid(1)
    )

public let brTextViewStyle: (UITextView) -> Void =
    autoLayoutStyle <>
    viewBorderColor(.whiteBorderColor,
                    borderWidth: 1,
                    cornerRadius: .br_grid(1)
    ) <>
    { (tf: UITextView) in
        tf.tintColor = UIColor.placeholderColor
        tf.backgroundColor = .white
        tf.font = .py_body()
    }

// Stack View
public func verticalStackStyle(
    _ spacing: CGFloat = .br_grid(2),
    distribution: UIStackView.Distribution = .fill,
    alignment: UIStackView.Alignment = .fill
) -> (UIStackView) -> Void {
        autoLayoutStyle <> {
            $0.axis = .vertical
            $0.alignment = alignment
            $0.distribution = distribution
            $0.spacing = spacing
        }
}

public func horizontalStackStyle(
    _ spacing: CGFloat = .br_grid(2),
    distribution: UIStackView.Distribution = .fill,
    alignment: UIStackView.Alignment = .fill
) -> (UIStackView) -> Void {
        autoLayoutStyle <> {
            $0.axis = .horizontal
            $0.alignment = alignment
            $0.distribution = distribution
            $0.spacing = spacing            
        }
}

//horizontal padding
public func horizontalPadding(
    _ value: CGFloat = .br_grid(4),
    parent: UIView)
-> (UIView)
-> Void {
    return { view in
        NSLayoutConstraint.activate([
            view.leadingAnchor.constraint(
                equalTo: parent.leadingAnchor,
                constant: value
            ),
            view.trailingAnchor.constraint(
                equalTo: parent.trailingAnchor,
                constant: -value
            )
        ])
    }
}

// fill content
public func fillContent(_ parent: UIView) -> (UIView) -> Void {
    return {
        NSLayoutConstraint.activate([
            $0.heightAnchor.constraint(equalTo: parent.heightAnchor),
            $0.widthAnchor.constraint(equalTo: parent.widthAnchor)
        ])
    }
}

public func sameHeight(_ parent: UIView) -> (UIView) -> Void {
    return {
            $0.heightAnchor.constraint(equalTo: parent.heightAnchor).isActive = true
    }
}

public func sameWidth(_ parent: UIView) -> (UIView) -> Void {
    return {
            $0.widthAnchor.constraint(equalTo: parent.widthAnchor).isActive = true
    }
}

public let clearStyle: (UIView) -> Void = {
    $0.backgroundColor = .clear
}

public let profileViewStyle: (UIImageView) -> Void =
    autoLayoutStyle
    <> roundedStyle(cornerRadius: .br_grid(10))
    <> {
        $0.contentMode = .scaleAspectFill
    } <> squareStyle(.br_grid(20))

public let scrollViewStyle: (UIScrollView) -> Void =
    autoLayoutStyle <> {
        $0.insetsLayoutMarginsFromSafeArea = false
    }

public let rootViewStyle: (UIView) -> Void =
    autoLayoutStyle
    <> clearStyle

public func marginStyle(_ insets: UIEdgeInsets = .square(.br_grid(4)))
-> (UIStackView) -> Void {
    return {
        $0.layoutMargins = insets
        $0.isLayoutMarginsRelativeArrangement = true
    }
}
