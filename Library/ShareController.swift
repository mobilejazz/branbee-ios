import UIKit
public typealias Callback = (
    _ activityType: UIActivity.ActivityType?,
    _ completed: Bool,
    _ returnedItems: [Any]?,
    _ error: Error?) -> Void
extension UIActivityViewController {
    public static func share(activityItems: [Any],
                      applicationActivities: [UIActivity]? = nil,
                      excludedActivityTypes: [UIActivity.ActivityType]? = nil,
                      callback: Callback? = nil) -> UIActivityViewController {
        let controller = UIActivityViewController(
            activityItems: activityItems,
            applicationActivities: applicationActivities
        )
        controller.excludedActivityTypes = excludedActivityTypes
        controller.completionWithItemsHandler = callback
        return controller
    }
}
