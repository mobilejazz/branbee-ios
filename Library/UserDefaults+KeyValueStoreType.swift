public protocol KeyValueStoreType: AnyObject {
    func set(_ value: Bool, forKey defaultName: String)
    func set(_ value: Int, forKey defaultName: String)
    func set(_ value: Any?, forKey defaultName: String)
        
    func bool(forKey defaultName: String) -> Bool
    func dictionary(forKey defaultName: String) -> [String: Any]?
    func integer(forKey defaultName: String) -> Int
    func object(forKey defaultName: String) -> Any?
    func string(forKey defaultName: String) -> String?
    
    // Branbee
    //var currentUser: BrUser? { get set }
    
    func synchronize() -> Bool
}

extension UserDefaults: KeyValueStoreType {}


//extension KeyValueStoreType {
//    public var currentUser: BrUser? {
//        get {
//            object(forKey: "currentUser") as? BrUser
//        }
//        set {
//            set(self, forKey: "currentUser")
//        }
//    }
//}
