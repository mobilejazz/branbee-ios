extension Encodable {
    public func dictionary() throws -> [String: Any] {
        try JSONSerialization.jsonObject(
            with: try JSONEncoder().encode(self),
            options: .fragmentsAllowed
            ) as! [String: Any]
    }
}
