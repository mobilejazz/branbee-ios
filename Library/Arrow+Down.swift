import UIKit

public let arrowDownViewStyle: (UITextField) -> Void = {
    let iconView = UIImageView(
        frame: CGRect(x: 10, y: 5, width: 20, height: 20)
    )
    iconView.contentMode = .center
    iconView.image = UIImage(
        named: "ic_arrow_drop_down"
    )
    let iconContainerView = UIView(frame: CGRect(x: 20, y: 0, width: 40, height: 30))
    iconContainerView.addSubview(iconView)
    $0.rightView = iconContainerView
    $0.rightViewMode = .always
}

extension UIImage {
    public convenience init?(named: String, in bundle: Bundle? = .main) {
        self.init(
            named: named,
            in: bundle,
            compatibleWith: nil
        )
    }
}
