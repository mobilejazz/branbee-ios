extension Array {
    public subscript(safe index: Int) -> Element? {
        if count > index {
            return self[index]
        }
        return nil
    }
}
