public struct Failure: LocalizedError, Equatable {
    public let message: String?
    public let cause: String?
    public init(
        message: String?,
        cause: String?
    ) {
        self.message = message
        self.cause = cause
    }
}

extension Failure {
    public static func message(_ message: String) -> Self {
        .init(message: message, cause: nil)
    }
}
