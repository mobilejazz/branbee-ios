import UIKit

let darkStyle: (UILabel) -> Void = {
    $0.textColor = .black
}

let lightStyle: (UILabel) -> Void = {
    $0.textColor = .white
}

let primaryStyle: (UILabel) -> Void = {
    $0.textColor = .primaryColor
}

let grayStyle: (UILabel) -> Void = {
    $0.textColor = .brGrayColor
}

public let br32CenterDarkBold: (UILabel) -> Void = {
    $0.font = UIFont.py_title1(size: .br_grid(8)).bolded
} <> centerStyle
  <> darkStyle

public let br32CenterLightBold: (UILabel) -> Void = {
    $0.font = UIFont.py_title1(size: .br_grid(8)).bolded
} <> centerStyle
    <> lightStyle

public let br24PrimaryBold: (UILabel) -> Void = {
    $0.font = UIFont.py_title1(size: .br_grid(6)).bolded
} <> centerStyle
    <> primaryStyle

public let br20GrayMedium: (UILabel) -> Void = {
    $0.font = UIFont.py_title1(size: .br_grid(5)).bolded
} <> leftStyle
  <> grayStyle

public let br20LightMedium: (UILabel) -> Void = {
    $0.font = UIFont.py_title1(size: .br_grid(5)).bolded
} <> leftStyle
  <> lightStyle

public let br20DarkMedium: (UILabel) -> Void = {
    $0.font = UIFont.py_title1(size: .br_grid(5)).bolded
} <> leftStyle
  <> darkStyle
