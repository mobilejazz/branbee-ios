import UIKit

public func roundedStyle<View: UIView>(cornerRadius: CGFloat) -> (View) -> Void {
    return {
        $0.layer.cornerRadius = cornerRadius
        $0.layer.masksToBounds = true
        if #available(iOS 13, *) {
            $0.layer.cornerCurve = .continuous            
        }
    }
}

public func autoLayoutStyle<View: UIView>(_ v: View) -> Void {
    v.translatesAutoresizingMaskIntoConstraints = false
}

public func aspectRatioStyle<View: UIView>(size: CGSize) -> (View) -> Void {
    return {
        $0.widthAnchor.constraint(
            equalTo: $0.heightAnchor,
            multiplier: size.height / size.width)
        .isActive = true
    }
}

public func fontStyle(ofSize size: CGFloat, weight: UIFont.Weight) -> (UILabel) -> Void {
    return {
        $0.font = .systemFont(ofSize: size, weight: weight)
    }
}

public func textColorStyle(_ color: UIColor)
-> (UILabel) -> Void  {
    { $0.textColor = color }
}

public let centerStyle: (UILabel) -> Void = {
    $0.textAlignment = .center
}

public let leftStyle: (UILabel) -> Void = {
    $0.textAlignment = .left
}

public let rightStyle: (UILabel) -> Void = {
    $0.textAlignment = .right
}

public func squareStyle<V: UIView>(_ constant: CGFloat) -> (V) -> Void  {
    return {
        $0.heightAnchor.constraint(equalToConstant: constant).isActive = true
        $0.widthAnchor.constraint(equalToConstant: constant).isActive = true
    }
}

public func rectangleStyle<V: UIView>(horizontal: CGFloat, vertical: CGFloat) -> (V) -> Void  {
    return {
        $0.heightAnchor.constraint(equalToConstant: vertical).isActive = true
        $0.widthAnchor.constraint(equalToConstant: horizontal).isActive = true
    }
}

public func heightAnchor<V: UIView>(_ constant: CGFloat) -> (V) -> Void {
    return {
        $0.heightAnchor.constraint(
            equalToConstant: constant).isActive = true
    }
}

public func widthAnchor<V: UIView>(_ constant: CGFloat) -> (V) -> Void {
    return {
        $0.widthAnchor.constraint(
            equalToConstant: constant).isActive = true
    }
}


public func centerAutoLayoutStyle<V: UIView>(_ parent: V) -> (V) -> Void {
{
    $0.translatesAutoresizingMaskIntoConstraints = false
    $0.centerXAnchor.constraint(equalTo: parent.centerXAnchor).isActive = true
    $0.centerYAnchor.constraint(equalTo: parent.centerYAnchor).isActive = true
    }
}

public func centerXStyle<V: UIView>(_ parent: V) -> (V) -> Void {{
    $0.translatesAutoresizingMaskIntoConstraints = false
    $0.centerXAnchor.constraint(equalTo: parent.centerXAnchor).isActive = true
}}

public func bottomRightStyle<V: UIView>(
    _ parent: V,
    _ padding: UIEdgeInsets = .square(4)) -> (V) -> Void {{
        $0.bottomAnchor.constraint(
            equalTo: parent.bottomAnchor,
            constant: -padding.bottom).isActive = true
        $0.rightAnchor.constraint(
            equalTo: parent.rightAnchor,
            constant: -padding.right).isActive = true
    }}

public func bottomEdgeStyle<V: UIView>(
    _ parent: V,
    _ padding: UIEdgeInsets = .square(4)) -> (V) -> Void {{
        $0.bottomAnchor.constraint(
            equalTo: parent.bottomAnchor,
            constant: -padding.bottom).isActive = true
        $0.rightAnchor.constraint(
            equalTo: parent.rightAnchor,
            constant: -padding.right).isActive = true
        $0.leftAnchor.constraint(
            equalTo: parent.leftAnchor,
            constant: padding.left).isActive = true
    }}

public func bottomLeftStyle<V: UIView>(
    _ parent: V,
    _ padding: UIEdgeInsets = .square(4)) -> (V) -> Void {{
        $0.bottomAnchor.constraint(
            equalTo: parent.bottomAnchor,
            constant: -padding.bottom).isActive = true
        $0.rightAnchor.constraint(
            equalTo: parent.leftAnchor,
            constant: padding.left).isActive = true
    }}

public func topLeftStyle<V: UIView>(
    _ parent: V,
    _ inset: UIEdgeInsets = .square(4)) -> (V) -> Void {{
        $0.topAnchor.constraint(
            equalTo: parent.topAnchor,
            constant: inset.top).isActive = true
        $0.leftAnchor.constraint(
            equalTo: parent.leftAnchor,
            constant: inset.left).isActive = true
    }}

public func topRightStyle<V: UIView>(
    _ parent: V,
    _ inset: UIEdgeInsets = .square(4)) -> (V) -> Void {{
        $0.topAnchor.constraint(
            equalTo: parent.topAnchor,
            constant: inset.top).isActive = true
        $0.rightAnchor.constraint(
            equalTo: parent.rightAnchor,
            constant: -inset.right).isActive = true
    }}


public func centerYStyle<V: UIView>(_ parent: V) -> (V) -> Void {{
    $0.translatesAutoresizingMaskIntoConstraints = false
    $0.centerYAnchor.constraint(equalTo: parent.centerYAnchor).isActive = true
}}

extension UIView {
    
    public func pinBackground(
        _ color: UIColor = .white,
        cornerRadius: CGFloat = .zero,
        inset: UIEdgeInsets = .zero
    ) -> UIView {
        let view: UIView = UIView()            
        view.backgroundColor = color
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = cornerRadius
        view.insertSubview(self, at: 0)
        view.pin(to: self, inset: inset)
        return view
    }
    
    public func pinDashedBackground(
        _ color: UIColor = .white,
        cornerRadius: CGFloat = .zero,
        inset: UIEdgeInsets = .zero
    ) -> UIView {
        let view: UIView = DashedView()
        view.backgroundColor = color
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = cornerRadius
        view.insertSubview(self, at: 0)
        view.pin(to: self, inset: inset)
        return view
    }
    
    
    public func pin(to view: UIView, inset: UIEdgeInsets = .zero) {
        NSLayoutConstraint.activate([
            leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: inset.left),
            trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -inset.right),
            topAnchor.constraint(equalTo: view.topAnchor, constant: inset.top),
            bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -inset.bottom)
        ])
    }
    
}


extension UIView {
    public func constrainEdges(to view: UIView, priority: UILayoutPriority = .required, insets: UIEdgeInsets = .zero) {
        let constraints = [
            self.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: insets.left),
            self.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -insets.right),
            self.topAnchor.constraint(equalTo: view.topAnchor, constant: insets.top),
            self.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -insets.bottom)
        ]
        
        constraints.forEach { $0.priority = priority }
        NSLayoutConstraint.activate(constraints)
    }
}

extension UIView {
    public func constrainSafeAreaEdges(to view: UIView, priority: UILayoutPriority = .required, insets: UIEdgeInsets = .zero) {
        let constraints = [
            self.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: insets.left),
            self.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -insets.right),
            self.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: insets.top),
            self.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -insets.bottom)
        ]
        
        constraints.forEach { $0.priority = priority }
        NSLayoutConstraint.activate(constraints)
    }
}


extension UIView {
    public func constraintsBottonLayoutMargins(to view: UIView, priority: UILayoutPriority = .required, insets: UIEdgeInsets = .zero) {
        let constraints = [
            self.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: insets.left),
            self.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -insets.right),
            self.topAnchor.constraint(equalTo: view.topAnchor, constant: insets.top),
            self.bottomAnchor.constraint(equalTo: view.layoutMarginsGuide.bottomAnchor, constant: -insets.bottom)
        ]
        
        constraints.forEach { $0.priority = priority }
        NSLayoutConstraint.activate(constraints)
    }
}


extension CGFloat {
    public static func br_grid(_ value: Int) -> CGFloat {
        CGFloat(4 * value)
    }
}

public func viewBorderColor(
    _ color: UIColor = .clear,
    borderWidth width: CGFloat = 1,
    cornerRadius radius: CGFloat = .br_grid(2)
) -> (UIView)
  -> Void  {
    return {
        $0.layer.borderWidth = width
        $0.layer.borderColor = color.cgColor
        $0.layer.cornerRadius = radius
        $0.layer.masksToBounds = true
    }
}


/// UIEdge Additions
extension UIEdgeInsets {
    public static func square(_ a: CGFloat = .br_grid(4)) -> UIEdgeInsets {
        UIEdgeInsets(top: a, left: a, bottom: a, right: a)
    }
    public static let leading: (CGFloat) -> UIEdgeInsets = {
        UIEdgeInsets(top: .zero, left: $0, bottom: .zero, right: .zero)
    }
    public static let trailing: (CGFloat) -> UIEdgeInsets = {
        UIEdgeInsets(top: .zero, left: .zero, bottom: .zero, right: $0)
    }
    public static let topEdge: (CGFloat) -> UIEdgeInsets = {
        UIEdgeInsets(top: $0, left: .zero, bottom: .zero, right: .zero)
    }
    public static let bottomEdge: (CGFloat) -> UIEdgeInsets = {
        UIEdgeInsets(top: .zero, left: .zero, bottom: $0, right: .zero)
    }
    public static let topHorizontalEdge: (CGFloat) -> UIEdgeInsets = {
        UIEdgeInsets(top: $0, left: $0, bottom: .zero, right: $0)
    }
    public static let bottomHorizontalEdge: (CGFloat) -> UIEdgeInsets = {
        UIEdgeInsets(top: .zero, left: $0, bottom: $0, right: $0)
    }
    public static let horizontal: (CGFloat) -> UIEdgeInsets = {
        UIEdgeInsets(top: .zero, left: $0, bottom: .zero, right: $0)
    }
    public static let vertical: (CGFloat) -> UIEdgeInsets = {
        UIEdgeInsets(top: $0, left: .zero, bottom: $0, right: .zero)
    }
    public static func rectangle(v: CGFloat, h: CGFloat) -> UIEdgeInsets {
        UIEdgeInsets(top: v, left: h, bottom: v, right: h)
    }
}


public func hPadding(_ value: CGFloat)
-> (UITextField) -> Void  {
    return {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: value, height: $0.frame.height))
        $0.leftView = paddingView
        $0.rightView = paddingView
        $0.leftViewMode = .always
        $0.rightViewMode = .always
    }
}

public func leftPadding(_ value: CGFloat = .br_grid(4))
-> (UITextField) -> Void {
    return {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: value, height: $0.frame.height))
        $0.leftView = paddingView
        $0.leftViewMode = .always
    }
}

public func rightPadding(_ value: CGFloat = .br_grid(4))
-> (UITextField) -> Void {
    return {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: value, height: $0.frame.height))
        $0.rightView = paddingView
        $0.rightViewMode = .always
    }
}

public let shadowStyle: (UIView) -> Void = {
    $0.layer.masksToBounds = false
    $0.layer.shadowColor = UIColor.gray.cgColor
    $0.layer.shadowOpacity = 0.2
    $0.layer.shadowOffset =  CGSize(width: 2.0, height: 2.0)
    $0.layer.shadowRadius = 1
    $0.layer.cornerRadius = .br_grid(1)
    $0.layer.shadowPath = UIBezierPath(rect: $0.bounds).cgPath
}

public func addShadow(_ cornerRadius: CGFloat) -> (UIView) -> Void {
    return {
        let shadowLayer = UIView(frame: $0.frame)
        shadowLayer.backgroundColor = .red
        shadowLayer.setAsShadow(bounds: $0.bounds, cornerRadius: cornerRadius)
        $0.superview?.insertSubview(shadowLayer, belowSubview: $0)
    }
}

public let brShadowStyle: (UIView) -> Void = {
    $0.layer.masksToBounds = false
    $0.layer.shadowColor = UIColor.gray.cgColor
    $0.layer.shadowOpacity = 0.4
    $0.layer.shadowOffset =  CGSize(width: 1.0, height: 1.0)
    $0.layer.shadowRadius = .br_grid(1)
    $0.layer.cornerRadius = .br_grid(1)
    $0.layer.shouldRasterize = true
    $0.layer.rasterizationScale = UIScreen.main.scale
    $0.layer.shadowPath = UIBezierPath(rect: $0.bounds).cgPath
}
