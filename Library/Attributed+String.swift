import UIKit
public let html: (String) -> NSAttributedString? = { string in
    let data = Data(string.utf8)
    let attributedString = try? NSMutableAttributedString(
        data: data,
        options: [
            .documentType: NSAttributedString.DocumentType.html,
            .characterEncoding: String.Encoding.utf8.rawValue
        ],
        documentAttributes: nil
    )
    return attributedString
}
