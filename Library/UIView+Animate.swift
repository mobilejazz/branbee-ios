import UIKit
public func withAnimation(
    duration: TimeInterval = 0.4,
    options: UIView.AnimationOptions = .transitionCrossDissolve,
    _ animations: @escaping () -> Void)
{
    UIView.animate(
        withDuration: duration,
        delay: .zero,
        options: options,
        animations: animations,
        completion: nil
    )
}


// keyboard height
public let keyboardHeight: (NSNotification) -> CGFloat = {
    ($0.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as! NSValue)
        .cgRectValue
        .size
        .height
}
