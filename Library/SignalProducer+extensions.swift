import ReactiveSwift
extension SignalProducer {
    public static func future(
        _ attemptToFulfill: @escaping (@escaping (Result<Value, Error>) -> Void) -> Void
    ) -> SignalProducer {
        SignalProducer { observer, _ in
            attemptToFulfill { result in
                switch result {
                case let .success(value):
                    observer.send(value: value)
                    observer.sendCompleted()
                case let .failure(error):
                    observer.send(error: error)
                }
            }
        }
    }    
}
