import class Foundation.NSValue
import AVFoundation

public protocol AssetImageGeneratorType {
    
    init(asset: AVAsset)
    
    func generateCGImagesAsynchronously(
        forTimes requestedTimes: [NSValue],
        completionHandler handler: @escaping AVFoundation.AVAssetImageGeneratorCompletionHandler
    )
}

extension AVAssetImageGenerator: AssetImageGeneratorType {}

import ReactiveSwift
import UIKit
public let thumbnail: (URL) -> SignalProducer<UIImage?, Never> = { url in
    SignalProducer<UIImage?, Never>.future { promise in
        let avAsset = AVURLAsset(url: url, options: [:])
        let imageGenerator = AVAssetImageGenerator(asset: avAsset)
        imageGenerator.appliesPreferredTrackTransform = true
        var actualTime: CMTime = CMTime.zero
        
        if let image = cache[url.path] {
            promise(.success(image))
        }
        
        do {
            let imageRef = try imageGenerator.copyCGImage(
                at: CMTimeMake(value: 3, timescale: 1),
                actualTime: &actualTime
            )
            let image = UIImage(cgImage: imageRef)
            cache[url.path] = image
            promise(.success(image))            
        } catch {
            promise(.success(nil))
        }
               
    }
}

private var cache: [String: UIImage] = [:]


public func getThumbnailImageFromVideoUrl(url: URL, completion: @escaping ((_ image: UIImage?)-> Void)) {
    
    if let image = cache[url.path] {
        DispatchQueue.main.async {
            completion(image)
        }        
    }
    
    DispatchQueue.global().async {
        let asset = AVAsset(url: url)
        let avAssetImageGenerator = AVAssetImageGenerator(asset: asset)
        avAssetImageGenerator.appliesPreferredTrackTransform = true
        let thumnailTime = CMTimeMake(value: 1, timescale: 1)
        do {
            
            let cgThumbImage = try avAssetImageGenerator.copyCGImage(at: thumnailTime, actualTime: nil)
            let thumbImage = UIImage(cgImage: cgThumbImage).downSample(
                to: CGSize(
                    width: 300,
                    height: 200
                ),
                scale: 1.0
            )
            cache[url.path] = thumbImage
            DispatchQueue.main.async {
                completion(thumbImage)
            }
        } catch {
            print(error.localizedDescription)
            DispatchQueue.main.async {
                completion(nil)
            }
        }
    }
}
