import ReactiveSwift
import BrModels

@dynamicMemberLookup
public struct SystemEnvironment<Environment> {
    public var date: () -> Date
    public var locale: Locale
    public var environment: Environment
    public var mainQueue: () -> DateScheduler
    public var globalQueue: () -> DateScheduler
    
    public init(
        date: @escaping () -> Date = Date.init,
        locale: Locale = .current,
        environment: Environment,
        mainQueue: @escaping () -> DateScheduler = { QueueScheduler.main },
        globalQueue: @escaping () -> DateScheduler = {
            QueueScheduler(
                qos: .default,
                name: "com.branbree.ios",
                targeting: .none
            )
        }
    ) {
        self.date = date
        self.locale = locale
        self.environment = environment
        self.mainQueue = mainQueue
        self.globalQueue = globalQueue
    }
        
    public subscript<Dependency>(
        dynamicMember keyPath: WritableKeyPath<Environment, Dependency>
    ) -> Dependency {
        get { self.environment[keyPath: keyPath] }
        set { self.environment[keyPath: keyPath] = newValue }
    }
    /// Creates a live system environment with the wrapped environment provided.
    ///
    /// - Parameter environment: An environment to be wrapped in the system environment.
    /// - Returns: A new system environment.
    public static func live(environment: Environment) -> Self {
        Self(
            date: Date.init,
            locale: .current,
            environment: environment,
            mainQueue: { QueueScheduler.main },
            globalQueue: { QueueScheduler(
                qos: .default,
                name: "com.branbree.ios",
                targeting: .none
            ) }
        )
    }
    
    /// Transforms the underlying wrapped environment.
    public func map<NewEnvironment>(
        _ transform: @escaping (Environment) -> NewEnvironment
    ) -> SystemEnvironment<NewEnvironment> {
        .init(
            date: self.date,
            locale: self.locale,
            environment: transform(self.environment),
            mainQueue: self.mainQueue,
            globalQueue: self.globalQueue
        )
    }
}

public func dispatchAfter(time: TimeInterval, work: @escaping() -> Void) {
    DispatchQueue.main.asyncAfter(deadline: .now() + time) {
        work()
    }
}
