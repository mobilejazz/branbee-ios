import Foundation
public let brDateFormatter: () -> DateFormatter = {
    let formatter = DateFormatter()
    formatter.dateFormat = "dd MMMM YYYY"
    return formatter
}
