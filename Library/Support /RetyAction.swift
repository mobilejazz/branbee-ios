public struct RetryAction: Equatable {
    public static func == (lhs: RetryAction, rhs: RetryAction) -> Bool {
        return true
    }
    public let run: () -> Void
    public init(
        run: @escaping () -> Void
    ) {
        self.run = run
    }
}
