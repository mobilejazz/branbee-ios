import UIKit
extension UIViewController {
  public static var defaultNib: String {
    return self.description()
        .components(separatedBy: ".")
        .dropFirst()
        .joined(separator: ".")
  }

  public static var storyboardIdentifier: String {
    return self.description()
        .components(separatedBy: ".")
        .dropFirst()
        .joined(separator: ".")
  }
}


public enum Storyboard: String {
  case Login

  public func instantiate<VC: UIViewController>(_ viewController: VC.Type,
                                                inBundle bundle: Bundle = .shared) -> VC {
    guard let vc = UIStoryboard(name: self.rawValue, bundle: Bundle(identifier: bundle.identifier))
        .instantiateViewController(withIdentifier: VC.storyboardIdentifier) as? VC
    else { fatalError("Couldn't instantiate \(VC.storyboardIdentifier) from \(self.rawValue)") }
    return vc
  }
}

extension UIViewController {
    @objc
    public func dismissKeyboard(){
        view.endEditing(true)
    }
}

public let tapGestureDismissalStyle: (UIViewController) -> Void = {
    let tap: UITapGestureRecognizer = UITapGestureRecognizer(
               target: $0,
               action: #selector($0.dismissKeyboard))
    $0.view.addGestureRecognizer(tap)
}
