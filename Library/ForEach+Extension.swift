import ComposableArchitecture
extension Store where State: Equatable {
    public func forEach<LocalState: Equatable, LocalAction>(
        state: @escaping (State) -> [LocalState],
        action: @escaping ((Int, LocalAction)) -> Action
    ) -> [Store<LocalState, LocalAction>] {
        let scopedStore = scope(state: state)
        let viewStore = ViewStore(scopedStore)
        let localStores = zip(viewStore.indices, viewStore.state).map { index, element in
            scopedStore.scope(
                state: { index < $0.endIndex ? $0[index] : element },
                action: { action((index, $0)) }
            )
        }
        return localStores
    }

    public func forEach<LocalState: Equatable, LocalAction>(
        state: @escaping (State) -> [LocalState],
        action: @escaping (LocalAction) -> Action
    ) -> [Store<LocalState, LocalAction>] {
        let scopedStore = scope(state: state)
        let viewStore = ViewStore(scopedStore)
        let localStores = zip(viewStore.indices, viewStore.state).map { index, element in
            scopedStore.scope(
                state: { index < $0.endIndex ? $0[index] : element },
                action: { action($0) }
            )
        }
        return localStores
    }
}
