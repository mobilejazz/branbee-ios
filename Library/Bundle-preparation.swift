public class Root {}
extension Bundle {
    public static let shared: Bundle = {
        Bundle(for: Root.self)
    }()
}


extension Bundle {
    public var identifier: String {
        return self.infoDictionary?["CFBundleIdentifier"] as? String ?? "Unknown"
    }
}


public struct Single: Equatable {
    public init() {}
}
