import UIKit
import Library
import NewStory

internal let progressTitleLabelStyle: (UILabel) -> Void = {
    $0.font = .py_subhead(size: 14)
    $0.textAlignment = .center
    $0.textColor = .blueSecondColor
}

private let progressStackViewStyle: (UIStackView) -> Void =
    autoLayoutStyle
    <> verticalStackStyle(.br_grid(2),
                    distribution: .fillProportionally,
                    alignment: .fill)


internal let contentViewStyle: (UIView) -> Void = autoLayoutStyle <> {
    $0.backgroundColor = .white
}
class BranbeeRefreshControl: UIRefreshControl {
            
    var progress: CGFloat? = .none {
        didSet {
            guard let progress = progress else { return }
            progress == 1
                ? endRefreshing()
                : beginRefreshing()
        }
    }
    
    override init() {
        super.init(frame: .zero)
        
        self.tintColor = .clear
        
        let label = UILabel()
        label.text = "Publishing your story..."
        progressTitleLabelStyle(label)
        
        let progressView = UIProgressView()
        progressView.progress = 0.1
        brProgressViewStyle(progressView)
        
        let progressStackView = UIStackView(arrangedSubviews: [
            label, progressView
        ])
        progressStackViewStyle(progressStackView)
        
        let contentView = UIView()
        contentViewStyle(contentView)
        self.addSubview(contentView)
        contentView.constrainEdges(
            to: self
        )
        contentView.addSubview(progressStackView)
        progressStackView.constrainEdges(
            to: contentView,
            insets: .rectangle(v: .br_grid(2), h: .br_grid(4))
        )
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
 
}
