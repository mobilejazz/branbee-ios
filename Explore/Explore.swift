import UIKit
import Library
import ExploreClient
import ComposableArchitecture
import ReactiveSwift
import BrModels
import ImageLoader
import NewStory
import SDWebImage

public struct ExploreState: Equatable {
    public var items: [BrFeedItem] = []
    public var didReachTheEnd: Bool = false
    public var project: BrProject?
    public var comments: CommentsState?
    public var search: SearchState?
    public var notifications: NotificationsState?
    public var storyDetails: StoryDetailState?
    var sdetails: StoryDetailStateCopy? {
        get {
            storyDetails.map(StoryDetailStateCopy.init)
        }
        set {
            storyDetails = newValue?.value
        }
    }
    public var projectDetails: ProjectDetailState?
    var details: ProjectDetailStateCopy? {
        get {
            projectDetails.map(ProjectDetailStateCopy.init)
        }
        set {
            projectDetails = newValue?.value
        }
    }
    public var loadingState: LoadingState? = nil
    public var isPublishingNewStory: Bool = false
    public var donateState: DonateState?
    public var newStory: NewStoryState?
    public var alert: _AlertState<ExploreAction>?
    public var isLoadingMoreRequestInFlight: Bool = false
    
    public init(
        items: [BrFeedItem] = [],
        project: BrProject? = nil,
        comments: CommentsState? = nil,
        searchState: SearchState? = nil,
        details: StoryDetailState? = nil,
        projectDetails: ProjectDetailState? = nil,
        notifications: NotificationsState? = nil,
        isLoading: Bool = false,
        donateState: DonateState? = nil,
        newStory: NewStoryState? = nil
    ) {
        self.items = items
        self.project = project
        self.comments = comments
        self.search = searchState
        self.storyDetails = details
        self.projectDetails = projectDetails
        self.notifications = notifications
        self.donateState = donateState
        self.newStory = newStory
    }
    
}

extension ExploreState {
    var stories: [BrStory] {
        get {
            self.items.stories
        }
        set {}
    }
    var projects: [BrProject] {
        get {
            self.items.projects
        }
        set {}
    }
}

public enum ExploreAction: Equatable {
    case viewDidLoad
    case viewDidAppear
    case viewDismissed
    case destroy
    case destroyAll
    case searchButtonTapped
    case newStoryButtonTapped
    case notificationButtonTapped
    case loadMore
    case showStoryDetails(BrStory)
    case showProjectDetails(BrProject)
    case event(ExploreDelegateEvent)
    case comments(CommentsAction)
    case search(SearchAction)
    case donate(DonateAction)
    case newStory(NewStoryAction)
    case notifications(NotificationsAction)
    case storyDetails(StoryDetailAction)
    case projectDetails(ProjectDetailAction)
    case exploreCell(index: Int, action: StoryCellAction)
    case projectCell(index: Int, action: ProjectCellAction)
}

public struct ExploreEnvironment {
    var exploreClient: ExploreClient
    var stripeClient: StripeClient
    var commentsClient: (String, BrComment.´Type´) -> CommentsClient
    var storyDetailsClient: (String) -> StoryDetailClient
    var projectDetailsClient: (String) -> ProjectDetailsClient
    var linkProjectClient: (String, String) -> LinkProjectClient
    var linkStoryClient: (String, String) -> LinkStoryClient
    var donateClient: (_ projectId: String, _ projectName: String) -> DonateClient
    let addCardClient: AddCardClient
    var searchClient: SearchClient
    var selectCommunityClient: SelectCommunityClient
    var notificationsClient: NotificationsClient
    var newStoryClient: (_ draftId: String?) -> NewStoryClient
    var imageLoader: ImageLoader
    var communityDetailClient: (_ communityId: String) -> CommunityDetailClient
    
    public init(
        client: ExploreClient,
        stripeClient: StripeClient,
        commentsClient: @escaping (String, BrComment.´Type´) -> CommentsClient = {_,_ in .empty },
        storyDetailsClient: @escaping (String) -> StoryDetailClient = { _ in .empty },
        projectDetailsClient: @escaping (String) -> ProjectDetailsClient = { _ in .empty },
        linkProjectClient: @escaping (String, String) -> LinkProjectClient = {_,_ in .empty },
        linkStoryClient: @escaping (String, String) -> LinkStoryClient = {_,_ in .empty },
        searchClient: SearchClient = .empty,
        donateClient: @escaping (_ projectId: String, _ projectName: String) -> DonateClient = {_,_ in .empty },
        newStoryClient: @escaping (_ draftId: String?) -> NewStoryClient = { _ in .empty },
        addCardClient: AddCardClient = .empty,
        notificationsClient: NotificationsClient = .empty,
        selectCommunity: SelectCommunityClient = .empty,
        communityDetailClient: @escaping (_ communityId: String) -> CommunityDetailClient = { _ in .empty },
        imageLoader: ImageLoader
    ) {
        self.stripeClient = stripeClient
        self.exploreClient = client
        self.searchClient = searchClient
        self.commentsClient = commentsClient
        self.storyDetailsClient = storyDetailsClient
        self.projectDetailsClient = projectDetailsClient
        self.linkStoryClient = linkStoryClient
        self.imageLoader = imageLoader
        self.notificationsClient = notificationsClient
        self.linkProjectClient = linkProjectClient
        self.addCardClient = addCardClient
        self.donateClient = donateClient
        self.newStoryClient = newStoryClient
        self.selectCommunityClient = selectCommunity
        self.communityDetailClient = communityDetailClient
    }
}


public let exploreReducer: Reducer<ExploreState, ExploreAction, SystemEnvironment<ExploreEnvironment>> =
    .combine(
        Reducer { state, action, environment in
            struct CancelDelegateId: Hashable {}
            switch action {
            case .viewDidLoad:
                return .merge(
                    environment.exploreClient
                        .delegate
                        .cancellable(id: CancelDelegateId())
                        .observe(on: environment.mainQueue())
                        .map(ExploreAction.event),
                    environment.exploreClient
                        .onViewLoaded
                        .fireAndForget()
                )
            case .viewDidAppear:
                return .none
//                environment.exploreClient
//                    .onViewRefresh
//                    .observe(on: environment.mainQueue())
//                    .fireAndForget()
            case .viewDismissed:
                state.comments = nil
                state.search = nil
                state.notifications = nil
                state.storyDetails = nil
                state.projectDetails = nil
                state.donateState = nil
                state.newStory = nil
                state.alert = nil
                state.loadingState = nil
                return .none
            case .event(.createStory):
                return .none
            case let .event(.displayError(error)):
                state.alert = .init(
                    title: "Alert",
                    message: error.message,
                    dismissButton: .cancel(send: .viewDismissed)
                )
                return .none
            case let .event(.displayFeedPageError(error, action: action)):
                state.loadingState = .failure(
                    error,
                    action: action
                )
                return .none
            case .event(.hideLoading):
                state.loadingState = nil
                return .none
            case .event(.showLoading):
                state.loadingState = .loading
                return .none
            case let .event(.showProject(project)):
                state.project = project
                return .none
            case let .event(.showProjectComments(project)):
                return .none
            case let .event(.items(items, reachedTheEnd: didReachEnd)):
                state.items = items
                state.didReachTheEnd = didReachEnd
                state.isLoadingMoreRequestInFlight = false
                return .none
            case let .exploreCell(index: index, action: .commentsButtonTapped) where state.items[index].story != nil:
                let item = state.items[index]
                state.comments = CommentsState(
                    parentId: item.story!.id,
                    parentType: .story
                )
                return .none
            case let .projectCell(index: index, action: .commentsButtonTapped) where state.items[index].project != nil:
                let item = state.items[index]
                state.comments = CommentsState(
                    parentId: item.project!.id,
                    parentType: .project
                )
                return .none
            case let .projectCell(index: index, action: .donateButtonTapped) where state.items[index].project != nil:
                let item = state.items[index]
                state.donateState = DonateState(
                    project: item.project!
                )
                return .none
            case .exploreCell, .projectCell:
                return .none
            case .search, .notifications, .comments, .storyDetails, .projectDetails:
                return .none
            case .searchButtonTapped:
                state.search = SearchState()
                return .none
            case .notificationButtonTapped:
                state.notifications = NotificationsState()
                return .none
            case let .showStoryDetails(story):
                state.storyDetails = StoryDetailState(story: story)
                return .none
            case let .showProjectDetails(project):
                state.projectDetails = ProjectDetailState(project: project)
                return .none
            case .newStoryButtonTapped:
                state.newStory = NewStoryState(
                    communityName: "",
                    communityId: "",
                    selectCommunityState: SelectCommunityState()
                )
                return .none
            case .destroy:
                return .cancel(id: CancelDelegateId())
            case .loadMore:
                guard !state.didReachTheEnd, !state.isLoadingMoreRequestInFlight else { return .none }
                state.isLoadingMoreRequestInFlight = true
                return  environment
                    .exploreClient
                    .onEventLoadMore                    
                    .fireAndForget()
            case .newStory(.viewDidDisppear):
                state.newStory = nil
                return .none
            case .donate, .newStory, .event:
                return .none
            case .destroyAll:
                return .concatenate(
                    state.search != nil
                        ? Effect(value: .search(.destroy))
                        : .none
                    ,
                    Effect(value: .viewDismissed)
                )
            }
        },
        newStoryReducer.optional().pullback(
            state: \.newStory,
            action: /ExploreAction.newStory,
            environment: {
                $0.map {
                    NewStoryEnvironment(
                        newStory: $0.newStoryClient,
                        selectCommunity: $0.selectCommunityClient,
                        imageLoader: $0.imageLoader
                    )
                }
            }
        ),
        storyCellReducer.forEach(
            state: \.stories,
            action: /ExploreAction.exploreCell(index:action:),
            environment: { $0.map(\.imageLoader) }
        ),
        projectCellReducer.forEach(
            state: \.projects,
            action: /ExploreAction.projectCell(index:action:),
            environment: { $0.map(\.imageLoader) }
        ),
        searchReducer.optional().pullback(
            state: \.search,
            action: /ExploreAction.search,
            environment: { $0 }            
        ),
        storyDetailReducer.optional().pullback(
            state: \.sdetails,
            action: /ExploreAction.storyDetails,
            environment: {
                $0.map {
                    StoryDetailEnvironment(
                        storyDetailClient: $0.storyDetailsClient,
                        linkProjectClient: $0.linkProjectClient,
                        commentsClient: $0.commentsClient,
                        projectDetailClient: $0.projectDetailsClient,
                        donateClient: $0.donateClient,
                        linkStoryClient: $0.linkStoryClient,
                        addCardClient: $0.addCardClient,
                        imageLoader: $0.imageLoader,
                        stripeClient: $0.stripeClient
                    )
                }
            }
        ),
        projectDetailReducer.optional().pullback(
            state: \.details,
            action: /ExploreAction.projectDetails,
            environment: { $0.map {
                ProjectDetailEnvironment(
                    projectDetailClient: $0.projectDetailsClient,
                    linkStoryClient: $0.linkStoryClient,
                    donateClient: $0.donateClient,
                    addCardClient: $0.addCardClient,
                    imageLoader: $0.imageLoader,
                    storyDetailClient: $0.storyDetailsClient,
                    linkProjectClient: $0.linkProjectClient,
                    commentsClient: $0.commentsClient,
                    stripeClient: $0.stripeClient
                )
            }
            }
        ),
        commentsReducer.optional().pullback(
            state: \.comments,
            action: /ExploreAction.comments,
            environment: {
                $0.map { 
                    CommentsEnvironment(
                        commentsClient: $0.commentsClient,
                        imageLoader: $0.imageLoader
                    )
                }
            }
        ),
        notificationsReducer.optional().pullback(
            state: \.notifications,
            action: /ExploreAction.notifications,
            environment: { $0.map {
                NotificationsEnvironment(
                    notificationsClient: $0.notificationsClient,
                    imageLoader: $0.imageLoader                    
                )
            }}
        ),
        donationReducer.optional().pullback(
            state: \.donateState,
            action: /ExploreAction.donate,
            environment: {
                $0.map {
                    DonateEnvironment(
                        donateClient: $0.donateClient,
                        addCardClient: $0.addCardClient,
                        stripeClient: $0.stripeClient
                    )
                }
            })
    )



let notificationButtonStyle: (UIButton) -> Void = {
    $0.setImage(UIImage(named: "ic_notifications"))
    $0.tintColor = .white
}

let createStoryButtonStyle: (UIButton) -> Void = {
    $0.setImage(UIImage(named: "ic_add"))
    $0.tintColor = .white
}

import ComposableArchitecture


@available(iOS 13.0, *)
var dataSource: UICollectionViewDiffableDataSource<ExploreViewController.Section, BrFeedItem>!

public class ExploreViewController: UIViewController {
    
    enum Section {
        case main
    }
    
    let store: Store<ExploreState, ExploreAction>
    let viewStore: ViewStore<ExploreState, ExploreAction>
                
    public init(store: Store<ExploreState, ExploreAction>) {
        self.store = store
        self.viewStore = ViewStore(store)
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var collectionView: UICollectionView!
    var datasource: [BrFeedItem] = [] {
        didSet {
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        }
    }
            
    public override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        //viewStore.send(.destroy)
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Explore"
                
        self.viewStore.send(.viewDidLoad)
        
//        if #available(iOS 13, *) {
//            collectionView = UICollectionView(
//                frame: .zero,
//                collectionViewLayout: createLayout()
//            )
//            configureDataSource()
//        } else {
            let layout = UICollectionViewFlowLayout()
            collectionFlowLayoutStyle(layout)
            collectionView = UICollectionView(
                frame: .zero,
                collectionViewLayout: layout
            )
            collectionView.dataSource = self
        
                                
        
        collectionView.delegate = self
        //collectionView.prefetchDataSource = self
        collectionViewStyle(collectionView)
        self.view.addSubview(collectionView)
        collectionView.constrainEdges(to: view)
                        
        let loadingView = LoadingView()
        self.view.addSubview(loadingView)
        loadingView.frame = view.frame
        self.view.bringSubviewToFront(loadingView)
        
        viewStore.produced.loadingState
            .startWithValues { state in
                loadingView.state = state
                loadingView.isHidden = state == nil
            }
        
        viewStore.produced.isPublishingNewStory
            .startWithValues { isPublishing in
                if isPublishing {
                    self.collectionView.refreshControl = BranbeeRefreshControl()
                    self.collectionView.refreshControl?.beginRefreshing()
                } else {
                    self.collectionView.refreshControl?.endRefreshing()
                    self.collectionView.refreshControl = nil
                }
            }
                                                        
        viewStore.produced.items
            .assign(to: \.datasource, on: self)            
        
        // Comments
        store.scope(
            state: \.comments,
            action: ExploreAction.comments
        ).ifLet(then: { [weak self] store in
            guard let self = self else { return }
            self.navigationController?.pushViewController(CommentsViewController(store: store), animated: true)
        }) {
            self.navigationController?.popToViewController(self, animated: true)
        }
        
        // Search
        store.scope(
            state: \.search,
            action: ExploreAction.search
        ).ifLet(then: { [weak self] store in
            guard let self = self else { return }
            self.navigationController?.pushViewController(
                SearchViewController(store: store), animated: true)
        }) {
            self.navigationController?.popToViewController(self, animated: true)
        }
        
        // Notifications
        store.scope(
            state: \.notifications,
            action: ExploreAction.notifications
        ).ifLet(then: { [weak self] store in
            guard let self = self else { return }
            self.navigationController?.pushViewController(
                NotificationsViewController(store: store), animated: true)
        }) {
            self.navigationController?.popToViewController(self, animated: true)
        }
        
        // Story Details
        store.scope(
            state: \.storyDetails,
            action: ExploreAction.storyDetails
        ).ifLet(then: { [weak self] store in
            guard let self = self else { return }
            self.navigationController?.pushViewController(
                StoryDetailViewController(store: store), animated: false)
        }) {
            self.navigationController?.popToViewController(self, animated: true)
        }
        
        // Project Details
        store.scope(
            state: \.projectDetails,
            action: ExploreAction.projectDetails
        ).ifLet(then: { [weak self] store in
            guard let self = self else { return }
            self.navigationController?.pushViewController(
                ProjectDetailViewController(store: store), animated: true)
        }) {
            self.navigationController?.popToViewController(self, animated: true)
        }
        
        // New Story
        store.scope(
            state: \.newStory,
            action: ExploreAction.newStory
        ).ifLet(then: { [weak self] store in
            guard let self = self else { return }
            let vc = NewStoryViewController(store: store)
            vc.dismissAction = {
                self.viewStore.send(.viewDismissed)
            }
            self.navigationController?.present(
                UINavigationController(
                    rootViewController: vc),
                animated: true,
                completion: nil
            ) 
        }) {
            self.navigationController?
                .presentedViewController?
                .dismiss(animated: true, completion: nil)
        }
        
        // Donate
        store.scope(
            state: \.donateState,
            action: ExploreAction.donate
        ).ifLet(then: { [weak self] store in
            guard let self = self else { return }
            self.navigationController?.pushViewController(
                DonateViewController(store: store), animated: true)
        }) {
            self.navigationController?.popToViewController(self, animated: true)
        }
                
        viewStore.produced.alert
            .startWithValues { [weak self] (alert) in
                guard let self = self else { return }
                guard let alert = alert else { return }
                
                let actionController = UIAlertController(
                    title: alert.title,
                    message: alert.message,
                    preferredStyle: .alert
                )
                alert.primaryButton
                    .map { $0.toUIKit(send: self.viewStore.send) }
                    .map(actionController.addAction)
                
                alert.secondaryButton
                    .map { $0.toUIKit(send: self.viewStore.send) }
                    .map(actionController.addAction)
                
                self.present(actionController,
                             animated: true,
                             completion: nil)
        }
        
                            
        let searchButton = UIButton()
        searchButtonStyle(searchButton)
        searchButton.addTarget(self, action: #selector(searchButtonTapped), for: .touchUpInside)
        
        let createStoryButton = UIButton()
        createStoryButtonStyle(createStoryButton)
        createStoryButton.addTarget(self, action: #selector(newStoryButtonTapped), for: .touchUpInside)
                
        let notificationButton = UIButton()
        notificationButtonStyle(notificationButton)
        notificationButton.addTarget(self, action: #selector(notificationButtonTapped), for: .touchUpInside)
        
        self.navigationItem.rightBarButtonItems = [
            searchButton,
            notificationButton,
            createStoryButton].map(UIBarButtonItem.init(customView:))
    }
    
    @objc func notificationButtonTapped() {
        viewStore.send(.notificationButtonTapped)
    }
    
    @objc func searchButtonTapped() {
        DispatchQueue.main.async {
            self.viewStore.send(.searchButtonTapped)
        }        
    }
    
    @objc func newStoryButtonTapped() {
        viewStore.send(.newStoryButtonTapped)
    }
    
    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewStore.send(.viewDidAppear)
        if !isMovingToParent {
            viewStore.send(.destroyAll)
        }        
    }
}


private let collectionViewStyle: (UICollectionView) -> Void =
    autoLayoutStyle <> {
        $0.register(StoryViewCell.self,
                    forCellWithReuseIdentifier: StoryViewCell.reuseIdentifier)
        $0.register(ProjectViewCell.self,
                    forCellWithReuseIdentifier: ProjectViewCell.reuseIdentifier)
        $0.backgroundColor = .white
        $0.contentInset = .bottomEdge(.br_grid(2))
    }

private let collectionFlowLayoutStyle: (UICollectionViewFlowLayout) -> Void = {
    $0.scrollDirection = .vertical
    $0.sectionHeadersPinToVisibleBounds = true
    $0.minimumLineSpacing = .br_grid(4)
    $0.minimumInteritemSpacing = .br_grid(4)    
    $0.sectionInsetReference = .fromContentInset
    $0.sectionInset = .topHorizontalEdge(.br_grid(4))
}

extension ExploreViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        datasource.count
    }
    
    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    public func collectionView(
        _ collectionView: UICollectionView,
        didSelectItemAt indexPath: IndexPath) {
        let item = datasource[indexPath.row]
        switch item.type {
        case .story:
            viewStore.send(.showStoryDetails(item.story!))
        case .project:
            viewStore.send(.showProjectDetails(item.project!))
        case .community:
            fatalError()
        }
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let item = datasource[safe: indexPath.item] else { guard let exploreCell = collectionView.dequeueReusableCell(
                withReuseIdentifier: StoryViewCell.reuseIdentifier,
                for: indexPath
            ) as? StoryViewCell else {
                fatalError()
            }
            return exploreCell
        }
        
        switch item.type {
        case .story:
            guard let exploreCell = collectionView.dequeueReusableCell(
                withReuseIdentifier: StoryViewCell.reuseIdentifier,
                for: indexPath
            ) as? StoryViewCell else {
                fatalError()
            }
            exploreCell.configure(store.scope(
                state:  {
                    $0.items[safe: indexPath.item]?.story ?? BrStory.branbeeStory },
                action: { .exploreCell(
                    index: indexPath.item,
                    action: $0
                )}
            ))
            
            return exploreCell
        case .project:
            guard let projectCell = collectionView.dequeueReusableCell(
                withReuseIdentifier: ProjectViewCell.reuseIdentifier,
                for: indexPath
            ) as? ProjectViewCell else {
                fatalError()
            }
            projectCell.configure(store.scope(
                state:  { $0.items[safe: indexPath.item]?.project ?? BrProject.branbee },
                action: { .projectCell(index: indexPath.item, action: $0) }
            ))
            return projectCell
        default:
            return .init()
        }
    }
}


extension ExploreViewController: UICollectionViewDelegateFlowLayout {

    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellWidth = collectionView.bounds.width - .br_grid(8)
        guard let item = datasource[safe: indexPath.item] else { return .zero }
        switch item.type {
        case .story:
            return CGSize(
                width: cellWidth,
                height:
                    StorySectionType
                    .story(item.story!)
                    .cellHeight(cellWidth)
            )
        default:
            return CGSize(
                width: cellWidth,
                height: .br_grid(55)
            )
        }
    }

}

extension ExploreViewController {
    
    public func collectionView(
        _ collectionView: UICollectionView,
        willDisplay cell: UICollectionViewCell,
        forItemAt indexPath: IndexPath) {
        if indexPath.item == datasource.count - 1 {
            self.viewStore.send(.loadMore)
        }
    }
            
    public func collectionView(
        _ collectionView: UICollectionView,
        didEndDisplaying cell: UICollectionViewCell,
        forItemAt indexPath: IndexPath) {
    }
    
}




@available(iOS 13.0, *)
extension ExploreViewController {
    func configureHierarchy() {
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: createLayout())
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(collectionView)
        collectionView.backgroundColor = .systemBackground
        NSLayoutConstraint.activate([
            collectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            collectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            collectionView.topAnchor.constraint(equalTo: view.topAnchor)
        ])
    }
    
    
    func configureDataSource() {
                
        dataSource = UICollectionViewDiffableDataSource
            <Section, BrFeedItem>(collectionView: collectionView) { [weak self]
            (collectionView: UICollectionView, indexPath: IndexPath, feed: BrFeedItem) in
            guard let self = self else { fatalError() }
            switch feed.type {
            case .story:
                guard let storyCell = collectionView.dequeueReusableCell(
                    withReuseIdentifier: StoryViewCell.reuseIdentifier,
                    for: indexPath
                ) as? StoryViewCell else {
                    fatalError()
                }
                
                storyCell.configure(
                    self.store.scope(
                        state:  {
                            $0.items[safe: indexPath.item]?.story ?? BrStory.branbeeStory },
                        action: { .exploreCell(index: indexPath.item, action: $0)
                        }
                    )
                )
                return storyCell
                
            case .project:
                guard let projectCell = collectionView.dequeueReusableCell(
                    withReuseIdentifier: ProjectViewCell.reuseIdentifier,
                    for: indexPath
                ) as? ProjectViewCell else {
                    fatalError()
                }
                projectCell.configure(self.store.scope(
                    state:  { $0.items[safe: indexPath.item]?.project ?? .branbee },
                    action: { .projectCell(index: indexPath.item, action: $0) }
                ))
                return projectCell
            default:
                return .init()
            }
        }
        
        var snapshot = NSDiffableDataSourceSnapshot<Section, BrFeedItem>()
        snapshot.appendSections([.main])
        snapshot.appendItems(datasource)
        dataSource.apply(snapshot, animatingDifferences: false)
    }
    
//    func createLayout() -> UICollectionViewLayout {
//
//        let estimatedHeight = CGFloat(100)
//        let layoutSize = NSCollectionLayoutSize(
//            widthDimension: .fractionalWidth(1.0),
//            heightDimension: .estimated(estimatedHeight)
//        )
//        let item = NSCollectionLayoutItem(layoutSize: layoutSize)
//        let group = NSCollectionLayoutGroup.horizontal(
//            layoutSize: layoutSize,
//            subitem: item,
//            count: 1
//        )
//        let section = NSCollectionLayoutSection(group: group)
//        section.contentInsets = NSDirectionalEdgeInsets(
//            top: .br_grid(2),
//            leading: .br_grid(2),
//            bottom: .br_grid(2),
//            trailing: .br_grid(2)
//        )
//        section.interGroupSpacing = .br_grid(2)
//        let layout = UICollectionViewCompositionalLayout(section: section)
//        return layout
//    }
}


@available(iOS 13.0, *)
extension ExploreViewController {
    func createLayout() -> UICollectionViewLayout {
        let itemSize = NSCollectionLayoutSize(
            widthDimension: .fractionalWidth(1.0),
            heightDimension: .estimated(100)
        )
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        let groupSize = NSCollectionLayoutSize(
            widthDimension: .fractionalWidth(1.0),
            heightDimension: .estimated(100)
        )
        let group = NSCollectionLayoutGroup.horizontal(
            layoutSize: groupSize,
            subitem: item,
            count: 1
        )
        
        let spacing = CGFloat.br_grid(3)
        group.interItemSpacing = .fixed(spacing)

        let section = NSCollectionLayoutSection(group: group)
        section.interGroupSpacing = spacing
        section.contentInsets = NSDirectionalEdgeInsets(
            top: .br_grid(2),
            leading: .br_grid(2),
            bottom: .br_grid(2),
            trailing: .br_grid(2)
        )

        let layout = UICollectionViewCompositionalLayout(section: section)
        return layout
    }
}

//extension ExploreViewController: UICollectionViewDataSourcePrefetching {
//
//    public func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {
//        // prefetch items
//    }
//
//    public func collectionView(_ collectionView: UICollectionView, cancelPrefetchingForItemsAt indexPaths: [IndexPath]) {
//        // cancel prefetching
//    }
//
//}

