import UIKit
import Library
import ExploreClient
import ComposableArchitecture
import ReactiveSwift
import BrModels
import ImageLoader
import NewStory

extension Array where Element == BrFeedItem {
    public var stories: [BrStory] {
        self.compactMap(\.story)
    }
    public var projects: [BrProject] {
        self.compactMap(\.project)
    }
    public var communities: [BrCommunity] {
        self.compactMap(\.community)
    }
}

extension CommunityDetailState {
    var stories: [BrStory] {
        get {
            feed.stories
        }
        set {}
    }
    var projects: [BrProject] {
        get {
            feed.projects
        }
        set {}
    }
}

public struct CommunityDetailState: Equatable {
    public var community: BrCommunity
    public var feed: [BrFeedItem] = []
    public var alert: _AlertState<CommunityDetailAction>?
    public var isLoading: Bool = false
    public var focusedIndexPaths: [IndexPath]
    public var isJoined: Bool
    public var isSubscribed: Bool = false
    public var loadingState: LoadingState? = nil
    public var newStory: NewStoryState? = nil
    public var comments: CommentsState? = nil
    public var storyDetails: StoryDetailState? = nil
    
    var detail: StoryDetailStateCopy? {
        get {
            storyDetails.map(StoryDetailStateCopy.init)
        }
        set {
            storyDetails = newValue?.value
        }
    }
            
    public var projectDetails: ProjectDetailState? = nil
    
    var details: ProjectDetailStateCopy? {
        get {
            projectDetails.map(ProjectDetailStateCopy.init)
        }
        set {
            projectDetails = newValue?.value
        }
    }
        
    public var isRequestedMessageShown: Bool? = nil
    public var donateState: DonateState?
    
    public init(
        community: BrCommunity,
        feedItems: [BrFeedItem] = [],
        alert: _AlertState<CommunityDetailAction>? = nil,
        isLoading: Bool = false,
        focusedIndexPaths: [IndexPath] = [],
        isJoined: Bool = false
    ) {
        self.feed = feedItems
        self.alert = alert
        self.isLoading = isLoading
        self.focusedIndexPaths = focusedIndexPaths
        self.isJoined = isJoined
        self.community = community
    }
    
    
}

public enum CommunityDetailAction: Equatable {
    case viewDidLoad
    case viewDidAppear
    case viewDismissed
    case newStoryButtonTapped
    case viewDidDisappear
    case joinButtonTapped
    case menuButtonTapped
    case leaveCommunity
    case retryAction(RetryAction)
    case dismissRequestAlert
    case event(CommunityDetailDelegateEvent)
    case exploreCell(index: Int, action: StoryCellAction)
    case projectCell(index: Int, action: ProjectCellAction)
    case viewStoryDetail(BrStory)
    case viewProjectDetail(BrProject)
    case newStory(NewStoryAction)
    case comments(CommentsAction)
    case donate(DonateAction)
    case storyDetails(StoryDetailAction)
    case projectDetails(ProjectDetailAction)
}

public let communityDetailReducer: Reducer<CommunityDetailState, CommunityDetailAction, SystemEnvironment<ExploreEnvironment>> =
    .combine(
        Reducer { state, action, environment in
            struct CancelDelegateId: Hashable {}
            switch action {
            case .viewDidLoad:
                state.isSubscribed = true
                return .merge(
                    environment.communityDetailClient(state.community.id)
                        .delegate
                        .cancellable(id: CancelDelegateId())
                        .map(CommunityDetailAction.event),
                    environment.communityDetailClient(state.community.id)
                        .viewDidLoad
                        .fireAndForget()
                )
            case .viewDidAppear:
                guard !state.isSubscribed else { return .none }
                return .merge(
                    environment
                        .communityDetailClient(state.community.id)
                        .delegate
                        .cancellable(id: CancelDelegateId())
                        .map(CommunityDetailAction.event),
                    environment
                        .communityDetailClient(state.community.id)
                        .refreshView
                        .fireAndForget()
                )
            case .viewDismissed:
                state.alert = nil
                state.newStory = nil
                state.comments = nil
                state.storyDetails = nil
                state.projectDetails = nil
                state.donateState = nil
                state.newStory = nil
                state.loadingState = nil
                return .none
            case let .exploreCell(index: index, action: .readMoreTapped):
                state.focusedIndexPaths.append(
                    IndexPath(item: 0, section: index)
                )
//                for indice in state.feed.indices {
//                    state.feed[indice].isFocused = index == indice
//                }
                return .none
            case let .event(.showCommunity(community)):
                state.community = community
                return .none
            case .event(.showContent):
                state.loadingState = nil
                return .none
            case let .event(.showItems(feedItem, reachedEnd: reachedEnd)):
                state.feed = feedItem
                return .none
            case let .event(.displayFailure(failure, retryAction: action)):
                state.loadingState = .failure(
                    failure,
                    action: action
                )
                return .none
            case .event(.showJoinButton):
                state.isJoined = false
                return .none
            case .event(.showRequestSentMessage):
                state.isRequestedMessageShown = true
                return Effect(value: .dismissRequestAlert)
                    .delay(1.0, on: environment.mainQueue())
            case .event(.showJoinedButtons):
                state.isJoined = true
                return .none
            case .event(.showJoinedToCommunityMessage):
                state.isRequestedMessageShown = true
                return Effect(value: .dismissRequestAlert)
                    .delay(1.0, on: environment.mainQueue())
            case .event(.showLeavedCommunityMessage):
                state.isRequestedMessageShown = true
                return Effect(value: .dismissRequestAlert)
                    .delay(1.0, on: environment.mainQueue())
            case let .event(.displayTransientFailure(failure)):
                state.alert = _AlertState(
                    title: "Ooops!",
                    message: failure.message,
                    dismissButton: .cancel(send: .viewDismissed)
                )
                return .none
            case let .retryAction(action):
                return .fireAndForget(action.run)
            case .joinButtonTapped:
                return environment.communityDetailClient(state.community.id)
                    .join
                    .fireAndForget()
            case .leaveCommunity:
                return .concatenate(
                    environment
                        .communityDetailClient(state.community.id)
                        .leave
                        .fireAndForget(),
                    Effect(value: .viewDismissed)
                )
            case .menuButtonTapped:
                state.alert = _AlertState(
                    title: "Leave Community",
                    primaryButton: .destructive("Leave",
                                                send: .leaveCommunity
                    ),
                    secondaryButton: .cancel(send: .viewDismissed)
                )
                return .none
            case .viewDidDisappear:
                state.isSubscribed = false
                return .cancel(id: CancelDelegateId())
            case .newStoryButtonTapped:
                state.newStory = NewStoryState(
                    communityName: state.community.name,
                    communityId: state.community.id
                )
                return .none
            case let .exploreCell(index: index, action: .commentsButtonTapped) where state.feed[index].story != nil:
                let item = state.feed[index]
                state.comments = CommentsState(
                    parentId: item.story!.id,
                    parentType: .story
                )
                return .none
            case let .projectCell(index: index, action: .commentsButtonTapped) where state.feed[index].project != nil:
                let item = state.feed[index]
                state.comments = CommentsState(
                    parentId: item.project!.id,
                    parentType: .project
                )
                return .none
            case let .projectCell(index: index, action: .donateButtonTapped) where state.feed[index].project != nil:
                let item = state.feed[index]
                state.donateState = DonateState(
                    project: item.project!
                )
                return .none
            case .comments, .donate, .storyDetails, .newStory, .projectDetails:
                return .none
            case .projectCell:
                return .none
            case .exploreCell:
                return .none
            case let .viewStoryDetail(story):
                state.storyDetails = StoryDetailState(story: story)
                return .none
            case let .viewProjectDetail(project):
                state.projectDetails = ProjectDetailState(project: project)
                return .none
            case .event(.showLoading):
                state.loadingState = .loading
                return .none           
            case .event(.storyCreated):
                return .none
            case .dismissRequestAlert:
                state.isRequestedMessageShown = nil
                return .none
            }
        },
        newStoryReducer.optional().pullback(
            state: \.newStory,
            action: /CommunityDetailAction.newStory,
            environment: {
                $0.map {
                    NewStoryEnvironment(
                        newStory: $0.newStoryClient,
                        selectCommunity: $0.selectCommunityClient,
                        imageLoader: $0.imageLoader
                    )
                }
            }
        ),
        storyCellReducer.forEach(
            state: \.stories,
            action: /CommunityDetailAction.exploreCell(index:action:),
            environment: { $0.map(\.imageLoader) }
        ),
        projectCellReducer.forEach(
            state: \.projects,
            action: /CommunityDetailAction.projectCell(index:action:),
            environment: { $0.map(\.imageLoader) }
        ),
        storyDetailReducer.optional().pullback(
            state: \.detail,
            action: /CommunityDetailAction.storyDetails,
            environment: { $0.map {
                StoryDetailEnvironment(
                    storyDetailClient: $0.storyDetailsClient,
                    linkProjectClient: $0.linkProjectClient,
                    commentsClient: $0.commentsClient,
                    projectDetailClient: $0.projectDetailsClient,
                    donateClient: $0.donateClient,
                    linkStoryClient: $0.linkStoryClient,
                    addCardClient: $0.addCardClient,
                    imageLoader: $0.imageLoader,
                    stripeClient: $0.stripeClient
                )
            }
            }
        ),
        projectDetailReducer.optional().pullback(
            state: \.details,
            action: /CommunityDetailAction.projectDetails,
            environment: { $0.map {
                ProjectDetailEnvironment(
                    projectDetailClient: $0.projectDetailsClient,
                    linkStoryClient: $0.linkStoryClient,
                    donateClient: $0.donateClient,
                    addCardClient: $0.addCardClient,
                    imageLoader: $0.imageLoader,
                    storyDetailClient: $0.storyDetailsClient,
                    linkProjectClient: $0.linkProjectClient,
                    commentsClient: $0.commentsClient,
                    stripeClient:  $0.stripeClient
                )
                }
            }
        ),
        commentsReducer.optional().pullback(
            state: \.comments,
            action: /CommunityDetailAction.comments,
            environment: {
                $0.map {
                    CommentsEnvironment(
                        commentsClient: $0.commentsClient,
                        imageLoader: $0.imageLoader
                    )
                }
            }
        )
    )


private let roundedButtonStyle: (UIButton) -> Void =
    heightAnchor(.br_grid(9))
    <> widthAnchor(.br_grid(21))
    <> roundedStyle(cornerRadius: .br_grid(2))
    <> {
        $0.titleLabel?.font = UIFont.py_headline(size: 14)
        $0.setTitleColor(UIColor.white, for: .normal)
    }

private let joinedButtonStyle: (UIButton) -> Void =
    {
        $0.setTitle(String("Joined"))
        $0.backgroundColor = UIColor.blueColor
    } <> roundedButtonStyle

private let joinButtonStyle: (UIButton) -> Void = {
    $0.setTitle(String("Join"))
    $0.backgroundColor = UIColor.mainColor
} <> roundedButtonStyle

private let menuButtonStyle: (UIButton) -> Void =
    squareStyle(.br_grid(8))
    <> {
        $0.setImage(UIImage(named: "ic_more_vert"))
    }


import ComposableArchitecture
public class CommunityDetailViewController: UIViewController {
    
    let store: Store<CommunityDetailState, CommunityDetailAction>
    let viewStore: ViewStore<CommunityDetailState, CommunityDetailAction>
    
    public init(store: Store<CommunityDetailState, CommunityDetailAction>) {
        self.store = store
        self.viewStore = ViewStore(store)
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var collectionView: UICollectionView!
    var datasource: [BrFeedItem] = [] {
        didSet {
            collectionView.reloadData()
        }
    }
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Community"
        
        view.backgroundColor = .white
        
        viewStore.send(.viewDidLoad)
                                
        let layout = UICollectionViewFlowLayout()
        collectionFlowLayoutStyle(layout)
        collectionView = UICollectionView(
            frame: .zero,
            collectionViewLayout: layout
        )
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionViewStyle(collectionView)
                                        
        viewStore.produced.focusedIndexPaths
            .startWithValues { [weak self] indexPaths in
                self?.collectionView.performBatchUpdates {
                    self?.collectionView.reloadItems(at: Array(indexPaths.prefix(2))
                    )
                }
            }
        viewStore.produced.feed
            .observe(on: QueueScheduler.main)
            .assign(to: \.datasource, on: self)
                                    
        let joinedButton = UIButton()        
        joinedButton.addTarget(self,
                             action: #selector(joinedButtonTapped),
                             for: .touchUpInside)
        joinedButtonStyle(joinedButton)
        let menuButton = UIButton()
        menuButton.addTarget(self,
                             action: #selector(menuButtonTapped),
                             for: .touchUpInside)
        menuButtonStyle(menuButton)
                
        viewStore.produced.isJoined
            .startWithValues { isJoined in
                menuButton.isHidden = !isJoined
                isJoined
                    ? joinedButtonStyle(joinedButton)
                    : joinButtonStyle(joinedButton)
            }
        
        
        
        
        viewStore.produced.alert
            .startWithValues { [weak self] (alert) in
                guard let self = self else { return }
                guard let alert = alert else { return }
                let actionController = UIAlertController(
                    title: alert.title,
                    message: alert.message,
                    preferredStyle: .actionSheet
                )
                alert.primaryButton
                    .map { $0.toUIKit(send: self.viewStore.send) }
                    .map(actionController.addAction)
                alert.secondaryButton
                    .map { $0.toUIKit(send: self.viewStore.send) }
                    .map(actionController.addAction)
                self.present(actionController, animated: true, completion: nil)
            } 
       
        let orgLabel = UILabel()
        orgLabelStyle(orgLabel)
        let membersLabel = UILabel()
        membersLabelStyle(membersLabel)
                                
        let titleStackView = UIStackView(arrangedSubviews: [
            orgLabel, membersLabel
        ])
        titleStackViewStyle(titleStackView)
        let topStackView = UIStackView(arrangedSubviews: [
            titleStackView,            
            joinedButton,
            menuButton
        ])
        topStackViewStyle(topStackView)
        
        let rootStackView = UIStackView(arrangedSubviews: [
            topStackView,
            collectionView
        ])
        rootStackViewStyle(rootStackView)
        self.view.addSubview(rootStackView)
        rootStackView.constrainEdges(to: view, insets: view.safeAreaInsets)
                
        let loadingView = LoadingView()
        self.view.addSubview(loadingView)
        loadingView.frame = view.frame
        self.view.bringSubviewToFront(loadingView)
        
        viewStore.produced.loadingState
            .startWithValues { state in
                loadingView.state = state
                loadingView.isHidden = state == nil
            }
        
        
        let alertView = RequestSubmittedView()
        view.addSubview(alertView)
        alertView |>
            autoLayoutStyle
            <> bottomEdgeStyle(self.view, .init(top: 0, left: .br_grid(4), bottom: .br_grid(30), right: .br_grid(4)))
            <> heightAnchor(.br_grid(18))
        view.bringSubviewToFront(alertView)
        viewStore.produced.isRequestedMessageShown
            .startWithValues { state in
                if let state = state {
                    alertView.configure(
                        RequestSubmittedView.State(
                            isSuccess: state
                        )
                    )
                }
                alertView.isHidden = state == nil
            }
                        
        let createStoryButton = UIButton()
        createStoryButtonStyle(createStoryButton)
        createStoryButton.addTarget(
            self,
            action: #selector(newStoryButtonTapped),
            for: .touchUpInside
        )
                                
        self.navigationItem.rightBarButtonItems = [createStoryButton].map(UIBarButtonItem.init(customView:))
        
        viewStore.produced.community
            .startWithValues { community in
                orgLabel.text = community.name
                membersLabel.text = "\(community.members) members"
            }
        
        // New Story
        store.scope(
            state: \.newStory,
            action: CommunityDetailAction.newStory
        ).ifLet(then: { [weak self] store in
            guard let self = self else { return }
            self.navigationController?.pushViewController(
                NewStoryViewController(store: store), animated: true)
        }) {
            self.navigationController?.popToViewController(self, animated: true)
        }
        
        // Comments
        store.scope(
            state: \.comments,
            action: CommunityDetailAction.comments
        ).ifLet(then: { [weak self] store in
            guard let self = self else { return }
            self.navigationController?.pushViewController(CommentsViewController(store: store), animated: true)
        }) {
            self.navigationController?.popToViewController(self, animated: true)
        }
        // Story Details
        store.scope(
            state: \.storyDetails,
            action: CommunityDetailAction.storyDetails
        ).ifLet(then: { [weak self] store in
            guard let self = self else { return }
            self.navigationController?.pushViewController(
                StoryDetailViewController(store: store), animated: false)
        }) {
            self.navigationController?.popToViewController(self, animated: true)
        }
        
        // Project Details
        store.scope(
            state: \.projectDetails,
            action: CommunityDetailAction.projectDetails
        ).ifLet(then: { [weak self] store in
            guard let self = self else { return }
            self.navigationController?.pushViewController(
                ProjectDetailViewController(store: store), animated: true)
        }) {
            self.navigationController?.popToViewController(self, animated: true)
        }
        
        // Donate
        store.scope(
            state: \.donateState,
            action: CommunityDetailAction.donate
        ).ifLet(then: { [weak self] store in
            guard let self = self else { return }
            self.navigationController?.pushViewController(
                DonateViewController(store: store), animated: true)
        }) {
            self.navigationController?.popToViewController(self, animated: true)
        }
        
                
    }
    
    
    @objc
    public func newStoryButtonTapped() {
        viewStore.send(.newStoryButtonTapped)
    }
    
      
    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewStore.send(.viewDidAppear)
        if !isMovingToParent {
            viewStore.send(.viewDismissed)
        }
    }
    
    public override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        viewStore.send(.viewDidDisappear)
    }
    
    @objc func menuButtonTapped() {
        viewStore.send(.menuButtonTapped)
    }
    
    @objc func joinedButtonTapped() {
        DispatchQueue.main.async {
            self.viewStore.send(.joinButtonTapped)
        }        
    }
}

private let titleStackViewStyle: (UIStackView) -> Void =
    verticalStackStyle(.br_grid(0),
                       distribution: .fill,
                       alignment: .leading
    )

private let rootStackViewStyle: (UIStackView) -> Void =
    verticalStackStyle(.br_grid(1))


private let orgLabelStyle: (UILabel) -> Void = {
    $0.textColor = .mainColor
    $0.font = UIFont.py_body(size: .br_grid(4)).bolded
}

private let membersLabelStyle: (UILabel) -> Void =
    brGrayTextColor
    <> {
        $0.font = .py_caption1(size: .br_grid(3))
    }

private let topStackViewStyle: (UIStackView) -> Void =
    horizontalStackStyle(.br_grid(1),
                         distribution: .fill,
                         alignment: .center
    ) <> marginStyle(.square(.br_grid(4)))


private let collectionViewStyle: (UICollectionView) -> Void =
    autoLayoutStyle <> {
        $0.register(StoryViewCell.self,
                    forCellWithReuseIdentifier: StoryViewCell.reuseIdentifier)
        $0.register(ProjectViewCell.self,
                    forCellWithReuseIdentifier: ProjectViewCell.reuseIdentifier)
        $0.backgroundColor = .white
        //$0.contentInset = .horizontal(.br_grid(4))
    }

private let collectionFlowLayoutStyle: (UICollectionViewFlowLayout) -> Void = {
    $0.scrollDirection = .vertical
    $0.sectionHeadersPinToVisibleBounds = true
    $0.minimumLineSpacing = .br_grid(4)
    $0.minimumInteritemSpacing = .br_grid(4)
    $0.sectionInsetReference = .fromContentInset
    $0.sectionInset = .topHorizontalEdge(.br_grid(4))
}

extension CommunityDetailViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        return datasource.count
    }
    
    public func collectionView(
        _ collectionView: UICollectionView,
        didSelectItemAt indexPath: IndexPath) {
        let item = datasource[indexPath.section]
        switch item.type {
        case .story:
            viewStore.send(.viewStoryDetail(item.story!))
        case .project:
            viewStore.send(.viewProjectDetail(item.project!))
        case .community:
            fatalError()
        }        
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let item = datasource[indexPath.section]
        switch item.type {
        
        case .story:
            guard let storyCell = collectionView.dequeueReusableCell(
                withReuseIdentifier: StoryViewCell.reuseIdentifier,
                for: indexPath
            ) as? StoryViewCell else {
                fatalError()
            }
            storyCell.configure(store.scope(
                state:  { $0.feed[safe: indexPath.section]?.story ?? .branbeeStory },
                action: { .exploreCell(index: indexPath.section, action: $0) }
            ))
            return storyCell
        case .project:
            guard let projectCell = collectionView.dequeueReusableCell(
                withReuseIdentifier: ProjectViewCell.reuseIdentifier,
                for: indexPath
            ) as? ProjectViewCell else {
                fatalError()
            }
            projectCell.configure(store.scope(
                state:  { $0.feed[safe: indexPath.section]?.project ?? BrProject.branbee },
                action: { .projectCell(index: indexPath.section, action: $0) }
            ))
            return projectCell
        case .community:
            fatalError()
        }
                                              
    }
}


extension CommunityDetailViewController: UICollectionViewDelegateFlowLayout {
            
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellWidth = collectionView.bounds.width - .br_grid(8)
        let item = viewStore.feed[indexPath.section]
        switch item.type {
        case .story:
            return CGSize(
                width: cellWidth,
                height:
                    StorySectionType
                    .story(item.story!)
                    .cellHeight(cellWidth)
            )
        default:
            return CGSize(
                width: cellWidth,
                height: .br_grid(55)
            )
        }
        
       
        
       
    }
    
}
