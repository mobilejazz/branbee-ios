import UIKit
import Library
import ExploreClient
import ComposableArchitecture
import ReactiveSwift
import BrModels
import ImageLoader
import NewStory

extension SearchState {
    var stories: [BrStory] {
        get {
            items.stories
        }
        set {}
    }
    var projects: [BrProject] {
        get {
            items.projects
        }
        set {}
    }
    var communities: [BrCommunity] {
        get {
            items.communities
        }
        set {}
    }
}

public struct SearchState: Equatable {
    public var items: [BrFeedItem]
    public var loadingState: LoadingState?
    public var filter: SearchFilter
    public var searchText: String
    public var didReachTheEnd: Bool = false
    public var focusedIndexPaths: [IndexPath]
    public var communityDetail: CommunityDetailState?
    public var storyDetail: StoryDetailState?
    var detail: StoryDetailStateCopy? {
        get {
            storyDetail.map(StoryDetailStateCopy.init)
        }
        set {
            storyDetail = newValue?.value
        }
    }
    
    public var projectDetail: ProjectDetailState?
    var details: ProjectDetailStateCopy? {
        get {
            projectDetail.map(ProjectDetailStateCopy.init)
        }
        set {
            projectDetail = newValue?.value
        }
    }
    public var comments: CommentsState? = nil
    public var donateState: DonateState? = nil    
    public init(
        items: [BrFeedItem] = [],
        error: Failure? = nil,
        loadingState: LoadingState? = nil,
        filter: SearchFilter = .allTypes,
        searchText: String = "",
        focusedIndexPaths: [IndexPath] = [],
        communityDetail: CommunityDetailState? = nil,
        storyDetail: StoryDetailState? = nil
    ) {
        self.searchText = searchText
        self.items = items
        self.filter = filter
        self.loadingState = loadingState
        self.focusedIndexPaths = focusedIndexPaths
        self.communityDetail = communityDetail
        self.storyDetail = storyDetail
    }
    
    
}


public enum SearchAction: Equatable {
    case viewDidLoad
    case viewDidAppear, viewDidDisAppear
    case viewDismissed
    case destroy
    case loadMore
    case event(SearchDelegateEvent)
    case applyFilter(SearchFilter)
    case viewCommunity(BrCommunity)
    case viewStory(BrStory)
    case viewProject(BrProject)
    case communityDetail(CommunityDetailAction)
    case projectDetail(ProjectDetailAction)
    case storyDetail(StoryDetailAction)
    case searchTextChanged(String)
    case storyCell(index: Int, action: StoryCellAction)
    case projectCell(index: Int, action: ProjectCellAction)
    case communityCell(index: Int, action: SelectCommunityCellAction)
    case comments(CommentsAction)
    case donate(DonateAction)
}


public let searchReducer: Reducer<SearchState, SearchAction, SystemEnvironment<ExploreEnvironment>> =
    .combine(
        Reducer { state, action, environment in
            struct CancelDelegateId: Hashable {}
            switch action {
            case .viewDidLoad:
                return .merge(
                    environment.searchClient
                        .delegate
                        .map(SearchAction.event)
                       // .cancellable(id: CancelDelegateId())
                    ,
                    environment.searchClient
                        .viewDidLoad
                        .fireAndForget()
                )
            case .viewDidAppear:
                return .none
            case .viewDismissed:
                state.communityDetail = nil
                state.storyDetail = nil
                state.projectDetail = nil
                state.comments = nil
                state.donateState = nil
                return .none
            case let .event(.showFailure(error)):
                state.loadingState = .message(error.message ?? error.cause ?? "")
                return .none
            case .event(.hideLoading):
                state.loadingState = nil
                return .none
            case .event(.showLoading):
                state.loadingState = .loading
                return .none
            case let .event(.items(items, didReachEnd: didReachEnd)):
                state.didReachTheEnd = didReachEnd
                state.items = items
                return .none
            case let .storyCell(index: index, action: .commentsButtonTapped) where state.items[index].story != nil:
                let item = state.items[index]
                state.comments = CommentsState(
                    parentId: item.story!.id,
                    parentType: .story
                )
                return .none
            case let .projectCell(index: index, action: .commentsButtonTapped) where state.items[index].project != nil:
                let item = state.items[index]
                state.comments = CommentsState(
                    parentId: item.project!.id,
                    parentType: .project
                )
                return .none
            case let .projectCell(index: index, action: .donateButtonTapped) where state.items[index].project != nil:
                let item = state.items[index]
                state.donateState = DonateState(
                    project: item.project!
                )
                return .none                        
            case .storyCell, .communityCell, .projectCell:
                return .none
            case let .event(.showFullScreenFailure(failure, retryAction: retryAction)):
                state.loadingState = .failure(
                    failure,
                    action: retryAction
                )
                return .none
            case .event(.noItemFound):
                state.loadingState =
                    .message("No Item Found")
                state.items = []
                return .none
            case .event(.showSearchHint):
                return .none
            case let .applyFilter(filter):
                return environment.searchClient
                    .search(state.searchText, filter)
                    .fireAndForget()
            case let .searchTextChanged(text):
                state.searchText = text
                return environment.searchClient
                    .search(text, state.filter)
                    .debounce(1.0, on: environment.mainQueue())
                    .fireAndForget()
            case .destroy:
                return .cancel(id: CancelDelegateId())
            case .communityDetail:
                return .none
            case let .viewCommunity(community):
                state.communityDetail = .init(community: community)
                return .none
            case let .viewStory(story):
                state.storyDetail = .init(story: story)
                return .none
            case let .viewProject(project):
                state.projectDetail = .init(project: project)
                return .none
            case .storyDetail, .projectDetail:
                return .none
            case .comments:
                return .none
            case .donate:
                return .none
            case .loadMore:
                guard !state.didReachTheEnd else { return .none }
                return environment.searchClient.loadMore
                    .fireAndForget()
            case .viewDidDisAppear:
                return .none
            }
        },        
        communityDetailReducer.optional().pullback(
            state: \.communityDetail,
            action: /SearchAction.communityDetail,
            environment: { $0 }
        ),
        storyCellReducer.forEach(
            state: \.stories,
            action: /SearchAction.storyCell(index:action:),
            environment: { $0.map(\.imageLoader) }
        ),
        projectCellReducer.forEach(
            state: \.projects,
            action: /SearchAction.projectCell(index:action:),
            environment: { $0.map(\.imageLoader) }
        ),
        selectCommunityCellReducer.forEach(
            state: \.communities,
            action: /SearchAction.communityCell(index:action:),
            environment: { $0.map(\.imageLoader) }
        ),
        storyDetailReducer.optional().pullback(
            state: \.detail,
            action: /SearchAction.storyDetail,
            environment: { $0.map {
                StoryDetailEnvironment(
                    storyDetailClient: $0.storyDetailsClient,
                    linkProjectClient: $0.linkProjectClient,
                    commentsClient: $0.commentsClient,
                    projectDetailClient: $0.projectDetailsClient,
                    donateClient: $0.donateClient,
                    linkStoryClient: $0.linkStoryClient,
                    addCardClient: $0.addCardClient,
                    imageLoader: $0.imageLoader,
                    stripeClient: $0.stripeClient
                )
            }
            }
        ),
        projectDetailReducer.optional().pullback(
            state: \.details,
            action: /SearchAction.projectDetail,
            environment: { $0.map {
                ProjectDetailEnvironment(
                    projectDetailClient: $0.projectDetailsClient,
                    linkStoryClient: $0.linkStoryClient,
                    donateClient: $0.donateClient,
                    addCardClient: $0.addCardClient,
                    imageLoader: $0.imageLoader,
                    storyDetailClient: $0.storyDetailsClient,
                    linkProjectClient: $0.linkProjectClient,
                    commentsClient: $0.commentsClient,
                    stripeClient: $0.stripeClient
                )
            }
            }
        ),
        commentsReducer.optional().pullback(
            state: \.comments,
            action: /SearchAction.comments,
            environment: {
                $0.map {
                    CommentsEnvironment(
                        commentsClient: $0.commentsClient,
                        imageLoader: $0.imageLoader
                    )
                }
            }
        ),
        donationReducer.optional().pullback(
            state: \.donateState,
            action: /SearchAction.donate,
            environment: {
                $0.map {
                    DonateEnvironment(
                        donateClient: $0.donateClient,
                        addCardClient: $0.addCardClient,
                        stripeClient: $0.stripeClient
                    )
                }
            })
    )

private let rootStackViewStyle: (UIStackView) -> Void =
    verticalStackStyle(.br_grid(0))



import ComposableArchitecture
public class SearchViewController: UIViewController {
    
    let store: Store<SearchState, SearchAction>
    let viewStore: ViewStore<SearchState, SearchAction>
    
    let searchController = UISearchController()
    
    public init(store: Store<SearchState, SearchAction>) {
        self.store = store
        self.viewStore = ViewStore(store)
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var collectionView: UICollectionView!
    var datasource: [BrFeedItem] = [] {
        didSet {
            collectionView.reloadData()
        }
    }
            
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Search"
        self.view.backgroundColor = .white
        
        viewStore.send(.viewDidLoad)
                                
        let layout = UICollectionViewFlowLayout()
        collectionFlowLayoutStyle(layout)
        collectionView = UICollectionView(
            frame: .zero,
            collectionViewLayout: layout
        )
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionViewStyle(collectionView)
                
        navigationItem.hidesSearchBarWhenScrolling = false
        self.definesPresentationContext = true
        navigationItem.searchController = searchController
        searchControllerStyle(searchController)        
        searchController.delegate = self
        searchController.searchResultsUpdater = self
        searchController.searchBar.placeholder = "Search"
        
        let searchBar = SearchBarView()
        searchBar.delegate = self
        searchBar.datasource = .filters(SearchFilter.allCases)
        searchBarStyle(searchBar)
        let rootStackView = UIStackView(arrangedSubviews: [
            searchBar,
            collectionView
        ])
        rootStackViewStyle(rootStackView)
        self.view.addSubview(rootStackView)
        rootStackView.constrainSafeAreaEdges(to: view)
        
        let spinner = LoadingView()
        self.view.addSubview(spinner)
        spinner.frame = view.frame
        self.view.bringSubviewToFront(spinner)
        
        viewStore.produced.loadingState
            .startWithValues { state in
                spinner.state = state
                spinner.isHidden = state == nil
            }        
                               
        viewStore.produced.focusedIndexPaths
            .startWithValues { [weak self] indexPaths in
                self?.collectionView.performBatchUpdates {
                    self?.collectionView
                        .reloadItems(at: Array(indexPaths.prefix(1)))
                }
            }
        
        // Community Detail
        store.scope(
            state: \.communityDetail,
            action: SearchAction.communityDetail
        ).ifLet(then: { [weak self] store in
            guard let self = self else { return }
            self.navigationController?.pushViewController(CommunityDetailViewController(store: store), animated: true)
        }) {
            self.navigationController?.popToViewController(self, animated: true)
        }
        
        // Project Detail
        store.scope(
            state: \.projectDetail,
            action: SearchAction.projectDetail
        ).ifLet(then: { [weak self] store in
            guard let self = self else { return }
            self.navigationController?.pushViewController(ProjectDetailViewController(store: store), animated: true)
        }) {
            self.navigationController?.popToViewController(self, animated: true)
        }
        
        // Community Detail
        store.scope(
            state: \.storyDetail,
            action: SearchAction.storyDetail
        ).ifLet(then: { [weak self] store in
            guard let self = self else { return }
            self.navigationController?.pushViewController(StoryDetailViewController(store: store), animated: true)
        }) {
            self.navigationController?.popToViewController(self, animated: true)
        }
        
        // Comments
        store.scope(
            state: \.comments,
            action: SearchAction.comments
        ).ifLet(then: { [weak self] store in
            guard let self = self else { return }
            self.navigationController?.pushViewController(CommentsViewController(store: store), animated: true)
        }) {
            self.navigationController?.popToViewController(self, animated: true)
        }
        // Donate
        store.scope(
            state: \.donateState,
            action: SearchAction.donate
        ).ifLet(then: { [weak self] store in
            guard let self = self else { return }
            self.navigationController?.pushViewController(
                DonateViewController(store: store), animated: true)
        }) {
            self.navigationController?.popToViewController(self, animated: true)
        }
        
        viewStore.produced.items
            .observe(on: QueueScheduler.main)
            .assign(to: \.datasource, on: self)
                                           
    }
    
    @objc func notificationButtonTapped() {}
      
    
    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewStore.send(.viewDidAppear)
        if !isMovingToParent {
            viewStore.send(.viewDismissed)
        } 
    }
    
    public override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
    }
        
    
}


extension SearchViewController: SearchBarViewDelegate {
    public func searchBar(_ sb: SearchBarView, didSelectFilter filter: SearchFilter) {
        self.viewStore.send(.applyFilter(filter))
    }
}

extension SearchViewController {
    public func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
            if indexPath.section == datasource.count - 1 {
                viewStore.send(.loadMore)
            }
    }
}


extension SearchViewController: UISearchResultsUpdating, UISearchControllerDelegate {
    public func updateSearchResults(for searchController: UISearchController) {
        viewStore.send(
            .searchTextChanged(searchController.searchBar.text ?? "")
        )
    }
    
    public func didDismissSearchController(_ searchController: UISearchController) {
        viewStore.send(.viewDismissed)
    }    
}

private let collectionViewStyle: (UICollectionView) -> Void =
    autoLayoutStyle <> {
        $0.register(StoryViewCell.self,
                    forCellWithReuseIdentifier: StoryViewCell.reuseIdentifier)
        $0.register(ProjectViewCell.self,
                    forCellWithReuseIdentifier: ProjectViewCell.reuseIdentifier)
        $0.register(SelectCommunityViewCell.self,
                    forCellWithReuseIdentifier: SelectCommunityViewCell.reuseIdentifier)
        $0.backgroundColor = .white
        //$0.contentInset = .horizontal(.br_grid(4))
    }

private let collectionFlowLayoutStyle: (UICollectionViewFlowLayout) -> Void = {
    $0.scrollDirection = .vertical
    $0.sectionHeadersPinToVisibleBounds = true
    $0.minimumLineSpacing = .br_grid(4)
    $0.minimumInteritemSpacing = .br_grid(4)
    $0.sectionInsetReference = .fromContentInset
    $0.sectionInset = .topHorizontalEdge(.br_grid(4))
}

extension SearchViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        return datasource.count
    }
    
    public func collectionView(
        _ collectionView: UICollectionView,
        didSelectItemAt indexPath: IndexPath) {
        switch datasource[indexPath.section].type {
        case .story:
            guard let story = datasource[indexPath.section].story else { return }
            viewStore.send(.viewStory(story))
        case .project:
            guard let project = datasource[indexPath.section].project else { return }
            viewStore.send(.viewProject(project))
        case .community:
            guard let community = datasource[indexPath.section].community else { return }
            self.viewStore.send(.viewCommunity(community))
        }
        
        
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let item = viewStore.items[safe: indexPath.section] else { return UICollectionViewCell()
        }
        switch item.type {
        case .community:
            guard let communityCell = collectionView.dequeueReusableCell(
                withReuseIdentifier: SelectCommunityViewCell.reuseIdentifier,
                for: indexPath
            ) as? SelectCommunityViewCell else {
                fatalError()
            }
            communityCell.configure(store.scope(
                state:  { $0.items[safe: indexPath.section]?.community ?? BrCommunity.branbeCommunity },
                action: { .communityCell(index: indexPath.section, action: $0) }
            ))
            return communityCell
        case .story:
            guard let storyCell = collectionView.dequeueReusableCell(
                withReuseIdentifier: StoryViewCell.reuseIdentifier,
                for: indexPath
            ) as? StoryViewCell else {
                fatalError()
            }
            storyCell.configure(store.scope(
                state:  { $0.items[safe: indexPath.section]?.story ?? .branbeeStory },
                action: { .storyCell(index: indexPath.section, action: $0) }
            ))
            return storyCell
        case .project:
            guard let projectCell = collectionView.dequeueReusableCell(
                withReuseIdentifier: ProjectViewCell.reuseIdentifier,
                for: indexPath
            ) as? ProjectViewCell else {
                fatalError()
            }
            projectCell.configure(store.scope(
                state:  { $0.items[safe: indexPath.section]?.project ?? BrProject.branbee },
                action: { .projectCell(index: indexPath.section, action: $0) }
            ))
            return projectCell
        }
    }
}


extension SearchViewController: UICollectionViewDelegateFlowLayout {
            
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellWidth = collectionView.bounds.width - .br_grid(8)
        guard let item = viewStore.items[safe: indexPath.section] else { return .zero }
        switch item.type {
        case .story:
            return CGSize(
                width: cellWidth,
                height: StorySectionType
                    .story(item.story!)
                    .cellHeight(cellWidth)
            )
        case .community:
            return CGSize(
                width: cellWidth,
                height: .br_grid(38)
            )
        case .project:
            return CGSize(
                width: cellWidth,
                height: .br_grid(55)
            )
        }
    }
}

public let storyText: (BrStory?) -> String = {
    $0?.content.first(where: { $0.type == .text })?.content ?? ""
}
