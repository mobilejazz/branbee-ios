import UIKit
import Library
import ExploreClient
import ComposableArchitecture
import ReactiveSwift
import BrModels
import ImageLoader

public struct NotificationsState: Equatable {
    public var notifications: [BrNotification] = []
    public var alert: _AlertState<NotificationsAction>?
    public var loadingState: LoadingState? = nil
    public var isSubscribed: Bool = false
    public var isRequestAccepted: Bool?
    public var didReachedEnd: Bool = false
    public init(
        notifications: [BrNotification] = [],
        alert: _AlertState<NotificationsAction>? = nil,
        loadingState: LoadingState? = nil
    ) {
        self.notifications = notifications
        self.alert = alert
        self.loadingState = loadingState
    }
    
    
}

public enum NotificationsAction: Equatable {
    case viewDidLoad
    case viewDidAppear
    case viewDismissed
    case viewDidDisappear
    case dismissAlert
    case loadMore
    case retryAction(RetryAction)
    case event(NotificationsDelegateEvent)
    case cell(index: Int, action: NotificationCellAction)
}

public enum NotificationCellAction: Equatable {
    case acceptButtonTapped
    case rejectButtonTapped
}

let notificationCellReducer:
    Reducer<BrNotification, NotificationCellAction, Void> = .empty

public struct NotificationsEnvironment {
    var notificationsClient: NotificationsClient
    var imageLoader: ImageLoader
    public init(
        notificationsClient: NotificationsClient,
        imageLoader: ImageLoader
    ) {
        self.notificationsClient = notificationsClient
        self.imageLoader = imageLoader
    }
}

public let notificationsReducer: Reducer<NotificationsState, NotificationsAction, SystemEnvironment<NotificationsEnvironment>> =
    .combine(
        Reducer { state, action, environment in
            struct CancelDelegateId: Hashable {}
            switch action {
            case .viewDidLoad:
                state.isSubscribed = true
                return .merge(
                    environment.notificationsClient
                        .delegate
                        .observe(on: environment.mainQueue())
                        .cancellable(id: CancelDelegateId())
                        .map(NotificationsAction.event),
                    environment.notificationsClient
                        .viewDidLoad
                        .fireAndForget()
                )                
            case .viewDidAppear:
                guard !state.isSubscribed else { return .none }
                return environment.notificationsClient
                    .delegate
                    .cancellable(id: CancelDelegateId())
                    .map(NotificationsAction.event)
            case .viewDismissed:
                state.alert = nil
                return .none
            case let .retryAction(action):
                return .fireAndForget(action.run)
            case let .event(.displayFailure(failure, retryAction: action)):
                state.loadingState = .failure(
                    failure,
                    action: action
                )
                return .none
            case let .event(.displayNotifications(notifications, didReachedEnd: didReachedEnd)):
                state.didReachedEnd = didReachedEnd
                state.notifications = notifications
                return .none
            case .event(.displayNoNotifications):
                state.notifications = []
                state.loadingState = .message(
                    "No Notification"
                )
                return .none
            case let .event(.showSuccessMessage(requestAccepted: requestAccepted)):
                state.isRequestAccepted = requestAccepted
                return Effect(value: .dismissAlert)
                    .delay(2.0, on: environment.mainQueue())
            case .event(.hideLoading):
                state.loadingState = nil
                return .none
            case .event(.showLoading):
                state.loadingState = .loading
                return .none
            case let .event(.showAlert(failure)):
                state.alert = .init(
                    title: "Alert",
                    message: failure.message,
                    dismissButton: .cancel(send: .viewDismissed)
                )
                return .none
            case let .cell(index: index, action: .rejectButtonTapped):
                return environment.notificationsClient
                    .rejectNotification(state.notifications[index])
                    .fireAndForget()
            case let .cell(index: index, action: .acceptButtonTapped):
                return environment.notificationsClient
                    .acceptNotification(state.notifications[index])
                    .fireAndForget()
            case .viewDidDisappear:
                state.isSubscribed = false
                return .cancel(id: CancelDelegateId())            
            case .dismissAlert:
                state.isRequestAccepted = nil
                return .none
            case .loadMore:
                guard !state.didReachedEnd else { return .none }
                return environment.notificationsClient
                    .loadMore
                    .fireAndForget()
            }
        },
        notificationCellReducer.forEach(
            state: \.notifications,
            action: /NotificationsAction.cell(index:action:),
            environment: { _ in () }
        )
    )

import ComposableArchitecture
import NewStory
public class NotificationsViewController: UIViewController {
    
    let store: Store<NotificationsState, NotificationsAction>
    let viewStore: ViewStore<NotificationsState, NotificationsAction>
    
    public init(store: Store<NotificationsState, NotificationsAction>) {
        self.store = store
        self.viewStore = ViewStore(store)
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var collectionView: UICollectionView!
    var datasource: [BrNotification] = [] {
        didSet {
            collectionView.reloadData()
        }
    }
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Notifications"
        
        view.backgroundColor = .white
        
        viewStore.send(.viewDidLoad)
                                
        let layout = UICollectionViewFlowLayout()
        collectionFlowLayoutStyle(layout)
        collectionView = UICollectionView(
            frame: .zero,
            collectionViewLayout: layout
        )
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionViewStyle(collectionView)
                                                
        viewStore.produced.notifications
            .assign(to: \.datasource, on: self)
                               
        viewStore.produced.alert
            .startWithValues { [weak self] (alert) in
                guard let self = self else { return }
                guard let alert = alert else { return }
                let actionController = UIAlertController(
                    title: alert.title,
                    message: alert.message,
                    preferredStyle: .actionSheet
                )
                alert.primaryButton
                    .map { $0.toUIKit(send: self.viewStore.send) }
                    .map(actionController.addAction)
                alert.secondaryButton
                    .map { $0.toUIKit(send: self.viewStore.send) }
                    .map(actionController.addAction)
                self.present(actionController, animated: true, completion: nil)
            }
                                      
        self.view.addSubview(collectionView)
        collectionView.constrainEdges(to: view, insets: .zero)
        
        let spinner = LoadingView()        
        self.view.addSubview(spinner)
        spinner.frame = view.frame
        self.view.bringSubviewToFront(spinner)
        viewStore.produced.loadingState
            .startWithValues { state in
                spinner.state = state
                spinner.isHidden = state == nil
            }
        
        let alertView = RequestSubmittedView()
        view.addSubview(alertView)
        alertView |>
            autoLayoutStyle
            <> bottomEdgeStyle(self.view, .init(top: 0, left: .br_grid(4), bottom: .br_grid(30), right: .br_grid(4)))
            <> heightAnchor(.br_grid(18))
        view.bringSubviewToFront(alertView)
        viewStore.produced.isRequestAccepted
            .startWithValues { state in
                if let state = state {
                    alertView.configure(
                         RequestSubmittedView.State(
                            isSuccess: state
                        )
                    )
                }
                alertView.isHidden = state == nil
            }

        
    }
          
    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewStore.send(.viewDidAppear)
        if !isMovingToParent {
            viewStore.send(.viewDismissed)
        }
    }
    
    public override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        viewStore.send(.viewDidDisappear)
    }
    
       
}

private let titleStackViewStyle: (UIStackView) -> Void =
    verticalStackStyle(.br_grid(0),
                       distribution: .fill,
                       alignment: .leading
    )

private let rootStackViewStyle: (UIStackView) -> Void =
    verticalStackStyle(.br_grid(1))

private let orgLabelStyle: (UILabel) -> Void = {
    $0.textColor = .mainColor
    $0.font = UIFont.py_body(size: .br_grid(4)).bolded
}

private let collectionViewStyle: (UICollectionView) -> Void =
    autoLayoutStyle <> {
        $0.register(NotificationCell.self,
                    forCellWithReuseIdentifier: NotificationCell.reuseIdentifier)
        $0.backgroundColor = .white
        //$0.contentInset = .horizontal(.br_grid(4))
    }

private let collectionFlowLayoutStyle: (UICollectionViewFlowLayout) -> Void = {
    $0.scrollDirection = .vertical
    $0.sectionHeadersPinToVisibleBounds = true
    $0.minimumLineSpacing = .br_grid(4)
    $0.minimumInteritemSpacing = .br_grid(4)
    $0.sectionInsetReference = .fromContentInset
    $0.sectionInset = .zero//.topHorizontalEdge(.br_grid(4))
}

extension NotificationsViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        return datasource.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.section == datasource.count - 1 {
            viewStore.send(.loadMore)
        }
    }
    
    
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: NotificationCell.reuseIdentifier,
            for: indexPath
        ) as? NotificationCell else {
            fatalError()
        }
        cell.configure(store: store.scope(
            state:  { $0.notifications[safe: indexPath.section] ?? .project },
            action: { .cell(index: indexPath.section, action: $0) }
        ))
        return cell
    }
}


extension NotificationsViewController: UICollectionViewDelegateFlowLayout {
            
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellWidth = collectionView.bounds.width //- .br_grid(8)
        
        return CGSize(
            width: cellWidth,
            height: .br_grid(36)
        )
    }
    
}

