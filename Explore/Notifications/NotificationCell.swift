import Library
import UIKit
import ComposableArchitecture
import BrModels

private let typeImageViewStyle: (UIImageView) -> Void =
    squareStyle(.br_grid(8))
    <> {
        $0.contentMode = .scaleAspectFit
    }

private let titleLabelStyle: (UILabel) -> Void = {
    $0.textColor = .secondaryTextColor
    $0.font = .py_body(size: .br_grid(4))
}

private let subTitleLabelStyle: (UILabel) -> Void =
    brGrayTextColor
    <> {
        $0.font = .py_body(size: 14)
        $0.numberOfLines = 0
    }

private let titleStackViewStyle: (UIStackView) -> Void =
    verticalStackStyle(.br_grid(1),
                       distribution: .fill,
                       alignment: .leading
    )

private let topStackViewStyle: (UIStackView) -> Void =
    horizontalStackStyle(.br_grid(4),
                         distribution: .fill,
                         alignment: .top)
    <> marginStyle(.horizontal(.br_grid(2)))
    
private let bottomStackViewStyle: (UIStackView) -> Void = horizontalStackStyle(.br_grid(2))
    <> marginStyle(.horizontal(.br_grid(2)))

private let rootStackViewStyle: (UIStackView) -> Void =
    verticalStackStyle(.br_grid(1))
    <> marginStyle(.rectangle(v: .br_grid(2), h: .br_grid(0)))

let rejectButtonStyle: (UIButton) -> Void = heightAnchor(.br_grid(9)) <> widthAnchor(.br_grid(23)) <> {
    $0.titleLabel?.font = .py_headline(size: 14)
    $0.setTitle("REJECT")
    $0.setTitleColor(.systemRed, for: .normal)
}

let acceptButtonStyle: (UIButton) -> Void = heightAnchor(.br_grid(9))
    <> widthAnchor(.br_grid(23))
    <> {
        $0.titleLabel?.font = .py_headline(size: 14)
        $0.setTitle("ACCEPT")
        $0.setTitleColor(.systemGreen, for: .normal)
    }

class NotificationCell: UICollectionViewCell {
    
    public class var reuseIdentifier: String { "NotificationCell" }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
    
    var viewStore: ViewStore<BrNotification, NotificationCellAction>!
    func configure(store: Store<BrNotification, NotificationCellAction>) {
        viewStore = ViewStore(store)
        viewStore.produced.text
            .map(id)
            .assign(to: \.text, on: subTitleLabel)
        viewStore.produced.type
            .startWithValues { [weak self] type in
                guard let self = self else { return }
                switch type {
                case .project:
                    self.typeImageView.image = UIImage(named: "icon_project")
                    self.titleLabel.text = "Project Linking Request"
                case .community:
                    self.typeImageView.image = UIImage(named: "ic_community_small")
                    self.titleLabel.text = "Community Linking Request"
                case .story:
                    self.typeImageView.image = UIImage(named: "ic_story")
                    self.titleLabel.text = "Story Linking Request"
                case .other:
                    self.typeImageView.image = UIImage(named: "")
                    self.titleLabel.text = "Other Linking Request"
                case .request:
                    self.typeImageView.image = UIImage(named: "ic_action")
                    self.titleLabel.text = "Action Linking Request"
                }
            }
    }
    
    let typeImageView = UIImageView()
    let subTitleLabel = UILabel()
    let titleLabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        typeImageViewStyle(typeImageView)
        titleLabelStyle(titleLabel)
        subTitleLabelStyle(subTitleLabel)
        let titleStackView = UIStackView(arrangedSubviews: [
            titleLabel, subTitleLabel
        ])
        titleStackViewStyle(titleStackView)
        let topStackView = UIStackView(arrangedSubviews: [
            typeImageView,
            titleStackView
        ])
        topStackViewStyle(topStackView)
        let rejectButton = UIButton()
        rejectButtonStyle(rejectButton)
        rejectButton.addTarget(self,
                               action: #selector(rejectButtonTapped),
                               for: .touchUpInside)
        let acceptButton = UIButton()
        acceptButtonStyle(acceptButton)
        acceptButton.addTarget(self,
                               action: #selector(acceptButtonTapped),
                               for: .touchUpInside)
        
        let bottomStackView = UIStackView(arrangedSubviews: [
            UIView(),
            rejectButton,
            acceptButton,
        ])
        
        let spacer = UIView()
        spacer |> Library.heightAnchor(0.5)
        spacer.backgroundColor = .lightGray
        bottomStackViewStyle(bottomStackView)
        let rootStackView = UIStackView(arrangedSubviews: [
            topStackView,
            bottomStackView,
            spacer
        ])
        rootStackViewStyle(rootStackView)
        self.addSubview(rootStackView)
        rootStackView.constrainEdges(to: self)                
    }
    
    @objc func rejectButtonTapped() {
        viewStore?.send(.rejectButtonTapped)
    }
    
    @objc func acceptButtonTapped() {
        viewStore?.send(.acceptButtonTapped)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
