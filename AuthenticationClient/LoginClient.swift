import Library
import ComposableArchitecture
import ReactiveSwift
import Tagged

public struct LoginRequest {
    public let email: Email
    public let password: Password
    public init(
        email: Email,
        password: Password
    ) {
        self.email = email
        self.password = password
    }
}

public enum LoginDelegateEvent: Equatable {    
    case onDisplayErrorLogin(Failure)
    case loginSuccess
}

public struct LoginClient {
    public let login: (LoginRequest) -> SignalProducer<Never, Never>
    public let onDetachView: SignalProducer<Never, Never>
    public let delegate: SignalProducer<LoginDelegateEvent, Never>
    public init(
        login: @escaping (LoginRequest) -> SignalProducer<Never, Never> =
            { _ in .empty },
        onDetachView: SignalProducer<Never, Never> = .empty,
        delegate: SignalProducer<LoginDelegateEvent, Never>
    ) {
        self.login = login
        self.onDetachView = onDetachView
        self.delegate = delegate
    }
}

extension LoginClient {
    public func callAsFunction(_ request: LoginRequest)
    -> SignalProducer<Never, Never> {
        login(request)
    }
}
let loginDelegate = Signal<LoginDelegateEvent, Never>.pipe()

extension LoginClient {
    public static func mock(_ isSucceed: Bool) -> Self {
        .init(
            login: { (request)  in
                .fireAndForget {
                    dispatchAfter(time: 2.0, work: {
                        if isSucceed {
                            loginDelegate.input.send(value: .loginSuccess)
                        } else {
                            
                            if request.email == "test@br.com" &&
                                request.password == "test" {
                                loginDelegate.input.send(value: .loginSuccess)
                            } else {
                            loginDelegate.input.send(value: .onDisplayErrorLogin(.message("Failed to login")))
                            }
                        }
                    })
                }
            },
            delegate: loginDelegate.output.producer
        )
    }
}


