import Library
import ComposableArchitecture
import ReactiveSwift
import Tagged

public enum ForgotPasswordDelegateEvent: Equatable {
    case requestedSuccessfully
    case failedToValidateEmail
    case failedToRecoverPassword(Failure)
    
}


public struct ForgotPasswordClient {
    public let forgotPassword: (Email) -> SignalProducer<Never, Never>
    public let delegate: SignalProducer<ForgotPasswordDelegateEvent, Never>
    public init(
        forgotPassword: @escaping (Email) -> SignalProducer<Never, Never> = { _ in .empty },
        delegate: SignalProducer<ForgotPasswordDelegateEvent, Never>
    ) {
        self.forgotPassword = forgotPassword
        self.delegate = delegate
    }
}


extension ForgotPasswordClient {
    public func callAsFunction(_ email: Email) -> SignalProducer<Never, Never> {
        forgotPassword(email)
    }
}


let forgotPasswordDelegate = Signal<ForgotPasswordDelegateEvent, Never>.pipe()

extension ForgotPasswordClient {
    public static func mock(_ isSucced: Bool = true) -> Self {
        .init(
            forgotPassword: { email in
                .fireAndForget {
                    dispatchAfter(time: 2) {
                        if !isSucced {
                            forgotPasswordDelegate.input.send(value: .failedToRecoverPassword(.init(message: "Error", cause: nil)))
                        } else {
                            forgotPasswordDelegate.input.send(value: .requestedSuccessfully)
                        }
                    }
                    
                }
            },
            delegate: forgotPasswordDelegate.output.producer)
    }
}
