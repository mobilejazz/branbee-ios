import ReactiveSwift
import Library
import UIKit

public enum FBFailure: LocalizedError {
    case notGranted
    case cancelled
    case custom(Error)
}

public struct FacebookClient {
    
    public struct FBAccessToken {
        let token: String
        let userId: String
        public init(
            token: String,
            userId: String
        ) {
            self.token = token
            self.userId = userId
        }
    }
    
    public let didFinishLaunching: (UIApplication, [UIApplication.LaunchOptionsKey: Any]?) -> Void
    public let openURL: (UIApplication, URL, String?, Any?) -> Void
    public let logIn: () -> SignalProducer<FBAccessToken, FBFailure>
    public let logOut: () -> Void
    
    public init(
        didFinishLaunching: @escaping (UIApplication, [UIApplication.LaunchOptionsKey: Any]?) -> Void,
        openURL: @escaping (UIApplication, URL, String?, Any?) -> Void,
        logIn: @escaping () -> SignalProducer<FBAccessToken, FBFailure>,
        logOut: @escaping () -> Void
    ) {
        self.didFinishLaunching = didFinishLaunching
        self.openURL = openURL
        self.logIn = logIn
        self.logOut = logOut
    }
}
