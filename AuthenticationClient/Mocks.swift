import ReactiveSwift

// Empty Client
extension LoginClient {
    public static var empty: Self {
        Self(
            login:  { _ in .empty },
            delegate: .never
        )
    }
}

extension ForgotPasswordClient {
    public static var empty: Self {
        Self(
            forgotPassword: { _ in
                return .empty
            },
            delegate: .never
        )
    }
}

extension SignUpClient {
    public static var empty: Self {
        Self(
            register: { _ in
                .empty
            },
            onViewLoaded: .empty,
            onDetachView: .empty,
            delegate: .never
        )
    }
}


extension FacebookClient {
    public static var empty: Self {
        .init(
            didFinishLaunching: { _, _ in () },
            openURL: { _,_,_,_ in () },
            logIn: {
                .empty
            },
            logOut: { () }
        )
    }
}

let signUpDelegate = Signal<SignUpDelegateEvent, Never>.pipe()
extension SignUpClient {
    public static func mock(_ isSucceed: Bool = false) -> Self {
        Self(
            register: { request in
                .fireAndForget {
                    
                    signUpDelegate.input.send(value: .showLoading)
                    
                    if request.name.isEmpty || request.email.isEmpty ||
                        request.password.isEmpty {
                        signUpDelegate.input.send(value: .onDisplayFillRequiredFields)
                        return
                    }
                    
                    dispatchAfter(time: 2.0) {
                        if isSucceed {
                            signUpDelegate.input.send(value: .successRegistration)
                        } else {
                            signUpDelegate.input.send(value: .hideLoading)
                            signUpDelegate.input.send(value: .onFailedToRegister(.message("failed to register")))
                        }
                    }
                    
                    
                    
                }
            },
            onViewLoaded: .fireAndForget {
                loadCountries(isSucceed: isSucceed)
            },
            onDetachView: .empty,
            delegate: signUpDelegate.output.producer
        )
    }
}

import Library
private func loadCountries(isSucceed: Bool) {
    
    signUpDelegate.input.send(value: .showLoading)
        
    dispatchAfter(time: 2.0) {
        if isSucceed {
        signUpDelegate.input.send(value: .onDisplayCountries([.spain, .brazil]))
        signUpDelegate.input.send(value: .hideLoading)
        } else {
            signUpDelegate.input.send(value: .onDisplayFullScreenError(.message("error"), action: .init(run: {
                loadCountries(isSucceed: true)
            })))
        }
        
    }
  }
