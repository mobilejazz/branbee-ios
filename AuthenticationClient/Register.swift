import BrModels
public struct RegisterRequest: Equatable {
    public let name: String
    public let email: String
    public let password: String
    public let avatarPath: String?
    public let city: String?
    public let country: BrCountry?
    public let biography: String?
    public init(
        name: String,
        email: String,
        password: String,
        avatarPath: String?,
        city: String? = nil,
        country: BrCountry? = nil,
        biography: String? = nil
    ) {
        self.name = name
        self.email = email
        self.password = password
        self.avatarPath = avatarPath
        self.city = city
        self.country = country
        self.biography = biography
    }
}

public struct RegisterResponse: Equatable {
    let token: String
}

import Library
public enum SignUpDelegateEvent: Equatable {
    case onDisplayCountries([BrCountry])
    case onDisplayFullScreenError(Failure, action: RetryAction)
    case onFailedToRegister(Failure)
    case onFailedToValidateEmail
    case showLoading, hideLoading
    case onDisplayFillRequiredFields
    case successRegistration    
}

import ReactiveSwift
public struct SignUpClient {
    public var register: (RegisterRequest) -> SignalProducer<Never, Never>
    public var onViewLoaded: SignalProducer<Never, Never>
    public var onDetachView: SignalProducer<Never, Never>
    public var delegate: SignalProducer<SignUpDelegateEvent, Never>
    public init(
        register: @escaping (RegisterRequest) -> SignalProducer<Never, Never> = { _ in .empty },
        onViewLoaded: SignalProducer<Never, Never>,
        onDetachView: SignalProducer<Never, Never>,
        delegate: SignalProducer<SignUpDelegateEvent, Never>
    ) {
        self.register = register
        self.onDetachView = onDetachView
        self.onViewLoaded = onViewLoaded
        self.delegate = delegate
    }
}

extension SignUpClient {
    public func callAsFunction(_ request: RegisterRequest) -> SignalProducer<Never, Never> {
        register(request)
    }
}


