public struct AuthenticationClient {
    public let login: LoginClient
    public let register: SignUpClient
    public let forgotPassword: ForgotPasswordClient
    public let facebook: FacebookClient
    public init(
        login: LoginClient,
        register: SignUpClient,
        forgotPassword: ForgotPasswordClient,
        facebook: FacebookClient
    ) {
        self.login = login
        self.register = register
        self.forgotPassword = forgotPassword
        self.facebook = facebook
    }
}


extension AuthenticationClient {
    public static var empty: Self {
        .init(
            login: .empty,
            register: .empty,
            forgotPassword: .empty,
            facebook: .empty
        )
    }
}
