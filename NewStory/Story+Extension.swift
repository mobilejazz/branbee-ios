import BrModels
import Library
import UIKit


public enum StorySectionType: Equatable {
    case story(BrStory)
    public func cellHeight(_ width: CGFloat, showDetail: Bool = false) -> CGFloat {
        switch self {
        case let .story(story):
            var height: CGFloat = 110
            if let text = story.content |> textContent {
               
                height += showDetail
                    ? heightForText(text, width: width, font: .py_body(size: 14))
                    : max(
                        min(heightForText(text, width: width), CGFloat(85)),
                        .br_grid(8)
                    )
            }
            if let _ = story |> videoURL {
                height += .br_grid(52)
            }
            if let _ = story |> pictureURL {
                height += .br_grid(52)
            }                                    
            return height
        }
    }
    
}
