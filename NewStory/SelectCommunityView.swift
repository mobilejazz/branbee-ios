import UIKit
import Library
import ExploreClient
import ComposableArchitecture
import ReactiveSwift
import BrModels
import ImageLoader


public struct SelectCommunityState: Equatable {
    public var communities: [BrCommunity] = []
    public var error: Failure?
    public var loadingState: LoadingState? = nil
    public init(
        communities: [BrCommunity] = [],
        error: Failure? = nil
    ) {
        self.communities = communities
        self.error = error
    }
        
}

public enum SelectCommunityCellAction: Equatable {}

//public struct SelectCommunityCellState: Equatable {
//    let community: BrCommunity
//    var picture: UIImage?
//}

public enum SelectCommunityAction: Equatable {
    case viewDidLoad
    //case viewDidAppear, viewDidDisappear
    case viewDismissed
    case destroy
    case searchButtonTapped
    case selectedCommunity(BrCommunity)
    case event(SelectCommunityDelegateEvent)
    case cell(index: Int, action: SelectCommunityCellAction)
}

public struct SelectCommunityEnvironment {
    var selectCommunityClient: SelectCommunityClient
    var imageLoader: ImageLoader
    public init(
        selectCommunityClient: SelectCommunityClient,
        imageLoader: ImageLoader
    ) {
        self.selectCommunityClient = selectCommunityClient
        self.imageLoader = imageLoader
    }
}



public let selectCommunityReducer: Reducer<SelectCommunityState, SelectCommunityAction, SystemEnvironment<SelectCommunityEnvironment>> =
    .combine(
        Reducer { state, action, environment in
            struct EventId: Hashable {}
            switch action {
            case .viewDidLoad:
                return .merge(
                    environment.selectCommunityClient
                        .delegate
                        .cancellable(id: EventId())
                        .map(SelectCommunityAction.event),
                    environment.selectCommunityClient
                        .viewDidLoad                        
                        .fireAndForget()
                )
            
            case .viewDismissed:
                state.loadingState = nil
                state.error = nil
                return .none
            case .event(.hideLoading):
                state.loadingState = nil
                return .none
            case .event(.showLoading):
                state.loadingState = .loading
                return .none
            case .cell:
                return .none
            case let .event(.displayFailure(failure, retryAction: retryAction)):
                state.loadingState = .failure(failure, action: retryAction)
                return .none
            case let .event(.displayCommunities(communities)):
                state.communities = communities
                state.error = nil
                return .none
            case .event(.displayNoMessage):
                state.loadingState = .message("No Message")
                return .none
            case .searchButtonTapped:
                return .none
            case .selectedCommunity:
                return .none
            case .destroy:
                return .cancel(id: EventId())
            }
        },
        selectCommunityCellReducer.forEach(
            state: \.communities,
            action: /SelectCommunityAction.cell(index:action:),
            environment: { $0.map(\.imageLoader) }
        )
    )

public let selectCommunityCellReducer = Reducer<BrCommunity, SelectCommunityCellAction, SystemEnvironment<ImageLoader>> { state, action, env in
    return .none
}


import ComposableArchitecture
public class SelectCommunityViewController: UIViewController {
    let store: Store<SelectCommunityState, SelectCommunityAction>
    let viewStore: ViewStore<SelectCommunityState, SelectCommunityAction>
    
    public init(store: Store<SelectCommunityState, SelectCommunityAction>) {
        self.store = store
        self.viewStore = ViewStore(store)
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var collectionView: UICollectionView!
    var datasource: [BrCommunity] = [] {
        didSet {
            collectionView.reloadData()
        }
    }
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Select Community"
        
        viewStore.send(.viewDidLoad)
                                
        let layout = UICollectionViewFlowLayout()
        collectionFlowLayoutStyle(layout)
        collectionView = UICollectionView(
            frame: .zero,
            collectionViewLayout: layout
        )
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionViewStyle(collectionView)
        self.view.addSubview(collectionView)
        collectionView.constrainEdges(to: view)
        
        let loadingView = LoadingView()
        self.view.addSubview(loadingView)
        loadingView.frame = view.frame
        self.view.bringSubviewToFront(loadingView)
        
        viewStore.produced.loadingState
            .startWithValues { state in
                loadingView.state = state
                loadingView.isHidden = state == nil
            }
        
                        
        viewStore.produced.communities
            .assign(to: \.datasource, on: self)
                    
        let searchButton = UIButton()
        searchButtonStyle(searchButton)
        searchButton.addTarget(self,
                               action: #selector(searchButtonTapped),
                               for: .touchUpInside)        
        //self.navigationItem.rightBarButtonItems = [searchButton].map(UIBarButtonItem.init(customView:))
    }
       
    @objc func searchButtonTapped() {
        viewStore.send(.viewDismissed)
    }
    
    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if !isMovingToParent {
            viewStore.send(.viewDismissed)
        }
    }
                
}

private let collectionViewStyle: (UICollectionView) -> Void =
    autoLayoutStyle <> {
        $0.register(SelectCommunityViewCell.self,
                    forCellWithReuseIdentifier: SelectCommunityViewCell.reuseIdentifier)
        $0.backgroundColor = .white
        //$0.contentInset = .horizontal(.br_grid(4))
    }

private let collectionFlowLayoutStyle: (UICollectionViewFlowLayout) -> Void = {
    $0.scrollDirection = .vertical
    $0.sectionHeadersPinToVisibleBounds = true
    $0.minimumLineSpacing = .br_grid(4)
    $0.minimumInteritemSpacing = .br_grid(4)
    $0.sectionInsetReference = .fromContentInset
    $0.sectionInset = .topHorizontalEdge(.br_grid(4))
}

extension SelectCommunityViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        return datasource.count
    }
    
    public func collectionView(
        _ collectionView: UICollectionView,
        didSelectItemAt indexPath: IndexPath) {
        if let community = datasource[safe: indexPath.section] {
            viewStore.send(.selectedCommunity(community))
        }
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let selectCommunity = collectionView.dequeueReusableCell(
            withReuseIdentifier: SelectCommunityViewCell.reuseIdentifier,
            for: indexPath
        ) as? SelectCommunityViewCell else {
            fatalError()
        }
        selectCommunity.configure(store.scope(
            state:  { $0.communities[indexPath.section] },
            action: { .cell(index: indexPath.section, action: $0) }
        ))
        return selectCommunity
    }
}


extension SelectCommunityViewController: UICollectionViewDelegateFlowLayout {
            
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellWidth = collectionView.bounds.width - .br_grid(8)
        return CGSize(
            width: cellWidth,
            height: .br_grid(38)
        )
    }
    
}


