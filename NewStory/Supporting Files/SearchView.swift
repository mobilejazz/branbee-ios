import Library
import UIKit
import ExploreClient

let searchCellStyle: (UICollectionViewCell) -> Void =
    roundedStyle(cornerRadius: .br_grid(4))
    <> {
        $0.backgroundColor = UIColor(white: 0.95, alpha: 1.0)
    }
private let itemLabelStyle: (UILabel) -> Void =
    brDarkTextColor
    <> autoLayoutStyle
    <> centerStyle
    <> {
        $0.font = .py_body(size: 14)
    }

class SearchCollectionViewCell: UICollectionViewCell {
    
    class var reuseIdentifier: String {
        "SearchCollectionViewCell"
    }
    
    let itemLabel = UILabel()
    
    override var isSelected: Bool {
        didSet {
            self.backgroundColor = isSelected ? .blueColor: UIColor(white: 0.95, alpha: 1.0)
            self.itemLabel.textColor = isSelected ? .white: .secondaryTextColor
            
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.addSubview(itemLabel)
        itemLabelStyle(itemLabel)
        itemLabel.constrainEdges(
            to: self,
            insets: .rectangle(v: .br_grid(2), h: .br_grid(4))
        )
        searchCellStyle(self)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
                          
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

private let collectionViewStyle:
    (UICollectionView) -> Void = autoLayoutStyle <> {
        $0.register(SearchCollectionViewCell.self, forCellWithReuseIdentifier: SearchCollectionViewCell.reuseIdentifier)
    } <> {
        $0.backgroundColor = .clear
        $0.showsHorizontalScrollIndicator = false
        $0.contentInset = .horizontal(.br_grid(4))
    }

private let collectionFlowLayoutStyle:
    (UICollectionViewFlowLayout) -> Void = {
        $0.scrollDirection = .horizontal
        $0.minimumInteritemSpacing = .br_grid(4)
        $0.minimumLineSpacing = .br_grid(4)
    }

public enum Amount: Int, CaseIterable {
    case _5_ = 5
    case _10_ = 10
    case _15_ = 15
    case _20_ = 20
    case _25_ = 25
}

import CasePaths

public protocol SearchBarViewDelegate: AnyObject {
    func searchBar(_ sb: SearchBarView, didSelectFilter filter: SearchFilter)
    func searchBar(_ sb: SearchBarView, didSelectAmount amount: Amount)
}

extension SearchBarViewDelegate {
    public func searchBar(_ sb: SearchBarView, didSelectFilter filter: SearchFilter) {}
    public func searchBar(_ sb: SearchBarView, didSelectAmount amount: Amount) {}
}

public class SearchBarView: UIView {
            
    public enum Datasource {
        
        case filters([SearchFilter])
        case amounts([Amount])
        case none
        
        public var count: Int {
            switch self {
            case let .amounts(ams):
                return ams.count
            case let .filters(filrs):
                return filrs.count
            case .none:
                return .zero
            }
        }
        
        public func value(at indexPath: IndexPath) -> String? {
            switch self {
            case let .filters(filters):
                return filters[indexPath.row].rawValue.capitalized
            case let .amounts(amts):
                return "$\(amts[indexPath.row].rawValue)"
            case .none:
                return nil
            }
        }
                
        public func amount(_ indexPath: IndexPath) -> Amount? {
            extract(case: Datasource.amounts, from: self)?[indexPath.item]
        }
        
        public func filter(_ indexPath: IndexPath) -> SearchFilter? {
            extract(case: Datasource.filters, from: self)?[indexPath.item]
        }
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not Required")
    }
                
    public var delegate: SearchBarViewDelegate?
    
    public var datasource: Datasource = .none {
        didSet {
            collectionView.reloadData()
        }
    }
    
    var collectionView: UICollectionView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        let layout = UICollectionViewFlowLayout()
        collectionFlowLayoutStyle(layout)
        layout.estimatedItemSize = CGSize(
            width: .br_grid(20),
            height: frame.height
        )
        collectionView = UICollectionView(
            frame: .zero,
            collectionViewLayout: layout
        )
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionViewStyle(collectionView)
        self.addSubview(collectionView)
        collectionView.constrainEdges(to: self)
    }
}

extension SearchBarView: UICollectionViewDelegate, UICollectionViewDataSource {
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return datasource.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(
                withReuseIdentifier: SearchCollectionViewCell.reuseIdentifier,
                for: indexPath) as? SearchCollectionViewCell else { fatalError()
        }
        cell.itemLabel.text = datasource.value(at: indexPath)
        return cell
    }
    
    public func collectionView(
        _ collectionView: UICollectionView,
        didSelectItemAt indexPath: IndexPath) {
        
        if let amount = datasource.amount(indexPath) {
            delegate?.searchBar(self, didSelectAmount: amount)
        }
        
        if let filter = datasource.filter(indexPath) {
            delegate?.searchBar(self, didSelectFilter: filter)
        }
        
    }
    
}
