import UIKit
import BrModels
import ComposableArchitecture
import ReactiveSwift
import Library
import ImageLoader
import SDWebImage

private let personImageViewStyle: (UIImageView) -> Void =
    squareStyle(.br_grid(8))
    <> roundedStyle(cornerRadius: .br_grid(4))
    <> {
        $0.backgroundColor = .systemRed
    }

private let storyStatusImageViewStyle: (UIButton) -> Void =
    squareStyle(.br_grid(8))
    <> {
        $0.setImage(UIImage(named: "ic_bookmark"))
        $0.tintColor = .mainColor
    }

private let orgLabelStyle: (UILabel) -> Void = {
    $0.textColor = .mainColor
    $0.font = UIFont.py_body(size: 16).bolded
    $0.setContentHuggingPriority(.defaultHigh, for: .vertical)
}

private let personLabelStyle: (UILabel) -> Void =
    brGrayTextColor
    <> {
        $0.font = .py_caption1(size: 12)
    }

private let titleStackViewStyle: (UIStackView) -> Void =
    verticalStackStyle(.br_grid(1),
                       distribution: .fillEqually,
                       alignment: .leading
    )
private let topStackViewStyle: (UIStackView) -> Void =
    horizontalStackStyle(.br_grid(4),
                       distribution: .fill,
                       alignment: .center)
    <> marginStyle(.rectangle(v: .br_grid(2), h: .br_grid(4)))

private let storyStackViewStyle: (UIStackView) -> Void =
    verticalStackStyle(.br_grid(1),
                       distribution: .fill,
                       alignment: .leading)
    <> marginStyle(.horizontal(.br_grid(4)))


private let storyImageViewStyle: (UIImageView) -> Void =
    heightAnchor(.br_grid(52))
    

private let storyContentLabelStyle: (UILabel) -> Void =
    brGrayTextColor <> {
        //$0.textAlignment = .left
        $0.numberOfLines = 0
        //$0.lineBreakMode = .byTruncatingTail
        $0.font = .py_body(size: 14)
        //$0.isHidden = true
    } <> {
        $0.heightAnchor.constraint(
            lessThanOrEqualToConstant: .br_grid(18)
        ).isActive = true
    }

private let bottomStackViewStyle: (UIStackView) -> Void = horizontalStackStyle(.br_grid(3), alignment: .center)
    <> marginStyle(.rectangle(v: .br_grid(2), h: .br_grid(4)))

private let readMoreButtonStyle: (UIButton) -> Void = {
    $0.setTitle("Read More", for: .normal)
    $0.setTitleColor(.blue, for: .normal)
    $0.titleLabel?.font = .py_caption2(size: 14)
} <> heightAnchor(.br_grid(5))

private let dateLabelStyle: (UILabel) -> Void = brGrayTextColor <> {
    $0.font = .py_caption1(size: .br_grid(3))
    $0.textAlignment = .left
}

private let commentsBtnStyle: (UIButton) -> Void =
    //squareStyle(.br_grid(5)) <>
    {
        $0.setImage(UIImage(named: "ic_chat_off"))
        $0.tintColor = .black
        $0.contentMode = .scaleAspectFit
    }

let boostBtnStyle: (UIButton) -> Void =
    //squareStyle(.br_grid(5)) <>
    {
    $0.setImage(UIImage(named: "ic_boost_off"))
}

private let componentIconStyle: (UIImageView) -> Void =
    squareStyle(.br_grid(4))

private let componentLabelStyle: (UILabel) -> Void =
    brLabelDarkStyle <> {
    $0.font = .py_caption1(size: 14)
}

public let outputsStackViewStyle: (UIStackView) -> Void = horizontalStackStyle(.br_grid(2), alignment: .lastBaseline)

private let rootStackViewStyle: (UIStackView) -> Void =
    verticalStackStyle(.br_grid(1))

private let exploreCellStyle: (StoryViewCell) -> Void =
    roundedStyle(cornerRadius: .br_grid(1))
    <> {
        $0.backgroundColor = .white
    }

private let exploreDateFormatter: () -> DateFormatter = {
    let formatter = DateFormatter()
    formatter.dateFormat = "dd MMMM YYYY"
    return formatter
}

public enum StoryCellAction: Equatable {
    case readMoreTapped
    case commentsButtonTapped
}


public let storyCellReducer = Reducer<BrStory, StoryCellAction, SystemEnvironment<ImageLoader>> { state, action, environment in
    switch action {
//        guard !state.isLoading else {
//            return .none
//        }
//        state.isLoading = true
//        return Effect.merge(
//            videoURL(state.item.story)
//                .map(StoryCellAction.loadVideoImage)
//                .map(Effect.init(value:))
//                ?? .none,
//            Effect.init(value:
//                StoryCellAction.loadStoryImageURL(
//                    pictureURL(state.item.story)
//                )
//            ),
//            Effect.init(value:
//                StoryCellAction.loadProfileImage(
//                    state.item.story?.creator._photoURL
//                )
//            )
//        )
    case .readMoreTapped:
        return .none
    case .commentsButtonTapped:
        return .none
    }
}

public enum SavedStoryCellAction: Equatable {    
    case readMoreTapped
    case bookmarkButtonTapped
    case boostButtonTapped
    case commentsButtonTapped
}

public let pictureURL: (BrStory?) -> URL? = {
    guard let stringURL = $0?.content
            .first(where: { $0.type == .picture })?.content else { return nil }
    return URL(string: stringURL)
}

public let videoURL: (BrStory?) -> URL? = {
    guard let stringURL = $0?.content
            .first(where: { $0.type == .video })?.content else { return nil }
    guard let encodedString = stringURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else { return nil }
    return URL(string: encodedString)
}

public class StoryViewCell: UICollectionViewCell {
    
    public override var bounds: CGRect {
        didSet {
             setupShadow()
        }
    }
    
    private func setupShadow() {
        layer.masksToBounds = false
        layer.shadowColor = UIColor.gray.cgColor
        layer.shadowOpacity = 0.4
        layer.shadowOffset =  CGSize(width: 1.0, height: 1.0)
        layer.shadowRadius = .br_grid(1)
        layer.cornerRadius = .br_grid(1)
        layer.shouldRasterize = true
        layer.rasterizationScale = UIScreen.main.scale
        layer.shadowPath = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: .allCorners, cornerRadii: CGSize(width: 4, height: 4)).cgPath
    }
    
    public class var reuseIdentifier: String { "StoryViewCell" }
        
    let storyContentLabel = UILabel()
    let readMoreButton = UIButton()
        
    var viewStore: ViewStore<BrStory, StoryCellAction>!
    var savedViewStore: ViewStore<BrStory, SavedStoryCellAction>!
    
    private func subscribe<Action>(
        _ viewStore: ViewStore<BrStory, Action>
    ) {
        
        viewStore.produced.creator
            .map { $0.username }
            .assign(to: \.text, on: personLabel)
        
        viewStore.produced.community
            .map { $0.name }
            .assign(to: \.text, on: orgLabel)
        
        viewStore.produced.isBookmarked
            .map { !$0 }
            .assign(to: \.isHidden, on: bookMarkedButton)
        
        viewStore.produced.date
            .map(exploreDateFormatter().string(from:))
            .assign(to: \.text, on: dateLabel)
                
        viewStore.produced.content
            .startWithValues { contents in
                let hasImage = contents.contains(
                    where: { $0.type == .picture || $0.type == .video })
                self.storyImageView.isHidden = !hasImage
            }
        
        viewStore.produced.comments
            .map(String.init)
            .assign(to: \.text, on: commentsLabel)
        
        viewStore.produced.content
            .compactMap(textContent)
            //.map(htmlString)
            .map(convertToHtml)
            .startWithValues {
//                let mutableAttributeString = $0.map(NSMutableAttributedString.init(attributedString:))
//                mutableAttributeString?.mutableString
//                    .replaceOccurrences(
//                        of: "\n\n",
//                        with: "\n",
//                        options: [],
//                        range: NSMakeRange(0, mutableAttributeString?.length ?? 0)
//                    )
                self.storyContentLabel.attributedText = $0
                self.storyContentLabel.isHidden = $0 == nil
            }
        
        viewStore.produced.shoulds            
            .map(String.init)
            .assign(to: \.text, on: boostsLabel)
        
        if let videoURL = videoURL(viewStore.state) {
            getThumbnailImageFromVideoUrl(url: videoURL) {
                self.storyImageView.image = $0
            }
        } else {
            self.storyImageView.sd_setImage(with: pictureURL(viewStore.state))
        }
        
        if let photoURL = viewStore.state.creator._photoURL {
            self.personImageView.sd_setImage(with: photoURL)
        } else {
            self.personImageView.sd_setImage(with: URL(string: viewStore.state.community.picture))
        }        
       
    }
   

    
    public func configure(_ store: Store<BrStory, StoryCellAction>) {
        self.viewStore = ViewStore(store)//ViewStore(store)
        subscribe(viewStore)
    }
    
    public func configure(
        _ store: Store<BrStory, SavedStoryCellAction>
    ) {
        savedViewStore = ViewStore(store)
        subscribe(savedViewStore)
    }
        
    let personImageView = UIImageView()
    let orgLabel = UILabel()
    let personLabel = UILabel()
    let storyImageView = UIImageView()
    let bookMarkedButton = UIButton()
    let dateLabel = UILabel()
    let commentsLabel = UILabel()
    let boostsLabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        personImageViewStyle(personImageView)
        orgLabelStyle(orgLabel)
        personLabelStyle(personLabel)
        storyStatusImageViewStyle(bookMarkedButton)
        let titleStackView = UIStackView(arrangedSubviews: [
            orgLabel,
            personLabel
        ])
        titleStackViewStyle(titleStackView)
        let topStackView = UIStackView(arrangedSubviews: [
            personImageView,
            titleStackView,
            bookMarkedButton
        ])
        topStackViewStyle(topStackView)
        storyImageViewStyle(storyImageView)
        storyContentLabelStyle(storyContentLabel)
        readMoreButtonStyle(readMoreButton)
        readMoreButton.addTarget(self,
                    action: #selector(readMoreButtonTapped),
                    for: .touchUpInside)
        
        let storyStackView = UIStackView(arrangedSubviews: [
            storyContentLabel            
        ])
        
        storyStackViewStyle(storyStackView)
        dateLabelStyle(dateLabel)
        let commentsButton = UIButton()
        commentsButton.addTarget(self,
                action: #selector(commentsButtonTapped),
                for: .touchUpInside)
        commentsBtnStyle(commentsButton)
        componentLabelStyle(commentsLabel)
        let commentsStackView = UIStackView(arrangedSubviews: [
            commentsButton,
            commentsLabel
        ])
        outputsStackViewStyle(commentsStackView)
        
        let boostsButton = UIButton()
        commentsButton.addTarget(self, action: #selector(boostButtonTapped), for: .touchUpInside)
        boostBtnStyle(boostsButton)
        
        componentLabelStyle(boostsLabel)
        let boostsStackView = UIStackView(arrangedSubviews: [
            boostsButton,
            boostsLabel
        ])
        outputsStackViewStyle(boostsStackView)
        
        let bottomStackView = UIStackView(arrangedSubviews: [
            dateLabel,
            UIView(),
            commentsStackView,
            boostsStackView
        ])
        bottomStackViewStyle(bottomStackView)
        
        let rootStackView = UIStackView(arrangedSubviews: [
            topStackView,
            storyImageView,
            storyStackView,
            UIView(),
            bottomStackView
        ])        
        rootStackViewStyle(rootStackView)
        self.addSubview(rootStackView)
        rootStackView.constrainEdges(to: self)
                                
        exploreCellStyle(self)
    }
            
    @objc
    public func readMoreButtonTapped() {
        viewStore?.send(.readMoreTapped)
        savedViewStore?.send(.readMoreTapped)
    }
        
    @objc
    public func boostButtonTapped() {
        savedViewStore?.send(.boostButtonTapped)        
    }
    
    @objc
    public func bookmarkedButtonTapped() {
        savedViewStore?.send(.bookmarkButtonTapped)
    }
    
    @objc
    public func commentsButtonTapped() {
        savedViewStore?.send(.commentsButtonTapped)
        viewStore?.send(.commentsButtonTapped)
    }
    
    public override func prepareForReuse() {
        super.prepareForReuse()
        storyContentLabel.text = nil
    }
                          
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
            
}


public let convertToHtml: (String?) -> NSAttributedString? = {
    try? NSAttributedString(
        htmlString: $0 ?? "",
        font: .py_body(size: 14),
        useDocumentFontSize: false
    )
}

public let htmlString: (String) -> NSAttributedString? = { html in
    let _data = Data(html.utf8)
    if let attributedString = try? NSAttributedString(
        data: _data,
        options: [.documentType: NSAttributedString.DocumentType.html],
        documentAttributes: nil) {
        return attributedString
    }
    return nil
}

