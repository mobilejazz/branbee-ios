import UIKit
import Library
import BrModels
import ComposableArchitecture
import ImageLoader
import ReactiveSwift

private let donateButtonStyle: (UIButton) -> Void =
    heightAnchor(.br_grid(14))
    <> roundedStyle(cornerRadius: .br_grid(2))
    <> {
        $0.titleLabel?.font = UIFont.py_headline(size: 14)
        $0.setTitle(String("Donate"))
        $0.backgroundColor = UIColor.mainColor
        $0.setTitleColor(UIColor.white, for: .normal)
    }

private let orgLabelStyle: (UILabel) -> Void = {
    $0.textColor = .mainColor
    $0.font = UIFont.py_body(size: .br_grid(4)).bolded
}

private let membersLabelStyle: (UILabel) -> Void =
    brGrayTextColor
    <> {
        $0.font = .py_caption1(size: .br_grid(3))
    }

private let titleStackViewStyle: (UIStackView) -> Void =
    verticalStackStyle(.br_grid(0),
                       distribution: .fill,
                       alignment: .leading
    ) <> marginStyle(.square(.br_grid(4)))

private let topStackViewStyle: (UIStackView) -> Void =
    horizontalStackStyle(.br_grid(4),
                         distribution: .fill,
                         alignment: .center)
    <> marginStyle(.rectangle(v: .br_grid(2), h: .br_grid(4)))

private let storyStackViewStyle: (UIStackView) -> Void =
    verticalStackStyle(.br_grid(1),
                       distribution: .fill,
                       alignment: .leading)
    <> marginStyle(.leading(.br_grid(4)))


private let projectImageViewStyle: (UIImageView) -> Void = heightAnchor(.br_grid(53))
    <> {
        $0.image = UIImage(.blue)
    }

private let projectBodyLabelStyle: (UILabel) -> Void =
    brGrayTextColor <> {
        $0.textAlignment = .left
        $0.numberOfLines = 0
        $0.font = .py_body(size: .br_grid(4))
        $0.setContentCompressionResistancePriority(.defaultLow, for: .vertical)
    }

private let projectTitleLabelStyle: (UILabel) -> Void =
    brDarkTextColor <> {
        $0.textAlignment = .left
        $0.numberOfLines = 1
        $0.font = .py_headline(size: .br_grid(5))
    }

private let bottomStackViewStyle: (UIStackView) -> Void = horizontalStackStyle(.br_grid(3))
    <> marginStyle(.rectangle(v: .br_grid(2), h: .br_grid(4)))

private let dateLabelStyle: (UILabel) -> Void = brGrayTextColor <> {
    $0.font = .py_caption1(size: .br_grid(3))
}

private let commentsIconStyle: (UIImageView) -> Void =
    squareStyle(.br_grid(5)) <> {
        $0.image = UIImage(named: "ic_chat_off")
        $0.tintColor = .black
    }

private let commentsLabelStyle: (UILabel) -> Void = brLabelDarkStyle <> {
    $0.font = .py_body(size: 14)
}

public let componentStackViewStyle: (UIStackView) -> Void = horizontalStackStyle(.br_grid(2))

private let donateStackViewStyle: (UIStackView) -> Void = verticalStackStyle(.br_grid(3), alignment: .fill)
<> marginStyle()

private let rootStackViewStyle: (UIStackView) -> Void =
    verticalStackStyle(0)

private let makeDonationLabelStyle: (UILabel) -> Void = brLabelDarkStyle <> {
    $0.font = UIFont.py_headline(size: .br_grid(4)).bolded
    $0.textAlignment = .left
    $0.text = "Make Donation"
}

    
private let spacerStyle: (UIView) -> Void = autoLayoutStyle <> {
    $0.backgroundColor = .lightGray
} <> heightAnchor(1)

public struct ProjectDetailCellState: Equatable {
    let project: BrProject
    var projectImage: UIImage?
    var progress: Float
    public init(
        project: BrProject,
        projectImage: UIImage? = nil,
        progress: Float = .zero
    ) {
        self.project = project
        self.projectImage = projectImage
        self.progress = progress
    }
    
}

public enum ProjectDetailCellAction: Equatable {
    case load
    case projectImage(UIImage?)
    case donateButtonTapped
}


extension BrProject {
    var photoURL: URL? {
        guard let picture = picture else { return nil }
        return URL(string: picture)
    }
}

public let projectDetailCellReducer = Reducer<ProjectDetailCellState, ProjectDetailCellAction, SystemEnvironment<ImageLoader>> { state, action, environment in
    switch action {
    case .load:
        return state.project.photoURL
            .map(environment.loadFromURL)?
            .start(on: environment.globalQueue())
            .observe(on: environment.mainQueue())
            .map(ProjectDetailCellAction.projectImage)
            ?? .none
    case let .projectImage(image):
        state.projectImage = image
        return .none
    case .donateButtonTapped:
        return .none
    }
}

private let progressStackViewStyle: (UIStackView) -> Void =
    verticalStackStyle(.br_grid(1))
    <> marginStyle(.square(.br_grid(4)))

public class ProjectDetailCell: UICollectionViewCell {
    
    public class var reuseIdentifier: String { "ProjectDetailCell" }
        
    var viewStore: ViewStore<ProjectDetailCellState, ProjectDetailCellAction>!
    
    public func configure(_ store: Store<ProjectDetailCellState, ProjectDetailCellAction>) {
        
        viewStore = ViewStore(store)
        
        viewStore.send(.load)
                       
        viewStore.produced.projectImage
            .startWithValues { image in
                self.projectImageView.image = image
                self.projectImageView.isHidden = image == nil 
            }
                        
        let projectViewStore = ViewStore(store.scope(state: \.project))
        
        projectViewStore.produced.title
            .map(id)
            .assign(to: \.text, on: projectTitleLabel)
        
        projectViewStore.produced.date
            .map(brDateFormatter().string(from:))
            .assign(to: \.text, on: dateLabel)
        
        projectViewStore.produced.body
            .map(id)
            .map {
                try? NSAttributedString(
                    htmlString: $0,
                    font: .py_body(size: 14),
                    useDocumentFontSize: false
                )
            }
            .assign(to: \.attributedText, on: projectBodyLabel)
                        
        projectViewStore.produced.comments
            .map(String.init)
            .assign(to: \.text, on: commentsLabel)
        
        projectViewStore.produced.community
            .map(\.name)
            .map(id)
            .assign(to: \.text, on: orgLabel)
        
        projectViewStore.produced.community
            .map { "\($0.members) " + "members" }
            .assign(to: \.text, on: membersLabel)
        
        projectViewStore.produced.producer
            .map { ($0.pledged, $0.target) }
            .map { pledged, target in
                return "\(pledged)$ pledged of \(target)$ goal"
            }.assign(to: \.text, on: plegdeLabel)
                
        projectViewStore.produced.producer
            .map { Float($0.pledged) / Float($0.target) }
            .assign(to: \.progress, on: progressView)
        
    }
    
    let dateLabel = UILabel()
    let commentsLabel = UILabel()
    let membersLabel = UILabel()
    let orgLabel = UILabel()
    let projectTitleLabel = EdgeLabel(edge: .horizontal(.br_grid(4)))
    let projectBodyLabel = EdgeLabel(edge: .topHorizontalEdge(.br_grid(4)))
    let progressView = UIProgressView()
    let projectImageView = UIImageView()
    let plegdeLabel = UILabel()
            
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = .white
                                        
        orgLabelStyle(orgLabel)
        membersLabelStyle(membersLabel)
                        
        let titleStackView = UIStackView(arrangedSubviews: [
            orgLabel,
            membersLabel
        ])
        
        titleStackViewStyle(titleStackView)
                                                              
        projectImageViewStyle(projectImageView)
        projectBodyLabelStyle(projectBodyLabel)
        projectTitleLabelStyle(projectTitleLabel)
                               
        dateLabelStyle(dateLabel)
        
        let commentsIcon = UIImageView()
        commentsIconStyle(commentsIcon)
                
        commentsLabelStyle(commentsLabel)
        
        let commentsStackView = UIStackView(arrangedSubviews: [
            commentsIcon,
            commentsLabel
        ])
        componentStackViewStyle(commentsStackView)
                
        let makeDonationLabel = UILabel()
        makeDonationLabelStyle(makeDonationLabel)
        
        let donateButton = UIButton()
        donateButtonStyle(donateButton)
        donateButton.addTarget(self,
                              action: #selector(donateButtonTapped),
                              for: .touchUpInside
        )
                   
        let donateStackView = UIStackView(arrangedSubviews: [
            makeDonationLabel,
            donateButton
        ])
        donateStackViewStyle(donateStackView)
        
        let bottomStackView = UIStackView(arrangedSubviews: [
            dateLabel,
            commentsStackView
        ])
        bottomStackViewStyle(bottomStackView)
        
        progressViewStyle(progressView)
        plegdeLabelStyle(plegdeLabel)
        let progressStackView = UIStackView(arrangedSubviews: [
            progressView,
            plegdeLabel
        ])
        progressStackViewStyle(progressStackView)
        
            
        let spacer = UIView()
        spacer.backgroundColor = .lightGray
        spacer.heightAnchor.constraint(equalToConstant: 1).isActive = true
        let rootStackView = UIStackView(arrangedSubviews: [
            titleStackView,
            projectImageView,
            progressStackView.pinBackground(.blueSecondColor),
            donateStackView.pinBackground(.white),
            projectTitleLabel,
            projectBodyLabel,
            UIView(),
            bottomStackView,
            spacer            
        ])
        
        rootStackViewStyle(rootStackView)
        
        self.addSubview(rootStackView)
        rootStackView.constrainEdges(to: self)
                
    }
    
    @objc func donateButtonTapped() {
        viewStore.send(.donateButtonTapped)
    }
    
    public override func prepareForReuse() {
        super.prepareForReuse()
    }
                          
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


//let height = heightForText("BrProject.branbee.body", width: .br_grid(80), font: .py_body(size: .br_grid(4)))
//height + .br_grid(140)
