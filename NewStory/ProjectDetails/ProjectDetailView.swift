import UIKit
import Library
import BrModels
import ComposableArchitecture
import ImageLoader
import ReactiveSwift
import ExploreClient

@propertyWrapper
public enum Indirect<T>: Equatable where T: Equatable {
    indirect case storage(T)
    public init(wrappedValue: T) {
        self = .storage(wrappedValue)
    }
    public var wrappedValue: T {
        get {
            switch self {
            case .storage(let value):
                return value
            }
        }
        set {
            self = .storage( newValue )
        }
    }
}

extension ProjectDetailState {
    public static func == (lhs: ProjectDetailState, rhs: ProjectDetailState) -> Bool {
        lhs.isEqual(rhs)
    }

    public func isEqual (_ other: ProjectDetailState) -> Bool {
        self.project == other.project &&
        self.sections == other.sections &&
        self.error == other.error &&
        self.loadingState == other.loadingState &&
        self.linkStory == other.linkStory &&
        self.donate == other.donate &&
        self.storyDetail == other.storyDetail &&
        self.commentsState == other.commentsState &&
        self.isLinkButtonShown == other.isLinkButtonShown &&
        self.focusedIndexPaths == other.focusedIndexPaths &&
        self.isSubscribed == other.isSubscribed
    }
    
}


public struct ProjectDetailStateCopy: Equatable {
    public let project: BrProject
    public var sections: [ProjectDetailSectionType]
    public var error: Failure?
    public var loadingState: LoadingState?
    public var linkStory: LinkStoryState?
    public var donate: DonateState?
    public var storyDetail: StoryDetailState? = nil
    
    public var detail: StoryDetailStateCopy? {
        get {
            storyDetail.map(StoryDetailStateCopy.init)
        }
        set {
            storyDetail = newValue?.value
        }
    }
    
    public var commentsState: CommentsState? = nil
    public var isLinkButtonShown: Bool
    public var focusedIndexPaths: [IndexPath] = []
    public var isSubscribed: Bool = false
    public init(_ state: ProjectDetailState) {
        self.project = state.project
        self.sections = state.sections
        self.error = state.error
        self.loadingState = state.loadingState
        self.linkStory = state.linkStory
        self.donate = state.donate
        self.storyDetail = state.storyDetail
        self.commentsState = state.commentsState
        self.isLinkButtonShown = state.isLinkButtonShown
        self.focusedIndexPaths = state.focusedIndexPaths
        self.isSubscribed = state.isSubscribed
    }    
    
}

extension ProjectDetailStateCopy {
    public var value: ProjectDetailState {
        .init(
            project: project,
            sections: sections,
            error: error,
            loadingState: loadingState,
            linkStory: linkStory,
            donate: donate,
            storyDetail: storyDetail,
            commentsState: commentsState,
            isLinkButtonShown: isLinkButtonShown,
            focusedIndexPaths: focusedIndexPaths,
            isSubscribed: isSubscribed
        )
    }
}

public class ProjectDetailState: NSObject {
    
    public let project: BrProject
    public var sections: [ProjectDetailSectionType]
    public var error: Failure?
    public var loadingState: LoadingState?
    public var linkStory: LinkStoryState?
    public var donate: DonateState?
    public var storyDetail: StoryDetailState? = nil
    
    public var detail: StoryDetailStateCopy? {
        get {
            storyDetail.map(StoryDetailStateCopy.init)
        }
        set {
            storyDetail = newValue?.value
        }
    }

    public var commentsState: CommentsState? = nil
    public var isLinkButtonShown: Bool
    public var focusedIndexPaths: [IndexPath] = []
    public var isSubscribed: Bool = false
    public init(
        project: BrProject,
        sections: [ProjectDetailSectionType] = [],
        error: Failure? = nil,
        loadingState: LoadingState? = nil,
        linkStory: LinkStoryState? = nil,
        donate: DonateState? = nil,
        storyDetail: StoryDetailState? = nil,
        commentsState: CommentsState? = nil,
        isLinkButtonShown: Bool = false,
        focusedIndexPaths: [IndexPath] = [],
        isSubscribed: Bool = false
    ) {
        self.project = project
        self.sections = sections
        self.error = error
        self.loadingState = loadingState
        self.linkStory = linkStory
        self.donate = donate
        self.storyDetail = storyDetail
        self.commentsState = commentsState
        self.isLinkButtonShown = isLinkButtonShown
        self.focusedIndexPaths = focusedIndexPaths
        self.isSubscribed = isSubscribed
    }
}

extension ProjectDetailState {
    var projectDetail: ProjectDetailCellState? {
        get {
            sections
                .compactMap(/ProjectDetailSectionType.project)
                .first
        }
        set {
            guard let newValue = newValue else { return }
            if let index = sections.firstIndex(where: { $0.type == .project }) {
                sections[index] = .project(newValue)
            }
        }
    }
}

extension ProjectDetailState {
    var stories: [BrStory] {
        get {
            sections
                .compactMap(/ProjectDetailSectionType.stories)
                .last ?? []
        }
        set {
            if let index = sections.firstIndex(where: { $0.type == .stories }) {
                sections[index] = .stories(newValue)
            }
        }
    }
}

extension ProjectDetailState {
    var comment: BrComment? {
        get {
            sections
                .compactMap(/ProjectDetailSectionType.comments)
                .first
        }
        set {
            guard let newValue = newValue else { return }
            if let index = sections.firstIndex(where: { $0.type == .comments }) {
                sections[index] = .comments(newValue)
            }
        }
    }
}

extension ProjectDetailStateCopy {
    var projectDetail: ProjectDetailCellState? {
        get {
            sections
                .compactMap(/ProjectDetailSectionType.project)
                .first
        }
        set {
            guard let newValue = newValue else { return }
            if let index = sections.firstIndex(where: { $0.type == .project }) {
                sections[index] = .project(newValue)
            }
        }
    }
}

extension ProjectDetailStateCopy {
    var stories: [BrStory] {
        get {
            sections
                .compactMap(/ProjectDetailSectionType.stories)
                .last ?? []
        }
        set {
            if let index = sections.firstIndex(where: { $0.type == .stories }) {
                sections[index] = .stories(newValue)
            }
        }
    }
}

extension ProjectDetailStateCopy {
    var comment: BrComment? {
        get {
            sections
                .compactMap(/ProjectDetailSectionType.comments)
                .first
        }
        set {
            guard let newValue = newValue else { return }
            if let index = sections.firstIndex(where: { $0.type == .comments }) {
                sections[index] = .comments(newValue)
            }
        }
    }
}

public enum ProjectDetailAction: Equatable {
    case viewDidLoad
    case viewDidAppear
    case viewDismissed
    case viewDidDisppear
    case viewCommentsTapped
    case linkActionTapped
    case shareButtonTapped
    case viewStory(BrStory)
    case event(ProjectDetailsDelegateEvent)
    case storyCell(index: Int, action: StoryCellAction)
    case commentCell(CommentCellAction)
    case detailCell(ProjectDetailCellAction)
    case linkStory(LinkStoryAction)
    case donate(DonateAction)
    case storyDetail(StoryDetailAction)
    case comments(CommentsAction)
}

public struct ProjectDetailEnvironment {
    var projectDetailClient: (String) -> ProjectDetailsClient
    var linkStoryClient: (String, String) -> LinkStoryClient
    var donateClient: (_ projectId: String, _ projectName: String) -> DonateClient
    let addCardClient: AddCardClient
    var imageLoader: ImageLoader
    var storyDetailClient: (String) -> StoryDetailClient
    var linkProjectClient: (String, String) -> LinkProjectClient
    var commentsClient: (String, BrComment.´Type´) -> CommentsClient
    let stripeClient: StripeClient
    public init(
        projectDetailClient: @escaping (String) -> ProjectDetailsClient,
        linkStoryClient: @escaping (String, String) -> LinkStoryClient = { _,_ in .empty },
        donateClient: @escaping (_ projectId: String, _ projectName: String) -> DonateClient = { _,_ in .empty },
        addCardClient: AddCardClient = .empty,
        imageLoader: ImageLoader,
        storyDetailClient: @escaping (String) -> StoryDetailClient = { _ in .empty },
        linkProjectClient: @escaping (String, String) -> LinkProjectClient = { _,_ in .empty },
        commentsClient: @escaping (String, BrComment.´Type´) -> CommentsClient = { _,_ in .empty },
        stripeClient: StripeClient
    ) {        
        self.donateClient = donateClient
        self.linkStoryClient = linkStoryClient
        self.projectDetailClient = projectDetailClient
        self.addCardClient = addCardClient
        self.imageLoader = imageLoader
        self.storyDetailClient = storyDetailClient
        self.linkProjectClient = linkProjectClient
        self.commentsClient = commentsClient
        self.stripeClient = stripeClient
    }
}

public let projectDetailReducer = Reducer<ProjectDetailStateCopy, ProjectDetailAction, SystemEnvironment<ProjectDetailEnvironment>> .combine(
        Reducer { state, action, environment in
            struct CancelId: Hashable {}
            switch action {
            case .viewDidLoad:
                state.isSubscribed = true
                return .merge(
                    environment.projectDetailClient(state.project.id)
                        .delegate
                        .cancellable(id: CancelId())
                        .map(ProjectDetailAction.event),
                    environment.projectDetailClient(state.project.id)
                        .viewDidLoad
                        .start(on: environment.mainQueue())
                        .fireAndForget()
                )
            case .viewDidAppear:
                guard !state.isSubscribed else {
                    return .none
                }
                return environment.projectDetailClient(state.project.id)
                    .delegate
                    .observe(on: environment.mainQueue())
                    .cancellable(id: CancelId())
                    .map(ProjectDetailAction.event)
            case .viewDismissed:
                state.loadingState = nil
                state.error = nil
                state.linkStory = nil
                state.donate = nil
                state.comment = nil
                state.commentsState = nil
                state.storyDetail = nil
                return .none            
            case let .event(.displayProject(project, comment: comment, linkedStories: stories, isLinkedButtonShown: isLinkedButtonShown)):
                state.isLinkButtonShown = isLinkedButtonShown
                state.sections = [
                    .project(ProjectDetailCellState(project: project)),
                    .comments(comment),
                    .stories(stories)
                ]
                return .none
            case let .event(.displayFailure(failure, retryAction: action)):
                state.loadingState = .failure(failure, action: action)
                return .none
            case .detailCell(.donateButtonTapped):
                state.donate = DonateState(project: state.project)
                return .none
            case let .storyCell(index: index, action: .readMoreTapped):
                state.focusedIndexPaths.append(
                    IndexPath(item: index, section: 2)
                )
//                for indice in state.stories.indices {
//                    state.stories[indice].isFocused = index == indice
//                }
                return .none
            case let .storyCell(index: index, action: .commentsButtonTapped):
                let story = state.sections.compactMap(/ProjectDetailSectionType.stories)
                    .compactMap { $0[safe: index] }
                    .first
                guard let mStory = story else { return .none }
                state.commentsState = CommentsState(
                    parentId: mStory.id,
                    parentType: .story
                )
                return .none
            case .storyCell, .commentCell, .detailCell, .linkStory:
                return .none
            case .viewCommentsTapped:
                state.commentsState = CommentsState(
                    parentId: state.project.id,
                    parentType: .project
                )
                return .none
            case .linkActionTapped:
                state.linkStory = LinkStoryState(
                    projectId: state.project.id,
                    communityId: state.project.community.id
                )
                return .none            
            case .shareButtonTapped:
                return .none
            case .viewDidDisppear:
                state.isSubscribed = false
                return .cancel(id: CancelId())
            case .donate:
                return .none
            case .storyDetail(_):
                return .none
            case let .viewStory(story):
                state.storyDetail = .init(story: story)
                return .none
            case .comments(_):
                return .none
            case .event(.showLoading):
                state.loadingState = .loading
                return .none
            case .event(.hideLoading):
                state.loadingState = nil
                return .none
            }
        },
        storyDetailReducer.optional().pullback(
            state: \.detail,
            action: /ProjectDetailAction.storyDetail,
            environment: {
                $0.map {
                    StoryDetailEnvironment(
                        storyDetailClient: $0.storyDetailClient,
                        linkProjectClient: $0.linkProjectClient,
                        commentsClient: $0.commentsClient,
                        projectDetailClient: $0.projectDetailClient,
                        donateClient: $0.donateClient,
                        linkStoryClient: $0.linkStoryClient,
                        addCardClient: $0.addCardClient,
                        imageLoader: $0.imageLoader,
                        stripeClient: $0.stripeClient
                    )
                }
            }
        ),
        storyCellReducer.forEach(
            state: \.stories,
            action: /ProjectDetailAction.storyCell(index:action:),
            environment: { $0.map(\.imageLoader) }
        ),
        projectDetailCellReducer.optional().pullback(
            state: \.projectDetail,
            action: /ProjectDetailAction.detailCell,
            environment:  { $0.map(\.imageLoader) }
        ),
        commentCellReducer.optional().pullback(
            state: \.comment,
            action: /ProjectDetailAction.commentCell,
            environment: { $0.map(\.imageLoader) }
        ),
        linkStoryReducer.optional().pullback(
            state: \.linkStory,
            action: /ProjectDetailAction.linkStory,
            environment: {
                $0.map {
                    LinkStoryEnvironment(
                        linkStoryClient: $0.linkStoryClient,
                        imageLoader: $0.imageLoader
                    )
                }
            }
        ),
        donationReducer.optional().pullback(
            state: \.donate,
            action: /ProjectDetailAction.donate,
            environment: {
                $0.map {
                    DonateEnvironment(
                        donateClient: $0.donateClient,
                        addCardClient: $0.addCardClient,
                        stripeClient: $0.stripeClient
                    )
                }
            }
        ),
        commentsReducer.optional().pullback(
            state: \.commentsState,
            action: /ProjectDetailAction.comments,
            environment: {
                $0.map {
                    CommentsEnvironment(
                        commentsClient: $0.commentsClient,
                        imageLoader: $0.imageLoader
                    )
                }
            }
        )
    )
//}

import ComposableArchitecture
public class ProjectDetailViewController: UIViewController {
    
    let store: Store<ProjectDetailState, ProjectDetailAction>
    let viewStore: ViewStore<ProjectDetailState, ProjectDetailAction>
    
    public init(store: Store<ProjectDetailState, ProjectDetailAction>) {
        self.store = store
        self.viewStore = ViewStore(store, removeDuplicates: ==)
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var collectionView: UICollectionView!
    var datasource: [ProjectDetailSectionType] = [] {
        didSet {
            collectionView.reloadData()
        }
    }       
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Story Detail"
        
        viewStore.send(.viewDidLoad)
                                
        let layout = UICollectionViewFlowLayout()
        collectionFlowLayoutStyle(layout)
        collectionView = UICollectionView(
            frame: .zero,
            collectionViewLayout: layout
        )
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionViewStyle(collectionView)
        self.view.addSubview(collectionView)
        collectionView.constrainEdges(to: view)
                            
        let spinner = LoadingView()
        self.view.addSubview(spinner)
        spinner.frame = view.frame
        self.view.bringSubviewToFront(spinner)
        
        viewStore.produced.loadingState
            .startWithValues { state in
                spinner.state = state
                spinner.isHidden = state == nil
            }        
        
        viewStore.produced.sections
            .observe(on: QueueScheduler.main)
            .startWithValues { [weak self] sections in
                guard let self = self else { return }
                self.datasource = sections
                self.title = sections
                    .compactMap(/ProjectDetailSectionType.project)
                    .map(\.project.title)
                    .first
            }
        
        // Link Story
        store.scope(
            state: \.linkStory,
            action: ProjectDetailAction.linkStory
        ).ifLet(then: { [weak self] store in
            guard let self = self else { return }
            self.navigationController?.pushViewController(LinkStoryViewController(store: store), animated: true)
        }) {
            self.navigationController?.popToViewController(self, animated: true)
        }
        
        // Donate
        store.scope(
            state: \.donate,
            action: ProjectDetailAction.donate
        ).ifLet(then: { [weak self] store in
            guard let self = self else { return }
            self.navigationController?.pushViewController(DonateViewController(store: store), animated: true)
        }) {
            self.navigationController?.popToViewController(self, animated: true)
        }
        
        // Comments
        store.scope(
            state: \.commentsState,
            action: ProjectDetailAction.comments
        ).ifLet(then: { [weak self] store in
            guard let self = self else { return }
            self.navigationController?.pushViewController(CommentsViewController(store: store), animated: true)
        }) {
            self.navigationController?.popToViewController(self, animated: true)
        }
        
        // Story Details
        store.scope(
            state: \.storyDetail,
            action: ProjectDetailAction.storyDetail
        ).ifLet(then: { [weak self] store in
            guard let self = self else { return }
            self.navigationController?.pushViewController(StoryDetailViewController(store: store), animated: true)
        }) {
            self.navigationController?.popToViewController(self, animated: true)
        }
                    
        let shareButton = UIButton()
        shareButtonStyle(shareButton)
        shareButton.addTarget(self, action: #selector(shareButtonTapped), for: .touchUpInside)
                        
        self.navigationItem.rightBarButtonItems = [ shareButton].map(UIBarButtonItem.init(customView:))
        
    }
           
    @objc func shareButtonTapped() {
        self.viewStore.send(.shareButtonTapped)
    }
    
    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewStore.send(.viewDidAppear)
        if !isMovingToParent {
            viewStore.send(.viewDismissed)
        }
    }
    
    public override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        viewStore.send(.viewDidDisppear)
    }
}


private let collectionViewStyle: (UICollectionView) -> Void =
    autoLayoutStyle <> {
        
        $0.register(CommentViewCell.self,
                    forCellWithReuseIdentifier: CommentViewCell.reuseIdentifier)
        
        $0.register(ProjectDetailCell.self,
                    forCellWithReuseIdentifier: ProjectDetailCell.reuseIdentifier)
        
        $0.register(StoryViewCell.self,
                    forCellWithReuseIdentifier: StoryViewCell.reuseIdentifier)
        $0.register(
            ViewCommentsFooter.self,
            forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter,
            withReuseIdentifier: ViewCommentsFooter.reuseIdentifier
        )
        $0.register(
            LinkActionFooter.self,
            forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter,
            withReuseIdentifier: LinkActionFooter.reuseIdentifier
        )
                      
        $0.register(
            LinkActionHeader.self,
            forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader,
            withReuseIdentifier: LinkActionHeader.reuseIdentifier
        )
 
        $0.backgroundColor = .white
        $0.contentInset = .bottomEdge(.br_grid(2))
    }

private let collectionFlowLayoutStyle: (UICollectionViewFlowLayout) -> Void = {
    $0.scrollDirection = .vertical
    $0.sectionHeadersPinToVisibleBounds = true
    $0.minimumLineSpacing = .br_grid(4)
    $0.minimumInteritemSpacing = .br_grid(4)
    $0.sectionInsetReference = .fromContentInset
}

extension ProjectDetailViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch datasource[indexPath.section].type {
        case .stories:
            guard let story = viewStore.stories[safe: indexPath.row] else { return }
            viewStore.send(.viewStory(story))
        default:
            return
        }
    }
    
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        datasource[section].numberOfItems
    }
    
    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        return datasource.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
                
        switch datasource[indexPath.section] {
        case let .comments(.some(commentState)):
            guard let commentCell = collectionView.dequeueReusableCell(
                withReuseIdentifier: CommentViewCell.reuseIdentifier,
                for: indexPath
            ) as? CommentViewCell else {
                fatalError()
            }
            commentCell.configure(store.scope(
                state:  { $0.comment
                    ?? commentState
                },
                action: ProjectDetailAction.commentCell
            ))
            return commentCell
            
        case let .project(projectState):
            guard let projectDetailCell = collectionView.dequeueReusableCell(
                withReuseIdentifier: ProjectDetailCell.reuseIdentifier,
                for: indexPath
            ) as? ProjectDetailCell else {
                fatalError()
            }
            projectDetailCell.configure(store.scope(
                state:  { $0.projectDetail ?? projectState },
                action: ProjectDetailAction.detailCell
            ))
            return projectDetailCell
        case .stories:
            guard let storyCell = collectionView.dequeueReusableCell(
                withReuseIdentifier: StoryViewCell.reuseIdentifier,
                for: indexPath
            ) as? StoryViewCell else {
                fatalError()
            }
            
            storyCell.configure(store.scope(
                state: {
                    $0.stories[safe: indexPath.row] ?? BrStory.salma
                },
                action: { .storyCell(index: indexPath.item, action: $0) }
            ))
            
            
            return storyCell
        case .comments(.none):
            guard let commentCell = collectionView.dequeueReusableCell(
                withReuseIdentifier: CommentViewCell.reuseIdentifier,
                for: indexPath
            ) as? CommentViewCell else {
                fatalError()
            }
            commentCell.isHidden = true
            return commentCell
        }
    }
}

public enum ProjectDetailSectionType: Equatable {
    case project(ProjectDetailCellState)
    case comments(BrComment?)
    case stories([BrStory])
           
    
    public enum Kind {
        case project, comments, stories
    }
    
    public var type: Kind {
        switch self {
        case .comments:
            return .comments
        case .project:
            return .project
        case .stories:
        return .stories
        }
    }
    
    public var sectionHeaderHeight: CGFloat {
        switch self {
        case let .stories(stories):
            return stories.isEmpty
                ? .zero
                : .br_grid(12)
        default:
            return .zero
        }
    }
    
    public var numberOfItems: Int {
        switch self {
        case .comments(.none):
            return .zero
        case .comments, .project:
            return 1
        case let .stories(stories):
            return stories.count
        }
    }
    
    public var sectionFooterHeight: CGFloat {
        switch self {
        case .project:
            return .zero
        case .comments(.none):
            return .br_grid(0)
        case .comments(.some):
            return .br_grid(10)
        case .stories:
            return .br_grid(14)
        }
    }
    
    public func cellHeight(_ width: CGFloat, indexPath: IndexPath, isFocused: Bool = false) -> CGFloat {
        switch self {
        case let .project(projectState):
            return heightForText(
                projectState.project.body, width: width)                
                + (projectState.project.photoURL == nil ? .zero: .br_grid(52))
                + .br_grid(88)
        case let .comments(.some(commentState)):
            return heightForText(
                commentState.body,
                width: width,
                font: .py_body(size: 16)
            ) + .br_grid(15)
        case .comments(.none):
            return .br_grid(0)
        case let .stories(storiesStates):
            let content = storiesStates[indexPath.row]
                .content
                .filter { $0.type == .text }
                .first?.content ?? ""
                                    
            return isFocused
             ? heightForText(content, width: width) + .br_grid(80)
             : .br_grid(100)
        }
    }
    
    public var hasSectionFooter: Bool {
        switch self {
        case .stories:
            return false
        default:
            return true
        }
    }
    
    public var hasSectionHeader: Bool {
        switch self {
        case .stories:
            return true
        default:
            return false
        }
    }
    
}

extension ProjectDetailViewController {
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        let cellType = viewStore.sections[section]
        return .init(
            width: collectionView.bounds.width - .br_grid(8),
            height: cellType.sectionHeaderHeight
        )
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        let cellType = viewStore.sections[section]
        return .init(
            width: collectionView.bounds.width - .br_grid(8),
            height: cellType.sectionFooterHeight
        )
    }
    
    public func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let cellType = viewStore.sections[indexPath.section]
        if kind == UICollectionView.elementKindSectionHeader {
            if cellType.hasSectionHeader {
                guard let cell = collectionView.dequeueReusableSupplementaryView(
                    ofKind: UICollectionView.elementKindSectionHeader,
                    withReuseIdentifier: LinkActionHeader.reuseIdentifier,
                    for: indexPath
                ) as? LinkActionHeader else {
                    fatalError()
                }
                cell.titleLabel.text = "Link Story"
                return cell
            }
        }
        if kind == UICollectionView.elementKindSectionFooter {
            switch cellType {
            case .comments(_):
                guard let cell = collectionView.dequeueReusableSupplementaryView(
                    ofKind: UICollectionView.elementKindSectionFooter,
                    withReuseIdentifier: ViewCommentsFooter.reuseIdentifier,
                    for: indexPath
                ) as? ViewCommentsFooter else {
                    fatalError()
                }
                cell.action = {
                    print("View Comments")
                    self.viewStore.send(.viewCommentsTapped)
                }
                return cell
            case .stories(_):
                guard let cell = collectionView.dequeueReusableSupplementaryView(
                    ofKind: UICollectionView.elementKindSectionFooter,
                    withReuseIdentifier: LinkActionFooter.reuseIdentifier,
                    for: indexPath
                ) as? LinkActionFooter else {
                    fatalError()
                }
                
                cell.linkActionsButton.setTitle("Link Stories")
                
                cell.action = {
                    self.viewStore.send(.linkActionTapped)
                }
                return cell
            case .project:
                return .init()
            }
            
        }
        fatalError()
    }
    
}


extension ProjectDetailViewController: UICollectionViewDelegateFlowLayout {
            
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellType = viewStore.sections[indexPath.section]
        
        let cellWidth =
            collectionView.bounds.width - .br_grid(
            indexPath.section == .zero ? 0 : 8
        )
        return CGSize(
            width: cellWidth,
            height: cellType.cellHeight(cellWidth, indexPath: indexPath, isFocused:  false)
        )
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return section == .zero
            ? .square(.br_grid(0))
            : section == 2 ? .bottomHorizontalEdge(.br_grid(4)): .square(.br_grid(4))
    }
                
}
