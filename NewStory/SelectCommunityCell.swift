import Library
import UIKit
import ComposableArchitecture
import BrModels
import SDWebImage

private let organizationLabelStyle: (UILabel) -> Void = brLabelStyle <> {
    $0.font = .py_caption1(size: .br_grid(3))
}

private let bigTitleLabelStyle: (UILabel) -> Void =
    brLabelStyle <> {
        $0.numberOfLines = 3
        $0.font = UIFont.py_title2(size: .br_grid(5)).bolded
    }

private let backgroundImageViewStyle: (UIImageView) -> Void = autoLayoutStyle <> {
    $0.backgroundColor = .blueSecondColor
}

private let actionsIconStyle: (UIImageView) -> Void =
    squareStyle(.br_grid(5)) <> {
        $0.image = UIImage(named: "ic_actions_small")
        $0.contentMode = .scaleAspectFit
    }

private let storiesIconStyle: (UIImageView) -> Void =
    squareStyle(.br_grid(5)) <> {
        $0.image = UIImage(named: "ic_stories_small")
        $0.contentMode = .scaleAspectFit
    }

private let membersIconStyle: (UIImageView) -> Void =
    squareStyle(.br_grid(5)) <> {
        $0.image = UIImage(named: "ic_community_small")
        $0.contentMode = .scaleAspectFit
    }

private let componentLabelStyle: (UILabel) -> Void = brLabelStyle <> {
    $0.font = .py_caption1(size: 14)
}

private let comStackViewStyle: (UIStackView) -> Void = horizontalStackStyle(.br_grid(2), alignment: .center)

private let rootStackViewStyle: (UIStackView) -> Void =
    verticalStackStyle(.br_grid(4))
    <> marginStyle()

private let bottomStackViewStyle: (UIStackView) -> Void =
    horizontalStackStyle(.br_grid(1), distribution: .fillEqually)

private let cellStyle: (SelectCommunityViewCell) -> Void =
    roundedStyle(cornerRadius: .br_grid(2))

public class SelectCommunityViewCell: UICollectionViewCell {

    let membersLabel = UILabel()
    let actionsLabel = UILabel()
    let storiesLabel = UILabel()
    let bigTitleLabel = UILabel()
    let backgroundImageView = UIImageView()
    
    public class var reuseIdentifier: String {
        "SelectCommunityViewCell"
    }
    
    public func configure(_ store: Store<BrCommunity, SelectCommunityCellAction>) {
        
        let viewStore = ViewStore(store)
                        
        viewStore.produced.actions
            .map(String.init)
            .assign(to: \.text, on: actionsLabel)
        
        viewStore.produced.members
            .map(String.init)
            .assign(to: \.text, on: membersLabel)
        
        viewStore.produced.stories
            .map(String.init)
            .assign(to: \.text, on: storiesLabel)
        
        viewStore.produced.name
            .map(id)
            .assign(to: \.text, on: bigTitleLabel)
        
        backgroundImageView.sd_setImage(
            with: URL(string: viewStore.state.picture)
        )
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
                                
        let overlayView = UIView()
        overlayViewStyle(overlayView)
        backgroundImageView.addSubview(overlayView)
        overlayView.constrainEdges(to: backgroundImageView)
        backgroundImageViewStyle(backgroundImageView)
        self.addSubview(backgroundImageView)
        backgroundImageView.constrainEdges(to: self)
                            
        bigTitleLabelStyle(bigTitleLabel)
                                
        let membersIcon = UIImageView()
        membersIconStyle(membersIcon)
        componentLabelStyle(membersLabel)
        
        let membersStackView = UIStackView(arrangedSubviews: [
            membersIcon,
            membersLabel
        ])
        comStackViewStyle(membersStackView)
        
        let storiesIcon = UIImageView()
        storiesIconStyle(storiesIcon)
        componentLabelStyle(storiesLabel)
        
        let storiesStackView = UIStackView(arrangedSubviews: [
            storiesIcon,
            storiesLabel
        ])
        comStackViewStyle(storiesStackView)
        
        let actionsIcon = UIImageView()
        actionsIconStyle(actionsIcon)
        componentLabelStyle(actionsLabel)
        let actionsStackView = UIStackView(arrangedSubviews: [
            actionsIcon, actionsLabel
        ])
        comStackViewStyle(actionsStackView)
                
        let bottomStackView = UIStackView(arrangedSubviews: [
            membersStackView,
            storiesStackView,
            actionsStackView
        ])
        
        bottomStackViewStyle(bottomStackView)
                
        let rootStackView = UIStackView(arrangedSubviews: [
            UIView(),
            bigTitleLabel,
            bottomStackView
        ])
        
        rootStackView |>
            rootStackViewStyle <> {
                $0.spacing = .br_grid(2)
            }
        backgroundImageView.addSubview(rootStackView)
        rootStackView.constrainEdges(to: backgroundImageView)
        
        cellStyle(self)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public override func prepareForReuse() {
        super.prepareForReuse()
    }
    
}
