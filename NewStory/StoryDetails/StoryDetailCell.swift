import UIKit
import Library
import BrModels
import ComposableArchitecture
import ImageLoader
import ReactiveSwift
import AVKit
import SDWebImage

private let personImageViewStyle: (UIImageView) -> Void =
    squareStyle(.br_grid(8))
    <> roundedStyle(cornerRadius: .br_grid(4))
    <> {
        $0.image = UIImage(UIColor.systemRed)
    }

private let joinedButtonStyle: (UIButton) -> Void =
    heightAnchor(.br_grid(9))
    <> widthAnchor(.br_grid(21))
    <> roundedStyle(cornerRadius: .br_grid(2))
    <> {
        $0.titleLabel?.font = UIFont.py_headline(size: 14)
        $0.setTitle(String("Joined"))
        $0.backgroundColor = UIColor.blueColor
        $0.setTitleColor(UIColor.white, for: .normal)
    }

private let menuButtonStyleStyle: (UIButton) -> Void =
    squareStyle(.br_grid(8))
    <> {
        $0.setImage(UIImage(named: "ic_more_vert"))
    }

private let orgLabelStyle: (UILabel) -> Void = {
    $0.textColor = .mainColor
    $0.font = UIFont.py_body(size: .br_grid(4)).bolded
}

private let personLabelStyle: (UILabel) -> Void =
    brGrayTextColor
    <> {
        $0.font = .py_caption1(size: .br_grid(3))
    }

private let titleStackViewStyle: (UIStackView) -> Void =
    verticalStackStyle(.br_grid(0),
                       distribution: .fill,
                       alignment: .leading
    )
private let topStackViewStyle: (UIStackView) -> Void =
    horizontalStackStyle(.br_grid(4),
                         distribution: .fill,
                         alignment: .center)
    <> marginStyle(.rectangle(v: .br_grid(2), h: .br_grid(4)))

private let storyStackViewStyle: (UIStackView) -> Void =
    verticalStackStyle(.br_grid(1),
                       distribution: .fill,
                       alignment: .leading)
    <> marginStyle(.leading(.br_grid(4)))


private let storyImageViewStyle: (UIImageView) -> Void = heightAnchor(.br_grid(53))

private let storyContentLabelStyle: (UILabel) -> Void =
    brGrayTextColor <> {
        $0.textAlignment = .left
        $0.numberOfLines = 0
        $0.font = .py_body(size: .br_grid(4))
        $0.setContentCompressionResistancePriority(.defaultHigh, for: .vertical)
    }

private let bottomStackViewStyle: (UIStackView) -> Void = horizontalStackStyle(.br_grid(3))
    <> marginStyle(.rectangle(v: .br_grid(2), h: .br_grid(4)))

private let dateLabelStyle: (UILabel) -> Void = brGrayTextColor <> {
    $0.font = .py_caption1(size: .br_grid(3))
}

private let commentsIconStyle: (UIImageView) -> Void =
    squareStyle(.br_grid(5)) <> {
        $0.image = UIImage(named: "ic_chat_off")
        $0.tintColor = .black
    }

private let componentLabelStyle: (UILabel) -> Void = brLabelDarkStyle <> {
    $0.font = .py_body(size: 14)
}

private let commentsStackViewStyle: (UIStackView) -> Void = horizontalStackStyle(.br_grid(2))

private let boostsStackViewStyle: (UIStackView) -> Void = verticalStackStyle(.br_grid(3), alignment: .fill)
<> marginStyle()

private let rootStackViewStyle: (UIStackView) -> Void =
    verticalStackStyle(.br_grid(1))

private let currentBoostLabelStyle: (UILabel) -> Void = {
    $0.font = .py_headline(size: .br_grid(8))
    $0.textAlignment = .center
}

private let boostButtonStyle: (UIButton) -> Void =
    brMainButtonStyle
    <> {
        $0.setImage(UIImage(named: "ic_boost_on"))
        $0.tintColor = .white
        $0.setTitle("BOOST STORY")
    } <> heightAnchor(.br_grid(15))

private let leftBoostsLabelStyle: (UILabel) -> Void = brDarkTextColor
    <> {
        $0.font = .py_body(size: 14)
        $0.textAlignment = .center
    }
    
private let spacerStyle: (UIView) -> Void = autoLayoutStyle <> {
    $0.backgroundColor = UIColor(white: 0.95, alpha: 1.0)
} <> heightAnchor(0.5)

public struct StoryDetailCellState: Hashable {
    let story: BrStory
    var creatorImage: UIImage?
    var storyImage: UIImage?
    var isBoostButtonEnabled: Bool = true
    var numberOfBoosts: Int
    var remainingBoosts: Int
    public init(
        story: BrStory,
        creatorImage: UIImage? = nil,
        storyImage: UIImage? = nil,
        numberOfBoosts: Int = .zero,
        remainingBoosts: Int = .zero
    ) {
        self.story = story
        self.creatorImage = creatorImage
        self.storyImage = storyImage
        self.numberOfBoosts = numberOfBoosts
        self.remainingBoosts = remainingBoosts
    }
    
}

public enum StoryDetailCellAction: Equatable {   
    case setBoosts(Int, left: Int)
    case setBoostButtonState(enabled: Bool)
    case boostButtonTapped
}

public let stroyDetailCellReducer = Reducer<StoryDetailCellState, StoryDetailCellAction, SystemEnvironment<ImageLoader>> { state, action, environment in
    switch action {
    case .boostButtonTapped:
        return .none
    case let .setBoosts(boosts, left: left):
        state.numberOfBoosts = boosts
        state.remainingBoosts = left
        return .none
    case let .setBoostButtonState(enabled: isEnabled):
        state.isBoostButtonEnabled = isEnabled
        return .none
    }
}


public let textContent: ([BrContent]) -> String? = {
    $0.filter { $0.type == .text }
      .map(\.content)
      .first
}

public let photoContent: ([BrContent]) -> URL? = {
    $0.filter { $0.type == .picture }
      .map(\.content)
      .compactMap(URL.init(string:))
      .first
}

public let videoContent: ([BrContent]) -> URL? = {
    $0.filter { $0.type == .video }
      .map(\.content)
      .compactMap(URL.init(string:))
      .first
}




import WebKit
public class StoryDetailCell: UICollectionViewCell {
    
    public class var reuseIdentifier: String { "StoryDetailCell" }
        
    var viewStore: ViewStore<StoryDetailCellState, StoryDetailCellAction>!
    
    public func configure(_ store: Store<StoryDetailCellState, StoryDetailCellAction>) {
        
        viewStore = ViewStore(store)
                                        
        viewStore.produced.creatorImage
            .compactMap(id)
            .assign(to: \.image, on: personImageView)
                
        personImageView.sd_setImage(with: URL(string: viewStore.state.story.community.picture))
        
        viewStore.produced.story
            .compactMap { $0.content.first(where: { $0.videoURL != nil }) }
            .skipRepeats()
            .map(\.videoURL)
            .assign(to: \._videoURL, on: self)
               
        storyImageView.isHidden =
            photoContent(viewStore.story.content) == nil &&
            videoURL(viewStore.story) == nil
            
        storyImageView.sd_setImage(with: photoContent(viewStore.story.content))
        
        viewStore.produced.isBoostButtonEnabled
            .assign(to: \.isEnabled, on: boostButton)
        
        viewStore.produced.numberOfBoosts
            .map(String.init)
            .map { $0 + " Boosts" }
            .assign(to: \.text, on: currentBoostLabel)
        
        viewStore.produced.remainingBoosts
            .map(String.init)
            .map { $0 + " boosts left" }
            .assign(to: \.text, on: leftBoostsLabel)
        
        let storyViewStore = ViewStore(store.scope(state: \.story))
        
        storyViewStore.produced.content
            .map(textContent)
            .map(convertToHtml)
            .startWithValues({ text in
                guard let text = text else {
                    self.storyContentLabel.isHidden = true
                    return
                }
                self.storyContentLabel.attributedText = text
                self.webView.isHidden = true
            })
            
        
        storyViewStore.produced.comments
            .map(String.init)
            .assign(to: \.text, on: commentsLabel)
        
        storyViewStore.produced.community
            .map(\.name)
            .map(id)
            .assign(to: \.text, on: orgLabel)
        
        storyViewStore.produced.creator
            .map(\.username)
            .map(id)
            .assign(to: \.text, on: personLabel)
    }
    
    var playerController: AVPlayerViewController!
    
    var _videoURL: URL? = nil {
        didSet {
            guard let videoURL = _videoURL,
                  playerController?.player == nil else { return }
            if playerController.player?.timeControlStatus == .playing  {
                return
            }
            let player = AVPlayer(url: videoURL)
            playerController.player = player
            playerController.view.frame = self.storyImageView.bounds
            playerController.showsPlaybackControls = true
            playerController.allowsPictureInPicturePlayback = true
            self.storyImageView.isUserInteractionEnabled = true
            self.storyImageView.addSubview(playerController.view)
        }
    }
    
    let commentsLabel = UILabel()
    let personLabel = UILabel()
    let orgLabel = UILabel()
    let storyContentLabel = EdgeLabel(edge: .square(.br_grid(4)))
    let webView = WKWebView()
    let storyImageView = UIImageView()
    let personImageView = UIImageView()
    let currentBoostLabel = UILabel()
    let leftBoostsLabel = UILabel()
    let boostButton = UIButton()
            
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = .white
                
        personImageViewStyle(personImageView)
                
        orgLabelStyle(orgLabel)
        personLabelStyle(personLabel)
        
        let titleStackView = UIStackView(arrangedSubviews: [
            orgLabel, personLabel
        ])
        
        titleStackViewStyle(titleStackView)        
        
        let joinedButton = UIButton()
        joinedButtonStyle(joinedButton)
        let menuButton = UIButton()
        menuButtonStyleStyle(menuButton)
        
        let popStackView = UIStackView(arrangedSubviews: [
            joinedButton, menuButton
        ])
        
        popStackView |> horizontalStackStyle(
            .br_grid(1),
            distribution: .fill,
            alignment: .center
        )
        
        popStackView.isHidden = true
        
        let topStackView = UIStackView(arrangedSubviews: [
            personImageView,
            titleStackView,
            UIView(),
            popStackView
        ])
                        
        topStackViewStyle(topStackView)
        storyImageViewStyle(storyImageView)
 
        storyContentLabelStyle(storyContentLabel)
                       
        let dateLabel = UILabel()
        dateLabelStyle(dateLabel)
        
        let commentsIcon = UIImageView()
        commentsIconStyle(commentsIcon)
                
        componentLabelStyle(commentsLabel)
        
        let commentsStackView = UIStackView(arrangedSubviews: [
            commentsIcon,
            commentsLabel
        ])
        commentsStackViewStyle(commentsStackView)
                
        currentBoostLabelStyle(currentBoostLabel)
        
        boostButtonStyle(boostButton)
        boostButton.addTarget(self,
                              action: #selector(boostButtonTapped),
                              for: .touchUpInside
        )
                        
        leftBoostsLabelStyle(leftBoostsLabel)
        
        let boostsStackView = UIStackView(arrangedSubviews: [
            currentBoostLabel,
            boostButton,
            leftBoostsLabel
        ])
        boostsStackViewStyle(boostsStackView)
        
        let bottomStackView = UIStackView(arrangedSubviews: [
            dateLabel,
            commentsStackView
        ])
        bottomStackViewStyle(bottomStackView)
        
        let spacer = UIView()
        let spacerStackView = UIStackView(arrangedSubviews: [spacer])
        
        spacerStackView |>
            marginStyle(.horizontal(.br_grid(4)))
        
        spacerStyle(spacer)
        let rootStackView = UIStackView(arrangedSubviews: [
            topStackView,
            storyImageView,
            storyContentLabel,
            webView,
            UIView(),
            boostsStackView,
            bottomStackView,
            spacerStackView
        ])        
        rootStackViewStyle(rootStackView)
        self.addSubview(rootStackView)
        rootStackView.constrainEdges(to: self)
                
    }
    
    @objc func boostButtonTapped() {
        viewStore.send(.boostButtonTapped)
    }
    
    public override func prepareForReuse() {
        super.prepareForReuse()
        storyContentLabel.text = nil
        personLabel.text = nil
        orgLabel.text = nil
        commentsLabel.text = nil
    }
                              
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

