import UIKit
import Library
import ExploreClient
import ComposableArchitecture
import ReactiveSwift
import BrModels
import ImageLoader

public struct StoryDetailStateCopy: Equatable {
    public let story: BrStory
    public var sections: [StoryDetailSectionType]
    public var error: Failure?
    public var linkProject: LinkProjectState?
    public var isLinkButtonShown: Bool
    public var comments: CommentsState?
    public var projectDetails: ProjectDetailState?
    
    var detail: ProjectDetailStateCopy? {
        get {
            projectDetails.map(ProjectDetailStateCopy.init)
        }
        set {
            projectDetails = newValue?.value
        }
    }
    
    
    public var isBookMarked: Bool
    public var donateState: DonateState?
    public var loadingState: LoadingState?
    public init(
        _ state: StoryDetailState
    ) {
        self.story = state.story
        self.sections = state.sections
        self.error = state.error
        self.linkProject = state.linkProject
        self.isLinkButtonShown = state.isLinkButtonShown
        self.comments = state.comments
        self.projectDetails = state.projectDetails ?? state.details?.value
        self.isBookMarked = state.isBookMarked
        self.donateState = state.donateState
        self.loadingState = state.loadingState
    }
}


extension StoryDetailStateCopy {
    public var value: StoryDetailState {
        .init(
            story: story,
            sections: sections,
            error: error,
            linkProject: linkProject,
            isLinkButtonShown: isLinkButtonShown,
            projectDetails: projectDetails,
            donateState: donateState,
            isBookMarked: isBookMarked,
            comments: comments,
            loadingState: loadingState
        )
    }
}

extension StoryDetailStateCopy {
    var storyDetail: StoryDetailCellState? {
        get {
            sections
                .compactMap(/StoryDetailSectionType.story)
                .first
        }
        set {
            guard let newValue = newValue else { return }
            if let index = sections.firstIndex(where: { $0.type == .detail }) {
                sections[index] = .story(newValue)
            }
        }
    }
}

extension StoryDetailStateCopy {
    var projects: [BrProject] {
        get {
            sections
                .compactMap(/StoryDetailSectionType.projects)
                .last ?? []
        }
        set {
            if let index = sections.firstIndex(where: { $0.type == .projects }) {
                sections[index] = .projects(newValue)
            }
        }
    }
}

extension StoryDetailStateCopy {
    var comment: BrComment? {
        get {
            sections.compactMap(/StoryDetailSectionType.comment).first
        }
        set {
            guard let newValue = newValue else { return }
            if let index = sections.firstIndex(where: { $0.type == .comment }) {
                sections[index] = .comment(newValue)
            }
        }
    }
}



public class StoryDetailState: NSObject {
    public let story: BrStory
    public var sections: [StoryDetailSectionType]
    public var error: Failure?
    public var linkProject: LinkProjectState?
    public var isLinkButtonShown: Bool
    public var comments: CommentsState?
    public var projectDetails: ProjectDetailState?
    
    var details: ProjectDetailStateCopy? {
        get {
            projectDetails.map(ProjectDetailStateCopy.init)
        }
        set {
            projectDetails = newValue?.value
        }
    }
    
    public var isBookMarked: Bool
    public var donateState: DonateState?
    public var loadingState: LoadingState?
    public init(
        story: BrStory,
        sections: [StoryDetailSectionType] = [],
        error: Failure? = nil,
        linkProject: LinkProjectState? = nil,
        isLinkButtonShown: Bool = false,
        projectDetails: ProjectDetailState? = nil,
        donateState: DonateState? = nil,
        isBookMarked: Bool = false,
        comments: CommentsState? = nil,
        loadingState: LoadingState? = nil
    ) {
        self.story = story
        self.sections = sections
        self.error = error
        self.comments = comments
        self.linkProject = linkProject
        self.isLinkButtonShown = isLinkButtonShown
        self.projectDetails = projectDetails
        self.donateState = donateState
        self.isBookMarked = isBookMarked
        self.loadingState = loadingState
    }
}

extension StoryDetailState {
    var storyDetail: StoryDetailCellState? {
        get {
            sections
                .compactMap(/StoryDetailSectionType.story)
                .first
        }
        set {
            guard let newValue = newValue else { return }
            if let index = sections.firstIndex(where: { $0.type == .detail }) {
                sections[index] = .story(newValue)
            }
        }
    }
}

extension StoryDetailState {
    var projects: [BrProject] {
        get {
            sections
                .compactMap(/StoryDetailSectionType.projects)
                .last ?? []
        }
        set {
            if let index = sections.firstIndex(where: { $0.type == .projects }) {
                sections[index] = .projects(newValue)
            }
        }
    }
}

extension StoryDetailState {
    var comment: BrComment? {
        get {
            sections.compactMap(/StoryDetailSectionType.comment).first
        }
        set {
            guard let newValue = newValue else { return }
            if let index = sections.firstIndex(where: { $0.type == .comment }) {
                sections[index] = .comment(newValue)
            }
        }
    }
}



public enum StoryDetailAction: Equatable {
    case viewDidLoad
    case viewDidAppear
    case viewDismissed
    case destroy
    case viewCommentsTapped
    case linkActionTapped
    case bookMarkedButtonTapped
    case shareButtonTapped
    case event(StoryDetailDelegateEvent)
    case projectCell(index: Int, action: ProjectCellAction)
    case comments(CommentsAction)
    case commentCell(CommentCellAction)
    case detailCell(StoryDetailCellAction)
    case linkProject(LinkProjectAction)
    case donate(DonateAction)
    indirect
    case projectDetail(ProjectDetailAction)
    case viewProject(BrProject)
}


public struct StoryDetailEnvironment {
    var storyDetailClient: (String) -> StoryDetailClient
    var projectDetailClient: (String) -> ProjectDetailsClient
    var donateClient: (_ projectId: String, _ projectName: String) -> DonateClient
    let addCardClient: AddCardClient
    var linkStoryClient: (String, String) -> LinkStoryClient
    var linkProjectClient: (String, String) -> LinkProjectClient
    var commentsClient: (String, BrComment.´Type´) -> CommentsClient
    var imageLoader: ImageLoader
    let stripeClient: StripeClient
    public init(
        storyDetailClient: @escaping (String) -> StoryDetailClient = { _ in .empty },
        linkProjectClient: @escaping (String, String) -> LinkProjectClient = { _,_ in .empty },
        commentsClient: @escaping (String, BrComment.´Type´) -> CommentsClient = { _,_ in .empty },
        projectDetailClient: @escaping (String) -> ProjectDetailsClient = { _ in .empty },
        donateClient: @escaping (_ projectId: String, _ projectName: String) -> DonateClient = { _,_ in .empty },
        linkStoryClient: @escaping (String, String) -> LinkStoryClient = { _,_ in .empty },
        addCardClient: AddCardClient = .empty,
        imageLoader: ImageLoader,
        stripeClient: StripeClient
    ) {
        self.storyDetailClient = storyDetailClient
        self.linkProjectClient = linkProjectClient
        self.commentsClient = commentsClient
        self.imageLoader = imageLoader
        self.donateClient = donateClient
        self.projectDetailClient = projectDetailClient
        self.addCardClient = addCardClient
        self.linkStoryClient = linkStoryClient
        self.stripeClient = stripeClient
    }
}

extension Reducer {
    static func recurse(
        _ reducer: @escaping (Reducer) -> Reducer
    ) -> Reducer {
        var `self`: Reducer!
        self = Reducer { state, action, environment in
            reducer(self).run(&state, action, environment)
        }
        return self
    }
}

import CasePaths
public let storyDetailReducer: Reducer<StoryDetailStateCopy, StoryDetailAction, SystemEnvironment<StoryDetailEnvironment>> =
    .recurse { SSelf in
    Reducer.combine(
        Reducer { state, action, environment in
            struct CancelDelegateId: Hashable {}
            switch action {
            case .viewDidLoad:
                return .merge(
                    environment.storyDetailClient(state.story.id)
                        .delegate
                        .cancellable(id: CancelDelegateId())
                        .map(StoryDetailAction.event),
                    environment.storyDetailClient(state.story.id)
                        .viewDidLoad
                        .fireAndForget()
                )
            case .viewDidAppear:
                return .none
//                    environment.storyDetailClient(state.story.id)
//                        .delegate
//                        .cancellable(id: CancelDelegateId())
//                        .map(StoryDetailAction.event)
            case .viewDismissed:
                state.error = nil
                state.linkProject = nil
                state.comments = nil
                state.projectDetails = nil
                state.donateState = nil
                state.loadingState = nil
                return .none
            case let .event(.enableBoostButton(enabled: isEnabled)):
                return Effect(value:
                            .detailCell(.setBoostButtonState(enabled: isEnabled)))
            case let .event(.isBookMarked(bookMarked: bookMarked)):
                state.isBookMarked = bookMarked
                return .none
            case let .event(.boosts(boosts, remaining: remaining)):
                return Effect(value:
                        .detailCell(.setBoosts(boosts, left: remaining)))
            case .event(.showLoading):
                state.loadingState = .loading
                return .none
            case .event(.hideLoading):
                state.loadingState = nil
                return .none
            case let .event(.display(story: story, comment: comment, linkedProjects: linkedProjects, isLinkButtonShown: isLinkButtonShown)):
                state.isLinkButtonShown = isLinkButtonShown
                state.sections = [
                    .story(StoryDetailCellState(story: story)),
                    .comment(comment),
                    .projects(linkedProjects)
                ]
                return .none
            case let .event(.failedToDisplayStory(failure)):
                state.loadingState = .message(failure.message ?? failure.cause ?? "")
                return .none
            case let .event(.failedWithError(error)):
                state.error = error
                return .none
            case .detailCell(.boostButtonTapped):
                return environment
                    .storyDetailClient(state.story.id)
                    .boost
                    .fireAndForget()
            case let .projectCell(index, action: .commentsButtonTapped):
                let project = state.sections
                    .compactMap(/StoryDetailSectionType.projects)
                    .compactMap { $0[index] }
                    .first
                guard let mProject = project else { return .none }
                state.comments = .init(
                    parentId: mProject.id,
                    parentType: .project
                )
                return .none
            case let .projectCell(index, action: .donateButtonTapped):
                let project = state.sections
                    .compactMap(/StoryDetailSectionType.projects)
                    .compactMap { $0[index] }
                    .first
                guard let mProject = project else { return .none }
                state.donateState =
                    DonateState.init(project: mProject)
                return .none
            case .viewCommentsTapped:
                state.comments = .init(
                    parentId: state.story.id,
                    parentType: .story
                )
                return .none
            case .linkActionTapped:
                state.linkProject = LinkProjectState(
                    storyId: state.story.id,
                    communityId: state.story.community.id
                )
                return .none
            case .linkProject, .comments:
                return .none
            case .destroy:
                return .cancel(id: CancelDelegateId())
            case .bookMarkedButtonTapped:
                return environment
                    .storyDetailClient(state.story.id)
                    .bookMark
                    .fireAndForget()
            case .shareButtonTapped:
                return .none
            case .projectDetail(_):
                return .none
            case let .viewProject(project):
                state.projectDetails = .init(project: project)
                return .none
            case .donate:
                return .none
            case .projectCell, .commentCell, .detailCell:
                return .none
            }
        },
        stroyDetailCellReducer.optional().pullback(
            state: \.storyDetail,
            action: /StoryDetailAction.detailCell,
            environment: { $0.map(\.imageLoader) }
        ),
        projectCellReducer.forEach(
            state: \.projects,
            action: /StoryDetailAction.projectCell(index:action:),
            environment: { $0.map(\.imageLoader) }
        ),
        commentCellReducer.optional().pullback(
            state: \.comment,
            action: /StoryDetailAction.commentCell,
            environment: { $0.map(\.imageLoader) }
        ),
        linkProjectReducer.optional().pullback(
            state: \.linkProject,
            action: /StoryDetailAction.linkProject,
            environment: {
                $0.map {
                    LinkProjectEnvironment(
                        linkProjectClient: $0.linkProjectClient,
                        imageLoader: $0.imageLoader
                    )
                }
            }
        ),
        commentsReducer.optional().pullback(
            state: \.comments,
            action: /StoryDetailAction.comments,
            environment: {
                $0.map {
                CommentsEnvironment(
                    commentsClient: $0.commentsClient,
                    imageLoader: $0.imageLoader
                )
                }
            }
        ),
        projectDetailReducer.optional().pullback(
            state: \.detail,
            action: /StoryDetailAction.projectDetail,
            environment: {
                $0.map(\.projectDetail)
            }
        ),
        donationReducer.optional().pullback(
            state: \.donateState,
            action: /StoryDetailAction.donate,
            environment: {
                $0.map(\.donate)
            }
        )
    )
    //return SSelf
}


import CasePaths
public let storyDetail_Reducer: Reducer<StoryDetailState, StoryDetailAction, SystemEnvironment<StoryDetailEnvironment>> =
    .recurse { SSelf in
    Reducer.combine(
        Reducer { state, action, environment in
            struct CancelDelegateId: Hashable {}
            switch action {
            case .viewDidLoad:
                return .merge(
                    environment.storyDetailClient(state.story.id)
                        .delegate
                        .cancellable(id: CancelDelegateId())
                        .map(StoryDetailAction.event),
                    environment.storyDetailClient(state.story.id)
                        .viewDidLoad
                        .fireAndForget()
                )
            case .viewDidAppear:
                return .none
//                    environment.storyDetailClient(state.story.id)
//                        .delegate
//                        .cancellable(id: CancelDelegateId())
//                        .map(StoryDetailAction.event)
            case .viewDismissed:
                state.error = nil
                state.linkProject = nil
                state.comments = nil
                state.projectDetails = nil
                state.donateState = nil
                state.loadingState = nil
                return .none
            case let .event(.enableBoostButton(enabled: isEnabled)):
                return Effect(value:
                            .detailCell(.setBoostButtonState(enabled: isEnabled)))
            case let .event(.isBookMarked(bookMarked: bookMarked)):
                state.isBookMarked = bookMarked
                return .none
            case let .event(.boosts(boosts, remaining: remaining)):
                return Effect(value:
                        .detailCell(.setBoosts(boosts, left: remaining)))
            case .event(.showLoading):
                state.loadingState = .loading
                return .none
            case .event(.hideLoading):
                state.loadingState = nil
                return .none
            case let .event(.display(story: story, comment: comment, linkedProjects: linkedProjects, isLinkButtonShown: isLinkButtonShown)):
                state.isLinkButtonShown = isLinkButtonShown
                state.sections = [
                    .story(StoryDetailCellState(story: story)),
                    .comment(comment),
                    .projects(linkedProjects)
                ]
                return .none
            case let .event(.failedToDisplayStory(failure)):
                state.loadingState = .message(failure.message ?? failure.cause ?? "")
                return .none
            case let .event(.failedWithError(error)):
                state.error = error
                return .none
            case .detailCell(.boostButtonTapped):
                return environment
                    .storyDetailClient(state.story.id)
                    .boost
                    .fireAndForget()
            case let .projectCell(index, action: .commentsButtonTapped):
                let project = state.sections
                    .compactMap(/StoryDetailSectionType.projects)
                    .compactMap { $0[index] }
                    .first
                guard let mProject = project else { return .none }
                state.comments = .init(
                    parentId: mProject.id,
                    parentType: .project
                )
                return .none
            case let .projectCell(index, action: .donateButtonTapped):
                let project = state.sections
                    .compactMap(/StoryDetailSectionType.projects)
                    .compactMap { $0[safe: index] }
                    .first
                guard let mProject = project else { return .none }
                state.donateState =
                    DonateState.init(project: mProject)
                return .none
            case .viewCommentsTapped:
                state.comments = .init(
                    parentId: state.story.id,
                    parentType: .story
                )
                return .none
            case .linkActionTapped:
                state.linkProject = LinkProjectState(
                    storyId: state.story.id,
                    communityId: state.story.community.id
                )
                return .none
            case .linkProject, .comments:
                return .none
            case .destroy:
                return .cancel(id: CancelDelegateId())
            case .bookMarkedButtonTapped:
                return environment
                    .storyDetailClient(state.story.id)
                    .bookMark
                    .fireAndForget()
            case .shareButtonTapped:
                return .none
            case .projectDetail(_):
                return .none
            case let .viewProject(project):
                state.projectDetails = .init(project: project)
                return .none
            case .donate:
                return .none
            case .commentCell(_):
                return .none
            case .detailCell(.setBoosts):
                return .none
            case .detailCell(.setBoostButtonState):
                return .none
            }
        },
        stroyDetailCellReducer.optional().pullback(
            state: \.storyDetail,
            action: /StoryDetailAction.detailCell,
            environment: { $0.map(\.imageLoader) }
        ),
        projectCellReducer.forEach(
            state: \.projects,
            action: /StoryDetailAction.projectCell(index:action:),
            environment: { $0.map(\.imageLoader) }
        ),
        commentCellReducer.optional().pullback(
            state: \.comment,
            action: /StoryDetailAction.commentCell,
            environment: { $0.map(\.imageLoader) }
        ),
        linkProjectReducer.optional().pullback(
            state: \.linkProject,
            action: /StoryDetailAction.linkProject,
            environment: {
                $0.map {
                    LinkProjectEnvironment(
                        linkProjectClient: $0.linkProjectClient,
                        imageLoader: $0.imageLoader
                    )
                }
            }
        ),
        commentsReducer.optional().pullback(
            state: \.comments,
            action: /StoryDetailAction.comments,
            environment: {
                $0.map {
                CommentsEnvironment(
                    commentsClient: $0.commentsClient,
                    imageLoader: $0.imageLoader
                )
                }
            }
        ),
        donationReducer.optional().pullback(
            state: \.donateState,
            action: /StoryDetailAction.donate,
            environment: {
                $0.map(\.donate)
            }
        )
    )
    //return SSelf
}

extension StoryDetailEnvironment {
    var projectDetail: ProjectDetailEnvironment {
        ProjectDetailEnvironment(
            projectDetailClient: projectDetailClient,
            linkStoryClient: linkStoryClient,
            donateClient: donateClient,
            addCardClient: addCardClient,
            imageLoader: imageLoader,
            storyDetailClient: storyDetailClient,
            linkProjectClient: linkProjectClient,
            commentsClient: commentsClient,
            stripeClient: stripeClient
        )
    }
}

extension StoryDetailEnvironment {
    var donate: DonateEnvironment {
        DonateEnvironment(
            donateClient: donateClient,
            addCardClient: addCardClient,
            stripeClient: stripeClient
        )
    }
}



let bookMarkButtonStyle: (UIButton) -> Void = {
    $0.setImage(UIImage(named: "ic_bookmark"))
    $0.tintColor = .white
}

public let shareButtonStyle: (UIButton) -> Void = {
    $0.setImage(UIImage(named: "ic_share"))
    $0.tintColor = .white
}

import ComposableArchitecture
import AVKit

public class StoryDetailViewController: UIViewController {
    
    let playerController = AVPlayerViewController()
    
    let store: Store<StoryDetailState, StoryDetailAction>
    let viewStore: ViewStore<StoryDetailState, StoryDetailAction>
    
    public init(store: Store<StoryDetailState, StoryDetailAction>) {
        self.store = store
        self.viewStore = ViewStore(store)
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var collectionView: UICollectionView!
    var datasource: [StoryDetailSectionType] = [] {
        didSet {
//            if #available(iOS 13.0, *) {
//                var snapshot = NSDiffableDataSourceSnapshot<Section, StoryDetailSectionType>()
//                snapshot.appendSections([.main])
//                snapshot.appendItems(datasource)
//                dataSource.apply(snapshot, animatingDifferences: false)
//            } else {
                self.collectionView.reloadData()
           // }
        }
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Story Detail"
        
        viewStore.send(.viewDidLoad)
                                                
//        if #available(iOS 13, *) {
//            collectionView = UICollectionView(
//                frame: .zero,
//                collectionViewLayout: createLayout()
//            )
//            configureDataSource()
//        } else {
        let layout = UICollectionViewFlowLayout()
        collectionFlowLayoutStyle(layout)
        collectionView = UICollectionView(
            frame: .zero,
            collectionViewLayout: layout
        )
        collectionView.dataSource = self
        
                        
        collectionView.delegate = self
        collectionViewStyle(collectionView)
        self.view.addSubview(collectionView)
        collectionView.constrainEdges(to: view)     
                        
        viewStore.produced.sections
            .observe(on: QueueScheduler.main)
            .assign(to: \.datasource, on: self)
        
        let loadingView = LoadingView()
        self.view.addSubview(loadingView)
        loadingView.frame = view.frame
        self.view.bringSubviewToFront(loadingView)
        
        viewStore.produced.loadingState
            .startWithValues { state in
                loadingView.state = state
                loadingView.isHidden = state == nil
            }
        
        // Link Project
        store.scope(
            state: \.linkProject,
            action: StoryDetailAction.linkProject
        ).ifLet(then: { [weak self] store in
            guard let self = self else { return }            
            self.navigationController?.pushViewController(LinkProjectViewController(store: store), animated: true)
        }) {
            self.navigationController?.popToViewController(self, animated: true)
        }
        
        // Comments
        store.scope(
            state: \.comments,
            action: StoryDetailAction.comments
        ).ifLet(then: { [weak self] store in
            guard let self = self else { return }
            self.navigationController?.pushViewController(CommentsViewController(store: store), animated: true)
        }) {
            self.navigationController?.popToViewController(self, animated: true)
        }
        
        // Project Detail
        store.scope(
            state: \.projectDetails,
            action: StoryDetailAction.projectDetail
        ).ifLet(then: { [weak self] store in
            guard let self = self else { return }
            self.navigationController?.pushViewController(ProjectDetailViewController(store: store), animated: true)
        }) {
            self.navigationController?.popToViewController(self, animated: true)
        }
        
        // Donate
        store.scope(
            state: \.donateState,
            action: StoryDetailAction.donate
        ).ifLet(then: { [weak self] store in
            guard let self = self else { return }
            self.navigationController?.pushViewController(DonateViewController(store: store), animated: true)
        }) {
            self.navigationController?.popToViewController(self, animated: true)
        }
        
        let shareButton = UIButton()
        shareButtonStyle(shareButton)
        shareButton.addTarget(self, action: #selector(shareButtonTapped), for: .touchUpInside)
        
        let bookMarkButton = UIButton()
        bookMarkButton.tag = 10
        bookMarkButtonStyle(bookMarkButton)
        bookMarkButton.addTarget(self, action: #selector(bookMarkButtonTapped), for: .touchUpInside)
        
        viewStore.produced.isBookMarked
            .startWithValues {
                bookMarkButton.setImage(
                    UIImage(named: $0
                        ? "ic_bookmark"
                        : "ic_bookmark_border"
                    )
                )
            }
                
        viewStore.produced.isBookMarked
            .startWithValues({ isBookMarkedShown in
                self.navigationItem.rightBarButtonItems =
                    [bookMarkButton, shareButton].filter {
                        $0.tag != 10 || isBookMarkedShown
                    }
                    .map(UIBarButtonItem.init(customView:))
            })                          
        
    }
    
    @objc func bookMarkButtonTapped() {
        viewStore.send(.bookMarkedButtonTapped)
    }
    
    @objc func shareButtonTapped() {
        viewStore.send(.shareButtonTapped)
    }
    
    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewStore.send(.viewDidAppear)
        if !isMovingToParent {
            viewStore.send(.viewDismissed)
        }
    }
    
    public override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        viewStore.send(.destroy)
        playerController.player = nil        
    }
    
    
    
}


private let collectionViewStyle: (UICollectionView) -> Void =
    autoLayoutStyle <> {
        
        $0.register(CommentViewCell.self,
                    forCellWithReuseIdentifier: CommentViewCell.reuseIdentifier)
        
        $0.register(StoryDetailCell.self,
                    forCellWithReuseIdentifier: StoryDetailCell.reuseIdentifier)
        
        $0.register(ProjectViewCell.self,
                    forCellWithReuseIdentifier: ProjectViewCell.reuseIdentifier)
        
        $0.register(
            ViewCommentsFooter.self,
            forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter,
            withReuseIdentifier: ViewCommentsFooter.reuseIdentifier
        )
        
        $0.register(
            LinkActionFooter.self,
            forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter,
            withReuseIdentifier: LinkActionFooter.reuseIdentifier
        )
                      
        $0.register(
            LinkActionHeader.self,
            forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader,
            withReuseIdentifier: LinkActionHeader.reuseIdentifier
        )
 
        $0.backgroundColor = .white
        $0.contentInset = .bottomEdge(.br_grid(2))        
    }

private let collectionFlowLayoutStyle: (UICollectionViewFlowLayout) -> Void = {
    $0.scrollDirection = .vertical
    $0.sectionHeadersPinToVisibleBounds = true
    $0.minimumLineSpacing = .br_grid(4)
    $0.minimumInteritemSpacing = .br_grid(4)
    $0.sectionInsetReference = .fromContentInset
}

extension StoryDetailViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let project = viewStore.projects[safe: indexPath.row],
              datasource[indexPath.section].type == .projects else {
            return
        }
        viewStore.send(.viewProject(project))
    }

    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        datasource[section].numberOfItems
    }
    
    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        return datasource.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch datasource[indexPath.section] {
        case let .comment(.some(commentState)):
            guard let commentCell = collectionView.dequeueReusableCell(
                withReuseIdentifier: CommentViewCell.reuseIdentifier,
                for: indexPath
            ) as? CommentViewCell else {
                fatalError()
            }
            commentCell.configure(store.scope(
                state:  { $0.comment ?? commentState },
                action: StoryDetailAction.commentCell
            ))
            return commentCell
        case let .story(storyDetail):
            guard let storyDetailCell = collectionView.dequeueReusableCell(
                withReuseIdentifier: StoryDetailCell.reuseIdentifier,
                for: indexPath
            ) as? StoryDetailCell else {
                fatalError()
            }
            storyDetailCell.playerController = playerController
            storyDetailCell.configure(
                store.scope(
                    state:  { $0.storyDetail ?? storyDetail },
                    action: StoryDetailAction.detailCell
                )
            )
            return storyDetailCell
        case .projects:
                guard let projectCell = collectionView.dequeueReusableCell(
                    withReuseIdentifier: ProjectViewCell.reuseIdentifier,
                    for: indexPath
                ) as? ProjectViewCell else {
                    fatalError()
                }
                projectCell.configure(store.scope(
                    state:  { $0.projects[safe: indexPath.row] ?? BrProject.branbee },
                    action: { .projectCell(index: indexPath.row, action: $0) }
                ))
                return projectCell
        case .comment(.none):
            guard let commentCell = collectionView.dequeueReusableCell(
                withReuseIdentifier: CommentViewCell.reuseIdentifier,
                for: indexPath
            ) as? CommentViewCell else {
                fatalError()
            }
            commentCell.isHidden = true
            return commentCell
        }
    }
}




public enum StoryDetailSectionType: Hashable {
    
    case story(StoryDetailCellState)
    case comment(BrComment?)
    case projects([BrProject])
    
    public var sectionHeaderHeight: CGFloat {
        switch self {
        case let .projects(projects):
            return projects.isEmpty ? .zero: .br_grid(12)
        default:
            return .zero
        }
    }
    
    public var numberOfItems: Int {
        switch self {
        case .comment(.none):
            return 0
        case .comment, .story:
            return 1
        case let .projects(projects):
            return projects.count
        }
    }
    
    public enum Kind {
        case projects, detail, comment
    }
    
    public var type: Kind {
        switch self {
        case .story:
            return .detail
        case .comment:
            return .comment
        case .projects:
            return .projects
        }
    }
    
    public var sectionFooterHeight: CGFloat {
        switch self {
        case .story:
            return .zero
        case .comment(.some):
            return .br_grid(10)
        case .comment(.none):
            return .br_grid(0)
        case .projects:
            return .br_grid(14)
        }
    }
    
    public func cellHeight(_ width: CGFloat) -> CGFloat {
        switch self {
        case let .story(storyState):
            return StorySectionType
                .story(storyState.story)
                .cellHeight(width, showDetail: true) + .br_grid(80)
        case let .comment(.some(commentState)):
            return heightForText(commentState.body, width: width - .br_grid(10)) + .br_grid(15)
        case .comment(.none):
            return .br_grid(0)
        case .projects:
            return .br_grid(55)
        }
    }
    
    public var hasSectionFooter: Bool {
        switch self {
        case .story:
            return false
        default:
            return true
        }
    }
    
    public var hasSectionHeader: Bool {
        switch self {
        case .projects:
            return true
        default:
            return false
        }
    }
    
}

extension StoryDetailViewController {
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        let cellType = datasource[section]
        return .init(
            width: collectionView.bounds.width - .br_grid(8),
            height: cellType.sectionHeaderHeight
        )
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        let cellType = datasource[section]
        
        if cellType.type == .projects && !viewStore.isLinkButtonShown {
            return .zero
        }
        
        return .init(
            width: collectionView.bounds.width - .br_grid(8),
            height: cellType.sectionFooterHeight
        )
    }
    
    public func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let cellType = viewStore.sections[indexPath.section]
        if kind == UICollectionView.elementKindSectionHeader {
            if cellType.hasSectionHeader {
                guard let cell = collectionView.dequeueReusableSupplementaryView(
                    ofKind: UICollectionView.elementKindSectionHeader,
                    withReuseIdentifier: LinkActionHeader.reuseIdentifier,
                    for: indexPath
                ) as? LinkActionHeader else {
                    fatalError()
                }
                return cell
            }
        }
        if kind == UICollectionView.elementKindSectionFooter {
            switch cellType {
            case .comment(_):
                guard let cell = collectionView.dequeueReusableSupplementaryView(
                    ofKind: UICollectionView.elementKindSectionFooter,
                    withReuseIdentifier: ViewCommentsFooter.reuseIdentifier,
                    for: indexPath
                ) as? ViewCommentsFooter else {
                    fatalError()
                }
                cell.action = {
                    print("View Comments")
                    self.viewStore.send(.viewCommentsTapped)
                }
                return cell
            case .projects(_):
                guard let cell = collectionView.dequeueReusableSupplementaryView(
                    ofKind: UICollectionView.elementKindSectionFooter,
                    withReuseIdentifier: LinkActionFooter.reuseIdentifier,
                    for: indexPath
                ) as? LinkActionFooter else {
                    fatalError()
                }
                
                cell.linkActionsButton.setTitle("Link Actions")
                
                cell.action = {
                    self.viewStore.send(.linkActionTapped)
                }
                return cell
            case .story(_):
                return .init()
            }
            
        }
        fatalError()
    }
    
}


extension StoryDetailViewController: UICollectionViewDelegateFlowLayout {
            
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellType = viewStore.sections[indexPath.section]
        let cellWidth =
            collectionView.bounds.width - .br_grid(
            indexPath.section == .zero ? 0 : 8
        )
        return CGSize(
            width: cellWidth,
            height: cellType.cellHeight(cellWidth)
        )
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return section == .zero
            ? .square(.br_grid(0))
            : section == 2 ? .bottomHorizontalEdge(.br_grid(4)): .square(.br_grid(4))
    }
    
}


@available(iOS 13.0, *)
extension StoryDetailViewController {
    func createLayout() -> UICollectionViewLayout {
        let itemSize = NSCollectionLayoutSize(
            widthDimension: .fractionalWidth(1.0),
            heightDimension: .estimated(100)
        )
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        let groupSize = NSCollectionLayoutSize(
            widthDimension: .fractionalWidth(1.0),
            heightDimension: .estimated(100)
        )
        let group = NSCollectionLayoutGroup.horizontal(
            layoutSize: groupSize,
            subitem: item,
            count: 1
        )
        
        let spacing = CGFloat(10)
        group.interItemSpacing = .fixed(spacing)

        let section = NSCollectionLayoutSection(group: group)
        section.interGroupSpacing = spacing
        section.contentInsets = NSDirectionalEdgeInsets(
            top: .br_grid(2),
            leading: .br_grid(2),
            bottom: .br_grid(2),
            trailing: .br_grid(2)
        )
        
        let headerFooterSize = NSCollectionLayoutSize(
            widthDimension: .fractionalWidth(1.0),
            heightDimension: .estimated(44)
        )
        
        let sectionFooter = NSCollectionLayoutBoundarySupplementaryItem(
            layoutSize: headerFooterSize,
            elementKind: UICollectionView.elementKindSectionFooter,
            alignment: .bottom
        )
        let sectionHeader = NSCollectionLayoutBoundarySupplementaryItem(
            layoutSize: headerFooterSize,
            elementKind: UICollectionView.elementKindSectionHeader,
            alignment: .bottom
        )
                
        section.boundarySupplementaryItems = [sectionFooter, sectionHeader]
                                                              
        let layout = UICollectionViewCompositionalLayout(section: section)
        return layout
    }
    
    func configureDataSource() {
                
        dataSource = UICollectionViewDiffableDataSource
        <Section, StoryDetailSectionType>(collectionView: collectionView) { [weak self]
            (collectionView: UICollectionView, indexPath: IndexPath, feed: StoryDetailSectionType) in
            guard let self = self else { fatalError() }
            switch feed {
            case let .comment(.some(commentState)):
                guard let commentCell = collectionView.dequeueReusableCell(
                    withReuseIdentifier: CommentViewCell.reuseIdentifier,
                    for: indexPath
                ) as? CommentViewCell else {
                    fatalError()
                }
                commentCell.configure(self.store.scope(
                    state:  { $0.comment ?? commentState },
                    action: StoryDetailAction.commentCell
                ))
                return commentCell
            case let .story(storyDetail):
                guard let storyDetailCell = collectionView.dequeueReusableCell(
                    withReuseIdentifier: StoryDetailCell.reuseIdentifier,
                    for: indexPath
                ) as? StoryDetailCell else {
                    fatalError()
                }
                storyDetailCell.configure(
                    self.store.scope(
                        state:  { $0.storyDetail ?? storyDetail },
                        action: StoryDetailAction.detailCell
                    )
                )
                return storyDetailCell
            case .projects:
                guard let projectCell = collectionView.dequeueReusableCell(
                    withReuseIdentifier: ProjectViewCell.reuseIdentifier,
                    for: indexPath
                ) as? ProjectViewCell else {
                    fatalError()
                }
                projectCell.configure(
                    self.store.scope(
                        state:  { $0.projects[safe: indexPath.row] ?? BrProject.branbee },
                        action: { .projectCell(index: indexPath.row, action: $0) }
                    ))
                return projectCell
            case .comment(.none):
                guard let commentCell = collectionView.dequeueReusableCell(
                    withReuseIdentifier: CommentViewCell.reuseIdentifier,
                    for: indexPath
                ) as? CommentViewCell else {
                    fatalError()
                }
                commentCell.isHidden = true
                return commentCell
            }
        }
        
        
        
        dataSource.supplementaryViewProvider = { view, kind, indexPath in
            
            switch kind {
            case UICollectionView.elementKindSectionFooter:
                if indexPath.row == 1 {
                let view = self.collectionView.dequeueReusableSupplementaryView(
                    ofKind: kind,
                    withReuseIdentifier: ViewCommentsFooter.reuseIdentifier,
                    for: indexPath) as? ViewCommentsFooter
                  return view
                }
                                
                let view = self.collectionView.dequeueReusableSupplementaryView(
                    ofKind: kind,
                    withReuseIdentifier: LinkActionFooter.reuseIdentifier,
                    for: indexPath) as? LinkActionFooter
                return view
            case UICollectionView.elementKindSectionHeader:
                let view = self.collectionView.dequeueReusableSupplementaryView(
                    ofKind: kind,
                    withReuseIdentifier: LinkActionHeader.reuseIdentifier,
                    for: indexPath) as? LinkActionHeader
                  return view
            default:
                return nil
            }
            
        }
        
        var snapshot = NSDiffableDataSourceSnapshot<Section, StoryDetailSectionType
         >()
        snapshot.appendSections([.main])
        snapshot.appendItems(datasource)
        dataSource.apply(snapshot, animatingDifferences: false)
    }
}

@available(iOS 13.0, *)
private var dataSource: UICollectionViewDiffableDataSource<Section, StoryDetailSectionType>!

private enum Section {
    case main
}
