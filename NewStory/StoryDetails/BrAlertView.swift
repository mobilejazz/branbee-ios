import UIKit
import Library

private let titleLabelStyle: (UILabel) -> Void = {
    $0.textAlignment = .left
    $0.textColor = .white
    $0.numberOfLines = 1
    $0.font = .py_body(size: .br_grid(4))
}

private let checkViewStyle: (UIImageView) -> Void =
    squareStyle(.br_grid(6)) <> {
        $0.contentMode = .scaleAspectFit
        $0.tintColor = .white
}

private let rootStackViewStyle: (UIStackView) -> Void =
    horizontalStackStyle(.br_grid(4), alignment: .center)
    <> marginStyle(.square(.br_grid(4)))

private let viewStyle: (RequestSubmittedView) -> Void =
    roundedStyle(cornerRadius: .br_grid(1))
 
public class RequestSubmittedView: UIView {
    
    public struct State {
        var isSuccess: Bool
        public init(
            isSuccess: Bool
        ) {
            self.isSuccess = isSuccess
        }
    }
        
    public func configure(_ state: State) {
        checkImageView.image = UIImage(
            named: state.isSuccess
                    ? "ic_check_circle_outline"
                    : "ic_close_circle")
        titleLabel.text = state.isSuccess
            ? "Request Success"
            : "Request Failed"
        self.backgroundColor = state.isSuccess
            ? UIColor.systemGreen
            : .systemRed
    }
    
    let checkImageView = UIImageView()
    let titleLabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
                
        checkViewStyle(checkImageView)
        
        titleLabel.text = "Request Submitted "
        titleLabelStyle(titleLabel)
        let stackView = UIStackView(arrangedSubviews: [
            checkImageView,
            titleLabel
        ])
        rootStackViewStyle(stackView)
        self.addSubview(stackView)
        stackView.constrainEdges(to: self)
        
        self.backgroundColor = .systemGreen
        
        viewStyle(self)
    }
    
    required init?(coder: NSCoder) {
        fatalError("Unimplemented!")
    }
    
}
