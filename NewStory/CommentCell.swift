import UIKit
import Library
import BrModels
import ComposableArchitecture
import ImageLoader
import SDWebImage

private let personImageViewStyle: (UIImageView) -> Void =
    squareStyle(30)
    <> roundedStyle(cornerRadius: 15)
    <> {
        $0.image = UIImage(.systemRed)
    }

private let storyStatusImageViewStyle: (UIButton) -> Void =
    squareStyle(.br_grid(8))
    <> {
        $0.setImage(UIImage(named: "ic_more_vert"))
    }

private let nameLabelStyle: (UILabel) -> Void = {
    $0.textColor = .mainColor
    $0.font = UIFont.py_body(size: 16).bolded
}

private let topStackViewStyle: (UIStackView) -> Void =
    horizontalStackStyle(.br_grid(2),
                       distribution: .fill,
                       alignment: .center)

private let commentBodyLabelStyle: (UILabel) -> Void =
    brDarkTextColor <> {
        $0.textAlignment = .left
        $0.numberOfLines = 0
        $0.font = .py_body(size: 16)
    }

private let dateLabelStyle: (UILabel) -> Void = brGrayTextColor <> {
    $0.font = .py_caption1(size: .br_grid(3))
}

private let rootStackViewStyle: (UIStackView) -> Void =
    verticalStackStyle(0)
    //<> marginStyle(.horizontal(.br_grid(4)))

private let contentStackViewStyle: (UIStackView) -> Void =
    verticalStackStyle(.br_grid(3))
    <> marginStyle(.leading(.br_grid(10)))



public enum CommentCellAction: Equatable {}

public let commentCellReducer = Reducer<BrComment, CommentCellAction, SystemEnvironment<ImageLoader>> { state, action, env in
    return .none
}


public class CommentViewCell: UICollectionViewCell {
    
    public class var reuseIdentifier: String { "CommentViewCell" }
    
    public func configure(_ store: Store<BrComment, CommentCellAction>) {
        
        let viewStore = ViewStore(store)
        
        viewStore.produced.body
            .map(id)
            .assign(to: \.text, on: commentLabel)
        
        viewStore.produced.date
            .map(brDateFormatter().string(from:))
            .assign(to: \.text, on: dateLabel)
        
        viewStore.produced.creator
            .map(\.username)
            .map { $0.uppercased() }
            .map(id)
            .assign(to: \.text, on: nameLabel)
                
        personImageView.sd_setImage(with: viewStore.state.creator._photoURL)
        
    }
    
    let personImageView = UIImageView()
    let nameLabel = UILabel()
    let commentLabel = UILabel()
    let dateLabel = UILabel()
        
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = .white
                
        personImageViewStyle(personImageView)
        nameLabelStyle(nameLabel)
        
        let statusButton = UIButton()
        storyStatusImageViewStyle(statusButton)
        
        let topStackView = UIStackView(arrangedSubviews: [
            personImageView,
            nameLabel,
            statusButton
        ])
        
        topStackViewStyle(topStackView)
        commentBodyLabelStyle(commentLabel)
        commentLabel
            .setContentHuggingPriority(.defaultHigh, for: .vertical)
                
        dateLabelStyle(dateLabel)
                
        let contentStackView = UIStackView(arrangedSubviews: [
            commentLabel,
            dateLabel
        ])
        contentStackViewStyle(contentStackView)
        
        let rootStackView = UIStackView(arrangedSubviews: [
            topStackView,
            contentStackView,
            UIView()
        ])
        rootStackViewStyle(rootStackView)
        
        self.addSubview(rootStackView)
        rootStackView.constrainEdges(to: self)
    }
    
    public override func prepareForReuse() {
        super.prepareForReuse()
    }
                          
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
