import UIKit
import Library
import ReactiveSwift
import BrModels
import ExploreClient
import ImageLoader
import ComposableArchitecture
import AVKit

private let editCommunityButtonStyle: (UIButton) -> Void =
    squareStyle(.br_grid(8))
    <> {
        $0.setImage(UIImage(named: "ic_edit"))
        $0.tintColor = .darkGray
    }

private let orgLabelStyle: (UILabel) -> Void = {
    $0.textColor = .mainColor
    $0.font = UIFont.py_body(size: .br_grid(4)).bolded
}

private let titleStackViewStyle: (UIStackView) -> Void =
    verticalStackStyle(.br_grid(1),
                       distribution: .fill,
                       alignment: .leading
    )
private let topStackViewStyle: (UIStackView) -> Void =
    horizontalStackStyle(.br_grid(4),
                       distribution: .fill,
                       alignment: .center)
    <> marginStyle(.rectangle(v: .br_grid(2), h: .br_grid(4)))

private let storyStackViewStyle: (UIStackView) -> Void =
    verticalStackStyle(.br_grid(1),
                       distribution: .fill,
                       alignment: .fill)
    <> marginStyle(.leading(.br_grid(4)))

private let placeholderStackViewStyle: (UIStackView) -> Void =
    verticalStackStyle(.br_grid(2),
                       distribution: .fill,
                       alignment: .center
    )

private let storyImageViewStyle: (UIImageView) -> Void =
    heightAnchor(.br_grid(52)) <> {
        $0.image = UIImage(named: "pexels")
    }

private let descriptionTextViewStyle: (UITextView) -> Void = {
        $0.textColor = .gray
        $0.textAlignment = .left
        $0.font = .py_body(size: .br_grid(4))
        $0.textContainerInset = .square(.br_grid(4))
    }

private let spacerStyle: (UIView) -> Void = heightAnchor(0.5) <> {
    $0.backgroundColor = .lightGray
}

private let imageViewStyle: (UIImageView) -> Void =
    squareStyle(.br_grid(13))
    <> {
        $0.image = UIImage(named: "ic_file_upload")
        $0.contentMode = .scaleAspectFill
    }
    
private let addPicVideoButtonStyle: (UIButton) -> Void = {
    $0.titleLabel?.font = .py_body(size: .br_grid(4))
    $0.setTitleColor(.brGrayColor, for: .normal)
    $0.setTitle("Add an image or video")
}

private let rootStackViewStyle: (UIStackView) -> Void =
    verticalStackStyle(.br_grid(0))

private let makeStoryButtonStyle: (LoadingButton) -> Void = {
    $0.setImage(UIImage(named: "ic_send"))
    $0.tintColor = .white
} <> {
        $0.indicator = MaterialLoadingIndicator()
        $0.indicator.backgroundColor = .clear
        $0.indicator.tintColor = .white
    }


private let enabledStyle: (UIButton) -> Void = {
    $0.tintColor = .white
}

private let disabledStyle: (UIButton) -> Void = {
    $0.tintColor = .lightGray
}



let roundedEditButtonStyle: (UIButton) -> Void = autoLayoutStyle
    <> squareStyle(.br_grid(10))
    <> roundedStyle(cornerRadius: .br_grid(5))
    <> {
        $0.setImage(UIImage(named: "ic_edit"))
        $0.tintColor = .darkGray
        $0.backgroundColor = .white
    }


public struct NewStoryState: Equatable {
    public var alert: _AlertState<NewStoryAction>?
    public var isSavingStoryRequestInFlight: Bool = false
    public var thumbnail: UIImage?
    public var imageURL: URL? = nil
    public var content: String?
    public var videoURL: URL?
    public var cacheImage: String?
    public var communityName: String
    public var communityId: String
    public var draft: BrDraft?
    public var isMakeButtonEnabled: Bool = false
    public var storyCreated: Bool? = nil
    public var isPhotoPickerEnabled: Bool = false
    public var selectCommunity: SelectCommunityState?
    public var loadingState: LoadingState? = nil
    
    public init(
        communityName: String,
        communityId: String,
        alert: _AlertState<NewStoryAction>? = nil,
        isSavingStoryRequestInFlight: Bool = false,
        image: UIImage? = nil,
        content: String? = nil,
        videoURL: URL? = nil,
        cacheImage: String? = nil,
        draft: BrDraft? = nil,
        isMakeButtonEnabled: Bool = false,
        selectCommunityState: SelectCommunityState? = nil
    ) {
        self.alert = alert
        self.isSavingStoryRequestInFlight = isSavingStoryRequestInFlight
        self.thumbnail = image
        self.content = content
        self.videoURL = videoURL
        self.draft = draft
        self.communityName = communityName
        self.communityId = communityId
        self.cacheImage = cacheImage
        self.isMakeButtonEnabled = isMakeButtonEnabled
        self.selectCommunity = selectCommunityState
    }
    
    var isFilled: Bool {
        videoURL != nil || imageURL != nil
    }
        
}

public enum NewStoryAction: Equatable {
    case viewDidLoad
    case viewDidAppear
    case viewDidDisppear
    case viewDismissed
    case createStoryButtonTapped
    case addPictureButtonTapped
    case editAssetButtonTapped
    case editCommunityButtonTapped
    case saveDraft
    case closeButtonTapped
    case writeDraft(BrDraft?)
    case storyTextChanged(String?)
    case setStoryImage(UIImage?)
    case setStoryImageURL(URL)
    case setStoryVideoURL(URL)
    case event(NewStoryDelegateEvent)
    case selectCommunity(SelectCommunityAction)
}

public let newStoryReducer: Reducer<NewStoryState, NewStoryAction, SystemEnvironment<NewStoryEnvironment>> = .combine(
    Reducer { state, action, environment in
        struct CancelDelegateId: Hashable {}
        switch action {
        case .viewDidLoad:
            return .merge(
                environment.newStory(state.draft?.userId)
                    .delegate
                    .map(NewStoryAction.event)
                    .cancellable(id: CancelDelegateId()),
                environment.newStory(state.draft?.userId)
                    .viewRefresh
                    .fireAndForget()
            )
        case .viewDidAppear:
            return .none
        case .viewDismissed:
            state.isPhotoPickerEnabled = false
            state.alert = nil
            state.selectCommunity = nil
            return .none
        case .createStoryButtonTapped:
            state.isSavingStoryRequestInFlight = true
            if let videoURL = state.videoURL,
               let data = try? Data(contentsOf: videoURL) {
                return environment.newStory(nil)
                    .makeStoryFromFile(
                        BrByteArrayFromFile(
                            filename: UUID().uuidString,
                            data: data
                        ),
                        state.content ?? "")
                    .fireAndForget()
            }
            
            if let imagePath = state.imageURL?.path {
                return environment.newStory(state.draft?.userId)
                    .makeStoryFromImage(
                        imagePath,
                        state.content ?? "")
                    .fireAndForget()
            }
            
            return .none
        case .addPictureButtonTapped:
            state.isPhotoPickerEnabled = true
            return .none
        case .editAssetButtonTapped:
            state.isPhotoPickerEnabled = true
            return .none
        case .editCommunityButtonTapped:
            state.selectCommunity = SelectCommunityState()
            return .none
        case let .storyTextChanged(text):
            state.content = text
            return .none
        case let .setStoryImage(thumbnail):
            state.thumbnail = thumbnail
            return Effect(value: .viewDismissed)
        case let .setStoryVideoURL(videoURL):
            state.videoURL = videoURL
            return thumbnail(videoURL)
                .map(NewStoryAction.setStoryImage)
        case .event(.showLoading):
            state.loadingState = .loading
            return .none
        case let .event(.onDisplayCommunityName(communityName)):
            state.communityName = communityName
            return .none
        case .event(.showMakeStoryLoading):
            state.isSavingStoryRequestInFlight = true
            return .none
        case .event(.hideMakeStoryLoading):
            state.isSavingStoryRequestInFlight = false
            return .none
        case let .event(.onDisplayDraft(draft)):
            state.draft = draft
            return .none
        case let .event(.onDisplayEmailNotVerifiedWarning(userName: userName, email: email)):
            return .none
        case .event(.onDisplayFillTextMessage):
            state.alert = _AlertState(
                title: "Ooops!",
                message: "Please fill story content",
                dismissButton: .cancel(send:.viewDismissed)
            )
            return .none
        case .event(.hideLoading):
            state.loadingState = nil
            return .none
        case .event(.onDisplaySaveDraftDialog):
            state.alert = _AlertState(
                title: "Draft",
                message: "Do you want to save story draft?",
                primaryButton: .default("Save", send: .saveDraft),
                secondaryButton: .destructive("Cancel", send:.viewDismissed)
            )
            return .none
        case .event(.showSuccessMessage):
            state.loadingState = nil
            return .none
        case let .event(.onDisplayTransientError(failure)):
            state.alert = _AlertState(
                title: "Ooops!",
                message: failure.message,
                dismissButton: .cancel(send:.viewDismissed)
            )
            return .none
        case let .event(.onNotifyCloseScreen(storyCreated: didScreenClosed)):
            state.storyCreated = didScreenClosed
            return .none
        case let .event(.onNotifyEnableCreateStoryButton(enable: enable)):
            state.isMakeButtonEnabled = enable
            return .none
        case .saveDraft:
            return environment.newStory(state.draft?.userId)
                .saveDraft
                .fireAndForget()
        case let .setStoryImageURL(url):
            state.imageURL = url
            return .none
        case let .writeDraft(draft) where draft != nil:
            state.draft = draft
            state.content = draft!.text
            /*userId*/
            state.communityId = draft!.communityId
            state.cacheImage = draft!.cachedImageFileKey
            return .none
        case .writeDraft:
            return .none
        case let .selectCommunity(.selectedCommunity(community)):
            state.communityName = community.name
            state.communityId = community.id
            return .concatenate(
                Effect(value: .selectCommunity(.destroy)),
                Effect(value: .viewDismissed)
            )
        case .selectCommunity:
            return .none
        case .viewDidDisppear:            
            return .cancel(id: CancelDelegateId())
        case .closeButtonTapped:
            return environment
                .newStory(state.draft?.userId)
                .closeScreen
                .fireAndForget()
        }},
    selectCommunityReducer.optional().pullback(
        state: \.selectCommunity,
        action: /NewStoryAction.selectCommunity,
        environment: {
            $0.map(\.selectCommunityEnv)
        }
    )
)


public struct NewStoryEnvironment {
    var newStory: (String?) -> NewStoryClient
    var selectCommunity: SelectCommunityClient
    var imageLoader: ImageLoader
    public init(
        newStory: @escaping (String?) -> NewStoryClient,
        selectCommunity: SelectCommunityClient,
        imageLoader: ImageLoader
    ) {
        self.newStory = newStory
        self.selectCommunity = selectCommunity
        self.imageLoader = imageLoader
    }
}

extension NewStoryEnvironment {
    public var selectCommunityEnv: SelectCommunityEnvironment {
        SelectCommunityEnvironment(
            selectCommunityClient: selectCommunity,
            imageLoader: imageLoader
        )
    }
}

public let dismissButtonStyle: (UIButton) -> Void = {
    $0.setImage(UIImage(named: "ic_close"))
    $0.tintColor = .white
}

    
public class NewStoryViewController: UIViewController {
    
    let placeholderLabel = UILabel()
    let contentTextView = UITextView()
    public var dismissAction: (() -> Void)? = nil
    
    let store: Store<NewStoryState, NewStoryAction>
    let viewStore: ViewStore<NewStoryState, NewStoryAction>
    
    public init(store: Store<NewStoryState, NewStoryAction>) {
        self.store = store
        self.viewStore = ViewStore(store)
        super.init(nibName: nil, bundle: nil)
    }
        
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        viewStore.send(.viewDidLoad)
        
        title = "New Story"
        self.view.backgroundColor = .white
        if #available(iOS 13.0, *) {
            self.isModalInPresentation = true
        }
               
        let communityNameLabel = UILabel()
        orgLabelStyle(communityNameLabel)
        
        let editCommunityButton = UIButton()
        editCommunityButtonStyle(editCommunityButton)
        editCommunityButton.addTarget(self,
                               action: #selector(editCommunityButtonTapped),
                               for: .touchUpInside)
        let topStackView = UIStackView(arrangedSubviews: [
            communityNameLabel,
            editCommunityButton
        ])
        topStackViewStyle(topStackView)
        
        let storyImageView = UIImageView()
        storyImageViewStyle(storyImageView)
        
        let editAssetButton = UIButton()
        editAssetButton.addTarget(self,
                               action: #selector(editImageButtonTapped),
                               for: .touchUpInside)
        storyImageView.isUserInteractionEnabled = true
        storyImageView.addSubview(editAssetButton)
        roundedEditButtonStyle(editAssetButton)
        editAssetButton |>
            roundedEditButtonStyle
            <> bottomRightStyle(storyImageView, .square(.br_grid(4)))
                                        
        contentTextView.delegate = self
        placeholderLabel.text = "Write your experience and thoughts, ideas ..."
        placeholderLabel.sizeToFit()
        contentTextView.addSubview(placeholderLabel)
        placeholderLabel.frame.origin = CGPoint(
            x: .br_grid(5),
            y: .br_grid(4)
        )
        placeholderLabel.textColor = .lightGray
        placeholderLabel.isHidden = !contentTextView.text.isEmpty
        descriptionTextViewStyle(contentTextView)
                               
        let spacer = UIView()
        spacerStyle(spacer)
        
        let imageView = UIImageView()
        imageViewStyle(imageView)
        let addPicVideoButton = UIButton()
        addPicVideoButton.addTarget(self,
                               action: #selector(addAssetButtonTapped),
                               for: .touchUpInside)
        addPicVideoButtonStyle(addPicVideoButton)
        let placeholderStackView = UIStackView(arrangedSubviews: [
            imageView,
            addPicVideoButton
        ])
        
        placeholderStackViewStyle(placeholderStackView)
        
        let placeholder = placeholderStackView
            .pinDashedBackground(.white, inset: .square(-.br_grid(2)))
        let placeholderContainerStackView = UIStackView(arrangedSubviews: [
            placeholder
        ])
        placeholderContainerStackView |> marginStyle()
                        
        let rootStackView = UIStackView(arrangedSubviews: [
            topStackView,
            spacer,
            placeholderContainerStackView,
            storyImageView,
            contentTextView
        ])
        rootStackViewStyle(rootStackView)
        self.view.addSubview(rootStackView)
        rootStackView.constrainEdges(to: view)
        
        let loadingView = LoadingView()
        self.view.addSubview(loadingView)
        loadingView.frame = view.frame
        self.view.bringSubviewToFront(loadingView)
        
        viewStore.produced.loadingState
            .startWithValues { state in
                loadingView.state = state
                loadingView.isHidden = state == nil
            }
        
        let makeStoryButton = LoadingButton()
        makeStoryButtonStyle(makeStoryButton)
        makeStoryButton.addTarget(self,
                    action: #selector(makeButtonTapped),
                    for: .touchUpInside)
        self.navigationItem.rightBarButtonItems = [makeStoryButton].map(UIBarButtonItem.init(customView:))
                
        tapGestureDismissalStyle(self)
                
        viewStore.produced.content
            .startWithValues{ content in
                self.contentTextView.text = content
                _storyText_ = content
            }
        
        viewStore.produced.videoURL
            .startWithValues{ content in
                _mediaFilePath_ = content?.path
            }
                        
        viewStore.produced.thumbnail
            .startWithValues { image in
                placeholderContainerStackView.isHidden = image != nil
                storyImageView.isHidden = image == nil
                storyImageView.image = image
            }
        
        viewStore.produced.storyCreated
            .skipNil()
            .startWithValues { _ in
                self.dismiss(animated: true, completion: {
                    self.dismissAction?()
                })
            }
        
        viewStore.produced.isSavingStoryRequestInFlight
            .startWithValues { isRequestInFlight in
                if isRequestInFlight {
                    makeStoryButton.setImage(nil)
                    makeStoryButton.showLoader(userInteraction: false)
                } else {
                    makeStoryButton.setImage(UIImage(named: "ic_send"))
                    makeStoryButton.hideLoader()
                }
            }
        
        viewStore.produced
            .isFilled
            .startWithValues({ isEnabled in
                makeStoryButton.isEnabled = isEnabled
                isEnabled
                    ? enabledStyle(makeStoryButton)
                    : disabledStyle(makeStoryButton)
            })
                   
        viewStore.produced.communityName
            .map(id)
            .assign(to: \.text, on: communityNameLabel)
        
        self.viewStore.produced.isPhotoPickerEnabled
            .startWithValues { [weak self] isEnabled in
                guard isEnabled else {
                    if let pickerViewController = UIApplication.shared
                        .windows.first?
                        .rootViewController?
                        .presentedViewController as? UIImagePickerController {
                        pickerViewController.dismiss(animated: true, completion: nil)
                    }
                    return
                }
                let pickerController = UIImagePickerController()
                pickerController.sourceType = .savedPhotosAlbum
                pickerController.mediaTypes = ["public.image", "public.movie"]
                pickerController.delegate = self
                self?.present(pickerController,
                              animated: false,
                              completion: nil)
            }
        
        // Select Community
        store.scope(
            state: \.selectCommunity,
            action: NewStoryAction.selectCommunity
        ).ifLet(then: { [weak self] store in
            guard let self = self else { return }
            self.navigationController?.pushViewController(SelectCommunityViewController(store: store), animated: false)
        }) {
            self.navigationController?.popToViewController(self, animated: true)
        }
        
        let dismissButton = UIButton()
        dismissButtonStyle(dismissButton)
        dismissButton.addTarget(
            self,
            action: #selector(dismissButtonTapped),
            for: .touchUpInside
        )
        
        viewStore.produced.alert
            .startWithValues { [weak self] (alert) in
                guard let self = self else { return }
                guard let alert = alert else { return }
                
                let actionController = UIAlertController(
                    title: alert.title,
                    message: alert.message,
                    preferredStyle: .alert
                )
                alert.primaryButton
                    .map { $0.toUIKit(send: self.viewStore.send) }
                    .map(actionController.addAction)
                
                alert.secondaryButton
                    .map { $0.toUIKit(send: self.viewStore.send) }
                    .map(actionController.addAction)
                
                self.present(actionController,
                             animated: true,
                             completion: nil)
        }
                        
        self.navigationItem.leftBarButtonItems =
            [dismissButton]
                .map(UIBarButtonItem.init(customView:))
        
    }
    
    @objc func dismissButtonTapped() {
        viewStore.send(.closeButtonTapped)
    }
        
    @objc func makeButtonTapped() {
        viewStore.send(.createStoryButtonTapped)
    }
    
    @objc func addAssetButtonTapped() {
        viewStore.send(.addPictureButtonTapped)
    }
    
    @objc func editCommunityButtonTapped() {
        viewStore.send(.editCommunityButtonTapped)
    }
    
    @objc func editImageButtonTapped() {
        viewStore.send(.editAssetButtonTapped)
    }
                                  
    @objc func keyboardWillShowNotification(_ notification: NSNotification) {
        let height = keyboardHeight(notification)
        contentTextView.contentInset = .bottomEdge(height)
        contentTextView.scrollIndicatorInsets = .bottomEdge(height)
        var aRect = view.frame
        aRect.size.height -= height
                                                              
    }
    
    @objc func keyboardWillHideNotification(_ notification: NSNotification) {
        contentTextView.contentInset = .zero
        contentTextView.scrollIndicatorInsets = .zero
    }
    
    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewStore.send(.viewDidAppear)
        if !isMovingToParent {
            viewStore.send(.viewDismissed)
        }
    }    
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

func isNotNullable<V: Equatable>(_ value: Optional<V>) -> Bool {
    value.optional != .none
}

extension NewStoryViewController {
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = false
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShowNotification),
            name: UIResponder.keyboardWillShowNotification,
            object: nil
        )

        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillHideNotification),
            name: UIResponder.keyboardWillHideNotification,
            object: nil
        )
    }
            
    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(
            self,
            name: UIResponder.keyboardDidShowNotification,
            object: nil
        )
        NotificationCenter.default.removeObserver(
            self,
            name: UIResponder.keyboardWillHideNotification,
            object: nil
        )
        //viewStore.send(.dismissController)
    }
    
}

import Photos
extension NewStoryViewController: UIImagePickerControllerDelegate & UINavigationControllerDelegate {
    
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        viewStore.send(.viewDismissed)
    }
    
    public func imagePickerController(
        _ picker: UIImagePickerController,
        didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[.originalImage] as? UIImage,
           let url = info[.imageURL] as? URL {
            viewStore.send(.setStoryImage(image))
            viewStore.send(.setStoryImageURL(url))
        }
      
        if let videoURL = info[.mediaURL] as? NSURL {
            viewStore.send(.setStoryVideoURL(videoURL as URL))
        }
        picker.dismiss(animated: true)
        
        
    }
    
}


extension NewStoryViewController: UITextViewDelegate {
    public func textViewDidChange(_ textView: UITextView) {
        placeholderLabel.isHidden = !textView.text.isEmpty
    } 
}
