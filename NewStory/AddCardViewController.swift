import UIKit
import ComposableArchitecture
import ReactiveSwift
import Library
import ExploreClient
import BrModels

public struct AddCreditCardState: Equatable {
    var cardNumber: String?
    var expirationMonth: UInt
    var expMonths: [UInt]
    var expirationYear: UInt
    var expYears: [UInt]
    var cardHolderName: String?
    var cvcCode: String?
    var currency: String?
    var cardToken: String?
    var currencies: [String]
    var alert: _AlertState<AddCreditCardAction>?
    var isAddCardRequestInFlight: Bool = false
    var stripeIntent: StripeIntent?
    var cardAdded: Bool = false
    var isSubscribed: Bool = false
    public init(
        cardNumber: String? = nil,
        expirationMonth: UInt  = .zero,
        expMonths: [UInt] = Array(1...12),
        expirationYear: UInt = .zero,
        expYears: [UInt] = [2020, 2021, 2022, 2023, 2024],
        cardHolderName: String? = nil,
        cardToken: String? = nil,
        cvcCode: String? = nil,
        currency: String? = nil,
        currencies: [String] = Locale.isoCurrencyCodes,
        alert: _AlertState<AddCreditCardAction>? = nil,
        isSaveRequestInFlight: Bool = false,
        stripeIntent: StripeIntent? = nil
    ) {
        self.cardNumber = cardNumber
        self.expirationMonth = expirationMonth
        self.expMonths = expMonths
        self.expirationYear = expirationYear
        self.expYears = expYears
        self.cardHolderName = cardHolderName
        self.cvcCode = cvcCode
        self.cardToken = cardToken
        self.currency = currency
        self.currencies = currencies
        self.alert = alert
        self.isAddCardRequestInFlight = isSaveRequestInFlight
        self.stripeIntent = stripeIntent
    }
}

public enum AddCreditCardAction: Equatable {
    case viewDidLoad, viewDidAppear
    case viewDismissed
    case viewDidDisappear
    case saveCardButtonTapped
    case cardNumber(String?)
    case expirationMonth(UInt?)
    case expirationYear(UInt?)
    case cardHolderName(String?)
    case addedCard(Result<String?, Failure>)
    case cvcCode(String?)
    case currency(String?)
    case event(AddCardDelegateEvent)
}


public struct AddCardEnvironment {
    var stripeClient: StripeClient
    var addCardClient: AddCardClient
    public init(
        stripeClient: StripeClient,
        addCardClient: AddCardClient
    ) {
        self.stripeClient = stripeClient
        self.addCardClient = addCardClient
    }
}



public let addCreditCardReducer: Reducer<AddCreditCardState, AddCreditCardAction, SystemEnvironment<AddCardEnvironment>> = .combine(
    Reducer { state, action, environment in
        struct CancelDelegateId: Hashable {}
        switch action {
        case .viewDismissed:
            state.alert = nil
            return .none
        case .saveCardButtonTapped:
            state.isAddCardRequestInFlight = true
            return environment.addCardClient.didRequiredStripeIntentWithCardHolder(state.cardHolderName ?? "")
                .fireAndForget()
        case .viewDidLoad:
            state.currency = environment.locale.currencyCode
            state.isSubscribed = true
            return environment.addCardClient
                .delegate
                .cancellable(id: CancelDelegateId())
                .map(AddCreditCardAction.event)
        case .viewDidAppear:
            guard !state.isSubscribed else { return .none }
            return environment.addCardClient
                .delegate
                .cancellable(id: CancelDelegateId())
                .map(AddCreditCardAction.event)
        case let .cardNumber(cardNumber):
            state.cardNumber = cardNumber
            return .none
        case let .expirationMonth(expMonth):
            state.expirationMonth = expMonth ?? .zero
            return .none
        case let .expirationYear(expYear):
            state.expirationYear = expYear ?? .zero
            return .none
        case let .cardHolderName(name):
            state.cardHolderName = name
            return .none
        case let .cvcCode(cvcCode):
            state.cvcCode = cvcCode
            return .none
        case let .currency(currency):
            state.currency = currency
            return .none
        case let .event(.displayTransientError(error)):
            state.isAddCardRequestInFlight = false
            state.alert = .init(
                title: "Ooops!",
                message: error.message,
                dismissButton: .cancel(send: .viewDismissed)
            )
            return .none
        case .event(.onFailedToSetupCard):
            state.isAddCardRequestInFlight = false
            state.alert = .init(
                title: "Ooops!",
                message: "Failed to setup card",
                dismissButton: .cancel(send: .viewDismissed)
            )
            return .none
        case .event(.onDisplayFillHolderNameTransientMessage):
            state.isAddCardRequestInFlight = false
            state.alert = .init(
                title: "Ooops!",
                message: "Please fill holder name message!",
                dismissButton: .cancel(send: .viewDismissed)
            )
            return .none
        case let .event(.onNotifyStripeSetupIntentReceived(stripeIntent)):
            state.stripeIntent = stripeIntent
            let params: CardParams = .init(
                number: state.cardNumber,
                currency: state.currency,
                name: state.cardHolderName,
                expMonth: state.expirationMonth,
                expYear: state.expirationYear,
                cvc: state.cvcCode
            )
            return environment.stripeClient.createCard(
                params , stripeIntent.clientSecret
            ).mapError { .message($0.localizedDescription) }
            .catchToEffect()
            .map(AddCreditCardAction.addedCard)
        case let .event(.onNotifyCloseScreen(cardAdded: cardAdded)):
            state.isAddCardRequestInFlight = false
            state.cardAdded = cardAdded
            return .none
        case .event(.onDisplaySetupCardSuccessfullyMessage):
            state.isAddCardRequestInFlight = false
            return .none
        case .viewDidDisappear:
            state.isSubscribed = false
            return .cancel(id: CancelDelegateId())
        case let .addedCard(.success(.some(paymentId))):
            return environment.addCardClient
                .addCardWithToken(paymentId)
                .observe(on: environment.mainQueue())
                .fireAndForget()
        case .addedCard(.success(.none)):
            state.isAddCardRequestInFlight = false
            return environment.addCardClient.onEventStripeFailedToSetupCardWithFailure(nil)
                .fireAndForget()
        case let .addedCard(.failure(failure)):
            state.isAddCardRequestInFlight = false
//            state.alert = .init(
//                title: "Add Card Error",
//                message: failure.message,
//                dismissButton: .cancel(send: .viewDismissed)
//            )
            return environment.addCardClient.onEventStripeFailedToSetupCardWithFailure(failure)
                .fireAndForget()
        }
    }
)

private let itemStackViewStyle: (UIStackView) -> Void =
    verticalStackStyle(.br_grid(2),
                    distribution: .fill,
                    alignment: .fill
)

private let titleLabelStyle: (UILabel) -> Void =
    brLabelDarkStyle
    <> heightAnchor(.br_grid(10))
    <> centerStyle

private let itemLabelStyle: (UILabel) -> Void =
    brLabelDarkBoldStyle <> {
        $0.textAlignment = .left
}

private let rootStackViewStyle: (UIStackView) -> Void =
    verticalStackStyle(.br_grid(3), alignment: .fill)

private let expirationStackViewStyle: (UIStackView) -> Void =
    horizontalStackStyle(.br_grid(4),
                    distribution: .fillEqually,
                    alignment: .fill
)

private let topStackViewStyle: (UIStackView) -> Void =
    verticalStackStyle(.br_grid(4),
                       distribution: .fill,
                       alignment: .leading
    )

let securityCodeImageViewStyle: (UIImageView) -> Void = {
    $0.image = UIImage(named: "ic_credit_card")
    $0.contentMode = .scaleAspectFit
} <> heightAnchor(.br_grid(14))
  <> widthAnchor(.br_grid(16))

public class AddCreditCardViewController: UIViewController {
                     
    let store: Store<AddCreditCardState, AddCreditCardAction>
    let viewStore: ViewStore<AddCreditCardState, AddCreditCardAction>
        
    let expMonthPickerView = UIPickerView()
    let expYearPickerView = UIPickerView()
    let currencyPickerView = UIPickerView()
    let scrollView = UIScrollView()
    let rootView = UIView()
    var activeView = UIView()
    
    public init(store: Store<AddCreditCardState, AddCreditCardAction>) {
        self.store = store
        self.viewStore = ViewStore(store)
        super.init(nibName: nil, bundle: nil)
    }
        
    required init?(coder: NSCoder) {
        fatalError("unimplemented nscoder")
    }
        
        
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        viewStore.send(.viewDidLoad)
        
        self.view.backgroundColor = .white
        self.title = "Add Card"
        
        // scroll setup
        scrollViewStyle(scrollView)
        rootViewStyle(rootView)
        view.addSubview(scrollView)
        scrollView.addSubview(rootView)
        scrollView.constrainEdges(to: view)
        rootView.constrainEdges(to: scrollView)

        rootView |>
            sameWidth(scrollView)
            <> sameHeight(scrollView)

        // Name Stack
        let creditCardLabel = UILabel()
        itemLabelStyle(creditCardLabel)
        creditCardLabel.text = "Credit Card Number *"
        let creditCardTextField = UITextField()
        creditCardTextField |>
            defaultTextFieldStyle <> numberPadStyle
        creditCardTextField.placeholder = "XXXX-XXXX-XXXX-XXXX"
        let nameItemStackView = UIStackView(arrangedSubviews: [
            creditCardLabel,
            creditCardTextField
        ])
        itemStackViewStyle(nameItemStackView)

        // Expi Month Stack
        let expMonthLabel = UILabel()
        itemLabelStyle(expMonthLabel)
        expMonthLabel.text = "Expiration Months *"
        let expirationMonthTextField = UITextField()
        expirationTextFieldStyle(expirationMonthTextField)
        expirationMonthTextField.placeholder = "Current Month"
        expirationMonthTextField.addTarget(self,
                                      action: #selector(expMonthTextChanged),
                                      for: .editingChanged
        )
        let expMonthItemStackView = UIStackView(arrangedSubviews: [
            expMonthLabel,
            expirationMonthTextField
        ])
        itemStackViewStyle(expMonthItemStackView)
        
        // Expi Month Stack
        let expYearLabel = UILabel()
        itemLabelStyle(expYearLabel)
        expYearLabel.text = "Expiration Year *"
        let expirationYearTextField = UITextField()
        expirationTextFieldStyle(expirationYearTextField)
        expirationYearTextField.placeholder = "Current Year"
        expirationYearTextField.addTarget(self,
                                      action: #selector(expYearTextChanged),
                                      for: .editingChanged
        )
        let expYearItemStackView = UIStackView(arrangedSubviews: [
            expYearLabel,
            expirationYearTextField
        ])
        itemStackViewStyle(expYearItemStackView)
                
        let expirationStackView = UIStackView(
            arrangedSubviews: [
                expMonthItemStackView,
                expYearItemStackView
            ]
        )
        expirationStackViewStyle(expirationStackView)
                
        // Name Stack
        let cardHolderLabel = UILabel()
        itemLabelStyle(cardHolderLabel)
        cardHolderLabel.text = "Credit Holder"
        let cardHolderTextField = UITextField()
        cardHolderTextField |>
            defaultTextFieldStyle
            <> namePadStyle
        cardHolderTextField.placeholder = "John Pleased"
        let cardHolderItemStackView = UIStackView(arrangedSubviews: [
            cardHolderLabel,
            cardHolderTextField
        ])
        itemStackViewStyle(cardHolderItemStackView)
                
        // securityCode
        let securityCodeLabel = UILabel()
        itemLabelStyle(securityCodeLabel)
        securityCodeLabel.text = "CVC(security code)*"
        let securityCodeTextField = UITextField()
        securityCodeTextField |> defaultTextFieldStyle
                        <> numberPadStyle
        securityCodeTextField.placeholder = "XXXX"
        securityCodeTextField.addTarget(self,
                                        action: #selector(cvcCodeTextChanged),
                                        for: .editingChanged)
                                      
        let securityCodeIconStackView = UIStackView(arrangedSubviews: [
            securityCodeTextField
        ])
        
        securityCodeIconStackView |> horizontalStackStyle()
        
        let securityCodeItemStackView = UIStackView(arrangedSubviews: [
            securityCodeLabel,
            securityCodeIconStackView
        ])
        itemStackViewStyle(securityCodeItemStackView)
                
                
        let bottomStackView = UIStackView(
            arrangedSubviews: [
                securityCodeItemStackView,
                UIView()
            ]
        )
        
        expirationStackViewStyle(bottomStackView)

        let saveCardButton = LoadingButton()
        saveCardButton
            .addTarget(self,
                       action: #selector(saveCardButtonTapped),
                       for: .touchUpInside
        )
        saveCardButtonStyle(saveCardButton)
        
        let Spacer = UIView()
        let rootStackView = UIStackView(arrangedSubviews: [
            nameItemStackView,
            expirationStackView,
            cardHolderItemStackView,
            bottomStackView,
            Spacer,
            saveCardButton,
        ])

        rootStackViewStyle(rootStackView)
        rootView.addSubview(rootStackView)
        rootStackView |>
            rootViewStyle(parent: rootView, .square(.br_grid(4))
                
            )

        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShowNotification),
            name: UIResponder.keyboardWillShowNotification,
            object: nil
        )

        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillHideNotification),
            name: UIResponder.keyboardWillHideNotification,
            object: nil
        )

        creditCardTextField
            .addTarget(self,
                       action: #selector(creditCardTextChanged),
                       for: .editingChanged)
        creditCardTextField.delegate = self
        
        cardHolderTextField
            .addTarget(self,
                       action: #selector(cardHolderTextChanged),
                       for: .editingChanged)
        cardHolderTextField.delegate = self

        expMonthPickerView.delegate = self
        expMonthPickerView.dataSource = self
        expirationMonthTextField.inputView = expMonthPickerView
        expMonthPickerView.tag = PickerViewTag.expMonth.rawValue
        
        expYearPickerView.delegate = self
        expYearPickerView.dataSource = self
        expirationYearTextField.inputView = expYearPickerView
        expYearPickerView.tag = PickerViewTag.expYear.rawValue
        
        currencyPickerView.delegate = self
        currencyPickerView.dataSource = self
        currencyPickerView.tag = PickerViewTag.currency.rawValue
                        
        tapGestureDismissalStyle(self)
                                
        viewStore.produced
            .cardNumber
            .assign(to: \.text, on: creditCardTextField)
        
        viewStore.produced
            .cardHolderName
            .assign(to: \.text, on: cardHolderTextField)
        
//        viewStore.produced
//            .currency
//            .assign(to: \.text, on: currencyTextField)
        
        viewStore.produced.expirationYear
            .compactMap(id)
            .filter { $0 != .zero }
            .map(String.init)
            .assign(to: \.text, on: expirationYearTextField)
        
        viewStore.produced.expirationMonth
            .compactMap(id)
            .filter { $0 != .zero }
            .map(String.init)
            .assign(to: \.text, on: expirationMonthTextField)
                        
        viewStore.produced.cvcCode
            .compactMap(id)
            .map(String.init)
            .assign(to: \.text, on: securityCodeTextField)
        
        creditCardTextField.becomeFirstResponder()
        saveCardButton.addTarget(self,
                                 action: #selector(saveCardButtonTapped),
                                 for: .touchUpInside)

        viewStore.produced
            .isAddCardRequestInFlight
            .startWithValues {
                $0
                ? saveCardButton.showLoader(userInteraction: true)
                : saveCardButton.hideLoader()
        }

        viewStore.produced.alert
            .startWithValues { [weak self] (alert) in
                guard let self = self else { return }
                guard let alert = alert else { return }
                let alertController = UIAlertController(
                    title: alert.title,
                    message: alert.message,
                    preferredStyle: .alert
                )
                alertController.addAction(
                    UIAlertAction(
                        title: "Cancel",
                        style: .default,
                        handler: { _ in
                            self.viewStore.send(.viewDismissed)
                    })
                )
                self.present(alertController, animated: true, completion: nil)
        }
                                               
    }

    @objc
    func cardHolderTextChanged(_ textField: UITextField) {
        viewStore.send(.cardHolderName(textField.text))
    }
           
    @objc
    func creditCardTextChanged(_ textField: UITextField) {
        viewStore.send(.cardNumber(textField.text))
    }
    
    @objc
    func cvcCodeTextChanged(_ textField: UITextField) {
        viewStore.send(.cvcCode(textField.text))
    }
    
    @objc
    func expYearTextChanged(_ textField: UITextField) {
        guard let text = textField.text, let expYear = Int(text) else { return }
        viewStore.send(
            .expirationYear(NSNumber(value: expYear).uintValue))
    }
    
    @objc
    func expMonthTextChanged(_ textField: UITextField) {
        guard let text = textField.text, let expYear = Int(text) else { return }
        viewStore.send(
            .expirationMonth(NSNumber(value: expYear).uintValue))
    }
    
    
              
    @objc func keyboardWillShowNotification(_ notification: NSNotification) {
        scrollView.contentInset = .bottomEdge(keyboardHeight(notification))
        scrollView.scrollIndicatorInsets = .bottomEdge(keyboardHeight(notification))
        var aRect = rootView.frame
        aRect.size.height -= keyboardHeight(notification)
        if aRect.contains(activeView.frame.origin) {
            self.scrollView
                .scrollRectToVisible(
                    activeView.frame,
                    animated: true
                )
        }
    }
    
    @objc func keyboardWillHideNotification(_ notification: NSNotification) {
        scrollView.contentInset = .zero
        scrollView.scrollIndicatorInsets = .zero
    }
        
    @objc
    func saveCardButtonTapped(_ btn: UIButton) {
        viewStore.send(.saveCardButtonTapped)
    }
    
    public override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        viewStore.send(.viewDidDisappear)
    }
           
    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewStore.send(.viewDidAppear)
        if !isMovingToParent {
            self.viewStore.send(.viewDismissed)
        }
    }
    
}

extension AddCreditCardViewController: UITextFieldDelegate {
    
    public func textViewDidBeginEditing(_ textView: UITextView) {
        self.activeView = textView
    }
    
    public func textFieldDidBeginEditing(_ textField: UITextField) {
        self.activeView = textField
    }
}

let saveCardButtonStyle: (LoadingButton) -> Void =
    brMainLoadingButtonStyle
    <> heightAnchor(.br_grid(12))
    <> {
        $0.setTitle("SAVE CARD")
    }


enum PickerViewTag: Int {
    case expMonth = 0, expYear = 1, currency = 2
}


extension AddCreditCardViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        guard let tag = PickerViewTag(rawValue: pickerView.tag) else { fatalError() }
        switch tag {
        case .currency:
            return viewStore.currencies.count
        case .expMonth:
            return viewStore.expMonths.count
        case .expYear:
            return viewStore.expYears.count
        }
    }
    
    
    public func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        guard let tag = PickerViewTag(rawValue: pickerView.tag) else { fatalError() }
        switch tag {
        case .currency:
            return viewStore.currencies[row]
        case .expMonth:
            return String(viewStore.expMonths[row])
        case .expYear:
            return String(viewStore.expYears[row])
        }
    }
                
    public func pickerView(
        _ pickerView: UIPickerView,
        didSelectRow row: Int,
        inComponent component: Int) {
        guard let tag = PickerViewTag(rawValue: pickerView.tag) else { return }
        switch tag {
        case .currency:
            viewStore.send(
                .currency(viewStore.currencies[row])
            )
        case .expMonth:
            viewStore.send(
                .expirationMonth(viewStore.expMonths[row])
            )
        case .expYear:
            viewStore.send(
                .expirationYear(viewStore.expYears[row])
            )
        }
        
    }
    
}

extension AddCreditCardViewController {
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.tag == 1 {
            //viewStore.send(.setFirstResponder(.password))
        }
        return true
    }
}

private let emailStackViewStyle: (UIStackView) -> Void =  {
    $0.alignment = .fill
    $0.axis = .vertical
    $0.spacing = .br_grid(2)
}


private let expirationTextFieldStyle: (UITextField) -> Void =
    brTextFieldStyle <> {
        $0.tag = 2
        $0.autocorrectionType = .no
        $0.autocapitalizationType = .none
        $0.keyboardType = .emailAddress
        $0.placeholder = "Strings.authentication_email_textfield"
    } <> arrowDownViewStyle
    <> heightAnchor(.br_grid(14))


private let numberPadStyle: (UITextField) -> Void = {
    $0.keyboardType = .numberPad
}

private let namePadStyle: (UITextField) -> Void = {
    $0.keyboardType = .namePhonePad
}
    

private let defaultTextFieldStyle: (UITextField) -> Void =
    brTextFieldStyle <> {
        $0.tag = 1
        $0.autocorrectionType = .yes
        $0.autocapitalizationType = .none
        $0.keyboardType = .default
    } <> heightAnchor(.br_grid(14))

private let defaultTextViewStyle: (UITextView) -> Void =
    brTextViewStyle <> {
        $0.tag = 1
        $0.autocorrectionType = .yes
        $0.autocapitalizationType = .none
        $0.keyboardType = .default
        $0.textContainerInset = .square(.br_grid(4))
        $0.insetsLayoutMarginsFromSafeArea = true
    } <> heightAnchor(.br_grid(38))

private func rootViewStyle(parent: UIView, _ inset: UIEdgeInsets = .zero) -> (UIView) -> Void {
    return {
        NSLayoutConstraint.activate([
            $0.topAnchor.constraint(
                equalTo: parent.topAnchor,
                constant: inset.top),
            $0.leadingAnchor.constraint(
                equalTo: parent.leadingAnchor,
                constant: inset.left),
            $0.trailingAnchor.constraint(
                equalTo: parent.trailingAnchor,
                constant: -inset.right
            )
        ])
    }
}
