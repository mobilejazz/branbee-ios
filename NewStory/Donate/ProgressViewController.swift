import UIKit
import Library
import ReactiveSwift

private let loadingIndicatorStyle: (MaterialLoadingIndicator) -> Void =
    autoLayoutStyle <>
    squareStyle(.br_grid(25)) <> { indicator in
        indicator.color = .mainColor
        indicator.radius = .br_grid(12)
        indicator.lineWidth = .br_grid(2)
    }

private let titleLabelStyle: (UILabel) -> Void =
    centerStyle
    <> brLabelDarkBoldStyle
    <> {
        $0.font = UIFont.py_title1(size: .br_grid(6)).bolded
    }

private let orgLabelStyle: (UILabel) -> Void =
    centerStyle <> brLabelDarkBoldStyle
    <> {
        $0.font = UIFont.py_title1(size: 18).bolded
    }

private let subtitleLabelStyle: (UILabel) -> Void =
    centerStyle <> brDarkTextColor
    <> {
        $0.font = .py_title1(size: 18)
    }

private let thankLabelStyle: (UILabel) -> Void =
    centerStyle
    <> brDarkTextColor
    <> {
        $0.font = .py_title1(size: 18)
        $0.isHidden = true
        $0.text = "Thank you!"
    }
private let rootStackViewStyle: (UIStackView) -> Void = verticalStackStyle(
    .br_grid(3),
    distribution: .fill,
    alignment: .center
)

let centerStackViewStyle: (UIStackView) -> Void = verticalStackStyle(
    .br_grid(1),
    alignment: .center
)

let closeButtonStyle: (LoadingButton) -> Void = brMainLoadingButtonStyle
    <> autoLayoutStyle
    <> {
        $0.setTitle("CLOSE")
        $0.isHidden = true
}
<> heightAnchor(.br_grid(14))

private let checkImageViewStyle: (UIImageView) -> Void =
    squareStyle(.br_grid(24))
    <> roundedStyle(cornerRadius: .br_grid(12))
    <> {
        
        if #available(iOS 13.0, *) {
            $0.image = UIImage(systemName: "checkmark")?.applyingSymbolConfiguration(.init(font: UIFont.boldSystemFont(ofSize: 20)))
        } else {
            $0.image = UIImage(named: "ic_check")
        }
        
        $0.backgroundColor = UIColor(red: 71/255, green: 151/255, blue: 46/255, alpha: 1)
        $0.tintColor = .white
        $0.contentMode = .center
        $0.isHidden = true
    }

func Spacer(_ heigth: CGFloat = .br_grid(4)) -> UIView {
    let spacer = UIView()
    spacer |> heightAnchor(heigth)
    return spacer
}


public struct ProcessingState: Equatable {
    var projectName: String?
    var amount: String?
    
    public init(
        projectName: String? = nil,
        amount: String? = nil) {
        self.projectName = projectName
        self.amount = amount
    }
    
   
    var isProcessing: Bool {
        projectName == nil
    }
    
}


class ProgressViewController: UIViewController {
                
    var closeAction: (() -> Void)? = nil
    
    init(state: SignalProducer<ProcessingState, Never> = .init(value: .init())) {
        self.state = state
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let state: SignalProducer<ProcessingState, Never>
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = .white
        
        let loadingIndicator = MaterialLoadingIndicator()
        loadingIndicatorStyle(loadingIndicator)
                                        
        let checkImageView = UIImageView()
        checkImageViewStyle(checkImageView)
                
        let titleLabel = UILabel()
        titleLabelStyle(titleLabel)
        
        let subTitleLabel = UILabel()
        subtitleLabelStyle(subTitleLabel)
        
        let organizationLabel = UILabel()
        
        
        orgLabelStyle(organizationLabel)
        let thanksLabel = UILabel()
        thankLabelStyle(thanksLabel)
                
        let centerStackView = UIStackView(arrangedSubviews: [
            subTitleLabel,
            organizationLabel
        ])
        centerStackViewStyle(centerStackView)
        let spacer = UIView()
        spacer |> heightAnchor(.br_grid(4))
        
        let rootStackView = UIStackView(arrangedSubviews: [
            loadingIndicator,
            checkImageView,
            Spacer(),
            titleLabel,
            centerStackView,
            Spacer(.br_grid(3)),
            thanksLabel
        ])
        
        self.view.addSubview(rootStackView)
        rootStackView |>
            rootStackViewStyle
            <> centerXStyle(view)
            <> centerYStyle(view)
                
        let closeButton = LoadingButton()
        closeButton.addTarget(self, action: #selector(closeButtonTapped), for: .touchUpInside)
        closeButtonStyle(closeButton)
        
        self.view.addSubview(closeButton)
        closeButton.bottomAnchor.constraint(
            equalTo: view.safeAreaLayoutGuide.bottomAnchor,
            constant: -.br_grid(4)
        ).isActive = true
        closeButton.leadingAnchor.constraint(
            equalTo: view.safeAreaLayoutGuide.leadingAnchor,
            constant: .br_grid(4)
        ).isActive = true
        closeButton.trailingAnchor.constraint(
            equalTo: view.safeAreaLayoutGuide.trailingAnchor,
            constant: -.br_grid(4)
        ).isActive = true
        
        state.producer
            .startWithValues { state in
            state.isProcessing
                ? loadingIndicator.startAnimating()
                : loadingIndicator.stopAnimating()
            thanksLabel.isHidden = state.isProcessing
            
            subTitleLabel.text = state.isProcessing
            ? "Please wait"
            : "$\(state.amount ?? "") were donated to"
            organizationLabel.text = state.projectName
            organizationLabel.isHidden = state.isProcessing
            titleLabel.text =
                state.isProcessing
                ? "Processing Payment"
                : "Payment Successful"
            organizationLabel.isHidden = state.isProcessing
            checkImageView.isHidden = state.isProcessing
            closeButton.isHidden = state.isProcessing
        }
        
    }
    
    @objc func closeButtonTapped() {
        closeAction?()
    }
    
    
}

