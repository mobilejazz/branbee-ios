import UIKit
import Library
import ComposableArchitecture
import BrModels
import ExploreClient
import ReactiveSwift


private let iconImageStyle: (UIImageView) -> Void =
    squareStyle(.br_grid(6)) <> {
        $0.image = UIImage(named: "ic_credit_card")
        $0.tintColor = .secondaryTextColor
        $0.contentMode = .scaleAspectFit
    }

private let rootStackViewStyle: (UIStackView) -> Void =  verticalStackStyle(.br_grid(4)) <> {
    $0.layoutMargins = .square(.br_grid(4))
    $0.isLayoutMarginsRelativeArrangement = true
}

private let topStackViewStyle: (UIStackView) -> Void = horizontalStackStyle(.br_grid(2), alignment: .lastBaseline)

private let infoHolderStackViewStyle: (UIStackView) -> Void =
    horizontalStackStyle(.br_grid(3))

private let leadingStackViewStyle: (UIStackView) -> Void =
    verticalStackStyle(.br_grid(2), distribution: .fillEqually)

private let trailingStackViewStyle:
    (UIStackView) -> Void =
    verticalStackStyle(.br_grid(2),
                distribution: .fillEqually,
                alignment: .leading)

private let checkImageViewStyle: (UIImageView) -> Void =
    squareStyle(.br_grid(6)) <> {
        $0.image = UIImage(named: "ic_check_circle")
        $0.tintColor = .systemRed
        //$0.backgroundColor = .systemRed
    } //<> roundedStyle(cornerRadius: .br_grid(3))

private let creditCardLabelStyle: (UILabel) -> Void = brLabelDarkStyle <> {
    $0.font = UIFont.py_body(size: 16).bolded
} <> brDarkTextColor

private let titleLabelStyle: (UILabel) -> Void = brLabelDarkStyle <> {
    $0.font = UIFont.py_body(size: 12).bolded
} <> brDarkTextColor

private let contentLabelStyle: (UILabel) -> Void =
    brLabelDarkStyle
    <> {
        $0.font = .py_body(size: 12)
    } <> brGrayTextColor

private let viewStyle: (UIView) -> Void =
    viewBorderColor(.systemRed,
                       borderWidth: 1,
                       cornerRadius: .br_grid(1))
    <> {
        $0.backgroundColor = .white
    } <> brShadowStyle

class CreditCardView: UIView {
    
    func configure(_ signal: SignalProducer<CreditCard?, Never>) {
        signal.skipNil().startWithValues { [weak self] creditCard in
            guard let self = self else { return }
            self.holderLabelTitle.text = creditCard.holder
            self.expiresLabelTitle.text = "\(creditCard.expiryMonth)/\(creditCard.expiryYear)"
            self.numberLabelTitle.text = "XXXX XXXX XXXX XXXX " + creditCard.last4Digits
        }
        
    }
        
    let numberLabelTitle = UILabel()
    let holderLabelTitle = UILabel()
    let expiresLabelTitle = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let iconImageView = UIImageView()
        iconImageStyle(iconImageView)
        let titleLabel = UILabel()
        titleLabel.text = "Credit Card"
        creditCardLabelStyle(titleLabel)
        let checkImageView = UIImageView()
        checkImageViewStyle(checkImageView)
        let topStackView = UIStackView(arrangedSubviews: [
            iconImageView,
            titleLabel,
            UIView(),
            checkImageView
        ])
        topStackViewStyle(topStackView)
        
        let holderLabel = UILabel()
        titleLabelStyle(holderLabel)
        holderLabel.text = "Holder"
        let numberLabel = UILabel()
        titleLabelStyle(numberLabel)
        numberLabel.text = "Number"
        let expiresLabel = UILabel()
        titleLabelStyle(expiresLabel)
        expiresLabel.text = "Expires"
        
        let leadingStackView = UIStackView(arrangedSubviews: [
            holderLabel,
            numberLabel,
            expiresLabel
        ])
        leadingStackViewStyle(leadingStackView)
        contentLabelStyle(numberLabelTitle)
        contentLabelStyle(holderLabelTitle)
        contentLabelStyle(expiresLabelTitle)
                        
        let trailingStackView = UIStackView(arrangedSubviews: [
            holderLabelTitle,
            numberLabelTitle,
            expiresLabelTitle
        ])
        trailingStackViewStyle(trailingStackView)
                
        let infoHoldeStackView = UIStackView(
            arrangedSubviews: [
                leadingStackView,
                trailingStackView
            ]
        )
        infoHolderStackViewStyle(infoHoldeStackView)
        
        let rootStackView = UIStackView(arrangedSubviews: [
            topStackView,
            infoHoldeStackView
        ])
        rootStackViewStyle(rootStackView)
        self.addSubview(rootStackView)
        rootStackView.constrainEdges(to: self)
        
        viewStyle(self)
    }
                              
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

private let itemLabelStyle: (UILabel) -> Void =
    brDarkTextColor
    <> autoLayoutStyle
    <> centerStyle
    <> {
        $0.font = .py_body(size: 14)
    }

let donateTitleLabelStyle: (UILabel) -> Void = leftStyle
    <> brLabelDarkBoldStyle
    <> {
        $0.text = "How much would you like to donate?"
    }

let paymentMethodLabelStyle: (UILabel) -> Void =
    leftStyle
    <> brLabelDarkBoldStyle
    <> {
        $0.text = "Select Payment Method"
    }

let donationTextFieldStyle: (UITextField) -> Void = brTextFieldStyle
    <> heightAnchor(.br_grid(12))
    <> {
        $0.placeholder = "0.00"
        $0.keyboardType = .numberPad
        $0.textColor = .blueColor
        $0.rightViewMode = .always
        $0.font = UIFont.py_body(size: 16).bolded
        let label = UILabel()
        label.text = "$"
        label.textColor = .blueColor
        label |>
            autoLayoutStyle
            <> squareStyle(.br_grid(5))
        label.frame = CGRect(
            x: $0.frame.size.width - .br_grid(6),
            y: 5,
            width: 25,
            height: 25
        )
        $0.rightView = label
    }

public let searchBarStyle: (SearchBarView) -> Void = heightAnchor(.br_grid(12))

private let donateTopStackViewStyle: (UIStackView) -> Void = verticalStackStyle(.br_grid(2), alignment: .fill)
    <> marginStyle(.rectangle(v: .br_grid(4), h: .br_grid(2)))

private let paymentMethodStackViewStyle: (UIStackView) -> Void = verticalStackStyle(.br_grid(2), alignment: .fill)
    <> marginStyle(.rectangle(v: .br_grid(4), h: .br_grid(2)))


private let donationRootStackViewStyle: (UIStackView) -> Void =
    verticalStackStyle(.br_grid(2), alignment: .fill)
    <> marginStyle(.rectangle(v: .br_grid(4), h: .br_grid(2)))


private let newPaymentMethodString: () -> NSAttributedString = {
    let attributedString = NSMutableAttributedString(string: "Or add a new payment method", attributes: [
        .font: UIFont.py_body(size: .br_grid(4)),
        .foregroundColor: UIColor.mainColor,
        .kern: 0.15
    ])
    attributedString.addAttribute(.foregroundColor, value: UIColor(white: 0.0, alpha: 0.87), range: NSRange(location: 0, length: 8))
    return attributedString
}

let newMethodButtonStyle: (UIButton) -> Void =
    heightAnchor(.br_grid(10)) <> {
        $0.setAttributedTitle(newPaymentMethodString(), for: .normal)
    } <> {
        $0.contentHorizontalAlignment = .leading
    }

let continueButtonStyle: (UIButton) -> Void =
    brMainButtonStyle
    <> heightAnchor(.br_grid(12))
    <> { $0.setTitle("Continue") }

public struct DonateState: Equatable {
    public let project: BrProject
    public var creditCard: CreditCard? = nil
    public var alert: _AlertState<DonateAction>?
    public var isLoading: Bool = false
    public var isSubscribed: Bool = false
    public var amount: String? = nil
    public var processingState: ProcessingState? = nil
    public var loadingState: LoadingState? = nil
    public init(
        project: BrProject,
        creditCard: CreditCard? = nil,
        alert: _AlertState<DonateAction>? = nil,
        isLoading: Bool = false,
        amount: String? = nil
    ) {
        self.project = project
        self.creditCard = creditCard
        self.alert = alert
        self.isLoading = isLoading
        self.amount = amount
    }
    public var addCreditCard: AddCreditCardState? = nil
}

public enum DonateAction: Equatable {
    case viewDidLoad
    case viewDismissed
    case viewDidAppear
    case viewDidDisppear
    case closeButtonTapped
    case textChanged(String?)
    case newPaymentButtonTapped
    case continueButtonTapped
    case setCard(Result<CreditCard?, Failure>)
    case retryAction(RetryAction)
    case addCreditCard(AddCreditCardAction)
    case event(DonateDelegateEvent)
}

public struct DonateEnvironment {
    var donateClient: (_ projectId: String, _ projectName: String) -> DonateClient
    var addCardClient: AddCardClient
    var stripeClient: StripeClient
    public init(
        donateClient: @escaping (_ projectId: String, _ projectName: String) -> DonateClient = { _,_ in .empty },
        addCardClient: AddCardClient = .empty,
        stripeClient: StripeClient
    ) {
        self.stripeClient = stripeClient
        self.donateClient = donateClient
        self.addCardClient = addCardClient
    }
}

public let donationReducer: Reducer<DonateState, DonateAction, SystemEnvironment<DonateEnvironment>> =
    .combine(
        Reducer { state, action, environment in
            struct CancelDelegateId: Hashable {}
            switch action {
            case .viewDidLoad:
                state.isSubscribed = true
                return .merge(
                    environment.donateClient(state.project.id, state.project.title)
                        .delegate
                        .cancellable(id: CancelDelegateId())
                        .map(DonateAction.event),
                    environment.donateClient(state.project.id, state.project.title)
                        .viewDidLoad
                        .fireAndForget()
                )
            case .viewDidAppear:
                guard !state.isSubscribed else { return .none }
                return .merge(
                    environment
                        .donateClient(state.project.id, state.project.title)
                        .delegate
                        .cancellable(id: CancelDelegateId())
                        .map(DonateAction.event),
                    environment
                        .donateClient(state.project.id, state.project.title)
                        .refresh
                        .fireAndForget()
                    )
            case .viewDismissed:
                state.alert = nil
                state.addCreditCard = nil
                return .none
            case let .retryAction(action):
                return .fireAndForget(action.run)
            case .event(.hideLoading):
                state.loadingState = nil
                return .none
            case let .event(.showCard(card)):
                state.creditCard = card
                return .none
            case .event(.showDonateProgress):
                state.processingState = .init()
                return .none
            case .event(.hideDonateProgress):
                state.processingState = nil
                return .none
            case let .event(.showSuccessDonate(
                                projectName: projectName,
                                amount:  amount)):
                state.processingState = .init(
                    projectName: projectName,
                    amount: amount
                )
                return .none
            case .event(.showLoading):
                state.loadingState = .loading
                return .none
            case .event(.showMissingAmount):
                state.alert = .init(
                    title: "Ooops!",
                    message: "Missing amount",
                    dismissButton: .cancel(send: .viewDismissed)
                )
                return .none
            case .event(.showMissingCard):
                state.alert = .init(
                    title: "Ooops!",
                    message: "Missing Card",
                    dismissButton: .cancel(send: .viewDismissed)
                )
                return .none
            case .event(.showNoCard):
                state.alert = .init(
                    title: "Ooops!",
                    message: "No Card",
                    dismissButton: .cancel(send: .viewDismissed)
                )
                return .none
            case let .event(.showFailure(error)):
                state.alert = .init(
                    title: "Ooops!",
                    message: error.message,
                    dismissButton: .cancel(send: .viewDismissed)
                )
                return .none
            case let .event(.failObtainCard(failure, action: action)):
                state.loadingState = .failure(failure, action: action)
                return .none
            case let .event(.notifiyStripetoGet(cardId: id)):
                return environment.stripeClient .paymentMethods(id)
                    .mapError { Failure(message: $0.localizedDescription, cause: nil)}
                    .catchToEffect()
                    .map(DonateAction.setCard)
            case let .textChanged(amount):
                state.amount = amount
                return .none
            case .newPaymentButtonTapped:
                state.addCreditCard = AddCreditCardState()
                return .none
            case .continueButtonTapped:
                guard let amount = state.amount else { return .none }
                return environment
                    .donateClient(state.project.id, state.project.title)
                    .donateAmount(amount)
                    .fireAndForget()
            case .viewDidDisppear:
                state.isSubscribed = false
                return .cancel(id: CancelDelegateId())
            case .addCreditCard:
                return .none
            case .closeButtonTapped:
                state.processingState = nil
                return .none
            case let .setCard(.success(card)):
                state.loadingState = nil
                state.creditCard = card
                return .none
            case let .setCard(.failure(error)):
                state.loadingState = nil 
                state.alert = .init(
                    title: "Ooops!",
                    message: error.message,
                    dismissButton: .cancel(send: .viewDismissed)
                )
                return .none
            }
        },
        addCreditCardReducer.optional().pullback(
            state: \.addCreditCard,
            action: /DonateAction.addCreditCard,
            environment: {
                $0.map(\.addCard)
            }
        )
    )

extension DonateEnvironment {
    var addCard: AddCardEnvironment {
        .init(
            stripeClient: stripeClient,
            addCardClient: addCardClient
        )
    }
}



public class DonateViewController: UIViewController {
                
    let store: Store<DonateState, DonateAction>
    let viewStore: ViewStore<DonateState, DonateAction>
    
    public init(store: Store<DonateState, DonateAction>) {
        self.store = store
        self.viewStore = ViewStore(store)
        super.init(nibName: nil, bundle: nil)
    }
        
    required init?(coder: NSCoder) {
        fatalError("Unimplemeted")
    }
 
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        viewStore.send(.viewDidLoad)
        
        self.title = "Donate"
        self.view.backgroundColor = .white

        let titleLabel = UILabel()
        donateTitleLabelStyle(titleLabel)
        let donationTextField = UITextField()
        donationTextFieldStyle(donationTextField)
        donationTextField.addTarget(self,
                                    action: #selector(donationTextFieldChanged),
                                    for: .editingChanged)
        donationTextField.delegate = self
                
        let searchBar = SearchBarView()
        searchBar.datasource = .amounts(Amount.allCases)
        searchBar.delegate = self
        searchBarStyle(searchBar)
        
        let topStackView = UIStackView(
            arrangedSubviews: [
                titleLabel,
                donationTextField,
                searchBar
            ]
        )
        donateTopStackViewStyle(topStackView)
                                                
        let paymentMethodLabel = UILabel()
        paymentMethodLabelStyle(paymentMethodLabel)
        let cardView = CreditCardView()
        cardView.configure(viewStore.produced.creditCard)
        
        viewStore.produced.creditCard
            .map { $0 == nil }
            .assign(to: \.isHidden, on: cardView)
        
        
        let newMethodButton = UIButton()
        newMethodButtonStyle(newMethodButton)
        newMethodButton.addTarget(self,
                                  action: #selector(addNewPaymentButtonTapped),
                                  for: .touchUpInside)
        let paymentMethodStackView = UIStackView(arrangedSubviews: [
            paymentMethodLabel,
            cardView,
            newMethodButton,
        ])
        paymentMethodStackViewStyle(paymentMethodStackView)
        
        let continueButton = UIButton()
        continueButtonStyle(continueButton)
        continueButton.addTarget(
            self,
            action: #selector(continueButtonTapped),
            for: .touchUpInside
        )
        
        let rootStackView = UIStackView(arrangedSubviews: [
            topStackView,
            paymentMethodStackView,
            UIView(),
            continueButton.pinBackground(
                .init(white: 0.98, alpha: 1.0),
                cornerRadius: .br_grid(2),
                inset: .square(.br_grid(-4))
            )
        ])
        donationRootStackViewStyle(rootStackView)
        
        self.view.addSubview(rootStackView)
        rootStackView.constrainEdges(to: self.view)
        
        
        let loadingView = LoadingView()
        self.view.addSubview(loadingView)
        loadingView.frame = view.frame
        self.view.bringSubviewToFront(loadingView)
        
        viewStore.produced.loadingState
            .startWithValues { state in
                loadingView.state = state
                loadingView.isHidden = state == nil
            }
                        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
                   target: self,
                   action: #selector(dismissKeyboard))
        tap.delegate = self
        view.addGestureRecognizer(tap)
                
        viewStore.produced.amount
            .assign(to: \.text, on: donationTextField)
        
        viewStore.produced.alert
            .startWithValues { [weak self] (alert) in
                guard let self = self else { return }
                guard let alert = alert else { return }
                let alertController = UIAlertController(
                    title: alert.title,
                    message: alert.message,
                    preferredStyle: .alert
                )
                alertController.addAction(
                    UIAlertAction(
                        title: "Cancel",
                        style: .default,
                        handler: { _ in
                            self.viewStore.send(.viewDismissed)
                        })
                )
                self.present(alertController, animated: true, completion: nil)
            }
        
        // Add New Method
        store.scope(
            state: \.addCreditCard,
            action: DonateAction.addCreditCard
        ).ifLet(then: { [weak self] store in
            guard let self = self else { return }
            self.navigationController?.pushViewController(AddCreditCardViewController(store: store), animated: true)
        }) {
            self.navigationController?.popToViewController(self, animated: true)
        }
                
        let progressVC = ProgressViewController(
            state: viewStore.produced.processingState.skipNil()
        )                
        
        store.scope(state: \.processingState)
            .ifLet { [weak self] store in
                guard let self = self else { return }
                progressVC.closeAction = {
                    self.viewStore.send(.closeButtonTapped)
                    self.navigationController?.popViewController(animated: true)
                }
                progressVC.modalPresentationStyle = .overCurrentContext
                self.present(progressVC, animated: true)
            } else: {
                progressVC.dismiss(animated: true) {
                }
            }
        
    }
    
    @objc func donationTextFieldChanged(_ tf: UITextField) {
        viewStore.send(.textChanged(tf.text))
    }
                        
    @objc func continueButtonTapped() {
        viewStore.send(.continueButtonTapped)
    }
    
    @objc func addNewPaymentButtonTapped() {
        viewStore.send(.newPaymentButtonTapped)
    }
    
    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewStore.send(.viewDidAppear)
        if !isMovingFromParent {
            viewStore.send(.viewDismissed)
        }
    }
    
    public override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        viewStore.send(.viewDidDisppear)
    }

}

extension DonateViewController: SearchBarViewDelegate {
    public func searchBar(_ sb: SearchBarView, didSelectAmount amount: Amount) {
        viewStore.send(.textChanged("\(amount.rawValue)"))
    }
}

extension DonateViewController: UIGestureRecognizerDelegate {
    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if touch.view is UICollectionViewCell {
            return false
        }
        return true
    }
}

private let activationStyle: (Bool) -> (UITextField) -> Void = { active in { tf in
    tf |> viewBorderColor(
        active ? .blueColor: .whiteBorderColor,
        borderWidth: 1,
        cornerRadius: .br_grid(1)
    )
}}

extension DonateViewController: UITextFieldDelegate {
    public func textFieldDidBeginEditing(_ textField: UITextField) {
        textField |> activationStyle(true)
    }
    
    public func textFieldDidEndEditing(_ textField: UITextField)
    {
        textField |> activationStyle(false)
    }
}

