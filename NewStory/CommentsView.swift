import UIKit
import Library
import ExploreClient
import ComposableArchitecture
import ReactiveSwift
import BrModels
import ImageLoader
import InputBarAccessoryView

public struct CommentsState: Equatable {
    public let parentId: String
    public let parentType: BrComment.´Type´
    public var comments: [BrComment] = []
    public var comment: String?
    public var alert: _AlertState<CommentsAction>?
    public var didReachEnd: Bool = false
    public var loadingState: LoadingState? = nil
    
    public init(
        parentId: String,
        parentType: BrComment.´Type´,
        comments: [BrComment] = [],
        comment: String? = nil,
        alert: _AlertState<CommentsAction>? = nil
    ) {
        self.parentId = parentId
        self.parentType = parentType
        self.comments = comments
        self.comment = comment
        self.alert = alert
    }
    
    
}

public enum CommentsAction: Equatable {
    case viewDidLoad
    case viewDidAppear
    case viewWillDisappear
    case viewDismissed
    case loadMore
    case sendButtonTapped
    case textChanged(String?)
    case event(CommentDelegateEvent)
    case retry(RetryAction)
    case cell(index: Int, action: CommentCellAction)
}

public struct CommentsEnvironment {
    var commentsClient: (String, BrComment.´Type´) -> CommentsClient
    var imageLoader: ImageLoader
    public init(
        commentsClient: @escaping (String, BrComment.´Type´) -> CommentsClient,
        imageLoader: ImageLoader
    ) {
        self.commentsClient = commentsClient
        self.imageLoader = imageLoader
    }
}

public let commentsReducer: Reducer<CommentsState, CommentsAction, SystemEnvironment<CommentsEnvironment>> =
    .combine(
        Reducer { state, action, environment in
            struct CancelId: Hashable {}
            switch action {
            case .viewDidLoad:
                return .merge(
                    environment.commentsClient(state.parentId, state.parentType)
                        .delegate
                        .cancellable(id: CancelId())
                        .map(CommentsAction.event),
                    environment.commentsClient(state.parentId, state.parentType)
                        .viewDidLoad
                        .fireAndForget()
                )
            case .viewDidAppear:
                return .none
            case .viewDismissed:
                state.alert = nil
                state.loadingState = nil
                return .none
            case .event(.didRemoveCommentText):
                state.comment = nil
                return .none
            case let .event(.loadComments(
                                comments: comments,
                                reachedTheEnd: reachedTheEnd)
                ):
                state.didReachEnd = reachedTheEnd
                state.comments = comments
                return .none
            case .event(.displayEmptyState):
                state.loadingState = .message("No Comments")
                return .none
            case let .event(.displayError(failure, retryBlock: action)):
                state.loadingState = .failure(failure, action: action)
                return .none
            case .event(.showLoading):
                state.loadingState = .loading
                return .none
            case .event(.hideLoading):
                state.loadingState = nil
                return .none
            case let .event(.displayFailure(failure)):
                state.alert = _AlertState(
                    title: "Ooops!",
                    message: failure.message,
                    dismissButton: .cancel(send: .viewDismissed)
                )
                return .none
            case let .retry(action):
                return .fireAndForget(action.run)
            case .loadMore:
                guard !state.didReachEnd else { return .none }
                return environment
                    .commentsClient(
                        state.parentId, state.parentType)
                    .loadMore
                    .fireAndForget()
            case .cell:
                return .none
            case .viewWillDisappear:
                return .merge(
                    environment
                        .commentsClient(state.parentId, state.parentType)
                        .detachView
                        .fireAndForget(),
                    Effect.cancel(id: CancelId())
                )
            case let .textChanged(text):
                state.comment = text
                return .none
            case .sendButtonTapped:
                return environment
                    .commentsClient(state.parentId, state.parentType)
                    .sendComment(state.comment!)
                    .fireAndForget()
            }
        },
        commentCellReducer.forEach(
            state: \.comments,
            action: /CommentsAction.cell(index:action:),
            environment: { $0.map(\.imageLoader) }
        )
    )


final class BranbeeInputBar: InputBarAccessoryView {
    
    var image: UIImage? {
        didSet {
            guard let inputBar = self.leftStackView.arrangedSubviews.first as? InputBarButtonItem else { return }
            inputBar.image = image
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure() {
        inputTextView.placeholder = "Write a comment..."
        let button = InputBarButtonItem()
        button.onKeyboardSwipeGesture { item, gesture in
            if gesture.direction == .left {
                item.inputBarAccessoryView?.setLeftStackViewWidthConstant(to: 0, animated: true)
            } else if gesture.direction == .right {
                item.inputBarAccessoryView?.setLeftStackViewWidthConstant(to: .br_grid(9), animated: true)
            }
        }
        button.setSize(CGSize(width: .br_grid(9), height: .br_grid(9)), animated: false)
        roundedStyle(cornerRadius: 18)(button)
        button.imageView?.contentMode = .scaleAspectFill
        inputTextView.textContainerInset = UIEdgeInsets(top: 8, left: 16, bottom: 8, right: 16)
        inputTextView.placeholderLabelInsets = UIEdgeInsets(
            top: 8,
            left: 20,
            bottom: 8,
            right: 20
        )
        inputTextView.scrollIndicatorInsets = UIEdgeInsets(top: 8, left: 0, bottom: 8, right: 0)
        setLeftStackViewWidthConstant(to: .br_grid(9), animated: false)
        setStackViewItems([button], forStack: .left, animated: false)
        sendButton.title = nil
        sendButton.image = UIImage(named: "ic_send")
        sendButton.imageView?.tintColor = .mainColor
        sendButton.tintColor = .mainColor        
        shouldAnimateTextDidChangeLayout = true
    }
    
}


public class CommentsViewController: UIViewController {
    
    let store: Store<CommentsState, CommentsAction>
    let viewStore: ViewStore<CommentsState, CommentsAction>
    
    public init(store: Store<CommentsState, CommentsAction>) {
        self.store = store
        self.viewStore = ViewStore(store)
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var collectionView: UICollectionView!
    let inputBar = BranbeeInputBar()
    
    var datasource: [BrComment] = [] {
        didSet {
            self.collectionView.reloadData()
        }
    }
    
    public override var inputAccessoryView: UIView? {
        inputBar
    }
    
    public override var canBecomeFirstResponder: Bool {
        true
    }
    
    public override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        viewStore.send(.viewWillDisappear)
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Comments"
        
        viewStore.send(.viewDidLoad)
        
        inputBar.becomeFirstResponder()
                                
        let layout = UICollectionViewFlowLayout()
        collectionFlowLayoutStyle(layout)
        collectionView = UICollectionView(
            frame: .zero,
            collectionViewLayout: layout
        )
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionViewStyle(collectionView)
        self.view.addSubview(collectionView)
                
        collectionView.constraintsBottonLayoutMargins(to: view)
        
        let loadingView = LoadingView()
        self.view.addSubview(loadingView)
        loadingView.frame = view.frame
        self.view.bringSubviewToFront(loadingView)
        
        viewStore.produced.loadingState
            .startWithValues { state in
                loadingView.state = state
                loadingView.isHidden = state == nil
            }
        
        viewStore.produced.alert
            .startWithValues { [weak self] (alert) in
                guard let self = self else { return }
                guard let alert = alert else { return }
                let actionController = UIAlertController(
                    title: alert.title,
                    message: alert.message,
                    preferredStyle: .alert
                )
                alert.primaryButton
                    .map { $0.toUIKit(send: self.viewStore.send) }
                    .map(actionController.addAction)
                alert.secondaryButton
                    .map { $0.toUIKit(send: self.viewStore.send) }
                    .map(actionController.addAction)
                self.present(actionController,
                             animated: true,
                             completion: nil)
        }
                             
        inputBar.delegate = self
        inputBar.inputTextView.keyboardType = .twitter
        collectionView.keyboardDismissMode = .interactive
        
        viewStore.produced.comment
            .startWithValues{ value in
                if value == nil {
                    self.inputBar.inputTextView.resignFirstResponder()
                }
                self.inputBar.inputTextView.text = value
            }
        
        inputBar.image = UIImage(named: "pexels")
                                                               
        viewStore.produced.comments
            .observe(on: QueueScheduler.main)
            .assign(to: \.datasource, on: self)
    }
           
    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewStore.send(.viewDidAppear)
        if !isMovingToParent {
            viewStore.send(.viewDismissed)
        }
    }
    
    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        viewStore.send(.viewWillDisappear)
    }
    
}

extension CommentsViewController: InputBarAccessoryViewDelegate {
    
    public func inputBar(_ inputBar: InputBarAccessoryView, textViewTextDidChangeTo text: String) {
        guard !text.isEmpty else { return }
        viewStore.send(.textChanged(text))
    }
    
    public func inputBar(_ inputBar: InputBarAccessoryView, didPressSendButtonWith text: String) {
        viewStore.send(.sendButtonTapped)
    }
    
    public func inputBar(_ inputBar: InputBarAccessoryView, didChangeIntrinsicContentTo size: CGSize) {
        collectionView.contentInset.bottom = size.height + 300
    }
    
}

private let collectionViewStyle: (UICollectionView) -> Void =
    autoLayoutStyle <> {
        $0.register(CommentViewCell.self,
                    forCellWithReuseIdentifier: CommentViewCell.reuseIdentifier)
        $0.backgroundColor = .white
        $0.contentInset = .topEdge(.br_grid(4))
    }

private let collectionFlowLayoutStyle: (UICollectionViewFlowLayout) -> Void = {
    $0.scrollDirection = .vertical
    $0.sectionHeadersPinToVisibleBounds = true
    $0.minimumLineSpacing = .br_grid(4)
    $0.minimumInteritemSpacing = .br_grid(4)
    $0.sectionInsetReference = .fromContentInset
    $0.sectionInset = .horizontal(.br_grid(4))
}

extension CommentsViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        datasource.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.section == datasource.count - 1 {
            viewStore.send(.loadMore)
        }
    }
                   
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let commentCell = collectionView.dequeueReusableCell(
            withReuseIdentifier: CommentViewCell.reuseIdentifier,
            for: indexPath
        ) as? CommentViewCell else {
            fatalError()
        }
        commentCell.configure(store.scope(
            state:  { $0.comments[indexPath.section] },
            action: { .cell(index: indexPath.section, action: $0) }
        ))
        return commentCell
    }
}

extension CommentsViewController: UICollectionViewDelegateFlowLayout {            
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellWidth = collectionView.bounds.width - .br_grid(8)
        let body = viewStore.comments[safe: indexPath.section]?.body ?? ""
        let cellHeight = heightForText(body, width: collectionView.bounds.width - .br_grid(18), font: .py_body(size: 16)) + .br_grid(20)
        return CGSize(
            width: cellWidth,
            height: cellHeight
        )
    }
}

