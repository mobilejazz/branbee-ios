import UIKit
import Library

private let viewCommentsButtonStyle: (UIButton) -> Void =
    autoLayoutStyle
    <> {
        $0.setTitleColor(.secondaryTextColor, for: .normal)
        $0.titleLabel?.font = .py_headline(size: 16)
        $0.setTitle("View Comments")
        $0.titleLabel?.textAlignment = .center
    }

private let linkActionLabelStyle: (UILabel) -> Void =
    autoLayoutStyle
    <> brDarkTextColor
    <> {
        $0.font = .py_headline(size: 16)
        $0.text = "Link Actions"
        $0.textAlignment = .left
    }


private let linkActionButtonStyle: (UIButton) -> Void =   
    brButtonStyle
    <> {
        $0.backgroundColor = .blueColor
        $0.setImage(UIImage(named: "ic_add"))
        $0.tintColor = .white
    }

public class ViewCommentsFooter: UICollectionReusableView {
    
    public var action: (() -> Void)? = nil
    
    public class var reuseIdentifier: String {
        ViewCommentsFooter.self.description()
    }
    public override func prepareForReuse() {
        super.prepareForReuse()
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        let viewCommentsButton = UIButton()
        viewCommentsButton.addTarget(self,
                                    action: #selector(viewCommentsButtonTapped),
                                    for: .touchUpInside)
        viewCommentsButtonStyle(viewCommentsButton)
        self.addSubview(viewCommentsButton)
        viewCommentsButton.constrainEdges(
            to: self
        )
    }
    
    @objc func viewCommentsButtonTapped() {
        action?()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

public class LinkActionHeader: UICollectionReusableView {
    public let titleLabel = UILabel()
    public class var reuseIdentifier: String {
        LinkActionHeader.self.description()
    }
    public override func prepareForReuse() {
        super.prepareForReuse()
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = .white
        
        linkActionLabelStyle(titleLabel)
        self.addSubview(titleLabel)
        titleLabel.constrainEdges(
            to: self,
            insets: .horizontal(.br_grid(4))
        )
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

public class LinkActionFooter: UICollectionReusableView {
    
    public var action: (() -> Void)? = nil
    
    public class var reuseIdentifier: String {
        LinkActionFooter.self.description()
    }
    
    public let linkActionsButton = UIButton()
    
    public override func prepareForReuse() {
        super.prepareForReuse()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        linkActionsButton.addTarget(self,
                                    action: #selector(linkActionButtonTapped),
                                    for: .touchUpInside)
        linkActionButtonStyle(linkActionsButton)
        self.addSubview(linkActionsButton)
        linkActionsButton.constrainEdges(
            to: self,
            insets: .horizontal(.br_grid(4))
        )
    }
    
    @objc func linkActionButtonTapped() {
        action?()
    }
    
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

