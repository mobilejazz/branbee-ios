import UIKit
import Library
import ExploreClient
import ComposableArchitecture
import ReactiveSwift
import BrModels
import ImageLoader

public struct LinkProjectState: Equatable {
    public let storyId: String
    public let communityId: String
    public var projectStates: [LinkProjectCellState]
    public var isLoadingRequestInFlight: Bool
    public var alert: _AlertState<LinkProjectAction>?
    public var loadingState: LoadingState?
    public var didProjectLinked: Bool? = nil
    public var isRequestAccepted: Bool? = nil
    //searchState?
    public init(
        storyId: String,
        communityId: String,
        projects: [BrProject] = [],
        isLoadingRequestInFlight: Bool = false
    ) {
        self.storyId = storyId
        self.communityId = communityId
        self.projectStates = projects.map { LinkProjectCellState(project:$0) }
        self.isLoadingRequestInFlight = isLoadingRequestInFlight
    }
}

public enum LinkProjectAction: Equatable {
    case viewDidLoad
    case viewDidAppear
    case viewDismissed
    case viewDidDisppear
    case searchButtonTapped
    case dismissAlert
    case event(LinkProjectDelegateEvent)
    case actionCell(index: Int, action: LinkProjectCellAction)
}

public struct LinkProjectEnvironment {
    public var linkProjectClient: (String, String) -> LinkProjectClient
    public var imageLoader: ImageLoader
    public init(
        linkProjectClient: @escaping (String, String) -> LinkProjectClient,
        imageLoader: ImageLoader
    ) {
        self.linkProjectClient = linkProjectClient       
        self.imageLoader = imageLoader
    }
}

public let linkProjectReducer: Reducer<LinkProjectState, LinkProjectAction, SystemEnvironment<LinkProjectEnvironment>> =
    .combine(
        Reducer { state, action, environment in
            struct CancelDelegateId: Hashable {}
            struct AlertId: Hashable {}
            switch action {
            case .viewDidLoad:
                return .merge(
                    environment.linkProjectClient(
                        state.storyId, state.communityId
                    )
                    .delegate
                    .cancellable(id: CancelDelegateId())
                    .map(LinkProjectAction.event),
                    environment.linkProjectClient(state.storyId, state.communityId)
                        .viewDidLoad
                        .fireAndForget()
                )
            case .viewDidAppear:
                return .none
            case .viewDismissed:
                state.alert = nil
                state.loadingState = nil
                return .none
            case let .event(.showProjects(projects)):
                state.projectStates = projects.map(LinkProjectCellState.init(project:))
                return .none
            case .searchButtonTapped:
                return .none
            case let .event(.showFailure(failure, retryBlock: retryBlock)):
                state.loadingState = .failure(
                    failure,
                    action: retryBlock
                )
                return .none
            case .event(.displayRequestSentMessage):
                state.isRequestAccepted = true
                return Effect(value: .dismissAlert)
                    .delay(2.0, on: environment.mainQueue())
                    .cancellable(id: AlertId())
            case .event(.showNoProjectFound):
                state.loadingState = .message("No Project Found!")
                return .none
            case let .event(.showAlert(failure)):
                state.alert = .init(
                    title: "Alert",
                    message: failure.message,
                    dismissButton: .cancel(send: .viewDismissed)
                )
                return .none
            case let .event(.dismissView(projectLinked: projectLinked)):
                state.didProjectLinked = projectLinked
                return .cancel(id: AlertId())
            case .viewDidDisppear:
                return .merge(
                    Effect.cancel(id: CancelDelegateId()),
                    Effect.cancel(id: AlertId())
                )
            case .event(.showLoading):
                state.loadingState = .loading
                return .none
            case .event(.hideLoading):
                state.loadingState = nil
                return .none
            case .dismissAlert:
                state.isRequestAccepted = nil
                return .none
            case let .actionCell(index, action: .selectButtonTapped):
                let project = state.projectStates[index].project
                return environment
                    .linkProjectClient(state.storyId, state.communityId)
                    .linkProject(project)
                    .fireAndForget()
            case .actionCell:
                return .none
            }
        },
        linkProjectCellReducer.forEach(
            state: \.projectStates,
            action: /LinkProjectAction.actionCell(index:action:),
            environment: {
                $0.map(\.imageLoader)
            }
        )    
    )
public let searchButtonStyle: (UIButton) -> Void = {
    $0.setImage(UIImage(named: "ic_search"))
    $0.tintColor = .white
}

import ComposableArchitecture
public class LinkProjectViewController: UIViewController {
    
    let store: Store<LinkProjectState, LinkProjectAction>
    let viewStore: ViewStore<LinkProjectState, LinkProjectAction>
    
    public init(store: Store<LinkProjectState, LinkProjectAction>) {
        self.store = store
        self.viewStore = ViewStore(store)
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var collectionView: UICollectionView!
    var datasource: [LinkProjectCellState] = [] {
        didSet {
            collectionView.reloadData()
        }
    }
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Link Action"
        
        viewStore.send(.viewDidLoad)
                                
        let layout = UICollectionViewFlowLayout()
        collectionFlowLayoutStyle(layout)
        collectionView = UICollectionView(
            frame: .zero,
            collectionViewLayout: layout
        )
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionViewStyle(collectionView)
        self.view.addSubview(collectionView)
        collectionView.constrainEdges(to: view)
        
        let loadingView = LoadingView()
        self.view.addSubview(loadingView)
        loadingView.frame = view.frame
        self.view.bringSubviewToFront(loadingView)
        
        viewStore.produced.loadingState
            .startWithValues { state in
                loadingView.state = state
                loadingView.isHidden = state == nil
            }
        
        let alertView = RequestSubmittedView()
        view.addSubview(alertView)
        alertView |>
            autoLayoutStyle
            <> bottomEdgeStyle(self.view,
                .init(
                    top: 0,
                    left: .br_grid(4),
                    bottom: .br_grid(30),
                    right: .br_grid(4))
            )
            <> heightAnchor(.br_grid(18))
        view.bringSubviewToFront(alertView)
        viewStore.produced.isRequestAccepted
            .startWithValues { state in
                if let state = state {
                    alertView.configure(
                         RequestSubmittedView.State(
                            isSuccess: state
                        )
                    )
                }
                alertView.isHidden = state == nil
            }
        
        
        viewStore.produced.projectStates
            .assign(to: \.datasource, on: self)
        
        viewStore.produced.alert
            .startWithValues { [weak self] (alert) in
                guard let self = self else { return }
                guard let alert = alert else { return }
                
                let actionController = UIAlertController(
                    title: alert.title,
                    message: alert.message,
                    preferredStyle: .alert
                )
                alert.primaryButton
                    .map { $0.toUIKit(send: self.viewStore.send) }
                    .map(actionController.addAction)
                
                alert.secondaryButton
                    .map { $0.toUIKit(send: self.viewStore.send) }
                    .map(actionController.addAction)
                
                self.present(actionController,
                             animated: true,
                             completion: nil)
        }
        
        
        viewStore.produced.didProjectLinked
            .skipNil()
            .startWithValues { (dismissed) in
                if dismissed {
                    self.navigationController?.popViewController(animated: true)
                } else {
                    
                }
            }
        
                    
        let searchButton = UIButton()
        searchButtonStyle(searchButton)
        searchButton.addTarget(self,
                               action: #selector(searchButtonTapped),
                               for: .touchUpInside
            )
        
        //self.navigationItem.rightBarButtonItems = [searchButton].map(UIBarButtonItem.init(customView:))
        
    }
    
    @objc func searchButtonTapped() {
        viewStore.send(.searchButtonTapped)
    }
    
    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewStore.send(.viewDidAppear)
        if !isMovingToParent {
            viewStore.send(.viewDismissed)
        }
    }
    
    public override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)        
        viewStore.send(.viewDidDisppear)
    }
}

private let collectionViewStyle: (UICollectionView) -> Void =
    autoLayoutStyle <> {
        $0.register(LinkProjectViewCell.self,
                    forCellWithReuseIdentifier: LinkProjectViewCell.reuseIdentifier)
        $0.backgroundColor = .white
    }

private let collectionFlowLayoutStyle: (UICollectionViewFlowLayout) -> Void = {
    $0.scrollDirection = .vertical
    $0.sectionHeadersPinToVisibleBounds = true
    $0.minimumLineSpacing = .br_grid(4)
    $0.minimumInteritemSpacing = .br_grid(4)
    $0.sectionInsetReference = .fromContentInset
    $0.sectionInset = .topHorizontalEdge(.br_grid(4))
}

extension LinkProjectViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        return datasource.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let projecCell = collectionView.dequeueReusableCell(
            withReuseIdentifier: LinkProjectViewCell.reuseIdentifier,
            for: indexPath
        ) as? LinkProjectViewCell else {
            fatalError()
        }
        projecCell.configure(store.scope(
            state:  { $0.projectStates[safe: indexPath.section] ?? .init(project: .branbee) },
            action: { .actionCell(index: indexPath.section, action: $0) }
        ))
        return projecCell                     
    }
}

extension LinkProjectViewController: UICollectionViewDelegateFlowLayout {
            
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        CGSize(
            width: collectionView.bounds.width - .br_grid(8),
            height: .br_grid(45)
        )
    }
    
}


