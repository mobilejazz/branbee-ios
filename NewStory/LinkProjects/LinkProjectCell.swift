import Library
import UIKit
import ComposableArchitecture
import BrModels
import ImageLoader

private let organizationLabelStyle: (UILabel) -> Void = brLabelStyle <> {
    $0.font = .py_caption1(size: .br_grid(3))
}

private let selectButtonStyle: (UIButton) -> Void = brMainButtonStyle
    <> heightAnchor(.br_grid(9))
    <> widthAnchor(.br_grid(23))
    <> {
        $0.setTitle("Select")
    }

private let donateBottomStackViewStyle: (UIStackView) -> Void = horizontalStackStyle(
    .br_grid(4),
    alignment: .center
)

private let bigTitleLabelStyle: (UILabel) -> Void =
    brLabelStyle <> {
        $0.font = UIFont.py_title2(size: .br_grid(5)).bolded
        $0.adjustsFontSizeToFitWidth = true
    }

private let backgroundImageViewStyle: (UIImageView) -> Void = autoLayoutStyle <> {
    $0.backgroundColor = .blueSecondColor
    $0.isUserInteractionEnabled = true
}

private let orgIconStyle: (UIImageView) -> Void =
    squareStyle(.br_grid(5))
    <> {
        $0.image = UIImage(named: "ic_community_small")
        $0.contentMode = .scaleAspectFit
    }

private let componentLabelStyle: (UILabel) -> Void = brLabelDarkStyle

private let boostsStackViewStyle: (UIStackView) -> Void = horizontalStackStyle(.br_grid(1))

private let rootStackViewStyle: (UIStackView) -> Void =
    verticalStackStyle(0)
    <> marginStyle()

public struct LinkProjectCellState: Equatable {
    let project: BrProject
    var picture: UIImage?
    public init(
        project: BrProject,
        picture: UIImage? = nil
    ) {
        self.project = project
        self.picture = picture
    }
    public init(
        project: BrProject
    ) {
        self.project = project
        self.picture = nil
    }
}

public enum LinkProjectCellAction: Equatable {
    case loadCell
    case setImage(UIImage?)
    case selectButtonTapped
}


public let linkProjectCellReducer =
Reducer<LinkProjectCellState, LinkProjectCellAction, SystemEnvironment<ImageLoader>>{
    state, action, environment in
    switch action {
    case .loadCell:
        guard let pictureString = state.project.picture,
              let photoURL = URL(string: pictureString) else { return .none }
        return environment.loadFromURL(photoURL)
            .start(on: environment.globalQueue())
            .observe(on: environment.mainQueue())
            .map(LinkProjectCellAction.setImage)
    case let .setImage(picture):
        state.picture = picture
        return .none
    case .selectButtonTapped:
        return .none
    }
}

internal let overlayViewStyle: (UIView) -> Void = {
    $0.backgroundColor = UIColor.black.withAlphaComponent(0.4)
} <> autoLayoutStyle

public class LinkProjectViewCell: UICollectionViewCell {
    
    public class var reuseIdentifier: String { "LinkProjectViewCell" }
        
    var viewStore: ViewStore<BrProject, LinkProjectCellAction>!
    func configure(_ store: Store<LinkProjectCellState, LinkProjectCellAction>) {
        
        viewStore = ViewStore(store.scope(state: \.project))
        
        viewStore.send(.loadCell)
        
        viewStore.produced.community
            .map{ $0.name }
            .assign(to: \.text, on: orgLabel)
        
        viewStore.produced.title
            .map(id)
            .assign(to: \.text, on: bigTitleLabel)
        
        ViewStore(store).produced.picture
            .compactMap(id)
            .assign(to: \.image, on: backgroundImageView)
            
    }
    
    let backgroundImageView = UIImageView()
    let orgIcon = UIImageView()
    let orgLabel = UILabel()
    let bigTitleLabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let overlayView = UIView()
        overlayViewStyle(overlayView)
        backgroundImageView.addSubview(overlayView)
        overlayView.constrainEdges(to: backgroundImageView)
        backgroundImageViewStyle(backgroundImageView)
        self.addSubview(backgroundImageView)
        backgroundImageView.constrainEdges(to: self)
        orgIconStyle(orgIcon)
        organizationLabelStyle(orgLabel)
                        
        let boostsStackView = UIStackView(arrangedSubviews: [
            orgIcon, orgLabel
        ])
        boostsStackViewStyle(boostsStackView)
        bigTitleLabelStyle(bigTitleLabel)
                                
        let selectButton = UIButton()
        selectButton.isUserInteractionEnabled = true
        selectButton.addTarget(self,
                              action: #selector(selectButtonTapped),
                              for: .touchUpInside)
        selectButtonStyle(selectButton)
        
        let donateBottonStackView = UIStackView(arrangedSubviews: [
            UIView(),
            selectButton
        ])
        
        donateBottomStackViewStyle(donateBottonStackView)
        
        let rootStackView = UIStackView(arrangedSubviews: [
            boostsStackView,
            bigTitleLabel,
            UIView(),
            donateBottonStackView
        ])
        rootStackView |>
            rootStackViewStyle <> {
                $0.spacing = .br_grid(2)
            }
        backgroundImageView.addSubview(rootStackView)
        rootStackView.constrainEdges(to: backgroundImageView)
        
        self |> roundedStyle(cornerRadius: .br_grid(2))
        
    }
    
    @objc func selectButtonTapped() {
        viewStore.send(.selectButtonTapped)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public override func prepareForReuse() {
        super.prepareForReuse()
        
    }
    
}
