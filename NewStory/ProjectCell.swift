import UIKit
import Library
import BrModels
import ComposableArchitecture
import ImageLoader
import ReactiveSwift
import SDWebImage

private let organizationLabelStyle: (UILabel) -> Void = brLabelStyle <> {
    $0.font = .py_body(size: .br_grid(3))
}

public let plegdeLabelStyle: (UILabel) -> Void = brLabelStyle <> {
    $0.font = .py_body(size: 14)
}

private let componentLabelLightStyle: (UILabel) -> Void = {
    $0.font = .py_caption1(size: 14)
    $0.textColor = .white
}

public let brProgressViewStyle: (UIProgressView) -> Void = {
    $0.tintColor = .mainColor
    $0.backgroundColor = .white
    $0.trackTintColor = .white
} <> heightAnchor(.br_grid(2))
  <> roundedStyle(cornerRadius: .br_grid(1))

public let progressViewStyle: (UIProgressView) -> Void =
    brProgressViewStyle
  <> viewBorderColor(.white,
                   borderWidth: 1,
                   cornerRadius: .br_grid(1)
)

private let donateButtonStyle: (UIButton) -> Void =
    brButtonStyle <> {
        $0.backgroundColor = .mainColor
    }
    <> heightAnchor(.br_grid(9))
    <> {
        $0.setTitle("Donate")
        $0.titleLabel?.font = UIFont.py_body(size: 14).bolded
    } <> widthAnchor(.br_grid(23))

private let donateBottomStackViewStyle: (UIStackView) -> Void = horizontalStackStyle(
    .br_grid(4),
    alignment: .center
)

private let bigTitleLabelStyle: (UILabel) -> Void =
    brLabelStyle <> {
        $0.font = UIFont.py_title2(size: .br_grid(5)).bolded
        $0.adjustsFontSizeToFitWidth = true
        $0.numberOfLines = 1
        
    }

private let backgroundImageViewStyle: (UIImageView) -> Void = autoLayoutStyle <> {
    $0.contentMode = .scaleToFill
}

private let commentsButtonStyle: (UIButton) -> Void = squareStyle(.br_grid(5)) <> {
    $0.setImage(UIImage(named: "ic_chat_off"))
    $0.tintColor = .white
    $0.contentMode = .scaleAspectFit
}

private let donatationCellStyle:(ProjectViewCell) -> Void =
     roundedStyle(cornerRadius: .br_grid(1))

public enum ProjectCellAction: Equatable {
    case commentsButtonTapped
    case donateButtonTapped
}

public let projectCellReducer = Reducer<BrProject, ProjectCellAction, SystemEnvironment<ImageLoader>> { state, action, environment in
    switch action {
    case .commentsButtonTapped, .donateButtonTapped:
        return .none
    }
}

private let orgIconStyle: (UIImageView) -> Void = squareStyle(.br_grid(4)) <> {
    $0.image = UIImage(named: "ic_community_small")
    $0.tintColor = .white
    $0.contentMode = .scaleAspectFit
}

public class ProjectViewCell: UICollectionViewCell {
    
    public class var reuseIdentifier: String { "ProjectViewCell" }
    
    var viewStore: ViewStore<BrProject, ProjectCellAction>!
    
    public func configure(_ store: Store<BrProject, ProjectCellAction>) {
        
        viewStore = ViewStore(store)
                                           
        viewStore.produced.community
            .map { $0.name }
            .assign(to: \.text, on: orgLabel)
        
        viewStore.produced.title
            .map(id)
            .assign(to: \.text, on: bigTitleLabel)
        
        viewStore.produced.comments
            .map(String.init)
            .assign(to: \.text, on: commentsLabel)
                
        viewStore.produced.producer
            .map { ($0.pledged, $0.target) }
            .map { pledged, target in
                return "\(pledged)$ pledged of \(target)$ goal"
            }.assign(to: \.text, on: plegdeLabel)
                
        viewStore.produced.producer            
            .map { Float($0.pledged) / Float($0.target) }
            .assign(to: \.progress, on: progressView)
        
        backgroundImageView.sd_setImage(with: viewStore.photoURL)        
    }
    
    let backgroundImageView = UIImageView()
    let orgLabel = UILabel()
    let orgIcon = UIImageView()
    let bigTitleLabel = UILabel()
    let plegdeLabel = UILabel()
    let commentsLabel = UILabel()
    let progressView = UIProgressView(progressViewStyle: .bar)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
                                
        let overlayView = UIView()
        overlayViewStyle(overlayView)
        backgroundImageView.addSubview(overlayView)
        overlayView.constrainEdges(to: backgroundImageView)
        backgroundImageViewStyle(backgroundImageView)
        self.addSubview(backgroundImageView)
        backgroundImageView.constrainEdges(to: self)                
        
        orgIconStyle(orgIcon)

        organizationLabelStyle(orgLabel)
        let boostsStackView = UIStackView(arrangedSubviews: [
            orgIcon,
            orgLabel
        ])
        outputsStackViewStyle(boostsStackView)
                        
        bigTitleLabelStyle(bigTitleLabel)
                                
        plegdeLabelStyle(plegdeLabel)
                                        
        progressViewStyle(progressView)
                                
        let commentsButton = UIButton()
        commentsButton.addTarget(self,
                                 action: #selector(commentsButtonTapped),
                                 for: .touchUpInside)
        commentsButtonStyle(commentsButton)
                
        componentLabelLightStyle(commentsLabel)
        
        let commentsStackView = UIStackView(arrangedSubviews: [
            commentsButton,
            commentsLabel
        ])
        outputsStackViewStyle(commentsStackView)
                
        let donateButton = UIButton()
        donateButton.addTarget(self,
                                 action: #selector(donateButtonTapped),
                                 for: .touchUpInside)
        donateButtonStyle(donateButton)
        
        let donateBottonStackView = UIStackView(arrangedSubviews: [
            UIView(),
            commentsStackView,
            donateButton
        ])
        
        donateBottomStackViewStyle(donateBottonStackView)
        
        let rootStackView = UIStackView(arrangedSubviews: [
            boostsStackView,
            bigTitleLabel,
            UIView(),
            progressView,
            plegdeLabel,
            donateBottonStackView
        ])
        rootStackView |>
            donateRootStackViewStyle <> {
                $0.spacing = .br_grid(2)
            }
        
        self.insertSubview(rootStackView, aboveSubview: backgroundImageView)
        rootStackView.constrainEdges(to: self)
        
        
        donatationCellStyle(self)
    }
    
    @objc func commentsButtonTapped() {
        viewStore.send(.commentsButtonTapped)
    }
    
    @objc func donateButtonTapped() {
        viewStore.send(.donateButtonTapped)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public override func prepareForReuse() {
        super.prepareForReuse()
    }
    
}

private let donateRootStackViewStyle: (UIStackView) -> Void =
    verticalStackStyle(.br_grid(1))
<> marginStyle()
