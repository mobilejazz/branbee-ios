import UIKit
import Library
import ExploreClient
import ComposableArchitecture
import ReactiveSwift
import BrModels
import ImageLoader

public struct LinkStoryState: Equatable {
    public let projectId: String
    public let communityId: String
    public var storiesStates: [LinkStoryCellState]
    public var searchText: String?
    public var isSearchBarShown: Bool = false
    public var loadingState: LoadingState? = nil
    public var alert: _AlertState<LinkStoryAction>? = nil
    public var didDismissed: Bool? = nil
    public init(
        projectId: String,
        communityId: String,
        stories: [BrStory] = [],        
        searchText: String? = nil
    ) {
        self.projectId = projectId
        self.communityId = communityId
        self.storiesStates = stories.map(LinkStoryCellState.init(story:))
        self.searchText = searchText
    }
}

public enum LinkStoryAction: Equatable {
    case viewDidLoad
    case viewDidAppear
    case viewDismissed
    case viewDidDisppear
    case searchButtonTapped
    case searchTextChanged(String?)
    case event(LinkStoryDelegateEvent)
    case actionCell(index: Int, action: LinkStoryCellAction)
}

public struct LinkStoryEnvironment {
    public var linkStoryClient: (String, String) -> LinkStoryClient
    public var imageLoader: ImageLoader
    public init(
        linkStoryClient: @escaping (String, String) -> LinkStoryClient,
        imageLoader: ImageLoader
    ) {        
        self.linkStoryClient = linkStoryClient
        self.imageLoader = imageLoader
    }
}

public let linkStoryReducer: Reducer<LinkStoryState, LinkStoryAction, SystemEnvironment<LinkStoryEnvironment>> =
    .combine(
        Reducer { state, action, environment in
            struct CancelDelegateId: Hashable {}
            switch action {
            case .viewDidLoad:
                return .merge(
                    environment.linkStoryClient(state.projectId, state.communityId)
                        .delegate
                        .cancellable(id: CancelDelegateId())
                        .map(LinkStoryAction.event),
                    environment.linkStoryClient(state.projectId, state.communityId)
                        .viewDidLoad
                        .fireAndForget()
                )
            case .viewDidAppear:
                return .none
            case .viewDismissed:
                state.loadingState = nil
                state.alert = nil
                state.isSearchBarShown = false
                return .none
            case let .event(.showStories(stories, didReachEnd: didReachEnd)):
                state.storiesStates = stories.map(LinkStoryCellState.init(story:))
                return .none
            case .searchButtonTapped:
                state.isSearchBarShown = true
                return .none
            case let .event(.showFailure(failure, retryBlock: retryBlock)):
                state.loadingState = .failure(failure, action: retryBlock)
                return .none
            case .event(.displayRequestSentMessage):
                return .none
            case .event(.showNoStoryFound):
                state.loadingState = .message("No Story Found")
                return .none
            case let .event(.showAlert(failure)):
                state.alert = .init(
                    title: "Alert",
                    message: failure.message,
                    dismissButton: .cancel(send: .viewDismissed)
                )
                return .none
            case .event(.dismissView(storyLinked: let storyLinked)):
                state.didDismissed = true
                return .none
            case .event(.showLoading):
                state.loadingState = .loading
                return .none
            case .event(.hideLoading):
                state.loadingState = nil
                return .none
            case let .searchTextChanged(text):
                state.searchText = text
                return environment
                    .linkStoryClient(state.projectId, state.communityId)
                    .searchText(text ?? "")
                    .debounce(1.5, on: environment.mainQueue())
                    .fireAndForget()
            case .viewDidDisppear:
                return .cancel(id: CancelDelegateId())
            case let .actionCell(index, action: .selectButtonTapped):
                let story = state.storiesStates[index].story
                return environment
                    .linkStoryClient(state.projectId, state.communityId)
                    .linkStory(story)
                    .fireAndForget()                    
            case .actionCell:
                return .none
            }
        },
        linkStoryCellReducer.forEach(
            state: \.storiesStates,
            action: /LinkStoryAction.actionCell(index:action:),
            environment: { $0 }
        )
    )


public let searchControllerStyle: (UISearchController) -> Void = {
    $0.obscuresBackgroundDuringPresentation = false
    $0.searchBar.autocapitalizationType = .words
    $0.searchBar.sizeToFit()
    if #available(iOS 13.0, *) {
        $0.searchBar.searchTextField.font = UIFont.py_footnote()
    } else {
        ($0.searchBar.value(forKey: "searchField") as? UITextField)?
            .font = UIFont.py_footnote()
    }
    $0.searchBar.searchBarStyle = .prominent
}

import ComposableArchitecture
public class LinkStoryViewController: UIViewController {
    
    let store: Store<LinkStoryState, LinkStoryAction>
    let viewStore: ViewStore<LinkStoryState, LinkStoryAction>
    
    let searchController = UISearchController()    
    
    public init(store: Store<LinkStoryState, LinkStoryAction>) {
        self.store = store
        self.viewStore = ViewStore(store)
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var collectionView: UICollectionView!
    var datasource: [LinkStoryCellState] = [] {
        didSet {
            collectionView.reloadData()
        }
    }
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Link Story"
        
        viewStore.send(.viewDidLoad)
                                
        let layout = UICollectionViewFlowLayout()
        collectionFlowLayoutStyle(layout)
        collectionView = UICollectionView(
            frame: .zero,
            collectionViewLayout: layout
        )
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionViewStyle(collectionView)
        self.view.addSubview(collectionView)
        collectionView.constrainEdges(to: view)
        
        // Loading View
        let loadingView = LoadingView()
        self.view.addSubview(loadingView)
        loadingView.frame = view.frame
        self.view.bringSubviewToFront(loadingView)
        
        viewStore.produced.loadingState
            .startWithValues { state in
                loadingView.state = state
                loadingView.isHidden = state == nil
            }
        
        viewStore.produced.didDismissed
            .compactMap(id)
            .startWithValues { didDismissed in
                self.navigationController?.popViewController(animated: true)
                self.dismiss(animated: true, completion: nil)
            }
        
        viewStore.produced.alert
            .startWithValues { [weak self] (alert) in
                guard let self = self else { return }
                guard let alert = alert else { return }
                let actionController = UIAlertController(
                    title: alert.title,
                    message: alert.message,
                    preferredStyle: .alert
                )
                alert.primaryButton
                    .map { $0.toUIKit(send: self.viewStore.send) }
                    .map(actionController.addAction)
                alert.secondaryButton
                    .map { $0.toUIKit(send: self.viewStore.send) }
                    .map(actionController.addAction)
                self.present(
                    actionController,
                    animated: true,
                    completion: nil
                )
        }
        
        viewStore.produced.storiesStates
            .assign(to: \.datasource, on: self)
                    
        let searchButton = UIButton()
        searchButtonStyle(searchButton)
        searchButton.addTarget(self,
                               action: #selector(searchButtonTapped),
                               for: .touchUpInside
            )
                
        viewStore.produced.isSearchBarShown
            .startWithValues { [weak self] isShown in
                guard let self = self else { return  }
                self.searchController.isActive = isShown
                //self.navigationItem
                  //  .hidesSearchBarWhenScrolling = !isShown
            }
                        
        self.definesPresentationContext = true
        navigationItem.searchController = searchController
        searchControllerStyle(searchController)
        searchController.delegate = self
        searchController.searchResultsUpdater = self
        searchController.searchBar.placeholder = "Search"        
                                                            
        self.navigationItem.rightBarButtonItems = [searchButton].map(UIBarButtonItem.init(customView:))
        
    }
    
    @objc func searchButtonTapped() {
        viewStore.send(.searchButtonTapped)
    }
    
    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewStore.send(.viewDidAppear)
        if !isMovingToParent {
            viewStore.send(.viewDismissed)
        }
    }
}

extension LinkStoryViewController: UISearchResultsUpdating, UISearchControllerDelegate {
    public func updateSearchResults(for searchController: UISearchController) {
        viewStore.send(
            .searchTextChanged(searchController.searchBar.text)
        )
    }
    
    public func didDismissSearchController(_ searchController: UISearchController) {
        viewStore.send(.viewDismissed)
    }
}

private let collectionViewStyle: (UICollectionView) -> Void =
    autoLayoutStyle <> {
        $0.register(LinkStoryCell.self,
                    forCellWithReuseIdentifier: LinkStoryCell.reuseIdentifier)
        $0.backgroundColor = .white
    }

private let collectionFlowLayoutStyle: (UICollectionViewFlowLayout) -> Void = {
    $0.scrollDirection = .vertical
    $0.sectionHeadersPinToVisibleBounds = true
    $0.minimumLineSpacing = .br_grid(4)
    $0.minimumInteritemSpacing = .br_grid(4)
    $0.sectionInsetReference = .fromContentInset
    $0.sectionInset = .topHorizontalEdge(.br_grid(4))
}

extension LinkStoryViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        return datasource.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let storyCell = collectionView.dequeueReusableCell(
            withReuseIdentifier: LinkStoryCell.reuseIdentifier,
            for: indexPath
        ) as? LinkStoryCell else {
            fatalError()
        }
        storyCell.configure(store.scope(
            state:  { $0.storiesStates[safe: indexPath.section] ?? .init(story: .branbeeStory) },
            action: { .actionCell(index: indexPath.section, action: $0) }
        ))
        return storyCell
    }
}

extension LinkStoryViewController: UICollectionViewDelegateFlowLayout {
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.bounds.width - .br_grid(8)       
        return CGSize(
            width: width,
            height: .br_grid(95)
        )
    }
}
