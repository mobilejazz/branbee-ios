import Library
import UIKit
import ComposableArchitecture
import BrModels
import SDWebImage

public struct LinkStoryCellState: Equatable {
    let story: BrStory
    public init(
        story: BrStory
    ) {
        self.story = story
    }
}

public enum LinkStoryCellAction: Equatable {
    case selectButtonTapped
}

public let linkStoryCellReducer = Reducer<LinkStoryCellState, LinkStoryCellAction, SystemEnvironment<LinkStoryEnvironment>>{
        state, action, environment in
        return .none
    }

private let personImageViewStyle: (UIImageView) -> Void =
    squareStyle(.br_grid(8))
    <> roundedStyle(cornerRadius: .br_grid(4))
    <> {
        $0.backgroundColor = .systemRed
    }

private let storyStackViewStyle: (UIStackView) -> Void =
    verticalStackStyle(.br_grid(2),
                       distribution: .fill,
                       alignment: .fill)


private let storyImageViewStyle: (UIImageView) -> Void = heightAnchor(.br_grid(52))

private let storyContentLabelStyle: (UILabel) -> Void =
    brGrayTextColor <> {
        $0.textAlignment = .left
        $0.numberOfLines = 3
        $0.font = .py_caption2(size: .br_grid(4))
    }


private let rootStackViewStyle: (UIStackView) -> Void =
    verticalStackStyle(.br_grid(2))

private let donateButtonStyle: (UIButton) -> Void = brMainButtonStyle
    <> heightAnchor(.br_grid(9))
    <> widthAnchor(.br_grid(23))
    <> {
        $0.setTitle("Select")
    }

private let selectBottomStackViewStyle: (UIStackView) -> Void = horizontalStackStyle(
    .br_grid(4),
    alignment: .center
) <> marginStyle(.trailing(.br_grid(4)))


private let linkCellStyle: (LinkStoryCell) -> Void =
    roundedStyle(cornerRadius: .br_grid(1))
    <> brShadowStyle <> {
        $0.backgroundColor = .white
    }

class LinkStoryCell: UICollectionViewCell {
    
    class var reuseIdentifier: String { "LinkStoryCell" }
    
    var viewStore: ViewStore<LinkStoryCellState, LinkStoryCellAction>!
    func configure(_ store: Store<LinkStoryCellState, LinkStoryCellAction>) {
        
        viewStore = ViewStore(store)
                                     
        viewStore.produced.story
            .map { $0.content }
            .map(textContent)
            .map(convertToHtml)
            .assign(to: \.attributedText, on: storyContentLabel)
        
        self.storyImageView.sd_setImage(with: pictureURL(viewStore.story))
    }
    
    let storyImageView = UIImageView()
    let storyContentLabel = EdgeLabel(edge: .topHorizontalEdge(.br_grid(4)))
        
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        linkCellStyle(self)
                                        
        storyImageViewStyle(storyImageView)
        storyContentLabelStyle(storyContentLabel)
        
        let storyStackView = UIStackView(arrangedSubviews: [
            storyImageView,
            storyContentLabel,
        ])
        storyStackViewStyle(storyStackView)
        
        let selectButton = UIButton()
        selectButton.addTarget(self,
                               action: #selector(selectButtonTapped),
                               for: .touchUpInside)
        donateButtonStyle(selectButton)
        let selectBottonStackView = UIStackView(arrangedSubviews: [
            UIView(),
            selectButton
        ])
        
        selectBottomStackViewStyle(selectBottonStackView)
        let rootStackView = UIStackView(arrangedSubviews: [
            storyStackView,
            selectBottonStackView
        ])
        rootStackViewStyle(rootStackView)
        self.addSubview(rootStackView)
        rootStackView.constrainEdges(to: self)
    }
    
    @objc func selectButtonTapped() {
        viewStore.send(.selectButtonTapped)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
                          
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}



