import UIKit
import ComposableArchitecture
import ReactiveSwift
import Library
import Account


let (parent, child) = playgroundControllers(child:
    AddCreditCardViewController(
        store: Store(
            initialState: AddCreditCardState(),
            reducer: addCreditCardReducer,
            environment: AddCreditCardEnvironment(
                mainQueue: QueueScheduler(),
                globalQueue: QueueScheduler()
            )
    ))
)



import PlaygroundSupport
PlaygroundPage.current.liveView = parent

