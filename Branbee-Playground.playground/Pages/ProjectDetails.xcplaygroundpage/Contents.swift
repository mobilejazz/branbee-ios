import UIKit
import Library
import ComposableArchitecture
import ImageLoader
import ReactiveSwift
import Explore
import ExploreClient
import NewStory
import BrModels

import PlaygroundSupport
PlaygroundPage.current.liveView = ProjectDetailViewController(
    store: .init(
        initialState: .init(project: .testProject, sections: [.project(.init(project: BrProject.testProject))]),
        reducer: .empty,
        environment: ()
    ))
