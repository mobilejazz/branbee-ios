import Library
import UIKit

private let personImageViewStyle: (UIImageView) -> Void =
    squareStyle(.br_grid(8))
    <> {
        $0.backgroundColor = .systemRed
    }

private let storyStatusImageViewStyle: (UIImageView) -> Void =
    squareStyle(.br_grid(8))
    <> {
        $0.backgroundColor = .systemRed
    }

private let titleLabelStyle: (UILabel) -> Void = {
    $0.textColor = .secondaryTextColor
    $0.font = .py_body(size: .br_grid(4))
}

private let subTitleLabelStyle: (UILabel) -> Void =
    brGrayTextColor
    <> {
        $0.font = .py_body(size: 14)
        $0.numberOfLines = 0
    }

private let titleStackViewStyle: (UIStackView) -> Void =
    verticalStackStyle(.br_grid(1),
                       distribution: .fill,
                       alignment: .leading
    )
private let topStackViewStyle: (UIStackView) -> Void =
    horizontalStackStyle(.br_grid(4),
                       distribution: .fill,
                       alignment: .top)
    

private let bottomStackViewStyle: (UIStackView) -> Void = horizontalStackStyle(.br_grid(3))

private let rootStackViewStyle: (UIStackView) -> Void =
    verticalStackStyle(.br_grid(1))
    <> marginStyle(.rectangle(v: .br_grid(2), h: .br_grid(4)))


let rejectButtonStyle: (UIButton) -> Void = heightAnchor(.br_grid(9)) <> widthAnchor(.br_grid(23)) <> {
    $0.titleLabel?.font = .py_headline(size: 14)
}

let acceptButtonStyle: (UIButton) -> Void = heightAnchor(.br_grid(9))
    <> widthAnchor(.br_grid(23))
    <> {
        $0.titleLabel?.font = .py_headline(size: 14)
    }

class NotificationCell: UICollectionViewCell {
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let personImageView = UIImageView()
        personImageViewStyle(personImageView)
        let titleLabel = UILabel()
        titleLabel.text = "Story linking request"
        titleLabelStyle(titleLabel)
        let subTitleLabel = UILabel()
        subTitleLabel.text = "Petter requested to link an story in your action."
        subTitleLabelStyle(subTitleLabel)
        
        let titleStackView = UIStackView(arrangedSubviews: [
            titleLabel, subTitleLabel
        ])
        titleStackViewStyle(titleStackView)
        let topStackView = UIStackView(arrangedSubviews: [
            personImageView,
            titleStackView
        ])
        topStackViewStyle(topStackView)
        let rejectButton = UIButton()
        rejectButtonStyle(rejectButton)
        rejectButton.setTitle("REJECT")
        rejectButton.setTitleColor(.systemRed, for: .normal)
        let acceptButton = UIButton()
        acceptButtonStyle(acceptButton)
        acceptButton.setTitle("ACCEPT")
        acceptButton.setTitleColor(.systemGreen, for: .normal)
        let bottomStackView = UIStackView(arrangedSubviews: [
            UIView(),
            rejectButton,
            acceptButton,
        ])
        bottomStackViewStyle(bottomStackView)
        let rootStackView = UIStackView(arrangedSubviews: [
            topStackView,
            bottomStackView
        ])        
        rootStackViewStyle(rootStackView)
        
        self.addSubview(rootStackView)
        rootStackView.constrainEdges(to: self)
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


import PlaygroundSupport
PlaygroundPage.current.liveView = NotificationCell(
    frame: .init(origin: .zero, size: CGSize(
        width: .br_grid(90),
        height: .br_grid(48)
    ))
)
