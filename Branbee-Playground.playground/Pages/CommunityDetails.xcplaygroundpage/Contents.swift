import Library
import UIKit
import ReactiveSwift
import BrModels
import ComposableArchitecture
import ImageLoader
import Explore

let (parent, _)  =  playgroundControllers(
    child: CommunityDetailViewController(store: Store(
        initialState: CommunityDetailState(feedItems: [.init(item: .brStoryFeed),
             .init(item: .brProjectFeed)],
            isJoined: true,
            community: .branbeCommunity
        ),
        reducer: .empty,
        environment: ()
    )))

import PlaygroundSupport
PlaygroundPage.current.liveView = parent


