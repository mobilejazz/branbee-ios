import UIKit

extension NSAttributedString {

    public convenience init(
        htmlString html: String,
        font: UIFont? = nil,
        useDocumentFontSize: Bool = true
    ) throws {
        let options: [NSAttributedString.DocumentReadingOptionKey : Any] = [
            .documentType: NSAttributedString.DocumentType.html,
            .characterEncoding: String.Encoding.utf8.rawValue
        ]

        let data = html.data(using: .utf8, allowLossyConversion: true)
        guard (data != nil),
              let fontFamily = font?.familyName,
              let attr = try? NSMutableAttributedString(
                data: data!,
                options: options,
                documentAttributes: nil
              )
        else {
            try self.init(data: data ?? Data(html.utf8), options: options, documentAttributes: nil)
            return
        }

        let fontSize: CGFloat? = useDocumentFontSize ? nil : font!.pointSize
        let range = NSRange(location: 0, length: attr.length)
        attr.enumerateAttribute(.font, in: range, options: .longestEffectiveRangeNotRequired) { attrib, range, _ in
            if let htmlFont = attrib as? UIFont {
                let traits = htmlFont.fontDescriptor.symbolicTraits
                var descrip = htmlFont.fontDescriptor.withFamily(fontFamily)

                if (traits.rawValue & UIFontDescriptor.SymbolicTraits.traitBold.rawValue) != 0 {
                    descrip = descrip.withSymbolicTraits(.traitBold)!
                }

                if (traits.rawValue & UIFontDescriptor.SymbolicTraits.traitItalic.rawValue) != 0 {
                    descrip = descrip.withSymbolicTraits(.traitItalic)!
                }

                attr.addAttribute(.font, value: UIFont(descriptor: descrip, size: fontSize ?? htmlFont.pointSize), range: range)
            }
        }

        self.init(attributedString: attr)
    }
            
}

let value = "<p><br/></p><!--StartFragment--><p>WE MUST MOTIVATE OUR YOUTH BY PLANTING TREES  </p><!--EndFragment--><p><br/></p><p><br/></p><!--StartFragment--><p><em>HAF Project Manager</em></p><p><br/></p><div dir=\"ltr\"><p>High Atlas Foundation in Morocco is involving all of society’s parts <br/>and actors to help establish sustainable development and build a shining<br/> future where everyone gets the right support needed. As an example, to <br/>fight to show this vision, the HAF team is preparing each group of <br/>youth, those who are in the schools, universities, associations, and <br/>youth centers… They can change the future of their community and <br/>families… But how can this be happening in Morocco?</p><br/><p style=\"text-align: center;\"><img id=\"main-image\" class=\"zoom aligncenter\" src=\"https://i.postimg.cc/T29GBtZ9/Said.jpg\" width=\"600\" height=\"450\"/></p><br/><p>With positive thoughts, engagement, and encouragement, HAF is using <br/>simple methods and ways to motivate and involve Moroccan youth. For <br/>example, the <a href=\"https://highatlasfoundation.org/fruit-for-thought-at-the-childrens-protection-center-in-fes/\">Child Protection Center</a>s<br/> receive children of different ages and different cases, many of whom <br/>have no familiarity with growing trees, or that from a seed we can have <br/>future trees for Moroccan families. This is what we experienced with <br/>those kids and youth in one of those Child Protection Centers here in <br/>Morocco. To practice agriculture means, to many of the kids in the Fez <br/>Center, that you will feel messy and dirty! This is how they were <br/>thinking when the idea of building a fruit tree nursery at the same <br/>center came true with HAF and other partners. A few of them joined the <br/>new agricultural workshop in their centers for the first couple of <br/>weeks, and later the HAF team who took care of the nursery project <br/>invested more effort into having the young teenagers explore and change <br/>the routine that they may have had before.</p><br/><p style=\"text-align: center;\"><img src=\"https://i.postimg.cc/TY18Ffjd/SaidBn.jpg\" id=\"main-image\" class=\"zoom aligncenter\" width=\"387\" height=\"516\"/></p><br/><p>Day by day, they built a team involved in learning and discovering a <br/>new life that was established around them. It was waiting only for who <br/>was going to take the <a href=\"https://highatlasfoundation.org/hopeful-futures-caleb-tisdale-1-july-2019/\">initiative</a> to bring that young generation all together to a special place in a social environment.</p><br/><p>The kids are having the chance to learn how to plant seeds and to <br/>take care of the saplings, by weeding out the weeds, installing the <br/>irrigation system, grafting, and growing trees organically. More than <br/>that, the children will build a good relationship with the environment <br/>where they live because of the education and the lessons they learn from<br/> the nursery activities.</p><br/><p>The working program at the beginning in this center is to keep the <br/>kids with the nursery team watching the progress of building the nursery<br/> and preparing the land. Within a few days, the natural world that was <br/>scaring them became an open space with new activities, preparing the <br/>soil, installing the irrigation system, preparing the seeds to be ready <br/>to go in the soil, and dealing with the surrounding plants and existing <br/>trees.</p><br/><p>In just one month of working hard, planning, and encouragement, more <br/>children wanted to plant seeds by themselves and come to the garden each<br/> day to make sure the seeds received enough water and to remove the <br/>weeds. A child of 15 years old left the center, and when he came back <br/>four months later, the seeds he planted by his own hands had become <br/>saplings and even taller than he is. This was very surprising and served<br/> as a big motivation for the young child and his friends to be engaged <br/>in planting seeds and learn with the High Atlas Foundation about <br/>agriculture and how we participate in protecting the local environment. <br/>More than that, all the children at the Abdelaziz Ben Driss center, were<br/> able to see most of the seeds become future fruit trees that will go <br/>directly to their families and community.</p><br/><p style=\"text-align: center;\"><img src=\"https://i.postimg.cc/7L0r993S/SaidBna.jpg\" id=\"main-image\" class=\"zoom aligncenter\" width=\"350\" height=\"467\"/></p><br/><p>This is an example of how HAF is engaging a group of youth to be part<br/> of society even though they are confined to the center. Thank you, <br/>Ecosia from Germany for choosing Morocco and High Atlas Foundation as <br/>one of your partners to plant trees and serve the local communities with<br/> all the environmental, economic and social supports. Thank you to all <br/>those using Ecosia around the world.</p><br/></div><!--EndFragment--><p><br/></p><p><br/></p>"


public let html: (String) -> NSAttributedString? = { string in
    let data = Data(string.utf8)
    let attributedString = try? NSMutableAttributedString(
        data: data,
        options: [
            .documentType: NSAttributedString.DocumentType.html,
            .characterEncoding: String.Encoding.utf8.rawValue
        ],
        documentAttributes: nil
    )
    return attributedString
}

extension NSMutableAttributedString {

    func trimmedAttributedString(set: CharacterSet) -> NSMutableAttributedString {

        let invertedSet = set.inverted

        var range = (string as NSString).rangeOfCharacter(from: invertedSet)
        let loc = range.length > 0 ? range.location : 0

        range = (string as NSString).rangeOfCharacter(
                            from: invertedSet, options: .backwards)
        let len = (range.length > 0 ? NSMaxRange(range) : string.count) - loc

        let r = self.attributedSubstring(from: NSMakeRange(loc, len))
        return NSMutableAttributedString(attributedString: r)
    }
}



import BrModels
import PlaygroundSupport
import Library
let text = BrStory.salma.content.first?.content ?? ""

let label = UILabel(
    frame: .init(
        origin: .zero,
        size: CGSize(width: 400, height: 300)
    )
)
label.numberOfLines = 0
//label.attributedText = try .init(
//    htmlString: value,
//    font: UIFont.py_body(),
//    useDocumentFontSize: false)

label.attributedText = html(value)
    .map(NSMutableAttributedString.init)?
    .trimmedAttributedString(set: .newlines)
import WebKit
let webView = WKWebView(frame: .init(
    origin: .zero,
    size: CGSize(width: 400, height: 300)
))

webView.loadHTMLString(value, baseURL: nil)

PlaygroundPage.current.liveView = webView

