import UIKit
import Library
import Explore
import ComposableArchitecture
import ExploreClient
import ReactiveSwift
import ImageLoader
import BrModels

let (parent, _) = playgroundControllers(
    child: ExploreViewController(store: Store(
        initialState: ExploreState(
            items: [
                .init(item: .story_video, picture: UIImage(named: "pexels.jpg")
                ,profilePicture: UIImage(named: "pexels.jpg")),
                .init(item: .story_salma),
                .init(item: .story_picture, picture: UIImage(named: "pexels.jpg"), profilePicture: UIImage(named: "pexels.jpg"))
            ]
        ),
        reducer: .empty,
        environment: ()
    ))
)


import PlaygroundSupport
PlaygroundPage.current.liveView = parent
