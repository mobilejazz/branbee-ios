import Library
import UIKit
import Explore
import ExploreClient
import BrModels
import ReactiveSwift
import ComposableArchitecture

let delegate = Signal<LinkProjectDelegateEvent, Never>.pipe()
let client = LinkProjectClient(
    linkProject: { project in
        .empty
    },
    detachView: .empty,
    viewDidLoad: .empty,
    delegate: delegate.output.producer)

public let linkedProjectReducer: Reducer<LinkProjectState, LinkProjectAction, LinkProjectEnvironment> =
    .combine(
        Reducer { state, action, environment in
            switch action {
            case .viewDidLoad:
                return .merge(
                    environment.linkProjectClient.delegate
                        .start(on: environment.globalQueue)
                        .observe(on: environment.mainQueue)
                        .map(LinkProjectAction.event),
                    environment.linkProjectClient
                        .viewDidLoad
                        .delay(1.0, on: environment.mainQueue)
                        .fireAndForget()
                )
            case .viewDidAppear:
                return .none
            case .viewDismissed:
                return .none
            case .event(_):
                return .none
            case .actionCell:
                return .none
            case .searchButtonTapped:
                return .none
            }
        },
        linkProjectCellReducer.forEach(
            state: \.actionStates,
            action: /LinkProjectAction.actionCell(index:action:),
            environment: { $0 }
        )
    )


let (parent, _) = playgroundControllers(
    child:   LinkProjectViewController(
        store: .init(
            initialState: .init(projects: [.branbee, .branbee]
        ),
        reducer: linkedProjectReducer,
        environment: LinkProjectEnvironment(
            linkProjectClient: client,
            mainQueue: QueueScheduler.main,
            globalQueue: QueueScheduler.init(),
            imageLoader: .init(loadFromURL: { _ in
                .init(value: UIImage(.systemRed))
            })
        )))
)

import PlaygroundSupport
PlaygroundPage.current.liveView = parent
  
