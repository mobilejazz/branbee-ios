import UIKit
import Library
import ComposableArchitecture
import ExploreClient
import ReactiveSwift
import ImageLoader
import NewStory

let (parent, _) = playgroundControllers(
    child: DonateViewController(store: Store(
        initialState: DonateState(project: .unicef),
        reducer: donationReducer,
        environment: .live(
            environment: DonateEnvironment(
                donateClient: { _,_ in
                    .mock(.error)
                },
                addCardClient: .mock(.error),
                stripeClient: .empty
            )
        )                           
    ))
)

import PlaygroundSupport
PlaygroundPage.current.liveView = parent
