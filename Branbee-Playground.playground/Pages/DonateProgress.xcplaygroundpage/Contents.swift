import UIKit
import Library
import ReactiveSwift

private let loadingIndicatorStyle: (MaterialLoadingIndicator) -> Void =
    autoLayoutStyle <>
    squareStyle(.br_grid(25)) <> { indicator in
        indicator.color = .mainColor
        indicator.radius = .br_grid(12)
        indicator.lineWidth = .br_grid(2)
    }

private let titleLabelStyle: (UILabel) -> Void =
    centerStyle
    <> brLabelDarkBoldStyle
    <> {
        $0.font = .py_title1(size: .br_grid(6))
    }

private let orgLabelStyle: (UILabel) -> Void =
    centerStyle <> brLabelDarkBoldStyle
    <> {
        $0.font = .py_title1(size: 18)
    }

private let subtitleLabelStyle: (UILabel) -> Void =
    centerStyle <> brDarkTextColor
    <> {
        $0.font = .py_title1(size: 18)
    }

private let thankLabelStyle: (UILabel) -> Void =
    centerStyle
    <> brDarkTextColor
    <> {
        $0.font = .py_title1(size: 18)
        $0.isHidden = true
    }
let rootStackViewStyle: (UIStackView) -> Void = verticalStackStyle(
    .br_grid(3),
    distribution: .fill,
    alignment: .center
)

let centerStackViewStyle: (UIStackView) -> Void = verticalStackStyle(
    .br_grid(1),
    alignment: .center
)

let closeButtonStyle: (LoadingButton) -> Void = brMainLoadingButtonStyle
    <> autoLayoutStyle
    <> {
        $0.setTitle("CLOSE")
        $0.isHidden = true
}
<> heightAnchor(.br_grid(14))

let checkImageViewStyle: (UIImageView) -> Void =
    squareStyle(.br_grid(24))
    <> roundedStyle(cornerRadius: .br_grid(12))
    <> {
        $0.backgroundColor = .systemGreen
        $0.isHidden = true
    }

func Spacer(_ heigth: CGFloat = .br_grid(4)) -> UIView {
    let spacer = UIView()
    spacer |> heightAnchor(heigth)
    return spacer
}


struct ProcessingState: Equatable {
    var projectName: String?
    var amount: String?
   
    var isProcessing: Bool {
        projectName == nil
    }
    
}


class ProgressViewController: UIViewController {
                
    var closeAction: (() -> Void)? = nil
    
    init(state: SignalProducer<ProcessingState, Never> = .init(value: .init())) {
        self.state = state
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let state: SignalProducer<ProcessingState, Never>
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let loadingIndicator = MaterialLoadingIndicator()
        loadingIndicatorStyle(loadingIndicator)
                                        
        let checkImageView = UIImageView(image: UIImage(named: "check_ic"))
        checkImageViewStyle(checkImageView)
                
        let titleLabel = UILabel()
        titleLabelStyle(titleLabel)
        
        let subTitleLabel = UILabel()
        subtitleLabelStyle(subTitleLabel)
        
        let organizationLabel = UILabel()
        
        
        orgLabelStyle(organizationLabel)
        let thanksLabel = UILabel()
        thanksLabel.text = "Thank you"
        thankLabelStyle(thanksLabel)
                
        let centerStackView = UIStackView(arrangedSubviews: [
            subTitleLabel,
            organizationLabel
        ])
        centerStackViewStyle(centerStackView)
        let spacer = UIView()
        spacer |> heightAnchor(.br_grid(4))
        
        let rootStackView = UIStackView(arrangedSubviews: [
            loadingIndicator,
            checkImageView,
            Spacer(),
            titleLabel,
            centerStackView,
            Spacer(.br_grid(3)),
            thanksLabel
        ])
        
        self.view.addSubview(rootStackView)
        rootStackView |>
            rootStackViewStyle
            <> centerXStyle(view)
            <> centerYStyle(view)
                
        loadingIndicator.startAnimating()
                
        let closeButton = LoadingButton()
        closeButton.addTarget(self, action: #selector(closeButtonTapped), for: .touchUpInside)
        closeButtonStyle(closeButton)
        
        self.view.addSubview(closeButton)
        closeButton.bottomAnchor.constraint(
            equalTo: view.bottomAnchor,
            constant: -.br_grid(4)
        ).isActive = true
        closeButton.leadingAnchor.constraint(
            equalTo: view.leadingAnchor,
            constant: .br_grid(4)
        ).isActive = true
        closeButton.trailingAnchor.constraint(
            equalTo: view.trailingAnchor,
            constant: -.br_grid(4)
        ).isActive = true
        
        state.producer.startWithValues { state in
            state.isProcessing
                ? loadingIndicator.startAnimating()
                : loadingIndicator.stopAnimating()
            thanksLabel.isHidden = state.isProcessing
            
            subTitleLabel.text = state.isProcessing
            ? "Please wait"
            : "$\(state.amount ?? "") were donated to"
            organizationLabel.text = state.projectName
            organizationLabel.isHidden = state.isProcessing
            titleLabel.text =
                state.isProcessing
                ? "Processing Payment"
                : "Payment Successful"
            organizationLabel.isHidden = state.isProcessing
            checkImageView.isHidden = state.isProcessing
            closeButton.isHidden = state.isProcessing
        }
        
        
    }
    
    @objc func closeButtonTapped() {
        closeAction?()
    }
    
    
}

var signal = Signal<ProcessingState, Never>.pipe()

DispatchQueue.main.asyncAfter(deadline: .now() + 3.0, execute: {
    signal.input.send(
        value: .init())
})





class AnotherController: UIViewController {
    var signal = Signal<ProcessingState, Never>.pipe()
    override func viewDidLoad() {
        let vc = ViewController(state: self.signal.output.producer)
        
        self.view.backgroundColor = .red
      
        
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
            vc.modalPresentationStyle = .fullScreen
            vc.modalTransitionStyle = .coverVertical
            self.present(vc, animated: true)
            self.signal.input.send(.value(.init()))
        })
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 10.0, execute: {
            self.signal.input.send(
                value: .init(
                    projectName: "Project",
                    amount: "100"
                ))
        })
        
        
        
        
    }
}


let (parent, _) = playgroundControllers(child: AnotherController(nibName: nil, bundle: nil)
)





    




import PlaygroundSupport

PlaygroundPage.current.liveView = parent
