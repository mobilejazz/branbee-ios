import UIKit
import Library
import NewStory
import ComposableArchitecture
import ExploreClient
import ReactiveSwift
import ImageLoader

let (parent, _) = playgroundControllers(
    child: StoryDetailViewController(store: Store(
        initialState: StoryDetailState(
            story: .salma,
            sections: [
                .story(.init(
                    story: .salma,
                    creatorImage: UIImage(named: "pexels.jpg"),
                        storyImage: UIImage(named: "pexels.jpg"),
                        numberOfBoosts: 3,
                        remainingBoosts: 1
                )),
                .comment(.init(comment: .comment)),
                .projects([.init(item: .story_video)])
            ],
            isLinkButtonShown: true
        ),
        reducer: .empty,
        environment: ()
    ))
)

let cell = StoryDetailCell()

cell.frame = .init(
    origin: .zero,
    size: CGSize(
        width: .br_grid(100),
        height: .br_grid(130)
    ))

cell.configure(.init(
    initialState: .story(.init(story: .video)),
    reducer: .empty,
    environment: ()
))

import PlaygroundSupport
PlaygroundPage.current.liveView = parent
