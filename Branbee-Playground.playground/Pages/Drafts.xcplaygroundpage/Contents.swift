import Library
import UIKit
import Drafts
import ComposableArchitecture
import ReactiveSwift
import ExploreClient

let delegate = Signal<DraftsDelegateEvent, Never>.pipe()

let (parent, _) = playgroundControllers(
    child: DraftsViewController(store: .init(
                                    initialState: .init(),
                                    reducer: draftsReducer,
                                    environment: DraftsEnvironement(
                                        loader: .placeholder,
                                        mainQueue: QueueScheduler.main,
                                        globalQueue: QueueScheduler(),
                                        draftsClient: .init(
                                            deleteStory: { _ in .empty},
                                            detachView: .empty,
                                            refresh: .fireAndForget {
                                                delegate.input.send(value: .showDrafts([.draft]))
                                            },
                                            delegate: delegate.output.producer
                                        )
                                    ))
            )
)

import PlaygroundSupport
PlaygroundPage.current.liveView = parent
    
