import UIKit
import Library


private let personImageViewStyle: (UIImageView) -> Void =
    squareStyle(30)
    <> roundedStyle(cornerRadius: 15)
    <> {
        $0.image = UIImage(.systemRed)
    }

private let storyStatusImageViewStyle: (UIImageView) -> Void =
    squareStyle(.br_grid(8))
    <> {
        $0.image = UIImage(.systemRed)
    }

private let orgLabelStyle: (UILabel) -> Void = {
    $0.textColor = .mainColor
    $0.font = .py_body()
}

private let topStackViewStyle: (UIStackView) -> Void =
    horizontalStackStyle(.br_grid(2),
                       distribution: .fill,
                       alignment: .center)

private let storyContentLabelStyle: (UILabel) -> Void =
    brDarkTextColor <> {
        $0.textAlignment = .left
        $0.numberOfLines = 0
        $0.font = .py_body(size: 14)
    }

private let dateLabelStyle: (UILabel) -> Void = brGrayTextColor <> {
    $0.font = .py_caption1(size: .br_grid(3))
}

private let rootStackViewStyle: (UIStackView) -> Void =
    verticalStackStyle(.br_grid(1))
    <> marginStyle(.square(.br_grid(2)))

private let contentStackViewStyle: (UIStackView) -> Void =
    verticalStackStyle(.br_grid(1))
    <> marginStyle(.leading(.br_grid(10)))

class CommentsViewCell: UICollectionViewCell {
    
    class var reuseIdentifier: String { "CommentsViewCell" }
        
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = .white
        
        let personImageView = UIImageView()
        personImageViewStyle(personImageView)
        let orgLabel = UILabel()
        orgLabel.text = "Sukhnam Chander"
        orgLabelStyle(orgLabel)
        
        let storyStatusImageView = UIImageView()
        storyStatusImageViewStyle(storyStatusImageView)
        
        let topStackView = UIStackView(arrangedSubviews: [
            personImageView,
            orgLabel,
            storyStatusImageView
        ])
        topStackViewStyle(topStackView)
                        
        let storyContentLabel = UILabel()
        storyContentLabel.text = "Isdkfh sdfhis dfhkjsdh fkjsdhfkjshad fkjhsadkf hsda fkjsdhafkjhsda fsadkjhf kdsahfsdaufiosdhaklhg dsaghskjdahg sadkgsdkhgk jsdahgksdhg"
        storyContentLabelStyle(storyContentLabel)
        storyContentLabel
            .setContentHuggingPriority(.defaultHigh, for: .vertical)
        
        let dateLabel = UILabel()
        dateLabelStyle(dateLabel)
        dateLabel.text = "12 January 2020"
                
        let contentStackView = UIStackView(arrangedSubviews: [
            storyContentLabel,
            dateLabel
        ])
        contentStackViewStyle(contentStackView)
        
        let rootStackView = UIStackView(arrangedSubviews: [
            topStackView,
            contentStackView,
        ])
        rootStackViewStyle(rootStackView)
        
        self.addSubview(rootStackView)
        rootStackView.constrainEdges(to: self)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
                          
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

import PlaygroundSupport
PlaygroundPage.current.liveView =
    CommentsViewCell(
        frame: CGRect(
            origin: .zero,
            size: CGSize(
                width: .br_grid(80),
                height: .br_grid(40)
            )
    ))



