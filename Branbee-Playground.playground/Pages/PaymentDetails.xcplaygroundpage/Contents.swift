import UIKit
import ComposableArchitecture
import Core
import Account

let (parent, _) = playgroundControllers(child:
        UINavigationController(rootViewController:
            PaymentDetailsViewController(
                    store: Store(
                    initialState: PaymentDetailsState(),
                    reducer: .empty,
                    environment: ()
            )
        )
    )
)

import PlaygroundSupport
PlaygroundPage.current.liveView = parent
    
