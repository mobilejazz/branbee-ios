import Authentication
import Library
import ComposableArchitecture
import UIKit


let (parent, _) = playgroundControllers(
    child:
        UINavigationController(
            rootViewController: LoginViewController(
                store: Store(
                    initialState: .init(),
                    reducer: loginReducer,
                    environment: .live(environment: LoginEnvironment(
                        login: .mock(false),
                        signUp: .empty,
                        forgotPassword: .empty
                    ))))
        )
)

import PlaygroundSupport
PlaygroundPage.current.liveView = parent
