import UIKit
import Library


private let personImageViewStyle: (UIImageView) -> Void =
    squareStyle(.br_grid(8))
    <> roundedStyle(cornerRadius: .br_grid(4))
    <> {
        $0.backgroundColor = .systemRed
    }

private let storyStatusImageViewStyle: (UIImageView) -> Void =
    squareStyle(.br_grid(8))
    <> {
        $0.backgroundColor = .systemRed
    }

private let storyStackViewStyle: (UIStackView) -> Void =
    verticalStackStyle(.br_grid(2),
                       distribution: .fill,
                       alignment: .fill)


private let storyImageViewStyle: (UIImageView) -> Void = heightAnchor(.br_grid(52))

private let storyContentLabelStyle: (UILabel) -> Void =
    brGrayTextColor <> {
        $0.textAlignment = .left
        $0.numberOfLines = 0
        $0.font = .py_caption2(size: .br_grid(4))
    }


private let rootStackViewStyle: (UIStackView) -> Void =
    verticalStackStyle(0)

private let donateButtonStyle: (UIButton) -> Void = brMainButtonStyle
    <> heightAnchor(.br_grid(9))
    <> widthAnchor(.br_grid(23))
    <> {
        $0.setTitle("Select")
    }

private let donateBottomStackViewStyle: (UIStackView) -> Void = horizontalStackStyle(
    .br_grid(4),
    alignment: .center
) <> marginStyle(.trailing(.br_grid(4)))

let text = "Majid sdfhis dfhkjsdh fkjsdhfkjshad fkjhsadkf hsda fkjsdhafkjhsda fsadkjhf kdsahfsdaufiosdhaklhg dsaghskjdahg sadkgsdkhgk jsdahgksdhg"

class LinkStoryCell: UICollectionViewCell {
    
    class var reuseIdentifier: String { "LinkStoryCell" }
        
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = .white
                        
        let storyImageView = UIImageView()
        storyImageView.backgroundColor = .systemRed
        storyImageViewStyle(storyImageView)
        
        let storyContentLabel = EdgeLabel(edge: .topHorizontalEdge(.br_grid(4)))
        storyContentLabel.text = text
        storyContentLabelStyle(storyContentLabel)
        
        let storyStackView = UIStackView(arrangedSubviews: [
            storyImageView,
            storyContentLabel,
        ])
        storyStackViewStyle(storyStackView)
        
        let selectButton = UIButton()
        donateButtonStyle(selectButton)
        let donateBottonStackView = UIStackView(arrangedSubviews: [
            UIView(),
            selectButton
        ])
        
        donateBottomStackViewStyle(donateBottonStackView)
        let rootStackView = UIStackView(arrangedSubviews: [
            storyStackView,
            donateBottonStackView
        ])
        rootStackViewStyle(rootStackView)
        self.addSubview(rootStackView)
        rootStackView.constrainEdges(to: self)                
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
                          
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

private let flexibleCellHeight: (String, CGFloat) -> CGFloat = {
    NSAttributedString(
        string: $0,
        attributes: [.font: UIFont.py_caption2(size: 16)])
            .boundingRect(with: .init(
                        width: $1,
                        height: .greatestFiniteMagnitude),
                      options: .usesLineFragmentOrigin, context: nil).height
}


let height: CGFloat = .br_grid(78) +  flexibleCellHeight(text, .br_grid(80))

import PlaygroundSupport
PlaygroundPage.current.liveView =
    LinkStoryCell(
        frame: CGRect(
            origin: .zero,
            size: CGSize(
                width: .br_grid(80),
                height: height)
        ))
