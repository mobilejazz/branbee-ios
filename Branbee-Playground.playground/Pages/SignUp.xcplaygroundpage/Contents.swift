import Authentication
import Library
import ComposableArchitecture
import UIKit


let (parent, _) = playgroundControllers(
    child:
        UINavigationController(
            rootViewController: SignUpViewController(
                store: Store(
                    initialState: .init(),
                    reducer: registerReducer,
                    environment: .live(environment:
                            .mock(true)
                    )))
        )
)

import PlaygroundSupport
PlaygroundPage.current.liveView = parent
