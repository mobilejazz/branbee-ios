import Library
import UIKit

private let organizationLabelStyle: (UILabel) -> Void = brLabelStyle <> {
    $0.font = .py_caption1(size: .br_grid(3))
}

private let plegdeLabelStyle: (UILabel) -> Void = brLabelStyle <> {
    $0.font = .py_caption2(size: 14)
}

private let progressViewStyle: (UIProgressView) -> Void = {
    $0.tintColor = .systemRed
    $0.backgroundColor = .white
} <> heightAnchor(.br_grid(2))
  <> viewBorderColor(.white,
                   borderWidth: 1,
                   cornerRadius: .br_grid(1)
)

private let donateButtonStyle: (UIButton) -> Void = brMainButtonStyle
    <> heightAnchor(.br_grid(9))
    <> widthAnchor(.br_grid(23))
    <> {
        $0.setTitle("Select")
    }

private let donateBottomStackViewStyle: (UIStackView) -> Void = horizontalStackStyle(
    .br_grid(4),
    alignment: .center
)

private let bigTitleLabelStyle: (UILabel) -> Void =
    brLabelStyle <> {
        $0.font = UIFont.py_title2(size: .br_grid(5)).bolded
        $0.adjustsFontSizeToFitWidth = true
    }

private let backgroundImageViewStyle: (UIImageView) -> Void = autoLayoutStyle <> {
    $0.backgroundColor = .blueSecondColor
}

private let componentIconStyle: (UIImageView) -> Void = squareStyle(.br_grid(5))

private let componentLabelStyle: (UILabel) -> Void = brLabelStyle <> {
    $0.font = .py_caption1(size: 14)
}

private let componentStackViewStyle: (UIStackView) -> Void = horizontalStackStyle(.br_grid(2), alignment: .center)

private let rootStackViewStyle: (UIStackView) -> Void =
    verticalStackStyle(.br_grid(4))
    <> marginStyle()

private let bottomStackViewStyle: (UIStackView) -> Void =
    horizontalStackStyle(.br_grid(1), distribution: .fillEqually)

class SelectCommunityViewCell: UICollectionViewCell {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
                
        let backgroundImageView = UIImageView()
        backgroundImageViewStyle(backgroundImageView)
        self.addSubview(backgroundImageView)
        backgroundImageView.constrainEdges(to: self)
                    
        let bigTitleLabel = UILabel()
        bigTitleLabel.text = "Water for Za'atari refugee camp"
        bigTitleLabelStyle(bigTitleLabel)
                                
        let personsIcon = UIImageView()
        personsIcon.backgroundColor = .red
        componentIconStyle(personsIcon)
        let personsLabel = UILabel()
        personsLabel.text = "51,378"
        componentLabelStyle(personsLabel)
        
        let personsStackView = UIStackView(arrangedSubviews: [
            personsIcon, personsLabel
        ])
        componentStackViewStyle(personsStackView)
        
        let articlesIcon = UIImageView()
        articlesIcon.backgroundColor = .red
        componentIconStyle(articlesIcon)
        let articlesLabel = UILabel()
        articlesLabel.text = "28,492"
        componentLabelStyle(articlesLabel)
        
        let articlesStackView = UIStackView(arrangedSubviews: [
            articlesIcon, articlesLabel
        ])
        componentStackViewStyle(articlesStackView)
        
        let boostsIcon = UIImageView()
        boostsIcon.backgroundColor = .red
        componentIconStyle(boostsIcon)
        let boostsLabel = UILabel()
        boostsLabel.text = "2"
        componentLabelStyle(boostsLabel)
        
        let boostsStackView = UIStackView(arrangedSubviews: [
            boostsIcon, boostsLabel
        ])
        componentStackViewStyle(boostsStackView)
                
        let bottomStackView = UIStackView(arrangedSubviews: [
            personsStackView,
            articlesStackView,
            boostsStackView
        ])
        
        bottomStackViewStyle(bottomStackView)
                
        let rootStackView = UIStackView(arrangedSubviews: [
            UIView(),
            bigTitleLabel,
            bottomStackView
        ])
        
        rootStackView |>
            rootStackViewStyle <> {
                $0.spacing = .br_grid(2)
            }
        backgroundImageView.addSubview(rootStackView)
        rootStackView.constrainEdges(to: backgroundImageView)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
    }
    
}

import PlaygroundSupport
PlaygroundPage.current.liveView =
    SelectCommunityViewCell(
        frame: CGRect(
            origin: .zero,
            size: CGSize(width: .br_grid(80), height: .br_grid(42))
    ))
