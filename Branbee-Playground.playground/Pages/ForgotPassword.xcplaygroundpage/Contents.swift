import Authentication
import Library
import ComposableArchitecture
import UIKit


let (parent, _) = playgroundControllers(
    child:
        UINavigationController(
            rootViewController: ForgetPasswordViewController(
                store: Store(
                    initialState: .init(),
                    reducer: forgotPasswordReducer,
                    environment: .live(environment:
                        .mock(true)
                    )))
        )
)

import PlaygroundSupport
PlaygroundPage.current.liveView = parent
