import Library
import UIKit
import ReactiveSwift
import BranbeeSDK
import core
import BrModels
import SavedStories
import ComposableArchitecture
import ImageLoader


let (parent, _)  =  playgroundControllers(
    child: SavedStoriesViewController(store: Store(
        initialState: SavedStoryState(stories: [.init(item: .brStoryFeed), .init(item: .brBookMarkedStoryFeed)]),
        reducer: savedStoriesReducer,
        environment: ImageLoader.init(loadFromURL: { _ in
            return SignalProducer(value: UIImage(UIColor.purple))
        })
    )))

import PlaygroundSupport
PlaygroundPage.current.liveView = parent
