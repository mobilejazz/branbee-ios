import Library
import UIKit
import Validated
import ComposableArchitecture
import AuthenticationClient
import ReactiveSwift
import Tagged
import Account
import BrModels
import AccountClient


public let accountInReducer: Reducer<AccountInfoState, AccountInfoAction, AccountEnvironment> = .combine(
    Reducer { state, action, environment in
        switch action {
        case let .emailTextFieldDidChanged(email):
            state.email = email
            return .none
        case let .nameTextFieldDidChanged(name):
            state.name = name
            return .none
        case let .cityTextFieldDidChanged(city):
            state.city = city
            return .none
        case let .bioTextViewDidChanged(bio):
            state.biography = bio
            return .none
        case let .pickedCountry(country):
            state.country = country
            return .none
        case .viewDismissed:
            state.actionAlert = nil
            return .none
        case .nextButtonTapped:
            return environment.accountClient.updateUser(
                UserUpdateRequest(
                    email: state.email?.rawValue ?? "",
                    password: state.password?.rawValue ?? "",
                    name: state.name?.rawValue ?? "",
                    city: state.city ?? "",
                    country: state.country,
                    bio: state.biography,
                    avatarPath: ""
                )
            ).fireAndForget()
        case .showPhotos:
            state.pickerType = .photoLibrary
            return .none
        case .showCamera:
            state.pickerType = .camera
            return .none
        case .editButtonTapped:
            state.actionAlert = _ActionSheetState(
                title: "Change Profile Image",
                message: nil,
                buttons: [
                    .default("Select From Photos", send: .showPhotos),
                    .default("Select From Camera", send: .showCamera),
                    .cancel(send: .viewDismissed)
                ]
            )
            return .none
        case let .pickedImage(image):
            state.image = image
            return .none
        case .viewDidLoad:
            state.name = Name(rawValue: state.currentUser.username)
            state.email = state.currentUser.email.map(Email.init(rawValue:))
            state.country = state.currentUser.country
            state.city = state.currentUser.city
            state.biography = state.currentUser.biography
            guard let photoURL = state.currentUser._photoURL else {
                return .none
            }
            return .merge(
                environment.imageLoader
                    .loadFromURL(photoURL)
                    .compactMap(id)
                    .map(AccountInfoAction.pickedImage),
                environment.accountClient.onViewLoad
                    .fireAndForget(),
                environment.accountClient
                    .delegate
                    .compactMap(/AccountDelegateEvent.updateInfo)
                    .map(AccountInfoAction.event)
            )
        case let .retry(action):
            return .fireAndForget(action.run)
        case .event(.didUpdatedSuccessfully):
            return .none
        case let .event(.showCountries(countries)):
            state.countries = countries
            return .none
        case .event(.showLoading):
            state.isSavingChangesRequestInFlight = true
            return .none
        case .event(.hideLoading):
            state.isSavingChangesRequestInFlight = false
            return .none
        case .event(.invalidEmail):
            state.alert = .init(title: "Invalid Email")
            return .none
        case let .event(.showTransientFailure(failure)):
            state.alert = .init(title: "Ooops!", message: failure.message)
            return .none
        case let .event(.showFullScreenFailure(failure, retry: action)):
            state.alert = _AlertState(
                title: "Ooops!",
                message: failure.message,
                primaryButton: .default("Retry",
                                        send: .retry(action)
                ),
                secondaryButton: .cancel(send: .viewDismissed)
            )
            return .none
        case .event(.showFieldsRequiredMessage):
            state.alert = .init(title: "Ooops!", message: "Please fill all fields")
            return .none
        case let .passwordTextFieldDidChanged(password):
            state.password = password
            return .none
        }
    }
)



let delagete = Signal<AccountDelegateEvent, Never>.pipe()
let accountClient = AccountClient(
    updateUser: { _ in
        .fireAndForget {
            delagete.input.send(
                value:.updateInfo(
                    .showTransientFailure(
                        .init(message: "Error", cause: nil))
                )
            )
        }
    },
    onViewLoad: .never,
    onDetachView: .never,
    onEventUserUpdated: .never,
    logOut: .never,
    delegate: delagete.output.producer
)
                   
let (parent, _) = playgroundControllers(child:
                                            AccountInfoViewController(
                                                store: Store(
                                                    initialState: AccountInfoState(
                                                        currentUser: BrUser(
                                                            id: "1",
                                                            username: "Majid",
                                                            photo: "",
                                                            thumb: "",
                                                            biography: "there is no idea to follow this kind of itI want to"
                                                        ),
                                                        name: "Majid",
                                                        image: UIImage(UIColor.red),
                                                        email: "abdel@gmail.com",
                                                        password: "password",
                                                        country: .spain,
                                                        countries: [],
                                                        biography: "",
                                                        city: "",
                                                        isLoginRequestInFlight: false,
                                                        isFormValid: true,
                                                        actionAlert: nil,
                                                        errors: nil,
                                                        alert: nil
                                                    ),
                                                    reducer: accountInReducer,
                                                    environment: AccountEnvironment(
                                                        mainQueue: QueueScheduler.main,
                                                        globalQueue: QueueScheduler(),
                                                        imageLoader: .init(loadFromURL: { _ in
                                                            .init(value: UIImage(.blueColor))
                                                        }),
                                                        accountClient: .empty,
                                                        locale: .current
                                                    )
                                                )
                                            )
)
    
import PlaygroundSupport
PlaygroundPage.current.liveView = parent
