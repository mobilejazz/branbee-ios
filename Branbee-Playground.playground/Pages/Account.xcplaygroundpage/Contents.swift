import Library
import Account
import ComposableArchitecture
import UIKit
import ReactiveSwift
import BrModels
import BranbeeSDK
import core

class Delegate: AccountPresenterView {
    
    func onDisplayUserInfo(user: User) {
        observer.send(value: .displayInfo(branbeeUser(user)))
    }
    
    func onNotifyDisableFCM(userId: String) {
        observer.send(value: .didDisabledFCM(userId: userId))
    }
    
    func onNotifyNavigateToLogin() {
        observer.send(value: .logOutSuccessfully)
    }
                                
    let observer: Signal<AccountDelegateEvent, Never>.Observer
    init(_ observer: Signal<AccountDelegateEvent, Never>.Observer) {
        self.observer = observer
    }
}

public enum AccountDelegateEvent: Equatable {
    case displayInfo(BrUser)
    case logOutSuccessfully
    case didDisabledFCM(userId: String)
}

let signal = Signal<AccountDelegateEvent, Never>.pipe()
var delegate = Delegate(signal.input)
let presenter = BranbeeSDK.shared.presenterComponent.accountPresenter(view: delegate)


presenter.onViewLoaded()
presenter.onEventUserUpdated()
