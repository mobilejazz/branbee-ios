import core
import BrModels


public let brFeedItemType: (FeedItem.Type_) -> BrFeedItem.`Type` = {
    switch $0 {
    case .community:
        return .community
    case .project:
        return .project
    case .story:
        return .story
    default:
        fatalError()
    }
}

public let makeFeedItemType: (BrFeedItem.`Type`) -> FeedItem.Type_ = {
    switch $0 {
    case .community:
        return .community
    case .project:
        return .project
    case .story:
        return .story
    }
}

public let brContentType: (Content.Type_) -> BrContent.`Type` = {
    switch $0 {
    case .picture:
        return .picture
    case .text:
        return .text
    case .video:
        return .video
    default:
        fatalError()
    }
}

public let makeContentType: (BrContent.`Type`) -> Content.Type_ = {
    switch $0 {
    case .picture:
        return .picture
    case .text:
        return .text
    case .video:
        return .video
    }
}


public let branbeeUser: (User) -> BrUser = {
    BrUser(
        id: $0.id,
        email: $0.email,
        username: $0.name,
        photo: $0.picture,
        thumb: $0.thumb,
        city: $0.city,
        country: $0.country
            .map { BrCountry.init(id: $0.id, name: $0.name)},
        biography: $0.bio,
        emailVerified: $0.emailVerified.map { $0.boolValue }
    )
}

public let makeUser: (BrUser) -> User = {
    User(
        id: $0.id,
        name: $0.username,
        picture: $0.photo,
        thumb: $0.thumb,
        city: $0.city,
        email: $0.email,
        country: $0.country
            .map { Country(id: $0.id, name: $0.name)},
        bio: $0.biography,
        emailVerified: $0.emailVerified.map(KotlinBoolean.init(bool:))
    )
}

let formatter: () -> DateFormatter = {
    let formatter = DateFormatter()
    formatter.dateFormat = "EEE, dd MMM yyyy HH:mm:ss z"
    return formatter
}

let klockDateFormat: () -> KlockPatternDateFormat = {
    KlockPatternDateFormat(
        format: "EEE, dd MMM yyyy HH:mm:ss z",
        locale: .init(),
        tzNames: .init(namesToOffsets: [:]),
        options: .init(optionalSupport: false)
    )
}

let brDate: (KlockDateTimeTz) -> Date = {
    
    //let string = klockDateFormat().format(dd: $0)
    return formatter().date(from: $0.description())!
}

public let klockDate: (Date) -> KlockDateTimeTz = { date in
    klockDateFormat()
        .tryParse(
            str: formatter().string(from: date),
            doThrow: false
    )!
}


public let brStory: (Story) -> BrStory = {
    BrStory(
        id: $0.id,
        content: $0.content.map(brContent),
        shoulds: Int($0.shoulds),
        comments: Int($0.comments),
        creator: branbeeUser($0.creator),
        community: branbeeCommunity($0.community),
        date: brDate($0.date),
        remainingBoosts: Int($0.remainingBoosts),
        isBookmarked: $0.isBookmarked
    )
}

public let makeStory: (BrStory) -> Story = {
    Story(
        id: $0.id,
        content: $0.content.map(makeContent),
        shoulds: Int32($0.shoulds),
        comments: Int32($0.comments),
        creator: makeUser($0.creator),
        community: makeCommunity($0.community),
        date: klockDate($0.date),
        remainingBoosts: Int32($0.remainingBoosts),
        isBookmarked: $0.isBookmarked
    )
}

public let brContent: (Content) -> BrContent = {
    BrContent(
        type: brContentType($0.type),
        content: $0.content
    )
}

public let makeContent: (BrContent) -> Content = {
    Content(
        type: makeContentType($0.type),
        content: $0.content
    )
}

public let brProject: (Project) -> BrProject = {
    BrProject(
        id: $0.id,
        title: $0.title,
        body: $0.body,
        picture: $0.picture,
        target: Int($0.target),
        pledged: Int($0.pledged),
        comments: Int($0.comments),
        creator: branbeeUser($0.creator),
        community: branbeeCommunity($0.community),
        backers: Int($0.backers),
        date: brDate($0.date),
        enDate: $0.enDate.map(brDate)
    )
}

public let makeProject: (BrProject) -> Project = {
    Project(
        id: $0.id,
        title: $0.title,
        body: $0.body,
        picture: $0.picture,
        target: Int32($0.target),
        pledged: Int32($0.pledged),
        comments: Int32($0.comments),
        creator: makeUser($0.creator),
        community: makeCommunity($0.community),
        backers: Int32($0.backers),
        date: klockDate($0.date),
        enDate: $0.enDate.map(klockDate)
    )
}

public let branbeeCommunity:
(Community) -> BrCommunity = {
    BrCommunity(
        id: $0.id,
        name: $0.name,
        picture: $0.picture,
        thumb: $0.thumb,
        members: Int($0.members),
        actions: Int($0.actions),
        stories: Int($0.stories),
        isMember: $0.isMember?.boolValue,
        isPrivate: $0.isPrivate?.boolValue
    )
}

public let makeCommunity: (BrCommunity) -> Community = {
    Community(
        id: $0.id,
        name: $0.name,
        picture: $0.picture,
        thumb: $0.thumb,
        members: Int32($0.members),
        actions: Int32($0.actions),
        stories: Int32($0.stories),
        isMember: $0.isMember.map(KotlinBoolean.init(bool:)),
        isPrivate: $0.isPrivate.map(KotlinBoolean.init(bool:))
    )
}

public let brFeedItem: (FeedItem) -> BrFeedItem = {
    BrFeedItem(
        type: brFeedItemType($0.type),
        story: $0.story.map(brStory),
        project: $0.project.map(brProject),
        community: $0.community.map(branbeeCommunity)
    )
}

public let makeFeedItem: (BrFeedItem) -> FeedItem = {
    FeedItem(
        type: makeFeedItemType($0.type),
        story: $0.story.map(makeStory),
        project: $0.project.map(makeProject),
        community: $0.community.map(makeCommunity)
    )
}


public let nbrDraft: (StoryDraft) -> BrDraft = {
    BrDraft(
        userId: $0.userId,
        communityId: $0.communityId,
        communityName: $0.communityName,
        text: $0.text,
        cachedImageFileKey: $0.cachedImageFileKey,
        createdAtTimestampInMillis: Int($0.createdAtTimestampInMillis),
        updatedAtTimestampInMillis: Int($0.updatedAtTimestampInMillis)
    )
}

public let makeDraft: (BrDraft) -> StoryDraft = {
    StoryDraft(
        userId: $0.userId,
        communityId: $0.communityId,
        communityName: $0.communityName,
        text: $0.text,
        cachedImageFileKey: $0.cachedImageFileKey,
        createdAtTimestampInMillis: Int64($0.createdAtTimestampInMillis),
        updatedAtTimestampInMillis: Int64($0.updatedAtTimestampInMillis)
    )
}


public let brComment: (Comment) -> BrComment = {
    BrComment(
        creator: branbeeUser($0.creator),
        body: $0.body,
        date: brDate($0.date)
    )
}

public let makeComment: (BrComment) -> Comment = {
    Comment(
        creator: makeUser($0.creator),
        body: $0.body,
        date: klockDate($0.date)
    )
}

public let brCommentType: (Comment.ParentType) -> BrComment.´Type´ = {
    switch $0 {
    case .project:
        return .project
    case .story:
        return .story
    default:
        return .project
    }
}

public let makeCommentType: (BrComment.´Type´) -> Comment.ParentType = {
    switch $0 {
    case .project:
        return .project
    case .story:
        return .story    
    }
}


extension KotlinByteArray {
    public var bytes: [Int8] {
        var bytes: [Int8] = []
        while let item = Optional(iterator().nextByte()) {
            bytes.append(item)
        }
        return bytes
    }
}

extension Data {
    public var byteArray: KotlinByteArray {
        let bytes = map(Int8.init(bitPattern:))
        return KotlinByteArray(size: Int32(bytes.count)) {
            .init(value: bytes[$0.intValue])
        }
    }
}


public let brNotification: (NotificationEntry) -> BrNotification = {
    var notificationType: BrNotification.NotificationType!
    switch $0.type {
    case let story as NotificationEntry.Type_Story:
        notificationType = .story(id: story.id)
    case let story as NotificationEntry.Type_Community:
        notificationType = .community(id: story.id)
    case let story as NotificationEntry.Type_Project:
        notificationType = .project(id: story.id)
    case let story as NotificationEntry.Type_Request:
        notificationType = .request(acceptLink: story.acceptLink, rejectLink: story.rejectLink)
    default:
        notificationType = .other
    }
    return BrNotification(
        id: $0.id,
        text: $0.text,
        type: notificationType,
        status: Int($0.status),
        date: brDate($0.date)
    )
}
//
//public let makeNotifications: (BrNotification) -> NotificationEntry = {
//    NotificationEntry(
//        id: $0.id,
//        date: klockDate($0.date),
//        text: $0.text,
//        storyId: extract(case: BrNotification.NotificationType.story, from: $0.type) ?? "",
//        projectId: extract(case: BrNotification.NotificationType.project, from: $0.type) ?? "",
//        communityId: extract(case: BrNotification.NotificationType.community, from: $0.type) ?? "",
//        acceptLink: extract(case: BrNotification.NotificationType.request, from: $0.type)?.0 ?? "",
//        rejectLink: extract(case: BrNotification.NotificationType.request, from: $0.type)?.1 ?? "",
//        url: "",
//        status: Int32($0.status)
//    )
//}
