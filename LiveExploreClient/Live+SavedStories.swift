import ExploreClient
import BranbeeSDK
import core
import ReactiveSwift
import Library
import BrModels
import ComposableArchitecture

extension SavedStoriesClient {
    
    public static var live: Self {
        
        class Delegate: StoryBookmarksPresenterView {
            let (output, observer): (
                Signal<SavedStoriesDelegateEvent, Never>,
                Signal<SavedStoriesDelegateEvent, Never>.Observer
            )
            
            init(_ signal: (Signal<SavedStoriesDelegateEvent, Never>, Signal<SavedStoriesDelegateEvent, Never>.Observer)) {
                self.output = signal.0
                self.observer = signal.1
            }
            
            func onDisplayBookmarkedStories(stories: [Story]) {
                observer.send(value: .showStories(stories.map(brStory)))
            }
            
            func onDisplayNoBookmarkedStoriesMessage() {
                observer.send(value: .showNoBookMaredStories)
            }
            func onNotifyNavigateToComments(story: Story) {}
            func onNotifyNavigateToStoryDetail(story: Story) {}
        }
                
        let signal = Signal<SavedStoriesDelegateEvent, Never>.pipe()
        var delegate: Delegate! = Delegate(signal)
        
        let storyBookmarksPresenter: StoryBookmarksPresenter = branbeeShared
                .presenterComponent
                .storyBookmarksPresenter(view: delegate)                        
        return Self(
            detachView: .fireAndForget(
                storyBookmarksPresenter.onDetachView)
            ,
            refresh: .fireAndForget(
                storyBookmarksPresenter.onViewRefresh
            ),
            delegate: signal.output
                .producer
                .on(disposed: { delegate = nil })
        )
    }
    
}
