import ExploreClient
import BranbeeSDK
import core
import ReactiveSwift
import Library
import BrModels
import ComposableArchitecture

private class Delegate: LinkStoryPresenterView {
    
    func onDisplayNoStoriesFoundMessage() {
        observer.send(value: .showNoStoryFound)
    }
    
    func onDisplayStories(stories: [Story], reachedTheEnd: Bool) {
        observer.send(
            value: .showStories(stories.map(brStory),
                                didReachEnd: reachedTheEnd))
    }
    
    func onDisplayStoryLinkRequestSentMessage() {
        observer.send(value: .displayRequestSentMessage)
    }
                
    func onDisplayFullScreenError(
        t: KotlinThrowable,
        retryBlock: @escaping () -> Void
    ) {
        observer.send(
            value: .showFailure(
                t.failure,
                retryBlock: RetryAction(run: retryBlock))
        )
    }
    
    func onDisplayLoading() {
        observer.send(value: .showLoading)
    }
    
    func onHideLoading() {
        observer.send(value: .hideLoading)
    }
    
    func onDisplayTransientError(t: KotlinThrowable) {
        observer.send(value: .showAlert(t.failure))
    }
    
    func onNotifyCloseScreen(storyLinked : Bool) {
        observer.send(value: .dismissView(storyLinked: storyLinked))
    }
    
    let (output, observer): (
        Signal<LinkStoryDelegateEvent, Never>,
        Signal<LinkStoryDelegateEvent, Never>.Observer
    )
    
    init(_ signal: (Signal<LinkStoryDelegateEvent, Never>, Signal<LinkStoryDelegateEvent, Never>.Observer)) {
        self.output = signal.0
        self.observer = signal.1
    }
}

extension LinkStoryClient {
    
    public static func live(projectId: String, cummunityId: String) -> Self {
                                        
        return Self(
            linkStory: { story in
                .fireAndForget {
                    dependencies[projectId+cummunityId]?.presenter
                        .onActionStorySelected(story: makeStory(story))
                }
            },
            detachView: .fireAndForget{
                dependencies[projectId+cummunityId]?
                    .presenter.onDetachView()
            },
            viewDidLoad: .fireAndForget{
                dependencies[projectId+cummunityId]?.presenter.onViewLoaded()
            },
            delegate: {
                let signal = Signal<LinkStoryDelegateEvent, Never>.pipe()
                let delegate: Delegate! = Delegate(signal)
                let linkStoryPresenter: LinkStoryPresenter = branbeeShared
                    .presenterComponent
                    .linkStoryPresenter(
                        view: delegate,
                        projectId: projectId,
                        communityId: cummunityId
                    )
                
                if !dependencies.keys.contains(projectId+cummunityId) {
                    dependencies[projectId+cummunityId] = Dependencies(
                        presenter: linkStoryPresenter,
                        signal: signal.output,
                        observer: signal.input,
                        delegate: delegate
                    )
                }
                
                return dependencies[projectId+cummunityId]?.signal
                    .producer
                    .on(disposed: {
                        dependencies[projectId+cummunityId]?.observer.sendCompleted()
                        dependencies[projectId+cummunityId] = nil
                    }) ?? .none
            }()
        )
    }
    
}

private var dependencies: [AnyHashable: Dependencies<LinkStoryPresenter, LinkStoryDelegateEvent>] = [:]
