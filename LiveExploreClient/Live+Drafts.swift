import ExploreClient
import BranbeeSDK
import core
import ReactiveSwift
import Library
import BrModels
import ComposableArchitecture

extension DraftsClient {
    
    public static var live: Self {
        
        class Delegate: StoryDraftsPresenterView {
            
            func onDisplayDrafts(drafts: [StoryDraft]) {
                observer.send(value: .showDrafts(drafts.map(nbrDraft)))
            }
            
            func onDisplayNoDraftsMessage() {
                observer.send(value: .showDrafts([]))
            }
            
            func onNotifyNavigateToCreateStory(draft: StoryDraft) {}
            
            let (output, observer): (
                Signal<DraftsDelegateEvent, Never>,
                Signal<DraftsDelegateEvent, Never>.Observer
            )
            
            init(_ signal: (Signal<DraftsDelegateEvent, Never>, Signal<DraftsDelegateEvent, Never>.Observer)) {
                self.output = signal.0
                self.observer = signal.1
            }
            
            
        }
                
        let signal = Signal<DraftsDelegateEvent, Never>.pipe()
        var delegate: Delegate! = Delegate(signal)
        
        let storyDraftsPresenter: StoryDraftsPresenter = branbeeShared
            .presenterComponent
            .storyDraftsPresenter(view: delegate)               
        
        return Self(
            deleteStory: { draft in
                .fireAndForget {
                    storyDraftsPresenter.onActionRemoveDraft(draft: makeDraft(draft))
                }
            },
            detachView: .fireAndForget(storyDraftsPresenter.onDetachView),
            refresh: .fireAndForget(storyDraftsPresenter.onViewRefresh),
            delegate: signal.output
                .producer
                .on(disposed: { delegate = nil })
        )
    }
    
}
