import BrModels
import ReactiveSwift
import ExploreClient
import BranbeeSDK
import core
import Library

private class Delegate: StoryDetailPresenterView {
    
    func onDisplayBookmarkedIcon(bookmarked: Bool) {
        observer.send(value: .isBookMarked(bookMarked: bookmarked))
    }
    
    func onDisplayBoosts(numberOfBoosts: Int32, remainingBoosts: Int32) {
        observer.send(value: .boosts(
                        Int(numberOfBoosts),
                        remaining: Int(remainingBoosts)))
    }
    
    func onDisplayLoading() {
        self.observer.send(value: .showLoading)
    }
    
    func onDisplayStory(story: Story, comment: Comment?, linkedProjects: [Project], showLinkButton: Bool) {
        observer.send(value: .display(
                        story: brStory(story),
                        comment: comment.map(brComment),
                        linkedProjects: linkedProjects.map(brProject),
                        isLinkButtonShown: showLinkButton))
    }
    
    func onFailedToDisplayStory(t: KotlinThrowable) {
        observer.send(value: .failedToDisplayStory(t.failure))
    }
    
    func onFailedWithTransientError(e: KotlinException) {
        observer.send(value: .failedWithError(e.failure))
    }
    
    func onHideLoading() {
        observer.send(value: .hideLoading)
    }
    
    func onNotifyEnableBoostAction(enable: Bool) {
        observer.send(value: .enableBoostButton(enabled: enable))
    }
    
    func onNotifyNavigateToComments(storyId: String) {}
    func onNotifyNavigateToCommunityDetails(communityId: String) {}
    func onNotifyNavigateToProjectComments(projectId: String) {}
    func onNotifyNavigateToProjectDetail(project: Project) {}
    func onNotifyNavigateToProjectDonation(project: Project) {}
    func onNotifyNavigateToProjectLink(storyId: String, communityId: String) {}
    func onNotifyShare(story: Story) {}
                
    let observer:
        Signal<StoryDetailDelegateEvent, Never>.Observer
    
    init(_ signal: Signal<StoryDetailDelegateEvent, Never>.Observer) {
        self.observer = signal
    }
                
}

extension StoryDetailClient {
    
    public static func live(_ storyId: String) -> Self {
                                                        
        return Self(
            donateToProject: { project in
                .fireAndForget {
                    dependencies[storyId]?.presenter
                        .onActionDonateToProject(project: makeProject(project))
                }
            },
            detachView: .fireAndForget{
                dependencies[storyId]?.presenter.onDetachView()
            },
            viewDidLoad: .fireAndForget {
                dependencies[storyId]?.presenter.onViewLoaded()
            },
            refresh: .fireAndForget {
                dependencies[storyId]?.presenter.onViewRefresh()
            },
            linkProject: .fireAndForget { dependencies[storyId]?.presenter.onActionLinkProject()
            },
            bookMark: .fireAndForget {
                dependencies[storyId]?.presenter.onActionBookmark()
            },
            boost: .fireAndForget {
                dependencies[storyId]?.presenter.onActionBoost()
            },
            didProjectLinked: .fireAndForget { dependencies[storyId]?.presenter.onEventProjectLinked()
            },
            delegate: {
                let signal = Signal<StoryDetailDelegateEvent, Never>.pipe()
                let delegate: Delegate! = Delegate(signal.input)
                
                let storyDetailPresenter: StoryDetailPresenter =
                    branbeeShared
                        .presenterComponent
                        .storyDetailPresenter(view: delegate, storyId: storyId)
                
                if !dependencies.keys.contains(storyId) {
                    dependencies[storyId] = Dependencies(
                        presenter: storyDetailPresenter,
                        signal: signal.output,
                        observer: signal.input,
                        delegate: delegate
                    )
                }
                
                return dependencies[storyId]?.signal
                    .producer
                    .on(disposed: {
                        dependencies[storyId]?.observer.sendCompleted()
                        dependencies[storyId] = nil
                    }) ?? .none
            }()
        )
    }
}



private var dependencies: [AnyHashable: Dependencies<StoryDetailPresenter, StoryDetailDelegateEvent>] = [:]
