import ExploreClient
import BranbeeSDK
import core
import ReactiveSwift
import Library
import BrModels
import ComposableArchitecture

private class Delegate: CreateStoryPresenterView {
    
    func onDisplayCommunity(communityName: String) {
        observer.send(value: .onDisplayCommunityName(communityName))
    }
    
    func onDisplayCreateStoryLoading() {
        observer.send(value: .showMakeStoryLoading)
    }
    
    func onDisplayDraft(draft: StoryDraft) {
        observer.send(value: .onDisplayDraft(nbrDraft(draft)))
    }
    
    func onDisplayEmailNotVerifiedWarning(userName: String, email: String) {
        observer.send(value: .onDisplayEmailNotVerifiedWarning(
                        userName: userName,
                        email: email
        ))
    }
    
    func onDisplayFillTextMessage() {
        observer.send(value: .onDisplayFillTextMessage)
    }
    
    func onDisplayLoading() {
        observer.send(value: .showLoading)
    }
    
    func onDisplaySaveDraftDialog() {
        observer.send(value: .onDisplaySaveDraftDialog)
    }
    
    func onDisplaySuccessMessage() {
        observer.send(value: .showSuccessMessage)
    }
    
    func onDisplayTransientError(e_ e: KotlinException) {
        observer.send(value: .onDisplayTransientError(e.failure))
    }
    
    func onHideCreateStoryLoading() {
        observer.send(value: .hideMakeStoryLoading)
    }
    
    func onHideLoading() {
        observer.send(value: .hideLoading)
    }
    
    func onNotifyCloseScreen(storyCreated: Bool) {
        observer.send(value:
                        .onNotifyCloseScreen(storyCreated: storyCreated))
    }
    
    func onNotifyEnableCreateStoryButton(enable: Bool) {
        observer.send(value:
                        .onNotifyEnableCreateStoryButton(enable: enable))
    }
    
    func onNotifyNavigateToCommunitySelection() {}
    
    func onQueryMediaFilePath(existingDraft: StoryDraft?) -> String? {
        existingDraft?.cachedImageFileKey ?? _mediaFilePath_
    }
    
    func onQueryWrittenText() -> String? {
        _storyText_
    }                                                                
    let (output, observer): (
        Signal<NewStoryDelegateEvent, Never>,
        Signal<NewStoryDelegateEvent, Never>.Observer
    )
    
    init(_ signal: (Signal<NewStoryDelegateEvent, Never>, Signal<NewStoryDelegateEvent, Never>.Observer)) {
        self.output = signal.0
        self.observer = signal.1
    }
}

extension NewStoryClient {
           
    public static func live(_ draftId: String?) -> Self {
        
        let draftId = draftId ?? UUID().uuidString
        
        return Self(
            detachView: .fireAndForget {
                dependencies[draftId]?.presenter.onDetachView()
            },
            closeScreen: .fireAndForget {
                dependencies[draftId]?.presenter.onActionCloseScreen()
            },
            cancelDraft: .fireAndForget {
                dependencies[draftId]?.presenter.onActionCancelDraft()
            },
            saveDraft: .fireAndForget {
                dependencies[draftId]?.presenter.onActionSaveDraft()
            },
            viewRefresh: .fireAndForget {
                dependencies[draftId]?.presenter.onViewRefresh()
            },
            sendConfirmationEmail: .fireAndForget {
                dependencies[draftId]?.presenter.onActionResendConfirmationEmail() },
            changeCommunity: { community in
                .fireAndForget { dependencies[draftId]?.presenter.onEventCommunityChanged(
                    communityId: community.id,
                    communityName: community.name
                ) }
            }, makeStoryFromFile: { bytes, text in
                .fireAndForget {
                    dependencies[draftId]?.presenter.onActionCreateStory(
                        text: text,
                        byteArrayFromFile: bytes.map { ByteArrayFromFile(
                            byteArray: $0.data.byteArray,
                            filename: $0.filename
                        ) }
                    )
                }
            }, makeStoryFromImage: { imagePath, text in
                .fireAndForget {
                    dependencies[draftId]?.presenter.onActionCreateStory(
                        text: text,
                        imagePath: imagePath
                    )
                }
            }, delegate: {
                let signal = Signal<NewStoryDelegateEvent, Never>.pipe()
                let delegate: Delegate! = Delegate(signal)
                
                let createStoryPresenter: CreateStoryPresenter =
                    branbeeShared
                        .presenterComponent
                        .createStoryPresenter(
                            view: delegate,
                            storyDraftId: draftId
                        )
                
                if !dependencies.keys.contains(draftId) {
                    dependencies[draftId] = Dependencies(
                        presenter: createStoryPresenter,
                        signal: signal.output,
                        observer: signal.input,
                        delegate: delegate
                    )
                }
                
                return dependencies[draftId]?.signal
                    .producer
                    .on(disposed: {
                        dependencies[draftId]?.observer.sendCompleted()
                        dependencies[draftId] = nil
                    }) ?? .none
            }()
        )
    }
    
}

private var dependencies: [AnyHashable: Dependencies<CreateStoryPresenter, NewStoryDelegateEvent>] = [:]
