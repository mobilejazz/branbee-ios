import ExploreClient
import BranbeeSDK
import core
import ReactiveSwift
import Library
import BrModels
import ComposableArchitecture

private class Delegate: CommentsPresenterView {
    
    func onDisplayFullScreenError(e: KotlinThrowable, retryBlock: @escaping () -> Void) {
        observer.send(
            value: .displayError(e.failure,
                                 retryBlock: .init(run: retryBlock))
        )
    }
    
    
    func onDisplayComments(comments: [Comment], reachedTheEnd: Bool) {
        observer.send(value: .loadComments(
                        comments: comments.map(brComment),
                        reachedTheEnd: reachedTheEnd))
    }
    
    func onDisplayEmptyCommentsMessage() {
        observer.send(value: .displayEmptyState)
    }
    
    func onDisplayTransientError(e: KotlinThrowable) {
        observer.send(value: .displayFailure(e.failure))
    }
    
    func onRemoveNewCommentText() {
        observer.send(value: .didRemoveCommentText)
    }
    
    func onDisplayLoading() {
        observer.send(value: .showLoading)
    }
    
    func onHideLoading() {
        observer.send(value: .hideLoading)
    }
    
    let (output, observer): (
        Signal<CommentDelegateEvent, Never>,
        Signal<CommentDelegateEvent, Never>.Observer
    )
    
    init(_ signal: (Signal<CommentDelegateEvent, Never>, Signal<CommentDelegateEvent, Never>.Observer)) {
        self.output = signal.0
        self.observer = signal.1
    }
                
}

extension CommentsClient {
    
    public static func live(_ parentId: String, parentType: BrComment.´Type´) -> Self {
                                
        
        
        return Self(
            sendComment: { text in
                .fireAndForget {
                    dependencies[parentId]?.presenter.onActionSendComment(text: text)
                }
            },
            detachView: .fireAndForget {
                dependencies[parentId]?.presenter.onDetachView()
            },
            viewDidLoad: .fireAndForget {
                dependencies[parentId]?.presenter.onViewLoaded()
            },
            loadMore: .fireAndForget{
                dependencies[parentId]?.presenter.onEventLoadMore()
            },
            delegate: {
                let signal = Signal<CommentDelegateEvent, Never>.pipe()
                let delegate: Delegate! = Delegate(signal)
                        
                let commentsPresenter: CommentsPresenter = branbeeShared
                    .presenterComponent
                    .commentsPresenter(
                        view: delegate,
                        parentId: parentId,
                        parentType: makeCommentType(parentType)
                    )
                
                if !dependencies.keys.contains(parentId) {
                    dependencies[parentId] = Dependencies(
                        presenter: commentsPresenter,
                        signal: signal.output,
                        observer: signal.input,
                        delegate: delegate
                    )
                }
                
                return dependencies[parentId]?.signal
                    .producer
                    .on(disposed: {
                        dependencies[parentId]?.observer.sendCompleted()
                        dependencies[parentId] = nil
                    }) ?? .none
            }()
        )
    }
    
}
private var dependencies: [AnyHashable: Dependencies<CommentsPresenter, CommentDelegateEvent>] = [:]
