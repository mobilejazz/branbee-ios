import ExploreClient
import BranbeeSDK
import core
import ReactiveSwift
import Library
import BrModels
import ComposableArchitecture

//extension KotlinException {
//    public var failure: Failure {
//        .error(message, cause: cause?.getStackTraceAsString())
//    }
//}



extension KotlinThrowable {
    public var failure: Failure {
        Failure(message: message, cause: getStackTraceAsString())
    }
}

extension ExploreClient {
    
    public static var live: Self {
                                            
        class Delegate: ExplorePresenterView {
            
            func onDisplayFeedError(e: KotlinThrowable) {
                self.observer.send(value: .displayError(e.failure))
            }
            
            func onDisplayFeedItems(items: [FeedItem], reachedTheEnd: Bool) {
                self.observer.send(
                    value: .items(items.map(brFeedItem),
                                  reachedTheEnd: reachedTheEnd))
            }
            
            func onDisplayFeedPageError(e: KotlinThrowable) {
                self.observer.send(
                    value: .displayFeedPageError(
                        e.failure,
                        action: .init(run: {
                            self.observer.send(value: .hideLoading)
                        })
                    ))
            }
            
            func onDisplayLoading() {
                self.observer.send(value: .showLoading)
            }
            
            func onHideLoading() {
                self.observer.send(value: .hideLoading)
            }
            
            func onNavigateToCreateStory() {
                self.observer.send(value: .createStory)
            }
            
            func onNavigateToProject(project: Project) {
                self.observer.send(value: .showProject(brProject(project)))
            }
            
            func onNavigateToProjectComments(project: Project) {
                self.observer.send(value: .showProjectComments(brProject(project)))
            }
            
            func onNavigateToStory(story: Story) {
                self.observer.send(value: .showStory(brStory(story)))
            }
            
            func onNavigateToStoryComments(story: Story) {
                self.observer.send(value: .showStoryComments(brStory(story)))
            }
                                                                
            let (output, observer): (Signal<ExploreDelegateEvent, Never>, Signal<ExploreDelegateEvent, Never>.Observer)

            init(_ signal: (Signal<ExploreDelegateEvent, Never>, Signal<ExploreDelegateEvent, Never>.Observer)) {
                self.output = signal.0
                self.observer = signal.1
            }
                        
        }
        
        let signal = Signal<ExploreDelegateEvent, Never>.pipe()
        var delegate: Delegate! = Delegate(signal)
                        
        let explorePresenter: ExplorePresenter = branbeeShared
            .presenterComponent
            .explorePresenter(view: delegate)
        
        return ExploreClient(
            onActionCreateStory: .fireAndForget(
                explorePresenter.onActionCreateStory
            ),
            onActionRetryLoading: .fireAndForget(
                explorePresenter.onActionRetryLoading
            ),
            onActionViewProject: { project in
                .fireAndForget {
                    explorePresenter.onActionViewProject(project: makeProject(project))
                }
            },
            onActionViewProjectComments: { project in
                .fireAndForget {
                    explorePresenter.onActionViewProjectComments(project: makeProject(project))
                }
            },
            onActionViewStory: { story in
                .fireAndForget {
                    explorePresenter.onActionViewStory(story: makeStory(story))
                }
            },
            onActionViewStoryCommentsStory: { story in
                .fireAndForget {
                    explorePresenter.onActionViewStoryComments(story: makeStory(story))
                }
            },
            onDetachView: .fireAndForget {
                explorePresenter.onDetachView()
            },
            onEventLoadMore: Effect
                .fireAndForget {
                    explorePresenter.onEventLoadMore()
                },
            onViewLoaded: Effect.fireAndForget {
                explorePresenter.onViewLoaded()
            },
            onViewRefresh: .fireAndForget {
                explorePresenter.onViewRefresh()
            },
            delegate: signal.output
                .producer
                .on(disposed: { delegate = nil })
        )
    
    }
}
