import ExploreClient
import BranbeeSDK
import core
import ReactiveSwift
import Library
import ComposableArchitecture
import BrModels

let branbeeUser: (User) -> BrUser = {
    BrUser(
        id: $0.id,
        email: $0.email,
        username: $0.name,
        photo: $0.picture,
        thumb: $0.thumb,
        city: $0.city,
        country: $0.country
            .map { BrCountry.init(id: $0.id, name: $0.name)},
        biography: $0.bio,
        emailVerified: $0.emailVerified.map { $0.boolValue }
    )
}

private class Delegate: AccountPresenterView {
    
    func onDisplayUserInfo(user: User) {
        observer.send(value: .displayInfo(branbeeUser(user)))
    }
    
    func onNotifyDisableFCM(userId: String) {
        observer.send(value: .didDisabledFCM(userId: userId))
    }
    
    func onNotifyNavigateToLogin() {
        observer.send(value: .logOutSuccessfully)
    }
                                
    let observer: Signal<AccountDelegateEvent, Never>.Observer
    
    init(_ observer: Signal<AccountDelegateEvent, Never>.Observer) {
        self.observer = observer
    }
    
}


extension AccountClient {
    public static var live: Self {
                
        let signal = Signal<AccountDelegateEvent, Never>.pipe()
        var delegate: Delegate! = Delegate(signal.input)
                       
        let accountPresenter: AccountPresenter =
            branbeeShared
                .presenterComponent
                .accountPresenter(view: delegate)
                             
        return Self(
            onViewLoad: .fireAndForget {
                accountPresenter.onViewLoaded()
            }, onDetachView: .fireAndForget {
                accountPresenter.onDetachView()
            }, onEventUserUpdated: .fireAndForget {
                accountPresenter.onEventUserUpdated()
            }, logOut: .fireAndForget {
                accountPresenter.onActionLogout()
            }, delegate: signal.output
                .producer
                .on(disposed: { delegate = nil })
        )
         
    }
}


extension UpdateUserClient {
    public static var live: Self {
        class Delegate: UpdateUserPresenterView {
            
            func onDisplayUserInfo(user: User) {
                observer.send(value: .displayUser(branbeeUser(user)))
            }
            
            func onDisplayCountries(countries: [Country]) {
                observer.send(value: .showCountries(
                    countries.map {
                        BrCountry(
                            id: $0.id,
                            name: $0.name
                        )
                    }
                ))
            }
            
            func onDisplayFillRequiredFields() {
                observer.send(value: .showFieldsRequiredMessage)
            }
            
            func onDisplayFullScreenError(e: KotlinThrowable, retryBlock: @escaping () -> Void) {
                observer.send(
                    value: .showFullScreenFailure(
                        e.failure,
                        retry: RetryAction(run: retryBlock)
                    )
                )
            }
            
            func onDisplayLoading() {
                observer.send(value: .showLoading)
            }
            
            func onDisplayTransientError(e: KotlinThrowable) {
                observer.send(value: .showTransientFailure(e.failure))
            }
                                    
            func onDisplayUserUpdatedMessage() {
                observer.send(value: .didUpdatedSuccessfully)
            }
            
            func onFailedToValidateEmail() {
                observer.send(value: .invalidEmail)
            }
            
            func onHideLoading() {
                observer.send(value: .hideLoading)
            }
                        
            let (output, observer): (Signal<UpdateUserDelegateEvent, Never>, Signal<UpdateUserDelegateEvent, Never>.Observer)
            
            init(_ signal: (Signal<UpdateUserDelegateEvent, Never>, Signal<UpdateUserDelegateEvent, Never>.Observer)) {
                self.output = signal.0
                self.observer = signal.1
            }
        }
        
        let signal = Signal<UpdateUserDelegateEvent, Never>.pipe()
        var delegate: Delegate! = Delegate(signal)
        
        let updateUserPresenter: UpdateUserPresenter =
            branbeeShared
                .presenterComponent
                .updateUserPresenter(view: delegate)
                             
        return Self.init(
            onViewLoad: .fireAndForget(updateUserPresenter.onViewLoaded),
            onDetachView: .fireAndForget(updateUserPresenter.onDetachView),
            updateUser: { request in
                .fireAndForget {
                    updateUserPresenter.onActionUpdate(
                        email: request.email,
                        password: request.password,
                        name: request.name,
                        city: request.city,
                        country: request.country.map {
                            Country(id: $0.id, name: $0.name)
                        },
                        bio: request.bio,
                        avatarPath: request.avatarPath
                    )
                }
            },
            delegate: signal.output
                .producer
                    .on(disposed: { delegate = nil })
        )
            
        
         
    }
}
