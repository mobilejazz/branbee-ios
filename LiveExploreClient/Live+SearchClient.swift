import ExploreClient
import BranbeeSDK
import core
import ReactiveSwift
import Library
import BrModels
import ComposableArchitecture

private class Delegate: SearchFeedPresenterView {
    func onDisplayFeedItems(items: [FeedItem], reachedTheEnd: Bool) {
        observer.send(
            value: .items(items.map(brFeedItem),
            didReachEnd: reachedTheEnd)
        )
    }
    
    func onDisplayFullScreenError(
        t: KotlinThrowable,
        retryBlock: @escaping () -> Void) {
        observer.send(value: .showFullScreenFailure(
                        t.failure,
                        retryAction: .init(run: retryBlock)))
    }
    
    func onDisplayLoading() {
        observer.send(value: .showLoading)
    }
    
    func onDisplayNotFoundMessage() {
        observer.send(value: .noItemFound)
    }
    
    func onDisplaySearchHint() {
        observer.send(value: .showSearchHint)
    }
    
    func onDisplayTransientError(t: KotlinThrowable) {
        observer.send(value: .showFailure(t.failure))
    }
    
    func onHideLoading() {
        observer.send(value: .hideLoading)
    }
    
    func onNavigateToCommunity(community: Community) {}
    func onNavigateToProject(project: Project) {}
    func onNavigateToProjectComments(project: Project) {}
    func onNavigateToStory(story: Story) {}
    func onNavigateToStoryComments(story: Story) {}
    
    let (output, observer): (
        Signal<SearchDelegateEvent, Never>,
        Signal<SearchDelegateEvent, Never>.Observer
    )
    
    init(_ signal: (Signal<SearchDelegateEvent, Never>, Signal<SearchDelegateEvent, Never>.Observer)) {
        self.output = signal.0
        self.observer = signal.1
    }
                
}

extension SearchClient {
    public static var live: Self {
        return Self(
            detachView: .fireAndForget{
                dependencies[uuid]?.presenter.onDetachView()
            },
            loadMore: .fireAndForget{
                dependencies[uuid]?.presenter.onEventLoadMore()
            },
            viewDidLoad: .fireAndForget{
                dependencies[uuid]?.presenter.onViewLoaded()
            },
            search: { (text, filter) in
                .fireAndForget {
                    
                    if filter == .allTypes {
                        dependencies[uuid]?.presenter
                            .onActionSearch(
                                searchQuery: text,
                                showActions: true,
                                showStories: true,
                                showCommunities: true
                        )
                        return
                    }
                                        
                    dependencies[uuid]?.presenter.onActionSearch(
                        searchQuery: text,
                        showActions: filter == .projects,
                        showStories: filter == .stories,
                        showCommunities: filter == .communities
                    )
                }
            },
            delegate: {
                let signal = Signal<SearchDelegateEvent, Never>.pipe()
                let delegate: Delegate! = Delegate(signal)
                
                let searchFeedPresenter: SearchFeedPresenter = branbeeShared
                        .presenterComponent
                        .searchFeedPresenter(view: delegate)
                
                if !dependencies.keys.contains(uuid) {
                    dependencies[uuid] = Dependencies(
                        presenter: searchFeedPresenter,
                        signal: signal.output,
                        observer: signal.input,
                        delegate: delegate
                    )
                }
                
                return dependencies[uuid]?.signal
                    .producer
                    .on(disposed: {
                        dependencies[uuid]?.observer.sendCompleted()
                        dependencies[uuid] = nil
                    }) ?? .none
            }()
        )
    }
}

private var uuid = String("id")
private var dependencies: [AnyHashable: Dependencies<SearchFeedPresenter, SearchDelegateEvent>] = [:]
