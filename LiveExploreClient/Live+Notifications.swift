import ExploreClient
import BranbeeSDK
import core
import ReactiveSwift
import Library
import BrModels
import ComposableArchitecture
import CasePaths


public let makeNotifications: (BrNotification) -> NotificationEntry = {
    NotificationEntry(
        id: $0.id,
        date: klockDate($0.date),
        text: $0.text,
        storyId: extract(case: BrNotification.NotificationType.story, from: $0.type) ?? "",
        projectId: extract(case: BrNotification.NotificationType.project, from: $0.type) ?? "",
        communityId: extract(case: BrNotification.NotificationType.community, from: $0.type) ?? "",
        acceptLink: extract(case: BrNotification.NotificationType.request, from: $0.type)?.0 ?? "",
        rejectLink: extract(case: BrNotification.NotificationType.request, from: $0.type)?.1 ?? "",
        url: "",
        status: Int32($0.status)
    )
}

extension NotificationsClient {
           
    public static var live: Self {
                        
        class Delegate: NotificationsPresenterView {
            
            func onDisplayFullScreenError(t: KotlinThrowable, retryBlock: @escaping () -> Void) {
                observer.send(value: .displayFailure(t.failure, retryAction: .init(run: retryBlock)))
            }
            
            func onDisplayLoading() {
                observer.send(value: .showLoading)
            }
            
            func onDisplayNoNotificationsFoundMessage() {
                observer.send(value: .displayNoNotifications)
            }
            
            func onDisplayNotifications(notifications: [NotificationEntry], reachedTheEnd: Bool) {
                observer.send(value: .displayNotifications(notifications.map(brNotification), didReachedEnd: reachedTheEnd))
            }
            
            func onDisplayRequestAcceptedTransientMessage() {
                observer.send(value: .showSuccessMessage(requestAccepted: true))
            }
            
            func onDisplayRequestRejectedTransientMessage() {
                observer.send(value: .showSuccessMessage(requestAccepted: false))
            }
            
            func onDisplayTransientError(t: KotlinThrowable) {
                observer.send(value: .showAlert(t.failure))
            }
            
            func onHideLoading() {
                observer.send(value: .hideLoading)
            }
            
            func onNotifyNavigateToCommunity(communityId: String) {}
            func onNotifyNavigateToProject(projectId: String) {}
            func onNotifyNavigateToStory(storyId: String) {}
                                                                        
            let (output, observer): (
                Signal<NotificationsDelegateEvent, Never>,
                Signal<NotificationsDelegateEvent, Never>.Observer
            )
            
            init(_ signal: (Signal<NotificationsDelegateEvent, Never>, Signal<NotificationsDelegateEvent, Never>.Observer)) {
                self.output = signal.0
                self.observer = signal.1
            }
        }
                               
        let signal = Signal<NotificationsDelegateEvent, Never>.pipe()
        var delegate: Delegate! = Delegate(signal)
        
        let notificationsPresenter: NotificationsPresenter =
            branbeeShared
                .presenterComponent
                .notificationsPresenter(view: delegate)
//
//            delegate: signal.output
//                .producer
//                .on(disposed: { delegate = nil }))
        
        return Self(
            detachView: .fireAndForget(notificationsPresenter.onDetachView),
            viewDidLoad: .fireAndForget(notificationsPresenter.onViewLoaded),
            acceptNotification: { (notification) -> SignalProducer<Never, Never> in
                .fireAndForget {
                    notificationsPresenter
                        .onActionAcceptRequest(notificationEntry: makeNotifications(notification))
                }
            },
            rejectNotification: { notification in
                .fireAndForget {
                    notificationsPresenter
                        .onActionRejectRequest(notificationEntry: makeNotifications(notification))
                }
            },
            selectStoryId: { storyId in
                .fireAndForget {
                    notificationsPresenter
                        .onActionStoryNotificationSelected(storyId: storyId)
                }
            },
            selectProjectId: { projectId in
                .fireAndForget {
                    notificationsPresenter
                        .onActionProjectNotificationSelected(projectId: projectId)
                }
            },
            selectCommunityId: { communityId in
                .fireAndForget {
                    notificationsPresenter
                        .onActionCommunityNotificationSelected(communityId: communityId)
                }
            },
            loadMore: .fireAndForget(notificationsPresenter.onEventLoadMore),
            delegate:  signal.output
                        .producer
                        .on(disposed: { delegate = nil })
            )
    }
    
    
}
