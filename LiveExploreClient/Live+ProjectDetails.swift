import ExploreClient
import BranbeeSDK
import core
import ReactiveSwift
import Library
import BrModels
import ComposableArchitecture

extension ProjectDetailsClient {
    public static func live(_ projectId: String) -> Self {
        class Delegate: ProjectDetailPresenterView {
            func onDisplayLoading() {
                observer.send(value: .showLoading)
            }
            
            func onHideLoading() {
                observer.send(value: .hideLoading)
            }
            
            func onNotifyNavigateToCommunityDetails(communityId: String) {}
            
            func onNotifyNavigateToProjectComments(project: Project) {}
            
            func onNotifyNavigateToProjectDonation(project: Project) {}
            
            func onNotifyNavigateToStoryComments(story: Story) {}
            
            func onNotifyNavigateToStoryDetail(story: Story) {}
            
            func onNotifyNavigateToStoryLink(project: Project) {}
            
            func onNotifyShare(project: Project) {}
            
            func onDisplayFullscreenError(e: KotlinThrowable, retryBlock: @escaping () -> Void) {
                observer.send(
                    value: .displayFailure(e.failure,
                                           retryAction: .init(run: retryBlock))
                )
            }
            func onDisplayProject(
                project: Project,
                comment: Comment?,
                linkedStories: [Story],
                showLinkButton: Bool
            ) {
                observer.send(
                    value: .displayProject(
                        brProject(project),
                        comment: comment.map(brComment),
                        linkedStories: linkedStories.map(brStory),
                        isLinkedButtonShown: showLinkButton)
                )
            }
            
            let (output, observer): (
                Signal<ProjectDetailsDelegateEvent, Never>,
                Signal<ProjectDetailsDelegateEvent, Never>.Observer
            )
            
            init(_ signal: (Signal<ProjectDetailsDelegateEvent, Never>, Signal<ProjectDetailsDelegateEvent, Never>.Observer)) {
                self.output = signal.0
                self.observer = signal.1
            }
                        
        }
                               
       
        
        return Self(
            detachView: .fireAndForget{ dependencies[projectId]?.presenter.onDetachView()
            },
            viewDidLoad: .fireAndForget{ dependencies[projectId]?.presenter.onViewLoaded() },
            refreshView: .fireAndForget {
                dependencies[projectId]?.presenter.onViewRefresh()
            },
            delegate: {
                let signal = Signal<ProjectDetailsDelegateEvent, Never>.pipe()
                let delegate: Delegate! = Delegate(signal)
                let projectDetailPresenter: ProjectDetailPresenter =
                    branbeeShared
                        .presenterComponent
                        .projectDetailPresenter(view: delegate, projectId: projectId)
                
                if !dependencies.keys.contains(projectId) {
                    dependencies[projectId] = Dependencies(
                        presenter: projectDetailPresenter,
                        signal: signal.output,
                        observer: signal.input,
                        delegate: delegate
                    )
                }
                
                return dependencies[projectId]?.signal
                    .producer
                    .on(disposed: {
                        dependencies[projectId]?.observer.sendCompleted()
                        dependencies[projectId] = nil
                    }) ?? .none
            }()
        )
    }
    
}


private var dependencies: [AnyHashable: Dependencies<ProjectDetailPresenter, ProjectDetailsDelegateEvent>] = [:]
