import ExploreClient
import BranbeeSDK
import core
import ReactiveSwift
import Library
import ComposableArchitecture
import BrModels


extension PaymentDetailsClient {
    public static var live: Self {
        class Delegate: PaymentDetailsPresenterView {
            
            func onDisplayAddCardWithAExistingOneWarning() {
                observer.send(value: .onDisplayAddCardWithAExistingOneWarning)
            }
            
            func onDisplayCard(cardId: String, holder: String, last4Digits: String, expiryMonth: Int32, expiryYear: Int32) {
                observer.send(value: .onDisplayCard(
                    CreditCard(id: cardId,
                               holder: holder,
                               last4Digits: last4Digits,
                               expiryMonth: expiryMonth,
                               expiryYear: expiryYear
                    )
                ))                
            }
            
            func onDisplayLoading() {
                observer.send(value: .showLoading)
            }
            func onHideLoading() {
                observer.send(value: .hideLoading)
            }
            
            func onDisplayNoCard() {
                observer.send(value: .showNoCard)
            }
            
            func onDisplayTransientError(t: KotlinThrowable) {
                observer.send(value: .displayTransientError(t.failure))
            }
            
            func onFailureObtainingCard(t: KotlinThrowable?, retryBlock: @escaping () -> Void) {
                observer.send(value: .failedObtainCard(t?.failure, retryAction: RetryAction(run: retryBlock))
                )
            }
                                    
            func onNotifyNavigateToCardCreation() {}
            
            func onNotifyStripeToGetCard(cardId: String) {
                observer.send(value: .onNotifyStripeToGetCard(cardId))
            }
            
            let (output, observer): (Signal<PaymentsDelegateEvent, Never>, Signal<PaymentsDelegateEvent, Never>.Observer)
            
            init(_ signal: (
                    Signal<PaymentsDelegateEvent, Never>,
                    Signal<PaymentsDelegateEvent, Never>.Observer)) {
                self.output = signal.0
                self.observer = signal.1
            }
            
        }
        
        let signal = Signal<PaymentsDelegateEvent, Never>.pipe()
        var delegate: Delegate! = Delegate(signal)
        
        let paymentDetailsPresenter: PaymentDetailsPresenter = branbeeShared
            .presenterComponent
            .paymentDetailsPresenter(view: delegate)
        
        return Self(
            onViewLoad: .fireAndForget(
                paymentDetailsPresenter.onViewLoaded)
            ,
            onDetachView: .fireAndForget(
                paymentDetailsPresenter.onDetachView)
            ,
            onEventStripeSucceedGettingCard: { card in
                .fireAndForget {
                    paymentDetailsPresenter.onEventStripeSucceedGettingCard(
                        cardId: card.cardId,
                        holder: card.holder,
                        last4Digits: card.last4Digits,
                        expiryMonth: card.expiryMonth,
                        expiryYear: card.expiryYear
                    )
                }
            }, onActionCreateCard: .fireAndForget(
                paymentDetailsPresenter.onActionCreateCard)
            ,
            onActionDeleteCard: { continueToCreateCard in
                .fireAndForget {                                        
                    paymentDetailsPresenter
                        .onActionDeleteCard(continueToCreateCard: continueToCreateCard)
                }
            },
            onEventCardAdded: .fireAndForget {
                paymentDetailsPresenter.onEventCardAdded()
            },
            onViewRefresh: .fireAndForget {
                paymentDetailsPresenter.onViewRefresh()
            },
            onEventStripeFailedGettingCard: { (cardId, errorCode, errorMessage) in
                .fireAndForget {
                    paymentDetailsPresenter
                        .onEventStripeFailedGettingCard(
                            cardId: cardId,
                            errorCode: errorCode
                                .map(Int32.init)
                                .map(KotlinInt.init(int:)),
                            errorMessage: errorMessage
                        )
                }
            },
            delegate: signal.output
                .producer
                .on(disposed: { delegate = nil })
        )
              
    }
}
