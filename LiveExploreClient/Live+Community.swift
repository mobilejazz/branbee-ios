import ExploreClient
import BranbeeSDK
import core
import ReactiveSwift
import Library
import BrModels
import ComposableArchitecture

extension CommunityDetailClient {
           
    public static func live(_ communityId: String) -> Self {
                        
        class Delegate: CommunityDetailPresenterView {
            
            func onDisplayCommunity(community: Community) {
                observer.send(value: .showCommunity(branbeeCommunity(community)))
            }
            
            func onDisplayContent() {
                observer.send(value: .showContent)
            }
            
            func onDisplayFeedItenms(items: [FeedItem], reachedTheEnd: Bool) {
                observer.send(value: .showItems(items.map(brFeedItem), reachedEnd: reachedTheEnd))
            }
            
            func onDisplayFullscreenError(e: KotlinThrowable, retryBlock: @escaping () -> Void) {
                observer.send(value: .displayFailure(e.failure, retryAction: .init(run: retryBlock)))
            }
            
            func onDisplayJoinCommunityButton() {
                observer.send(value: .showJoinButton)
            }
            
            func onDisplayJoinCommunityRequestSentMessage() {
                observer.send(value: .showRequestSentMessage)
            }
            
            func onDisplayJoinedCommunityButtons() {
                observer.send(value: .showJoinedButtons)
            }
            
            func onDisplayJoinedToCommunityMessage() {
                observer.send(value: .showJoinedToCommunityMessage)
            }
            
            func onDisplayLeavedCommunityMessage() {
                observer.send(value: .showLeavedCommunityMessage)
            }
            
            func onDisplayLoading() {
                observer.send(value: .showLoading)
            }
            
            
        
            func onDisplayTransientError(e: KotlinThrowable) {
                observer.send(value: .displayTransientFailure(e.failure))
            }
            
            func onNavigateToCreateStory(community: Community) {}
            func onNavigateToProject(project: Project) {}
            func onNavigateToProjectComments(project: Project) {}
            func onNavigateToProjectDonation(project: Project) {}
            func onNavigateToStory(story: Story) {}
            func onNavigateToStoryComments(story: Story) {}
                                    
            let (output, observer): (
                Signal<CommunityDetailDelegateEvent, Never>,
                Signal<CommunityDetailDelegateEvent, Never>.Observer
            )
            
            init(_ signal: (Signal<CommunityDetailDelegateEvent, Never>, Signal<CommunityDetailDelegateEvent, Never>.Observer)) {
                self.output = signal.0
                self.observer = signal.1
            }
                        
        }
                                                                    
        return Self(
            detachView: .fireAndForget{
                dependencies[communityId]?.presenter.onDetachView()
            },
            viewDidLoad: .fireAndForget{
                dependencies[communityId]?.presenter.onViewLoaded()
            },
            loadMore: .fireAndForget{
                dependencies[communityId]?.presenter.onEventLoadMore()},
            refreshView: .fireAndForget{
                dependencies[communityId]?.presenter.onViewRefresh()
            },
            join:.fireAndForget{
                dependencies[communityId]?.presenter.onActionJoinCommunity()
            },
            leave: .fireAndForget{
                dependencies[communityId]?.presenter.onActionLeaveCommunity()
            },
            delegate: {
                let signal = Signal<CommunityDetailDelegateEvent, Never>.pipe()
                let delegate: Delegate! = Delegate(signal)
                
                let communityDetailPresenter: CommunityDetailPresenter =
                    branbeeShared
                        .presenterComponent
                        .communityDetailPresenter(
                            view: delegate,
                            communityId: communityId
                        )
                
                if !dependencies.keys.contains(communityId) {
                    dependencies[communityId] = Dependencies(
                        presenter: communityDetailPresenter,
                        signal: signal.output,
                        observer: signal.input,
                        delegate: delegate
                    )
                }
                
                return dependencies[communityId]?.signal
                    .producer
                    .on(disposed: {
                        dependencies[communityId]?.observer.sendCompleted()
                        dependencies[communityId] = nil
                    }) ?? .none
            }()
        )
    }
    
}

private var dependencies: [AnyHashable: Dependencies<CommunityDetailPresenter, CommunityDetailDelegateEvent>] = [:]
