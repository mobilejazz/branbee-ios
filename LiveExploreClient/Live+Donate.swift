import ExploreClient
import BranbeeSDK
import core
import ReactiveSwift
import Library
import BrModels
import ComposableArchitecture


private class Delegate: DonationPresenterView {
    
    func onDisplayCard(cardId: String, holder: String, last4Digits: String, expiryMonth: Int32, expiryYear: Int32) {
        observer.send(value: .showCard(
            CreditCard(
                id: cardId,
                holder: holder,
                last4Digits: last4Digits,
                expiryMonth: expiryMonth,
                expiryYear: expiryYear
            )
        ))
    }
    
    func onDisplayDonateProgress() {
        observer.send(value: .showDonateProgress)
    }
    
    func onDisplayDonateSuccess(projectName: String, amount: String) {
        observer.send(value: .showSuccessDonate(
                        projectName: projectName,
                        amount: amount))
    }
    
    func onDisplayLoading() {
        observer.send(value: .showLoading)
    }
    
    func onDisplayMissingAmountMessage() {
        observer.send(value: .showMissingAmount)
    }
    
    func onDisplayMissingCardWarning() {
        observer.send(value: .showMissingCard)
    }
    
    func onDisplayNoCard() {
        observer.send(value: .showNoCard)
    }
    
    func onDisplayTransientError(t: KotlinThrowable) {
        observer.send(value: .showFailure(t.failure))
    }
    
    func onFailureObtainingCard(t: KotlinThrowable?, retryBlock: @escaping () -> Void) {
        observer.send(value: .failObtainCard(t?.failure, action: RetryAction.init(run: retryBlock)))
    }
    
    func onHideDonateProgress() {
        observer.send(value: .hideDonateProgress)
    }
    
    func onHideLoading() {
        observer.send(value: .hideLoading)
    }
    
    func onNotifyCloseScreen() {
        //observer.send(value: .showDonateProgress)
    }
    
    func onNotifyNavigateToAddPaymentMethod() {
    }
    
    func onNotifyStripeToGetCard(cardId: String) {
        observer.send(value: .notifiyStripetoGet(cardId: cardId))
    }
                                                    
    let (output, observer): (
        Signal<DonateDelegateEvent, Never>,
        Signal<DonateDelegateEvent, Never>.Observer
    )
    
    init(_ signal: (Signal<DonateDelegateEvent, Never>, Signal<DonateDelegateEvent, Never>.Observer)) {
        self.output = signal.0
        self.observer = signal.1
    }
}

extension DonateClient {
           
    public static func live(projectId: String, projectName: String) -> Self {
                                                                               
        return Self(
            viewDidLoad: .fireAndForget{
                dependencies[projectId+projectName]?.presenter.onViewLoaded()
            },
            detachView: .fireAndForget{
                dependencies[projectId+projectName]?.presenter.onDetachView()
            },
            refresh: .fireAndForget{
                dependencies[projectId+projectName]?.presenter.onViewRefresh()
            },
            donateAmount: { amount in
                .fireAndForget {
                    dependencies[projectId+projectName]?.presenter.onActionDonate(amount: amount)
                }
            },
            failGettingCardId: { (id, errorCode, errorMessage)  in
                .fireAndForget {
                    dependencies[projectId+projectName]?.presenter.onEventStripeFailedGettingCard(
                        cardId: id,
                        errorCode: errorCode
                            .map(Int32.init)
                            .map(KotlinInt.init(value:)),
                        errorMessage: errorMessage
                    )
                }
            },
            cardAdded: .fireAndForget{
                dependencies[projectId+projectName]?.presenter.onEventCardAdded()
            },
            successGettingCart: { creditCard in
                .fireAndForget {
                    dependencies[projectId+projectName]?.presenter
                        .onEventStripeSucceedGettingCard(
                        cardId: creditCard.cardId,
                        holder: creditCard.holder,
                        last4Digits: creditCard.last4Digits,
                        expiryMonth: creditCard.expiryMonth,
                        expiryYear: creditCard.expiryYear
                    )
                }
            },
            delegate:  {
                let signal = Signal<DonateDelegateEvent, Never>.pipe()
                let delegate: Delegate! = Delegate(signal)
                
                let donationPresenter: DonationPresenter =
                    branbeeShared
                        .presenterComponent
                        .donationPresenter(
                            view: delegate,
                            projectId: projectId,
                            projectName: projectName
                        )
                
                if !dependencies.keys.contains(projectId+projectName) {
                    dependencies[projectId+projectName] = Dependencies(
                        presenter: donationPresenter,
                        signal: signal.output,
                        observer: signal.input,
                        delegate: delegate
                    )
                }
                
                return dependencies[projectId+projectName]?.signal
                    .producer
                    .on(disposed: {
                        dependencies[projectId+projectName]?.observer.sendCompleted()
                        dependencies[projectId+projectName] = nil
                    }) ?? .none
            }()
            )
    }
    
    
}


private var dependencies: [AnyHashable: Dependencies<DonationPresenter, DonateDelegateEvent>] = [:]
