import BranbeeSDK
import core
import ExploreClient
import Stripe
import ReactiveSwift

class IOSUnauthorizedResolution: Harmony_kotlinUnauthorizedResolution {
    public func resolve() -> Bool {
        return true
    }
}

extension StripeClient {
    public static var live: Self {
        .init { key in
            .fireAndForget {
                StripeAPI.defaultPublishableKey = key
            }
        } getStripeEphemeralKey: { apiVersion in
            SignalProducer.future { promise in
                branbeeShared.stripeComponent.getStripeEphemeralKeyInteractor()
                    .invoke(stripeApiVersion: apiVersion) { (key, error) in
                        if let error = error {
                            promise(.failure(error))
                        }
                        promise(.success(key))
                    }
            }
        } createCard: { params, clientSecret  in
            return .future { promise in
                let paymentMethodParams = STPPaymentMethodParams()
                paymentMethodParams.billingDetails = params.billingDetails
                paymentMethodParams.card = params.payMethodcardParms
                let confirmParams = STPSetupIntentConfirmParams(
                    clientSecret: clientSecret)
                confirmParams.paymentMethodParams = paymentMethodParams
                STPAPIClient.shared.confirmSetupIntent(with: confirmParams) { (intent, error) in
                    if let intent = intent {
                        switch intent.status {
                        case .succeeded:
                            promise(.success(intent.paymentMethodID))
                        case .requiresPaymentMethod:
                            promise(.failure(NSError(
                                domain: "stripe.payment.method",
                                code: -1,
                                userInfo: [NSLocalizedDescriptionKey: "requires PaymentMethod"]
                            )))
                        default:
                            break
                        }
                    }
                    
                    if let error = error {
                        promise(.failure(error))
                    }
                }
            }
        } paymentMethods: { cardId in
            
            class Provider: NSObject, STPCustomerEphemeralKeyProvider {
                func createCustomerKey(
                    withAPIVersion apiVersion: String,
                    completion: @escaping STPJSONResponseCompletionBlock
                ) {
                    branbeeShared.stripeComponent.getStripeEphemeralKeyInteractor()
                        .invoke(stripeApiVersion: apiVersion) { (key, error) in
                            if let error = error {
                                completion(nil, error)
                            }
                            
                           
                           
                            
                            if  let data = key?.data(using: .utf8), let json = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [AnyHashable: Any] {
                                completion(json , nil)
                            }
                            
                        }
                    }
            }
                       
            return .future { promise in
                let customerContext = STPCustomerContext(keyProvider: Provider())
                customerContext.listPaymentMethodsForCustomer { (paymentMethods, error) in
                    if let paymentMethod = paymentMethods?.first {
                        promise(.success(
                            CreditCard(
                                id: paymentMethod.stripeId,
                                holder: paymentMethod.billingDetails?.name ?? "",
                                last4Digits: paymentMethod.card!.last4!,
                                expiryMonth: Int32(paymentMethod.card!.expMonth),
                                expiryYear: Int32(paymentMethod.card!.expYear)
                            )
                        ))
                    }
                    if let error = error {
                        promise(.failure(error))
                    }
                }
            }
            
            
            
            
            
            
            
        }
        
    }
}
import BrModels
extension CardParams {
    var stripe: STPCardParams {
        let params = STPCardParams()
        params.name = name
        params.cvc = cvc
        params.number = number
        params.expMonth = expMonth
        params.expYear = expYear
        return params
    }
}

extension CardParams {
    var payMethodcardParms: STPPaymentMethodCardParams {
        let params = STPPaymentMethodCardParams()
        params.cvc = cvc
        params.number = number
        params.expMonth = NSNumber(value: expMonth)
        params.expYear = NSNumber(value: expYear)
        return params
    }
}



extension CardParams {
    var billingDetails: STPPaymentMethodBillingDetails {
        let params = STPPaymentMethodBillingDetails()
        params.name = name
        return params
    }
}

