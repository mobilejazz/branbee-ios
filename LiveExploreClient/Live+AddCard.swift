import ExploreClient
import BranbeeSDK
import core
import ReactiveSwift
import Library
import ComposableArchitecture


extension Failure {
    var kotlin: KotlinThrowable {
        KotlinThrowable(message: message)
    }
}

extension AddCardClient {
    
    public static var live: Self {
        
        class Delegate: AddCardPresenterView {
            
            func onDisplayFillHolderNameTransientMessage() {
                observer.send(value: .onDisplayFillHolderNameTransientMessage)
            }
            
            func onDisplaySetupCardSuccessfullyMessage() {
                observer.send(value: .onDisplaySetupCardSuccessfullyMessage)
            }
            
            func onDisplayTransientError(t: KotlinThrowable) {
                observer.send(value: .displayTransientError(t.failure))
            }
            
            func onFailedToSetupCard() {
                observer.send(value: .onFailedToSetupCard)
            }
            
            func onNotifyCloseScreen(cardAdded: Bool) {
                observer.send(value: .onNotifyCloseScreen(cardAdded: cardAdded))
            }
            
            func onNotifyStripeSetupIntentReceived(stripeSetupIntent: StripeSetupIntent) {
                observer.send(value: .onNotifyStripeSetupIntentReceived(
                    StripeIntent(
                        id: stripeSetupIntent.id,
                        clientSecret: stripeSetupIntent.clientSecret,
                        customer: stripeSetupIntent.customer
                    )
                ))
            }
            

            let (output, observer): (Signal<AddCardDelegateEvent, Never>, Signal<AddCardDelegateEvent, Never>.Observer)
            
            init(_ signal: (
                    Signal<AddCardDelegateEvent, Never>,
                    Signal<AddCardDelegateEvent, Never>.Observer)) {
                self.output = signal.0
                self.observer = signal.1
            }
            
        }
        
        let signal = Signal<AddCardDelegateEvent, Never>.pipe()
        var delegate: Delegate! = Delegate(signal)
        
        let addCardPresenter: AddCardPresenter = branbeeShared
            .presenterComponent
            .addCardPresenter(view: delegate)
          
        return Self(
            addCardWithToken: { token in
                .fireAndForget {
                    addCardPresenter
                        .onActionAddCard(stripeCardToken: token)
                }
            }, didRequiredStripeIntentWithCardHolder: { cardholder in
                .fireAndForget {
                    addCardPresenter
                        .onEventRequireStripeSetupIntent(cardHolder: cardholder)
                }
            }, onEventStripeFailedToSetupCardWithFailure: { failure in
                .fireAndForget {
                    addCardPresenter
                        .onEventStripeFailedToSetupCard(t: failure?.kotlin)
                }
            },
            onDetachView: .fireAndForget {
                addCardPresenter
                    .onDetachView()
            }, delegate: signal.output
                .producer
                .on(disposed: { delegate = nil })
        )
    }
}
