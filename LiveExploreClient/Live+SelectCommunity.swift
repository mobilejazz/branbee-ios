import ExploreClient
import BranbeeSDK
import core
import ReactiveSwift
import Library
import BrModels
import ComposableArchitecture

extension SelectCommunityClient {
           
    public static var live: Self {
                        
        class Delegate: SelectCommunityPresenterView {
            
            func onDisplayLoading() {
                observer.send(value: .showLoading)
            }
            
            func onHideLoading() {
                observer.send(value: .hideLoading)
            }
            
            func onDisplayCommunities(communities: [Community]) {
                observer.send(value: .displayCommunities(communities.map(branbeeCommunity)))
            }
            
            func onDisplayFullScreenError(e: KotlinException, retryBlock_ retryBlock: @escaping () -> Void) {
                observer.send(
                    value: .displayFailure(e.failure,
                                           retryAction: .init(run: retryBlock))
                )
            }
            
            func onDisplayNoCommunitiesFoundMessage() {
                observer.send(value: .displayNoMessage)
            }

            let (output, observer): (
                Signal<SelectCommunityDelegateEvent, Never>,
                Signal<SelectCommunityDelegateEvent, Never>.Observer
            )
            
            init(_ signal: (Signal<SelectCommunityDelegateEvent, Never>, Signal<SelectCommunityDelegateEvent, Never>.Observer)) {
                self.output = signal.0
                self.observer = signal.1
            }
                        
        }
                               
        let signal = Signal<SelectCommunityDelegateEvent, Never>.pipe()
        var delegate: Delegate! = Delegate(signal)
        
        let selectCommunityPresenter: SelectCommunityPresenter =
            branbeeShared
                .presenterComponent
                .selectCommunityPresenter(view: delegate)                
                                                    
        return Self(
            detachView: .fireAndForget(selectCommunityPresenter.onDetachView),
            viewDidLoad: .fireAndForget(selectCommunityPresenter.onViewLoaded),
            delegate: signal.output
                .producer
                .on(disposed: { delegate = nil })
        )
    }
    
}
