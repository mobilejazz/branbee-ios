import AuthenticationClient
import Library
import BranbeeSDK
import core
import ReactiveSwift
import ComposableArchitecture
import BrModels

extension KotlinException {
    public var failure: Failure {
        Failure(message: message, cause: cause?.getStackTraceAsString())
    }
}

private class Delegate: LoginPresenterView {
                                            
    let (output, observer): (Signal<LoginDelegateEvent, Never>, Signal<LoginDelegateEvent, Never>.Observer)

    init(_ signal: (Signal<LoginDelegateEvent, Never>, Signal<LoginDelegateEvent, Never>.Observer)) {
        self.output = signal.0
        self.observer = signal.1
    }
    
    func onDisplayErrorLogin(error: KotlinException) {
        observer.send(.value(
            .onDisplayErrorLogin(error.failure)))
    }
        
    public func onNotifyNavigateToMain() {
        observer.send(.value(.loginSuccess))
            
    }
    public func onNotifyNavigateToForgotPassword() {}
    public func onNotifyNavigateToSignUp() {}
}


extension LoginClient {
    
    public static var live: Self {
                                            
        
        let signal = Signal<LoginDelegateEvent, Never>.pipe()
        var delegate: Delegate! = Delegate(signal)
                        
        let loginPresenter: LoginPresenter =
            branbeeShared
                .presenterComponent
                .loginPresenter(view: delegate)
                                     
        return LoginClient(login: { request in
            .fireAndForget {
                loginPresenter.onActionLogin(
                    email: request.email.rawValue,
                    password: request.password.rawValue
                )
            }
        }, onDetachView: .fireAndForget {
            loginPresenter.onDetachView()
        },
        delegate: signal.output
            .producer
            .on(disposed: { delegate = nil })
        )
    
    }
}

extension SignUpClient {
    
    public static var live: Self {
                                            
        class Delegate: SignUpPresenterView {
            
            func onDisplayCountries(countries: [Country]) {
                observer.send(value:
                    .onDisplayCountries(countries.map {
                        BrCountry(id: $0.id, name: $0.name)
                    }
                ))
            }
                                    
            func onDisplayFullScreenError(e: KotlinException, retryBlock_ retryBlock: @escaping () -> Void) {
                observer.send(value: .onDisplayFullScreenError(
                                e.failure,
                                action: .init(run: retryBlock)))
            }
            
            func onDisplayFillRequiredFields() {
                observer.send(value: .onDisplayFillRequiredFields)
            }
                        
            func onNotifyNavigateToMain() {
                observer.send(value: .successRegistration)
            }
            
            func onFailedToRegister(e: KotlinException) {
                observer.send(value: .onFailedToRegister(e.failure))
            }
            
            func onFailedToValidateEmail() {
                observer.send(value: .onFailedToValidateEmail)
            }
            
            func onDisplayLoading() {}
            func onHideLoading() {}
            
                                                        
            let (output, observer): (
                Signal<SignUpDelegateEvent, Never>,
                Signal<SignUpDelegateEvent, Never>.Observer
            )

            init(_ signal: (Signal<SignUpDelegateEvent, Never>, Signal<SignUpDelegateEvent, Never>.Observer)) {
                self.output = signal.0
                self.observer = signal.1
            }
            
        }
        
        let signal = Signal<SignUpDelegateEvent, Never>.pipe()
        var delegate: Delegate! = Delegate(signal)
                        
        let signUpPresenter: SignUpPresenter = branbeeShared
            .presenterComponent
            .signUpPresenter(view: delegate)
                   
        
        
        return Self(register: { request in
            .fireAndForget {
                signUpPresenter.onActionRegister(
                    email: request.email,
                    password: request.password,
                    name: request.name,
                    city: request.city,
                    country: request.country.map { Country(id: $0.id, name: $0.name) },
                    bio: request.biography,
                    avatarPath: request.avatarPath
                )
            }
        }, onViewLoaded:
            .fireAndForget {
                signUpPresenter.onViewLoaded()
            }
        , onDetachView: .fireAndForget {
            signUpPresenter.onDetachView()
            delegate = nil
        }
        , delegate: signal.output
            .producer
            .on(disposed: { delegate = nil })
        )
    
    }
}



extension ForgotPasswordClient {
    
    public static var live: Self {
                                            
        class Delegate: ForgotPasswordPresenterView {
            
            func onDisplayPasswordRequestedSuccessfully() {
                observer.send(value: .requestedSuccessfully)
            }
            
            func onFailedToRecoverPassword(exception: KotlinException) {
                observer.send(value: .failedToRecoverPassword(exception.failure))
            }
            
            func onFailedToValidateEmail() {
                observer.send(value: .failedToValidateEmail)
            }
            
                                                    
            let (output, observer): (Signal<ForgotPasswordDelegateEvent, Never>, Signal<ForgotPasswordDelegateEvent, Never>.Observer)

            init(_ signal: (Signal<ForgotPasswordDelegateEvent, Never>, Signal<ForgotPasswordDelegateEvent, Never>.Observer)) {
                self.output = signal.0
                self.observer = signal.1
            }
                       
        }
        
        let signal = Signal<ForgotPasswordDelegateEvent, Never>.pipe()
        var delegate: Delegate! = Delegate(signal)
                        
        let forgotPasswordPresenter: ForgotPasswordPresenter = branbeeShared
            .presenterComponent
            .forgotPasswordPresenter(view: delegate)
                
        return Self(forgotPassword: { email in
            .fireAndForget {
                forgotPasswordPresenter
                    .onActionSendForgotPasswordRequest(email: email.rawValue)
            }
        }, delegate: signal.output
            .producer
            .on(disposed: { delegate = nil })
        )
    
    }
}
