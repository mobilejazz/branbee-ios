import AuthenticationClient

extension AuthenticationClient {
    public static var live: Self {
        .init(
            login: .live,
            register: .live,
            forgotPassword: .live,
            facebook: .empty
        )
    }
}
