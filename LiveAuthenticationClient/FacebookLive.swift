//import FacebookCore
//import FacebookLogin
//import ReactiveSwift
//import Library
//import AuthenticationClient
//
//private let loginManager = LoginManager()
//extension FacebookClient {
//    static var live: Self {
//
//        return Self { (app, options) in
//            ApplicationDelegate
//                .shared
//                .application(app, didFinishLaunchingWithOptions: options)
//        } openURL: { app, openURL, sourceApp, annotation in
//            ApplicationDelegate
//                .shared.application(
//                    app,
//                    open: openURL,
//                    sourceApplication: sourceApp,
//                    annotation: annotation
//                )
//        } logIn: {
//            .future { promise in
//                loginManager
//                    .logIn(permissions: [.publicProfile, .email]) { result in
//                    switch result {
//                    case .success(
//                            granted: _,
//                            declined: _,
//                            token: let token):
//                        guard let accessToken = token else { promise(.failure(.notGranted))
//                            return
//                        }
//
//                        promise(.success(
//                            FBAccessToken(
//                                token: accessToken.tokenString,
//                                userId: accessToken.userID
//                            )
//                        ))
//                    case .cancelled:
//                        promise(.failure(.cancelled))
//                    case let .failed(error):
//                        promise(.failure(.custom(error)))
//                    }
//                }
//            }
//
//        } logOut: { loginManager.logOut() }
//
//    }
//}
//
import BranbeeSDK
import core
class IOSUnauthorizedResolution: Harmony_kotlinUnauthorizedResolution {
    public func resolve() -> Bool {
        return true
    }
}
