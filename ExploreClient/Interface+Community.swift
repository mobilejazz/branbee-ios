/*
 - (void)onActionCreateStory __attribute__((swift_name("onActionCreateStory()")));
 - (void)onActionDonateProjectProject:(CoreProject *)project __attribute__((swift_name("onActionDonateProject(project:)")));
 - (void)onActionRetryLoading __attribute__((swift_name("onActionRetryLoading()")));
 - (void)onActionViewProjectProject:(CoreProject *)project __attribute__((swift_name("onActionViewProject(project:)")));
 - (void)onActionViewProjectCommentsProject:(CoreProject *)project __attribute__((swift_name("onActionViewProjectComments(project:)")));
 - (void)onActionViewStoryStory:(CoreStory *)story __attribute__((swift_name("onActionViewStory(story:)")));
 - (void)onActionViewStoryCommentsStory:(CoreStory *)story __attribute__((swift_name("onActionViewStoryComments(story:)")));
 - (void)onEventStoryCreated
 */

import ReactiveSwift
import Library
import BrModels

public enum CommunityDetailDelegateEvent: Equatable {
    case showCommunity(BrCommunity)
    case showContent
    case showLoading
    case showItems([BrFeedItem], reachedEnd: Bool)
    case displayFailure(Failure, retryAction: RetryAction)
    case showJoinButton
    case showRequestSentMessage
    case showJoinedButtons
    case storyCreated
    case showJoinedToCommunityMessage
    case showLeavedCommunityMessage
    case displayTransientFailure(Failure)
}

public struct CommunityDetailClient {
    public let detachView: SignalProducer<Never, Never>
    public let viewDidLoad: SignalProducer<Never, Never>
    public let loadMore: SignalProducer<Never, Never>
    public let refreshView: SignalProducer<Never, Never>
    public let join: SignalProducer<Never, Never>
    public let leave: SignalProducer<Never, Never>
    public let delegate: SignalProducer<CommunityDetailDelegateEvent, Never>
    public init(
        detachView: SignalProducer<Never, Never> = .empty,
        viewDidLoad: SignalProducer<Never, Never> = .empty,
        loadMore: SignalProducer<Never, Never> = .empty,
        refreshView: SignalProducer<Never, Never> = .empty,
        join: SignalProducer<Never, Never> = .empty,
        leave: SignalProducer<Never, Never> = .empty,
        delegate: SignalProducer<CommunityDetailDelegateEvent, Never>
    ) {
        self.detachView = detachView
        self.viewDidLoad = viewDidLoad
        self.delegate = delegate
        self.loadMore = loadMore
        self.refreshView = refreshView
        self.join = join
        self.leave = leave
    }
}


extension CommunityDetailClient {
    public static var empty: Self {
        .init(          
            delegate: .empty
        )
    }
}

let communities = [BrCommunity.branbeCommunity, .unicefCommunity]

let communityDetailDelegate = Signal<CommunityDetailDelegateEvent, Never>.pipe()
extension CommunityDetailClient {
    public static func mock(_ caseStudy: CaseStudy) -> (_ communityId: String) -> Self {
        return { communityId in
        Self(
            detachView: .empty,
            viewDidLoad: .fireAndForget {
                
                communityDetailDelegate.input.send(value: .showLoading)
                
                dispatchAfter(time: 2.0) {
                    switch caseStudy {
                    case .success:
                        communityDetailDelegate.input.send(value:   .showItems([BrFeedItem.brProjectFeed, BrFeedItem.brBookMarkedStoryFeed], reachedEnd: false
                        ))
                        communityDetailDelegate.input.send(value: .showContent)
                    case .error:
                        communityDetailDelegate.input.send(value: .displayFailure(.init(
                            message: "Failure", cause: nil), retryAction: .init(run: {
                                        
                                communityDetailDelegate.input.send(value: .showLoading)
                                                            
                                dispatchAfter(time: 2.0) {
                                    communityDetailDelegate.input.send(value: .showItems([.brProjectFeed, .brBookMarkedStoryFeed], reachedEnd: false
                                    ))
                                    communityDetailDelegate.input.send(value: .showContent)
                                }
                                
                               
                                                            
                                })))
                    case .empty:
                        communityDetailDelegate.input.send(value: .showItems([], reachedEnd: true))
                    }
                }
                
                
            },
            loadMore: .fireAndForget {
                communityDetailDelegate.input.send(value: .showItems([.brBookMarkedStoryFeed], reachedEnd: true
                ))
            },
            refreshView: .fireAndForget {
                communityDetailDelegate.input.send(value: .storyCreated)
            },
            join: .fireAndForget {
                if let community = communities.first(where: {$0.id == communityId}), community.isPrivate == true {
                    communityDetailDelegate.input.send(value: .showRequestSentMessage)
                } else {
                    communityDetailDelegate.input.send(value: .showJoinedToCommunityMessage)
                    communityDetailDelegate.input.send(value: .showJoinedButtons)
                }
                
            },
            leave: .fireAndForget {
                switch caseStudy {
                case .success:
                    communityDetailDelegate.input.send(value: .showLeavedCommunityMessage)
                    communityDetailDelegate.input.send(value: .showJoinButton)
                case .error:
                    communityDetailDelegate.input.send(value: .displayTransientFailure(.init(message: "Comme details", cause: nil)))
                case .empty:
                    break
                }
            },
            delegate: communityDetailDelegate.output.producer
        )
    }
    }
}
