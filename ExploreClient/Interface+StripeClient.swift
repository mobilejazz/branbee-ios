import BrModels
import ReactiveSwift

public struct StripeClient {
    public let setPublishableKey: (String) -> SignalProducer<Never, Never>
    public let getStripeEphemeralKey: (_ apiVersion: String) -> SignalProducer<String?, Error>
    public let createCard: (CardParams, _ clientSecret: String) -> SignalProducer<String?, Error>
    public let paymentMethods: (_ cardId: String) -> SignalProducer<CreditCard?, Error>
    public init(
        setPublishableKey: @escaping (String) -> SignalProducer<Never, Never>,
        getStripeEphemeralKey: @escaping (_ apiVersion: String) -> SignalProducer<String?, Error>,
        createCard: @escaping (CardParams, String) -> SignalProducer<String?, Error>,
        paymentMethods: @escaping (_ cardId: String) -> SignalProducer<CreditCard?, Error>
    ) {
        self.setPublishableKey = setPublishableKey
        self.getStripeEphemeralKey = getStripeEphemeralKey
        self.createCard = createCard
        self.paymentMethods = paymentMethods
    }
}


extension StripeClient {
    public static var empty: Self {
        .init { publishableKey in
            .empty
        } getStripeEphemeralKey: { _ in
            .empty
        } createCard: { _,_  in
            .empty
        } paymentMethods: { _ in
            .empty
        }

    }
}

extension StripeClient {
    public static func mock(caseStudy: CaseStudy) -> Self {
        .init { publishableKey in
            .empty
        } getStripeEphemeralKey: { _ in
            .init(value: caseStudy == .success
                    ? "id"
                    : nil
            )
        } createCard: { _,_  in
            .init(value: caseStudy == .success
                    ? "id"
                    : nil
            )
        } paymentMethods: { _ in
            caseStudy == .success
            ? .init(value: .visa)
                : SignalProducer(error: NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: "error getting card"]))
        }

    }
}
