import ReactiveSwift
import Library
import BrModels

public enum LinkStoryDelegateEvent: Equatable {
    case showFailure(Failure, retryBlock: RetryAction)
    case displayRequestSentMessage
    case showLoading, hideLoading
    case showNoStoryFound
    case showStories([BrStory], didReachEnd: Bool)
    case showAlert(Failure)
    case dismissView(storyLinked: Bool)
}

public struct LinkStoryClient {
    public let linkStory: (BrStory) -> SignalProducer<Never, Never>
    public let detachView: SignalProducer<Never, Never>
    public let viewDidLoad: SignalProducer<Never, Never>
    public let loadMore: SignalProducer<Never, Never>
    public let searchText: (String) -> SignalProducer<Never, Never>
    public let delegate: SignalProducer<LinkStoryDelegateEvent, Never>
    public init(
        linkStory: @escaping (BrStory) -> SignalProducer<Never, Never> = { _ in .empty },
        searchText: @escaping (String) -> SignalProducer<Never, Never> = { _ in .empty },
        detachView: SignalProducer<Never, Never> = .empty,
        viewDidLoad: SignalProducer<Never, Never> = .empty,
        loadMore: SignalProducer<Never, Never> = .empty,
        delegate: SignalProducer<LinkStoryDelegateEvent, Never>
    ) {
        self.linkStory = linkStory
        self.detachView = detachView
        self.viewDidLoad = viewDidLoad
        self.searchText = searchText
        self.loadMore = loadMore
        self.delegate = delegate
    }
}

extension LinkStoryClient {
    public static var empty: Self {
        Self(
            delegate: .empty
        )
    }
}

let linkStoryDelegate = Signal<LinkStoryDelegateEvent, Never>.pipe()
extension LinkStoryClient {
    public static func mock(_ caseStudy: CaseStudy = .success) -> Self {
        Self(
            linkStory: { (story) in
                .fireAndForget {
                    if caseStudy == .success {
                    linkStoryDelegate.input.send(value: .displayRequestSentMessage)
                    linkStoryDelegate.input.send(value: .dismissView(storyLinked: true))
                    } else {
                        linkStoryDelegate.input.send(value: .showAlert(.message("An Error")))
                    }
                }
            },
            searchText: { text in
                .fireAndForget {
                    linkStoryDelegate.input.send(value: .showStories([.branbeeStory], didReachEnd: true))
                }                
            },
            detachView: .none,
            viewDidLoad: .fireAndForget {
                
                linkStoryDelegate.input.send(value: .showLoading)
                
                dispatchAfter(time: 2.0) {
                    switch caseStudy {
                    case .success:
                        linkStoryDelegate.input.send(value: .showStories([.branbeeStory, .shortBranbeeStory], didReachEnd: true))
                        linkStoryDelegate.input.send(value: .hideLoading)
                    case .error:
                        linkStoryDelegate.input.send(value: .showFailure(.message("Message error"), retryBlock: .init(run: {
                            linkStoryDelegate.input.send(value: .showLoading)
                            
                            dispatchAfter(time: 3.0) {
                                linkStoryDelegate.input.send(value: .showStories([.branbeeStory, .shortBranbeeStory, .branbeeBookMarkedStory], didReachEnd: false))
                                linkStoryDelegate.input.send(value: .hideLoading)
                            }
                            
                        })))
                    case .empty:
                        linkStoryDelegate.input.send(value: .showNoStoryFound)
                    }
                }
                
                
            },
            loadMore: .fireAndForget {
                if caseStudy == .success {
                    linkStoryDelegate.input.send(value: .showStories([.branbeeStory, .shortBranbeeStory], didReachEnd: true))
                }
            },
            delegate: linkStoryDelegate.output.producer
        )
    }
}
