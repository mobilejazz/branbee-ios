import ReactiveSwift
public struct Dependencies<Presenter, Event> {
    public let delegate: Any?
    public let presenter: Presenter
    public let (signal, observer): (Signal<Event, Never>, Signal<Event, Never>.Observer)
    public init(
        presenter: Presenter,
        signal: Signal<Event, Never>, observer: Signal<Event, Never>.Observer,
        delegate: Any? = nil
    ) {        
        self.presenter = presenter
        self.signal = signal
        self.observer = observer
        self.delegate = delegate
    }
}
