import ReactiveSwift
import Library
import BrModels

//onNotifyCloseScreen
//onNotifyNavigateToAddPaymentMethod
public enum DonateDelegateEvent: Equatable {
    case showCard(CreditCard)
    case showDonateProgress, hideDonateProgress
    case showSuccessDonate(projectName: String, amount: String)
    case showLoading, hideLoading
    case showMissingAmount
    case showMissingCard
    case showNoCard
    case showFailure(Failure)
    case failObtainCard(Failure?, action: RetryAction)
    case notifiyStripetoGet(cardId: String)
}
//onActionAddPaymentMethod
//onActionCloseOnSuccess

public struct DonateClient {
    public var detachView: SignalProducer<Never, Never>
    public var refresh: SignalProducer<Never, Never>
    public var viewDidLoad: SignalProducer<Never, Never>
    public var cardAdded: SignalProducer<Never, Never>
    public var donateAmount: (String) -> SignalProducer<Never, Never>
    public var failGettingCardId: (String, Int?, String?) -> SignalProducer<Never, Never>
    public var successGettingCart: (CreditCard) -> SignalProducer<Never, Never>
    public var delegate: SignalProducer<DonateDelegateEvent, Never>
    public init(
        viewDidLoad: SignalProducer<Never, Never> = .empty,
        detachView: SignalProducer<Never, Never> = .empty,
        refresh: SignalProducer<Never, Never> = .empty,
        donateAmount: @escaping (String) -> SignalProducer<Never, Never> = { _ in .empty },
        failGettingCardId: @escaping (String, Int?, String?) -> SignalProducer<Never, Never> = { _,_,_ in .empty },
        cardAdded: SignalProducer<Never, Never> = .empty,
        successGettingCart: @escaping (CreditCard) -> SignalProducer<Never, Never> = { _ in .empty },
        delegate: SignalProducer<DonateDelegateEvent, Never>
    ) {
        self.viewDidLoad = viewDidLoad
        self.detachView = detachView
        self.refresh = refresh
        self.cardAdded = cardAdded
        self.failGettingCardId = failGettingCardId
        self.successGettingCart = successGettingCart
        self.donateAmount = donateAmount
        self.delegate = delegate
    }
}

extension DonateClient {
    public static var empty: Self {
        Self(
            viewDidLoad: .empty,
            detachView: .empty,
            refresh: .empty,
            donateAmount: { _ in .empty},
            failGettingCardId: { _,_,_ in .empty },
            cardAdded: .empty,
            successGettingCart: { _ in .empty },
            delegate: .empty
        )
    }
}

let donateDelegate = Signal<DonateDelegateEvent, Never>.pipe()
extension DonateClient {
    public static func mock(_ caseStudy: CaseStudy) -> Self {
        Self(
            viewDidLoad: .fireAndForget {
                getCard(caseStudy: caseStudy)
            },
            detachView: .empty,
            refresh: .empty,
            donateAmount: { amount in .fireAndForget {
                
                if amount == "" {
                    donateDelegate.input.send(value: .showMissingAmount)
                }
                
                switch caseStudy {
                case .success:
                    donateDelegate.input.send(value: .showDonateProgress)
                    dispatchAfter(time: 2.0) {
                        donateDelegate.input.send(value: .showSuccessDonate(projectName: "some project", amount: amount))
                    }
                case .error:
                    donateDelegate.input.send(value: .hideDonateProgress)
                    dispatchAfter(time: 2.0) {
                        donateDelegate.input.send(value: .showFailure(Failure.init(message: "Error", cause: nil)))
                    }
                case .empty:
                    break
                }
            }},
            failGettingCardId: { _,_,_ in .fireAndForget {
                donateDelegate.input.send(value: .failObtainCard(nil, action: RetryAction.init(run: {
                    getCard(caseStudy: caseStudy)
                })))
            } },
            cardAdded: .empty,
            successGettingCart: { _ in .fireAndForget {
                donateDelegate.input.send(value: .showCard(.visa))
                donateDelegate.input.send(value: .hideLoading)
            } },
            delegate: donateDelegate.output.producer
        )
    }
}

func getCard(caseStudy: CaseStudy) {
    donateDelegate.input.send(value: .showLoading)
    dispatchAfter(time: 2.0) {
        switch caseStudy {        
        case .success:
            donateDelegate.input.send(value: .notifiyStripetoGet(cardId: "id"))
        case .error:
            donateDelegate.input.send(value: .failObtainCard(.init(message: "Error", cause: nil), action: .init(run: {
                getCard(caseStudy: .success)
            })))
        case .empty:
            donateDelegate.input.send(value: .showNoCard)
            donateDelegate.input.send(value: .hideLoading)
        }
    }
}

