import ReactiveSwift
import Library
import BrModels

public enum ProjectDetailsDelegateEvent: Equatable {
    case showLoading, hideLoading
    case displayFailure(Failure, retryAction: RetryAction)
    case displayProject(BrProject, comment: BrComment?, linkedStories: [BrStory], isLinkedButtonShown: Bool)
}

/// doc
/*
onActionDonate
onActionLinkStory
onActionShare
onActionViewComments
onActionViewCommunity
onActionViewStory
onActionViewStoryComments
 
onEventStoryLinked
 */

public struct ProjectDetailsClient {
    public let detachView: SignalProducer<Never, Never>
    public let viewDidLoad: SignalProducer<Never, Never>
    public let refreshView: SignalProducer<Never, Never>
    public let delegate: SignalProducer<ProjectDetailsDelegateEvent, Never>    
    public init(
        detachView: SignalProducer<Never, Never> = .empty,
        viewDidLoad: SignalProducer<Never, Never> = .empty,
        refreshView: SignalProducer<Never, Never> = .empty,
        delegate: SignalProducer<ProjectDetailsDelegateEvent, Never>
    ) {
        self.detachView = detachView
        self.viewDidLoad = viewDidLoad
        self.refreshView = refreshView
        self.delegate = delegate
    }
}

extension ProjectDetailsClient {
    public static var empty: Self {
        .init(delegate: .empty)
    }
}
let projectDetailDelegate = Signal<ProjectDetailsDelegateEvent, Never>.pipe()
    
extension ProjectDetailsClient {
    public static func mock(_ caseStudy: CaseStudy) -> Self {
        .init(
            detachView: .empty,
            viewDidLoad: .fireAndForget {
                loadProjects(caseStudy: caseStudy)
            },
            refreshView: .fireAndForget {
                loadProjects(caseStudy: caseStudy)
            },
            delegate: projectDetailDelegate.output.producer)
    }
}
    
private func loadProjects(caseStudy: CaseStudy) {
    projectDetailDelegate.input.send(value: .showLoading)
    dispatchAfter(time: 2.0) {
        switch caseStudy {
        case .success:
            projectDetailDelegate.input.send(value: .displayProject(.branbee,                                comment: .comment,
                    linkedStories: [BrStory.branbeeStory, BrStory.branbeeBookMarkedStory],
                    isLinkedButtonShown: false))
            projectDetailDelegate.input.send(value: .hideLoading)
        case .error:
            projectDetailDelegate.input.send(value: .displayFailure(.message("error"), retryAction: .init(run: {
                loadProjects(caseStudy: .success)
            })))
        case .empty:
            break
        }
    }
}


