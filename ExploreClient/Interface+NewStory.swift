import ReactiveSwift
import Library
import BrModels

public enum NewStoryDelegateEvent: Equatable {
    case onDisplayCommunityName(String)
    case showMakeStoryLoading, hideMakeStoryLoading
    case onDisplayDraft(BrDraft)
    case onDisplayEmailNotVerifiedWarning(userName: String, email: String)
    case onDisplayFillTextMessage
    case showLoading, hideLoading
    case onDisplaySaveDraftDialog
    case showSuccessMessage
    case onDisplayTransientError(Failure)
    case onNotifyCloseScreen(storyCreated: Bool)
    case onNotifyEnableCreateStoryButton(enable: Bool)
}

/*
 - (void)onActionChangeCommunity __attribute__((swift_name("onActionChangeCommunity()")));
 - (void)onActionCloseScreen __attribute__((swift_name("onActionCloseScreen()")));
 - (void)onActionResendConfirmationEmail __attribute__((swift_name("onActionResendConfirmationEmail()")));
 - (void)onActionSaveDraft __attribute__((swift_name("onActionSaveDraft()")));
 */

public var _storyText_: String? = nil
public var _mediaFilePath_: String? = nil

public struct NewStoryClient {
    public let detachView: SignalProducer<Never, Never>
    public let closeScreen: SignalProducer<Never, Never>
    public let viewRefresh: SignalProducer<Never, Never>
    public let cancelDraft: SignalProducer<Never, Never>
    public let saveDraft: SignalProducer<Never, Never>
    public let sendConfirmationEmail: SignalProducer<Never, Never>
    public let makeStoryFromFile: (BrByteArrayFromFile?, String) -> SignalProducer<Never, Never>
    public let makeStoryFromImage: (String?, String) -> SignalProducer<Never, Never>
    public let changeCommunity: (BrCommunity) -> SignalProducer<Never, Never>
    public let delegate: SignalProducer<NewStoryDelegateEvent, Never>
    public init(
        detachView: SignalProducer<Never, Never>,
        closeScreen: SignalProducer<Never, Never>,
        cancelDraft: SignalProducer<Never, Never>,
        saveDraft: SignalProducer<Never, Never>,
        viewRefresh: SignalProducer<Never, Never>,
        sendConfirmationEmail: SignalProducer<Never, Never>,
        changeCommunity: @escaping (BrCommunity) -> SignalProducer<Never, Never>,
        makeStoryFromFile: @escaping (BrByteArrayFromFile?, String) -> SignalProducer<Never, Never>,
        makeStoryFromImage: @escaping (String?, String) -> SignalProducer<Never, Never>,
        delegate: SignalProducer<NewStoryDelegateEvent, Never>
    ) {
        self.detachView = detachView
        self.closeScreen = closeScreen
        self.cancelDraft = cancelDraft
        self.viewRefresh = viewRefresh
        self.sendConfirmationEmail = sendConfirmationEmail
        self.delegate = delegate
        self.makeStoryFromFile = makeStoryFromFile
        self.makeStoryFromImage = makeStoryFromImage
        self.changeCommunity = changeCommunity
        self.saveDraft = saveDraft
    }
}

extension NewStoryClient {
    public static var empty: Self {
        .init(
            detachView: .empty,
            closeScreen: .empty,
            cancelDraft: .empty,
            saveDraft: .empty,
            viewRefresh: .empty,
            sendConfirmationEmail: .empty,
            changeCommunity: { _ in .empty },
            makeStoryFromFile: { _, _ in .empty },
            makeStoryFromImage: { _, _ in .empty },
            delegate: .empty
        )
    }
}

let newStoryDelegate = Signal<NewStoryDelegateEvent, Never>.pipe()

public enum NewStoryCase {
    case unverified, success, error
}

extension NewStoryClient {
    public static func mock(_ caseStudy: NewStoryCase = .success) -> (_ draft: String?) -> Self {
        return { draft in
        .init(
            detachView: .empty,
            closeScreen: .fireAndForget {
                
                if Bool.random() {
                    newStoryDelegate.input.send(value: .onDisplaySaveDraftDialog)
                } else {
                    newStoryDelegate.input.send(value: .onNotifyCloseScreen(storyCreated: false))
                }
                
            },
            cancelDraft: .fireAndForget {
                newStoryDelegate.input.send(value: .onNotifyCloseScreen(storyCreated: false))
            },
            saveDraft: .fireAndForget {
                
                newStoryDelegate.input.send(value: .onNotifyCloseScreen(storyCreated: false))
                
                
            },
            viewRefresh: .fireAndForget {
                                                
                newStoryDelegate.input.send(value: .showLoading)
                
                dispatchAfter(time: 2.0) {
                    if let _ = draft {
                        newStoryDelegate.input.send(value: .onDisplayDraft(BrDraft.draft))
                    }
                    if caseStudy != .unverified {
                        newStoryDelegate.input.send(value: .hideLoading)
                    } else {
                        newStoryDelegate.input.send(value: .onDisplayEmailNotVerifiedWarning(
                            userName: "younews",
                            email: "youness gmail.com"
                        ))
                    }
                }
                
            },
            sendConfirmationEmail: .empty,
            changeCommunity: { _ in .empty },
            makeStoryFromFile: { _, _ in .empty },
            makeStoryFromImage: { imagePath, _ in .fireAndForget {
                
                guard let imagePath = imagePath else {
                    newStoryDelegate.input.send(value: .onDisplayFillTextMessage)
                    return
                }
                
                if caseStudy == .success {
                    newStoryDelegate.input.send(value: .showLoading)
                    dispatchAfter(time: 1.0) {
                        newStoryDelegate.input.send(value: .onNotifyEnableCreateStoryButton(enable: false))
                        newStoryDelegate.input.send(value: .showSuccessMessage)
                        newStoryDelegate.input.send(value: .onNotifyCloseScreen(storyCreated: true))
                    }
                }
                if caseStudy == .error {
                    newStoryDelegate.input.send(value: .hideLoading)
                    newStoryDelegate.input.send(value: .onNotifyEnableCreateStoryButton(enable: true))
                    newStoryDelegate.input.send(value: .onDisplayTransientError(Failure.init(message: "error", cause: nil)))
                }
                
            } },
            delegate: newStoryDelegate.output.producer
        )
    }
    }
}


func createStory(_ caseStudy: NewStoryCase) {
    
    switch caseStudy {
    case .error, .unverified:
        newStoryDelegate.input.send(value: .onDisplayFillTextMessage)
        newStoryDelegate.input.send(value: .hideLoading)
        newStoryDelegate.input.send(value: .onNotifyEnableCreateStoryButton(enable: true))
    case .success:
        newStoryDelegate.input.send(value: .showLoading)
        newStoryDelegate.input.send(value: .onNotifyEnableCreateStoryButton(enable: false))
        newStoryDelegate.input.send(value: .showSuccessMessage)
        newStoryDelegate.input.send(value: .onNotifyCloseScreen(storyCreated: true))
    }
    
}

