import ReactiveSwift
import Library
import BrModels

public enum ExploreDelegateEvent: Equatable {
    case displayError(Failure)
    case items([BrFeedItem], reachedTheEnd: Bool)
    case displayFeedPageError(Failure, action: RetryAction)
    case showLoading, hideLoading
    case createStory
    case showProject(BrProject)
    case showProjectComments(BrProject)
    case showStory(BrStory)
    case showStoryComments(BrStory)
}

public struct ExploreClient {
    public let createStory: SignalProducer<Never, Never>
    public let onActionRetryLoading: SignalProducer<Never, Never>
    public let onActionViewProject: (BrProject) -> SignalProducer<Never, Never>
    public let onActionViewProjectComments: (BrProject) -> SignalProducer<Never, Never>
    public let onActionViewStory: (BrStory) -> SignalProducer<Never, Never>
    public let onActionViewStoryCommentsStory: (BrStory) -> SignalProducer<Never, Never>
    public let onDetachView: SignalProducer<Never, Never>
    public let onEventLoadMore: SignalProducer<Never, Never>
    public let onViewLoaded: SignalProducer<Never, Never>
    public let onViewRefresh: SignalProducer<Never, Never>
    public let delegate: SignalProducer<ExploreDelegateEvent, Never>
    public init(
        onActionCreateStory: SignalProducer<Never, Never>,
        onActionRetryLoading: SignalProducer<Never, Never>,
        onActionViewProject: @escaping (BrProject) -> SignalProducer<Never, Never>,
        onActionViewProjectComments: @escaping (BrProject) -> SignalProducer<Never, Never>,
        onActionViewStory: @escaping (BrStory) -> SignalProducer<Never, Never>,
        onActionViewStoryCommentsStory: @escaping (BrStory) -> SignalProducer<Never, Never>,
        onDetachView: SignalProducer<Never, Never>,
        onEventLoadMore: SignalProducer<Never, Never>,
        onViewLoaded: SignalProducer<Never, Never>,
        onViewRefresh: SignalProducer<Never, Never>,
        delegate: SignalProducer<ExploreDelegateEvent, Never>
    ) {        
        self.createStory = onActionCreateStory
        self.onActionRetryLoading = onActionRetryLoading
        self.onActionViewProject = onActionViewProject
        self.onActionViewProjectComments = onActionViewProjectComments
        self.onActionViewStory = onActionViewStory
        self.onActionViewStoryCommentsStory = onActionViewStoryCommentsStory
        self.onDetachView = onDetachView
        self.onEventLoadMore = onEventLoadMore
        self.onViewLoaded = onViewLoaded
        self.onViewRefresh = onViewRefresh
        self.delegate = delegate
    }
}


extension ExploreClient {
    public static var empty: ExploreClient {
        .init(
            onActionCreateStory: .empty,
            onActionRetryLoading: .empty,
            onActionViewProject: { _ in .empty },
            onActionViewProjectComments: { _ in .empty },
            onActionViewStory: { _ in .empty },
            onActionViewStoryCommentsStory: { _ in .empty },
            onDetachView: .empty,
            onEventLoadMore: .empty,
            onViewLoaded: .empty,
            onViewRefresh: .empty,
            delegate: .empty
        )
    }
}
let exploreDelegate = Signal<ExploreDelegateEvent, Never>.pipe()
extension ExploreClient {
    public static func mock(_ caseStudy: CaseStudy) -> Self {
        .init(
            onActionCreateStory: .empty,
            onActionRetryLoading: .fireAndForget {
                loadPage(caseStudy: caseStudy)
            },
            onActionViewProject: { _ in .empty },
            onActionViewProjectComments: { _ in .empty },
            onActionViewStory: { _ in .empty },
            onActionViewStoryCommentsStory: { _ in .empty },
            onDetachView: .empty,
            onEventLoadMore: .fireAndForget {
                if caseStudy == .success {
                    dispatchAfter(time: 2.0) {
                        exploreDelegate.input.send(
                            value: .items(
                                [
                                 .story_video,
                                 .story_picture,
                                 .story_salma
                                ],
                                reachedTheEnd: true)
                        )
                    }
                }
            },
            onViewLoaded: .fireAndForget {
                loadPage(caseStudy: caseStudy)
            },
            onViewRefresh: .empty,
            delegate: exploreDelegate.output.producer
        )
    }
}

func loadPage(caseStudy: CaseStudy) {
    exploreDelegate.input.send(value: .showLoading)
    
    switch caseStudy {
    case .success:
        dispatchAfter(time: 1.0) {
            exploreDelegate.input.send(
                value: .items(
                    [
                    .story_video,
                    .story_picture,
                    .story_salma,
                    .brBookMarkedStoryFeed
                    ],
                    reachedTheEnd: false))
            
            exploreDelegate.input.send(
                value: .hideLoading)
        }
    case .error:
        dispatchAfter(time: 2.0) {
            exploreDelegate.input.send(
                value: .displayFeedPageError(.message("error explore"), action: .init(run: {
                    exploreDelegate.input.send(value: .hideLoading)
                })))
        }
    case .empty:
        exploreDelegate.input.send(
            value: .items(
                [],
                reachedTheEnd: true)
        )
    }
}


/*
 onActionCreateStory: .empty,
 onActionRetryLoading: .empty,
 onActionViewProject: { _ in .empty },
 onActionViewProjectComments: { _ in .empty },
 onActionViewStory: { _ in .empty },
 onActionViewStoryCommentsStory: { _ in .empty },
 onDetachView: .empty,
 onEventLoadMore: .fireAndForget {
     exploreDelegate.input.send(
         value: .items([.brUnicefProjectFeed], reachedTheEnd: true)
     )
 },
 onViewLoaded: .fireAndForget {
     
     exploreDelegate.input.send(value: .showLoading)
     DispatchQueue.main.asyncAfter(deadline: .now() + 4.0) {
         exploreDelegate.input.send(value: .hideLoading)
     }
     DispatchQueue.main.asyncAfter(deadline: .now() + 5.0) {
         exploreDelegate.input.send(
             value: .items(
                 [.brStoryFeed, .brProjectFeed, .brBookMarkedStoryFeed, .brProjectFeed, .brProjectFeed, .brStoryFeed, .brBookMarkedStoryFeed],
                     reachedTheEnd: false))
     }
     
     

     
 },
 onViewRefresh: .empty,
 delegate: exploreDelegate
     .output
     .producer
 */
