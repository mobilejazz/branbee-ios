import ReactiveSwift
import Library
import BrModels

public enum CommentDelegateEvent: Equatable {
    case loadComments(comments: [BrComment], reachedTheEnd: Bool)
    case displayEmptyState
    case displayError(Failure, retryBlock: RetryAction)
    case showLoading, hideLoading
    case displayFailure(Failure)
    case didRemoveCommentText
}

/*
 - (void)onActionSendCommentText:(NSString *)text __attribute__((swift_name("onActionSendComment(text:)")));
 - (void)onDetachView __attribute__((swift_name("onDetachView()")));
 - (void)onEventLoadMore __attribute__((swift_name("onEventLoadMore()")));
 - (void)onViewLoaded __attribute__((swift_name("onViewLoaded()")));
 */

public struct CommentsClient {
    public let sendComment: (String) -> SignalProducer<Never, Never>
    public let detachView: SignalProducer<Never, Never>
    public let loadMore: SignalProducer<Never, Never>
    public let viewDidLoad: SignalProducer<Never, Never>
    public let delegate: SignalProducer<CommentDelegateEvent, Never>
    public init(
        sendComment: @escaping (String) -> SignalProducer<Never, Never>,
        detachView: SignalProducer<Never, Never>,
        viewDidLoad: SignalProducer<Never, Never>,
        loadMore: SignalProducer<Never, Never>,
        delegate: SignalProducer<CommentDelegateEvent, Never>
    ) {
        self.sendComment = sendComment
        self.detachView = detachView
        self.viewDidLoad = viewDidLoad
        self.loadMore = loadMore
        self.delegate = delegate
    }
}


// Empty
extension CommentsClient {
    public static var empty: Self {
        Self(
            sendComment: { _ in .empty },
            detachView: .empty,
            viewDidLoad: .empty,
            loadMore: .empty,
            delegate: .empty
        )
    }
}

// Like Android

public enum CaseStudy {
    case success, error, empty
}

private var didReachEnd: Bool = false

let commentDelegate = Signal<CommentDelegateEvent, Never>.pipe()
extension CommentsClient {
    public static func mock(_ caseStudy: CaseStudy = .success) -> Self {
        Self(
            sendComment: { text in
                if caseStudy == .error {
                    commentDelegate.input
                        .send(value:
                                .displayFailure(.init(
                            message: "Error Commenting",
                            cause: nil
                            ))
                        )
                } else {
                    loadComments(caseStudy: caseStudy)
                    commentDelegate.input.send(value: .didRemoveCommentText)
                }
                return .none
            },
            detachView: .empty,
            viewDidLoad: .fireAndForget {
                loadComments(caseStudy: caseStudy)
            },
            loadMore: .fireAndForget {
                commentDelegate.input.send(value: .loadComments(
                    comments: [
                        .comment,
                        .shortComment,
                        .customComment,
                        .customComment,
                        .customComment,
                        .customComment,
                        .customComment,
                        .customComment,
                        .customComment
                    ],
                    reachedTheEnd: true
                ))
            },
            delegate: commentDelegate.output.producer
        )
        
    }
}



      

private func loadComments(caseStudy: CaseStudy) {
    commentDelegate.input.send(value: .showLoading)
    
    dispatchAfter(time: 2.0) {
        
        switch caseStudy {
        case .error:
            commentDelegate.input
                .send(value:
                        .displayFailure(.init(
                    message: "Error shown",
                    cause: nil
                    ))
                )
        case .empty:
            commentDelegate.input
                .send(value:
                    .displayEmptyState
                )
        case .success:
            commentDelegate.input.send(value: .loadComments(
                comments: [
                    .comment,
                    .shortComment,
                    .customComment,
                    .customComment,
                    .customComment,
                    .customComment,
                    .customComment],
                reachedTheEnd: false
            ))
            commentDelegate.input.send(value: .hideLoading)
           
        }
    }

}
