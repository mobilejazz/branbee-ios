import ReactiveSwift
import Library
import BrModels

public enum SelectCommunityDelegateEvent: Equatable {
    case displayFailure(Failure, retryAction: RetryAction)
    case displayCommunities([BrCommunity])
    case displayNoMessage
    case hideLoading, showLoading
}

public struct SelectCommunityClient {
    public let detachView: SignalProducer<Never, Never>
    public let viewDidLoad: SignalProducer<Never, Never>
    public let delegate: SignalProducer<SelectCommunityDelegateEvent, Never>
    public init(
        detachView: SignalProducer<Never, Never> = .empty,
        viewDidLoad: SignalProducer<Never, Never> = .empty,
        delegate: SignalProducer<SelectCommunityDelegateEvent, Never>
    ) {
        self.detachView = detachView
        self.viewDidLoad = viewDidLoad        
        self.delegate = delegate
    }
}


extension SelectCommunityClient {
    public static var empty: Self {
        .init(delegate: .empty)
    }
}

let selectCommunityDelegate = Signal<SelectCommunityDelegateEvent, Never>.pipe()

extension SelectCommunityClient {
    public static func mock(caseStudy: CaseStudy) -> Self {
        .init(
            detachView: .empty,
            viewDidLoad: .fireAndForget {
                loadCommunities(caseStudy: caseStudy)
            },
            delegate: selectCommunityDelegate.output.producer)
    }
}


private func loadCommunities(caseStudy: CaseStudy) {
    selectCommunityDelegate.input.send(value: .showLoading)
    dispatchAfter(time: 2) {
        switch caseStudy {
        case .success:
            selectCommunityDelegate.input.send(value: .displayCommunities([.branbeCommunity, .unicefCommunity]))
            selectCommunityDelegate.input.send(value: .hideLoading)
        case .error:
            selectCommunityDelegate.input.send(value: .displayFailure(.message("error"), retryAction: .init(run: {
                loadCommunities(caseStudy: .success)
            })))
        case .empty:
            break
        }
    }
}
