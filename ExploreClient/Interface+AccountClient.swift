import ReactiveSwift
import Library
import BrModels

public struct UserUpdateRequest: Equatable {
    public let email: String
    public let password: String?
    public let name: String
    public let city: String?
    public let country: BrCountry?
    public let bio: String?
    public let avatarPath: String?
    public init(
        email: String,
        password: String?,
        name: String,
        city: String?,
        country: BrCountry?,
        bio: String?,
        avatarPath: String?
    ) {
        self.email = email
        self.password = password
        self.name = name
        self.city = city
        self.country = country
        self.bio = bio
        self.avatarPath = avatarPath
    }
}


public enum AccountDelegateEvent: Equatable {
    case displayInfo(BrUser)
    case logOutSuccessfully
    case didDisabledFCM(userId: String)
}


public struct AccountClient {
    public let onViewLoad: SignalProducer<Never, Never>
    public let onDetachView: SignalProducer<Never, Never>
    public let onEventUserUpdated: SignalProducer<Never, Never>
    public let logOut: SignalProducer<Never, Never>
    public let delegate: SignalProducer<AccountDelegateEvent, Never>
    public init(
        onViewLoad: SignalProducer<Never, Never>,
        onDetachView: SignalProducer<Never, Never>,
        onEventUserUpdated: SignalProducer<Never, Never>,
        logOut: SignalProducer<Never, Never>,
        delegate: SignalProducer<AccountDelegateEvent, Never>
    ) {
        self.onViewLoad = onViewLoad
        self.onDetachView = onDetachView
        self.onEventUserUpdated = onEventUserUpdated
        self.logOut = logOut
        self.delegate = delegate
    }
}

extension AccountClient {
    public static var empty: Self {
        AccountClient(
            onViewLoad: .empty,
            onDetachView: .empty,
            onEventUserUpdated: .empty,
            logOut: .empty,
            delegate: .empty
        )
    }
}

let accountDelegate =  Signal<AccountDelegateEvent, Never>.pipe()
extension AccountClient {
    public static func mock(_ caseStudy: CaseStudy = .success) -> Self {
        AccountClient(
            onViewLoad: .fireAndForget {
                accountDelegate.input.send(value: .displayInfo(.majid))
            },
            onDetachView: .empty,
            onEventUserUpdated: .fireAndForget {
                accountDelegate.input.send(value: .displayInfo(.majid))
            },
            logOut: .fireAndForget {
                accountDelegate.input.send(value: .didDisabledFCM(userId: BrUser.majid.id))
                accountDelegate.input.send(value: .logOutSuccessfully)
            },
            delegate: accountDelegate.output.producer
        )
    }
}




//updateUser: @escaping (UserUpdateRequest) -> SignalProducer<Never, Never>,
public enum UpdateUserDelegateEvent: Equatable {
    case showCountries([BrCountry])
    case showLoading, hideLoading
    case invalidEmail
    case displayUser(BrUser)
    case didUpdatedSuccessfully
    case showTransientFailure(Failure)
    case showFullScreenFailure(Failure, retry: RetryAction)
    case showFieldsRequiredMessage
}

public struct UpdateUserClient {
    public let onViewLoad: SignalProducer<Never, Never>
    public let onDetachView: SignalProducer<Never, Never>
    public let updateUser: (UserUpdateRequest) -> SignalProducer<Never, Never>
    public let delegate: SignalProducer<UpdateUserDelegateEvent, Never>
    public init(
        onViewLoad: SignalProducer<Never, Never>,
        onDetachView: SignalProducer<Never, Never>,
        updateUser: @escaping (UserUpdateRequest) -> SignalProducer<Never, Never>,
        delegate: SignalProducer<UpdateUserDelegateEvent, Never>
    ) {
        self.onViewLoad = onViewLoad
        self.onDetachView = onDetachView
        self.updateUser = updateUser
        self.delegate = delegate
    }
}

let updateUserDelegate = Signal<UpdateUserDelegateEvent, Never>.pipe()
extension UpdateUserClient {
    public static func mock(_ caseStudy: CaseStudy) -> Self {
        Self(
            onViewLoad: .fireAndForget {
                loadRequiredInfo(caseStudy: caseStudy)
            },
            onDetachView: .none,
            updateUser: { (request) in
                .fireAndForget {
                    
                    updateUserDelegate.input.send(value: .showLoading)
                    
                    dispatchAfter(time: 3.0) {
                        switch caseStudy {
                        case .success:
                            updateUserDelegate.input.send(value: .didUpdatedSuccessfully)
                            updateUserDelegate.input.send(value: .hideLoading)
                        case .error:
                            updateUserDelegate.input.send(value: .hideLoading)
                            updateUserDelegate.input.send(value: .showTransientFailure(.message("alert")))
                        case .empty:
                            break
                        }
                    }
                    
                }
            },
            delegate: updateUserDelegate.output.producer
        )
    }
}

private func loadRequiredInfo(caseStudy: CaseStudy) {
    
    updateUserDelegate.input.send(value: .showLoading)
    
    dispatchAfter(time: 2.0) {
        switch caseStudy {
        
        case .success:
            
            updateUserDelegate.input.send(value: .showCountries([.brazil, .spain]))
            
            updateUserDelegate.input.send(value: .displayUser(.majid))
            
            updateUserDelegate.input.send(value: .hideLoading)
            
        case .error:
            updateUserDelegate.input.send(value: .showFullScreenFailure(.message("error"), retry: .init(run: {
                loadRequiredInfo(caseStudy: .success)
            })))
        case .empty:
            break
        }
    }
}

