import ReactiveSwift
import Library
import BrModels

//onNotifyNavigateToStory(id
//onNotifyNavigateToProject(id
//onNotifyNavigateToCommunity(id


public enum NotificationsDelegateEvent: Equatable {
    case displayFailure(Failure, retryAction: RetryAction)
    case displayNotifications([BrNotification], didReachedEnd: Bool)
    case displayNoNotifications
    case showSuccessMessage(requestAccepted: Bool)
    case hideLoading, showLoading
    case showAlert(Failure)
}

public struct NotificationsClient {
    public let detachView: SignalProducer<Never, Never>
    public let viewDidLoad: SignalProducer<Never, Never>
    public let acceptNotification: (BrNotification) -> SignalProducer<Never, Never>
    public let rejectNotification: (BrNotification) -> SignalProducer<Never, Never>
    public let selectStoryId: (String) -> SignalProducer<Never, Never>
    public let selectProjectId: (String) -> SignalProducer<Never, Never>
    public let selectCommunityId: (String) -> SignalProducer<Never, Never>
    public let loadMore: SignalProducer<Never, Never>
    public let delegate: SignalProducer<NotificationsDelegateEvent, Never>
    public init(
        detachView: SignalProducer<Never, Never> = .empty,
        viewDidLoad: SignalProducer<Never, Never> = .empty,
        acceptNotification:
            @escaping (BrNotification) -> SignalProducer<Never, Never> = { _ in .empty },
        rejectNotification:
            @escaping (BrNotification) -> SignalProducer<Never, Never> = { _ in .empty },
        selectStoryId: @escaping (String) -> SignalProducer<Never, Never> = { _ in .empty },
        selectProjectId: @escaping (String) -> SignalProducer<Never, Never> = { _ in .empty },
        selectCommunityId: @escaping (String) -> SignalProducer<Never, Never> = { _ in .empty },
        loadMore: SignalProducer<Never, Never> = .empty,
        delegate: SignalProducer<NotificationsDelegateEvent, Never>
    ) {
        self.detachView = detachView
        self.viewDidLoad = viewDidLoad
        self.acceptNotification = acceptNotification
        self.rejectNotification = rejectNotification
        self.selectStoryId = selectStoryId
        self.selectProjectId = selectProjectId
        self.selectCommunityId = selectCommunityId
        self.loadMore = loadMore
        self.delegate = delegate
    }
}


extension NotificationsClient {
    public static var empty: Self {
        .init(
            detachView: .empty,
            viewDidLoad: .empty,
            acceptNotification: { _ in .empty},
            rejectNotification: { _ in .empty},
            selectStoryId: { _ in .empty},
            selectProjectId: { _ in .empty},
            selectCommunityId: { _ in .empty },
            loadMore: .empty,
            delegate: .empty
        )
    }
}


extension NotificationsClient {
    public static func mock(_ caseStudy: CaseStudy) -> Self {
        .init(
            detachView: .empty,
            viewDidLoad: .fireAndForget {
                loadNotificationPage(caseStudy: caseStudy)
            },
            acceptNotification: { _ in
                .fireAndForget {
                    switch caseStudy {
                    case .success:
                        notificationsDetailDelegate.input.send(value: .showSuccessMessage(requestAccepted: true))
                        loadNotificationPage(caseStudy: caseStudy)
                    case .error:
                        notificationsDetailDelegate.input.send(value: .showAlert(.message("alert")))
                    default:
                        break
                    }
                    
                }
            },
            rejectNotification: { _ in
                .fireAndForget {
                    switch caseStudy {
                    case .success:
                        notificationsDetailDelegate.input.send(value: .showSuccessMessage(requestAccepted: false))
                        loadNotificationPage(caseStudy: caseStudy)
                    case .error:
                        notificationsDetailDelegate.input.send(value: .showAlert(.message("alert")))
                    default:
                        break
                    }
                    
                }
            },
            selectStoryId: { _ in .empty },
            selectProjectId: { _ in .empty },
            selectCommunityId: { _ in .empty },
            loadMore: .fireAndForget {
                if caseStudy == .success {
                    notificationsDetailDelegate.input.send(value: .displayNotifications([.story, .project],
                            didReachedEnd: true))
                }
            },
            delegate: notificationsDetailDelegate.output.producer
        )
    }
}

// FIXME: Notification nagivations

let notificationsDetailDelegate = Signal<NotificationsDelegateEvent, Never>.pipe()
func loadNotificationPage(caseStudy: CaseStudy) {
    
    notificationsDetailDelegate.input.send(value: .showLoading)
    
    dispatchAfter(time: 3.0) {
        switch caseStudy {
        case .success:
            notificationsDetailDelegate.input.send(value: .displayNotifications([.story, .project],
                    didReachedEnd: false))
            notificationsDetailDelegate.input.send(value: .hideLoading)
        case .error:
            notificationsDetailDelegate.input.send(value: .displayFailure(.message("error loading"), retryAction: .init(run: {
                loadNotificationPage(caseStudy: .success)
            })))
        case .empty:
            notificationsDetailDelegate.input.send(value: .displayNoNotifications)
        }
    }
    
}
