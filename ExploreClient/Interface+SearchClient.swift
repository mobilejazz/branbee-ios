import ReactiveSwift
import BrModels

public enum SearchFilter: String, CaseIterable {
    case allTypes = "All Types"
    case stories
    case communities
    case projects
}

public struct SearchClient {
    public let detachView: SignalProducer<Never, Never>
    public let loadMore: SignalProducer<Never, Never>
    public let viewDidLoad: SignalProducer<Never, Never>
    public let search: (String, SearchFilter) -> SignalProducer<Never, Never>
    public let delegate: SignalProducer<SearchDelegateEvent, Never>
    public init(
        detachView: SignalProducer<Never, Never> = .empty,
        loadMore: SignalProducer<Never, Never> = .empty,
        viewDidLoad: SignalProducer<Never, Never> = .empty,
        search: @escaping (String, SearchFilter) -> SignalProducer<Never, Never> = { _,_ in
            .empty
        },
        delegate: SignalProducer<SearchDelegateEvent, Never>
    ) {
        self.detachView = detachView
        self.loadMore = loadMore
        self.viewDidLoad = viewDidLoad
        self.search = search
        self.delegate = delegate
    }
}

extension SearchClient {
    public static var empty: Self {
        .init(           
            delegate: .empty
        )
    }
}

import Library
public enum SearchDelegateEvent: Equatable {
    case items([BrFeedItem], didReachEnd: Bool)
    case showFullScreenFailure(Failure, retryAction: RetryAction)
    case showFailure(Failure)
    case showLoading, hideLoading
    case noItemFound
    case showSearchHint
    
    
    /*
     onNavigateToCommunity
     onNavigateToProject
     onNavigateToProjectComments
     onNavigateToStory
     onNavigateToStoryComments
     */
}
let searchDelegate = Signal<SearchDelegateEvent, Never>.pipe()
private func searchPage(_ caseStudy: CaseStudy, filter: SearchFilter) {
    searchDelegate.input.send(value: .showLoading)
    dispatchAfter(time: 1) {
        switch caseStudy {
        case .success:
            
            switch filter {
            case .allTypes:
                searchDelegate.input.send(value: .items(
                    [.brProjectFeed, .brStoryFeed, .brCommunityFeed]
                    , didReachEnd: true)
                )
            case .stories:
                searchDelegate.input.send(value: .items([.brStoryFeed], didReachEnd: true))
            case .communities:
                searchDelegate.input.send(value: .items([.brCommunityFeed], didReachEnd: true))
            case .projects:
                searchDelegate.input.send(value: .items([.brProjectFeed], didReachEnd: true))
            }
            
            
            
            searchDelegate.input.send(value: .hideLoading)
        case .error:
            searchDelegate.input.send(value: .showFullScreenFailure(.message("error"), retryAction: .init(run: {
                searchPage(.success, filter: filter)
            })))
        case .empty:
            searchDelegate.input.send(value: .noItemFound)
        }
    }
}


extension SearchClient {
    public static func mock(_ caseStudy: CaseStudy) -> Self {
        .init(
            detachView: .empty,
            loadMore: .fireAndForget {
                if caseStudy == .success {
                    searchDelegate.input.send(value: .items([.brProjectFeed], didReachEnd: true))
                }
            },
            viewDidLoad: .fireAndForget {
                searchPage(caseStudy, filter: .allTypes)
            },
            search: { (text, filter) in
                .fireAndForget {
                    searchPage(caseStudy, filter: filter)
                }
            },
            delegate: searchDelegate.output.producer
        )
    }
}
