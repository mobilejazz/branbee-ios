import ReactiveSwift
import Library


public struct StripeIntent: Hashable {
    public let id, clientSecret, customer: String
    public init(
        id: String,
        clientSecret: String,
        customer: String
    ) {
        self.id = id
        self.clientSecret = clientSecret
        self.customer = customer
    }
}

public enum AddCardDelegateEvent: Equatable {
    case onDisplayFillHolderNameTransientMessage
    case onNotifyStripeSetupIntentReceived(StripeIntent)
    case onNotifyCloseScreen(cardAdded: Bool)
    case onFailedToSetupCard
    case displayTransientError(Failure)
    case onDisplaySetupCardSuccessfullyMessage
}

public struct AddCardClient {
    public var addCardWithToken: (String) -> SignalProducer<Never, Never>
    public var didRequiredStripeIntentWithCardHolder: (String) -> SignalProducer<Never, Never>
    public var onEventStripeFailedToSetupCardWithFailure: (Failure?) -> SignalProducer<Never, Never>
    public var onDetachView: SignalProducer<Never, Never>
    public var delegate: SignalProducer<AddCardDelegateEvent, Never>
    public init(
        addCardWithToken: @escaping (String) -> SignalProducer<Never, Never>,
        didRequiredStripeIntentWithCardHolder: @escaping (String) -> SignalProducer<Never, Never>,
        onEventStripeFailedToSetupCardWithFailure: @escaping (Failure?) -> SignalProducer<Never, Never>,
        onDetachView: SignalProducer<Never, Never>,
        delegate: SignalProducer<AddCardDelegateEvent, Never>
    ) {
        self.addCardWithToken = addCardWithToken
        self.didRequiredStripeIntentWithCardHolder =         didRequiredStripeIntentWithCardHolder
        self.onEventStripeFailedToSetupCardWithFailure = onEventStripeFailedToSetupCardWithFailure
        self.onDetachView = onDetachView
        self.delegate = delegate
    }
}



extension AddCardClient {
    public static var empty: Self {
        AddCardClient(
            addCardWithToken: { _ in
                .empty
            },
            didRequiredStripeIntentWithCardHolder: { _ in
                .empty
            },
            onEventStripeFailedToSetupCardWithFailure: { _ in
                .empty
            },
            onDetachView: .empty,
            delegate: .empty
        )
    }
}

let addCardEvent = Signal<AddCardDelegateEvent, Never>.pipe()
extension AddCardClient {
    public static func mock(_ caseStudy: CaseStudy) -> Self {
        AddCardClient(
            addCardWithToken: { token in
                    switch caseStudy {
                    case .success:
                        addCardEvent.input.send(value: .onNotifyCloseScreen(cardAdded: true))
                    case .error:
                        addCardEvent.input.send(value: .displayTransientError(.message("error alert")))
                    case .empty:
                        break
                    }
                return .none
            },
            didRequiredStripeIntentWithCardHolder: { cardholer in
                                    
                    if cardholer == "" {
                        addCardEvent.input.send(value: .onDisplayFillHolderNameTransientMessage)
                        return .none
                    }
                                    
                    switch caseStudy {
                    case .success:
                        addCardEvent.input.send(value: .onNotifyStripeSetupIntentReceived(StripeIntent.init(
                                id: "id",
                                clientSecret: "xz",
                                customer: "majid"))
                            )
                    case .error:
                        addCardEvent.input.send(value: .displayTransientError(.message("error alert")))
                    case .empty:
                        break
                    }                    
                    
                return .none
            },
            onEventStripeFailedToSetupCardWithFailure: { _ in
                addCardEvent.input.send(value: .onFailedToSetupCard)
                return .none
            },
            onDetachView: .empty,
            delegate: addCardEvent.output.producer.delay(1.0, on: QueueScheduler.main)
        )
    }
}
