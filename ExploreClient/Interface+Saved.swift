import ReactiveSwift
import Library
import BrModels

public enum SavedStoriesDelegateEvent: Equatable {
    case showStories([BrStory])
    case showNoBookMaredStories
}

public struct SavedStoriesClient {
    public let detachView: SignalProducer<Never, Never>
    public let refresh: SignalProducer<Never, Never>
    public let delegate: SignalProducer<SavedStoriesDelegateEvent, Never>
    public init(
        detachView: SignalProducer<Never, Never> = .empty,
        refresh: SignalProducer<Never, Never> = .empty,
        delegate: SignalProducer<SavedStoriesDelegateEvent, Never>
    ) {
        self.detachView = detachView
        self.refresh = refresh
        self.delegate = delegate
    }
}

extension SavedStoriesClient {
    public static var empty: Self {
        Self(            
            delegate: .empty
        )
    }
}
let savedStoriesdelegate = Signal<SavedStoriesDelegateEvent, Never>.pipe()
extension SavedStoriesClient {
    public static func mock(_ caseStudy: CaseStudy) -> Self {
        Self(
            detachView: .empty,
            refresh: .fireAndForget {
                switch caseStudy {
                case .success:
                    savedStoriesdelegate.input.send(
                        value: .showStories([.branbeeBookMarkedStory,
                             .branbeeBookMarkedStory]))
                case .error:
                    break
                case .empty:
                    savedStoriesdelegate.input.send(
                        value: .showNoBookMaredStories)
                }
               
            },
            delegate: savedStoriesdelegate.output.producer
        )
    }
}
