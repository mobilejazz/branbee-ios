import ReactiveSwift
import Library
import BrModels

public enum PaymentsDelegateEvent: Equatable {
    case onDisplayAddCardWithAExistingOneWarning
    case onNotifyNavigateToCardCreation
    case onDisplayCard(CreditCard)
    case showLoading, hideLoading
    case showNoCard
    case displayTransientError(Failure)
    case failedObtainCard(Failure?, retryAction: RetryAction)
    case onNotifyStripeToGetCard(String)
}

/*
 //paymentDetailsPresenter.onEventStripeSucceedGettingCard(cardId: <#T##String#>, holder: <#T##String#>, last4Digits: <#T##String#>, expiryMonth: <#T##Int32#>, expiryYear: <#T##Int32#>)
 //paymentDetailsPresenter.onActionCreateCard()
 //paymentDetailsPresenter.onActionDeleteCard(continueToCreateCard: <#T##Bool#>)
 //paymentDetailsPresenter.onEventCardAdded()
 //paymentDetailsPresenter.onViewRefresh()
 //paymentDetailsPresenter.onEventStripeFailedGettingCard(cardId: <#T##String#>, errorCode: <#T##KotlinInt?#>, errorMessage: <#T##String?#>)
 */


public struct PaymentDetailsClient {
    public let onViewLoad: SignalProducer<Never, Never>
    public let onDetachView: SignalProducer<Never, Never>
    public let onEventStripeSucceedGettingCard: (CreditCard) -> SignalProducer<Never, Never>
    public let onActionCreateCard: SignalProducer<Never, Never>
    public let onActionDeleteCard: (Bool) -> SignalProducer<Never, Never>
    public let onEventCardAdded: SignalProducer<Never, Never>
    public let onViewRefresh: SignalProducer<Never, Never>
    public let onEventStripeFailedGettingCard: (String, Int?, String?) -> SignalProducer<Never, Never>
    public let delegate: SignalProducer<PaymentsDelegateEvent, Never>
    public init(
        onViewLoad: SignalProducer<Never, Never> = .empty,
        onDetachView: SignalProducer<Never, Never> = .empty,
        onEventStripeSucceedGettingCard: @escaping (CreditCard) -> SignalProducer<Never, Never> = { _ in
            .empty
        },
        onActionCreateCard: SignalProducer<Never, Never> = .empty,
        onActionDeleteCard: @escaping (Bool) -> SignalProducer<Never, Never> = { _ in .empty
        },
        onEventCardAdded: SignalProducer<Never, Never> = .empty,
        onViewRefresh: SignalProducer<Never, Never> = .empty,
        onEventStripeFailedGettingCard: @escaping (String, Int?, String?) -> SignalProducer<Never, Never> = { _,_,_ in
            .empty
        },
        delegate: SignalProducer<PaymentsDelegateEvent, Never>
    ) {
        self.onViewLoad = onViewLoad
        self.onDetachView = onDetachView
        self.onEventStripeSucceedGettingCard = onEventStripeSucceedGettingCard
        self.onActionCreateCard = onActionCreateCard
        self.onActionDeleteCard = onActionDeleteCard
        self.onEventCardAdded = onEventCardAdded
        self.onViewRefresh = onViewRefresh
        self.onEventStripeFailedGettingCard = onEventStripeFailedGettingCard
        self.delegate = delegate
    }
}


extension PaymentDetailsClient {
    public static var empty: Self {
        .init(delegate: .empty)
    }
}

let pddelegate = Signal<PaymentsDelegateEvent, Never>.pipe()
extension PaymentDetailsClient {
    public static func mock(_ caseStudy: CaseStudy) -> Self {
        .init(
            onViewLoad: .fireAndForget {
                getPayCard(caseStudy: caseStudy)
            },
            onDetachView: .empty,
            onEventStripeSucceedGettingCard: { (card) in
                dispatchAfter(time: 0, work: {
                    pddelegate.input.send(value: .onDisplayCard(card))
                    pddelegate.input.send(value: .hideLoading)
                })
                return .none
            },
            onActionCreateCard: .fireAndForget {
                switch caseStudy {
                case .success:
                    pddelegate.input.send(value: .onNotifyNavigateToCardCreation)
                default:
                    pddelegate.input.send(value: .onDisplayAddCardWithAExistingOneWarning)
                }
            },
            onActionDeleteCard: { (continute) in
                .fireAndForget {
                    pddelegate.input.send(value: .showLoading)
                    dispatchAfter(time: 2) {
                        switch caseStudy {
                        case .success:
                            pddelegate.input.send(value: .showNoCard)
                            pddelegate.input.send(value: .hideLoading)
                        case .error:
                            pddelegate.input.send(value: .displayTransientError(.message("error deleting card")))
                            pddelegate.input.send(value: .hideLoading)
                        case .empty:
                            break
                        }
                    }
                }
            },
            onEventCardAdded: .empty,
            onViewRefresh: .empty,
            onEventStripeFailedGettingCard: { (_, _, _) in
                .fireAndForget {
                    pddelegate.input.send(value: .failedObtainCard(nil, retryAction: RetryAction.init(run: {
                        getPayCard(caseStudy: .success)
                    })))
                }
            },
            delegate: pddelegate.output.producer
        )
    }
}


//override fun onActionCreateCard() {
//    if (currentCardId != null) {
//      view.get()?.onDisplayAddCardWithAExistingOneWarning()
//    } else {
//      view.get()?.onNotifyNavigateToCardCreation()
//    }
//  }

/*
 .init(onViewLoad: .fireAndForget {
     pddelegate.input.send(value: .onDisplayCard(.init(
                 id: "",
                 holder: "Majid",
                 last4Digits: "3212",
                 expiryMonth: 12,
                 expiryYear: 08))
     )
 },
 delegate: pddelegate.output.producer)
 */

private func getPayCard(caseStudy: CaseStudy) {
    pddelegate.input.send(value: .showLoading)
    dispatchAfter(time: 2.0) {
        switch caseStudy {
        case .success:
            pddelegate.input.send(value: .onNotifyStripeToGetCard(""))
        case .error:
            pddelegate.input.send(value: .failedObtainCard(.message("error"), retryAction: .init(run: {
                getPayCard(caseStudy: .success)
            })))
        case .empty:
            pddelegate.input.send(value: .showNoCard)
            pddelegate.input.send(value: .hideLoading)
        }
    }
    
  }
