import ReactiveSwift
import Library
import BrModels

public enum DraftsDelegateEvent: Equatable {
    case showDrafts([BrDraft])
}

public struct DraftsClient {
    public let detachView: SignalProducer<Never, Never>
    public let refresh: SignalProducer<Never, Never>
    public let deleteStory: (BrDraft) -> SignalProducer<Never, Never>
    public let delegate: SignalProducer<DraftsDelegateEvent, Never>
    public init(
        deleteStory: @escaping (BrDraft) -> SignalProducer<Never, Never>,
        detachView: SignalProducer<Never, Never>,
        refresh: SignalProducer<Never, Never>,
        delegate: SignalProducer<DraftsDelegateEvent, Never>
    ) {
        self.deleteStory = deleteStory
        self.detachView = detachView
        self.refresh = refresh
        self.delegate = delegate
    }
}

extension DraftsClient {
    public static var empty: Self {
        Self(
            deleteStory: { _ in .empty },
            detachView: .empty,
            refresh: .empty,            
            delegate: .empty
        )
    }
}
let draftdelegate = Signal<DraftsDelegateEvent, Never>.pipe()
extension DraftsClient {
    public static func mock(caseStudy: CaseStudy) -> Self {
        Self(
            deleteStory: { story in
                .fireAndForget {
                    draftdelegate.input.send(value: .showDrafts([.draft, .imageDraft, .textDraft].filter { $0.userId != story.userId }))
                }
            },
            detachView: .empty,
            refresh: .fireAndForget {
                switch caseStudy {
                case .success:
                    draftdelegate.input.send(value: .showDrafts([.draft, .imageDraft, .textDraft]))
                case .error:
                    draftdelegate.input.send(value: .showDrafts([]))
                case .empty:
                    draftdelegate.input.send(value: .showDrafts([]))
                }
            },
            delegate: draftdelegate.output.producer
        )
    }
}
