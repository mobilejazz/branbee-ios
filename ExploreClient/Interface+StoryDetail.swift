import ReactiveSwift
import Library
import BrModels

public struct StoryDetailClient {
    //onActionRetryLoading
    public var didProjectLinked: SignalProducer<Never, Never>
    public var detachView: SignalProducer<Never, Never>
    public var refresh: SignalProducer<Never, Never>
    public var viewDidLoad: SignalProducer<Never, Never>
    public var linkProject: SignalProducer<Never, Never>
    public var bookMark: SignalProducer<Never, Never>
    public var boost: SignalProducer<Never, Never>
    public var donateToProject: (BrProject) -> SignalProducer<Never, Never>
    public var delegate: SignalProducer<StoryDetailDelegateEvent, Never>
    public init(        
        donateToProject: @escaping (BrProject) -> SignalProducer<Never, Never> = { _ in .empty },
        detachView: SignalProducer<Never, Never> = .empty,
        viewDidLoad: SignalProducer<Never, Never> = .empty,
        refresh: SignalProducer<Never, Never> = .empty,
        linkProject: SignalProducer<Never, Never> = .empty,
        bookMark: SignalProducer<Never, Never> = .empty,
        boost: SignalProducer<Never, Never> = .empty,
        didProjectLinked: SignalProducer<Never, Never> = .empty,
        delegate: SignalProducer<StoryDetailDelegateEvent, Never> = .empty
    ) {
        self.donateToProject = donateToProject
        self.detachView = detachView
        self.viewDidLoad = viewDidLoad
        self.refresh = refresh
        self.delegate = delegate
        self.linkProject = linkProject
        self.bookMark = bookMark
        self.boost = boost
        self.didProjectLinked = didProjectLinked
    }
}

public enum StoryDetailDelegateEvent: Equatable {
    case isBookMarked(bookMarked: Bool)
    case boosts(Int, remaining: Int)
    case showLoading, hideLoading
    case display(story: BrStory, comment: BrComment?, linkedProjects: [BrProject], isLinkButtonShown: Bool)
    case failedToDisplayStory(Failure)
    case failedWithError(Failure)
    case enableBoostButton(enabled: Bool)
    // navigatetocomment(storyId
    // navigateToCommunityDetail(commId)
    // nav to project comments(projId)
    // nav to project detail(Project)
    // nav to pro donnation ( Project)
    // nav to pro link ( storyId, communityId)
    // shareStory(Story)
}

extension StoryDetailClient {
    public static var empty: Self {
        Self(
            donateToProject: { _ in .empty },
            detachView: .empty,
            viewDidLoad: .empty,
            refresh: .empty,
            linkProject: .empty,
            bookMark: .empty,
            boost: .empty,
            didProjectLinked: .empty,
            delegate: .empty
        )
    }
    
}
let storyDetailDelegate = Signal<StoryDetailDelegateEvent, Never>.pipe()

extension StoryDetailClient {
    public static func mock(caseStudy: CaseStudy) -> Self {
        Self(
            donateToProject: { _ in .empty },
            detachView: .none,
            viewDidLoad: .fireAndForget {
                loadStory(caseStudy: caseStudy)
            },
            refresh: .fireAndForget {
                loadStory(caseStudy: caseStudy)
            },
            linkProject: .empty,
            bookMark: .fireAndForget {
                switch caseStudy {
                case .success:
                    storyDetailDelegate.input.send(value: .isBookMarked(bookMarked: Bool.random()))
                case .error:
                    storyDetailDelegate.input.send(value: .failedWithError(.message("error")))
                    storyDetailDelegate.input.send(value: .isBookMarked(bookMarked: false))
                case .empty:
                    break
                }
            },
            boost: .fireAndForget {
                switch caseStudy {
                case .success:
                    storyDetailDelegate.input.send(value: .boosts(Int.random(in: 1...10), remaining: Int.random(in: 1...10)))
                case .error:
                    storyDetailDelegate.input.send(value: .failedWithError(.message("error")))
                case .empty:
                    break
                }
            },
            didProjectLinked: .empty,
            delegate: storyDetailDelegate.output.producer
        )
    }
}

private func loadStory(caseStudy: CaseStudy) {
        
    storyDetailDelegate.input.send(value: .showLoading)
    
    dispatchAfter(time: 1.0) {
        switch caseStudy {
        case .success:
            storyDetailDelegate.input.send(
                       value: .display(
                           story: BrStory.salma,
                           comment: nil,
                           linkedProjects: [],
                           isLinkButtonShown: true)
                   )
            storyDetailDelegate.input.send(value: .enableBoostButton(enabled: true))
            
            storyDetailDelegate.input.send(
                value: .isBookMarked(bookMarked: true))
            
            storyDetailDelegate.input.send(
                value: .hideLoading)
            
        case .error:
            storyDetailDelegate.input.send(value: .failedToDisplayStory(.message("failed")))
        case .empty:
                                    
            storyDetailDelegate.input.send(
                       value: .display(
                           story: .branbeeStory,
                           comment: nil,
                           linkedProjects: [],
                           isLinkButtonShown: true)
                   )
            storyDetailDelegate.input.send(value: .enableBoostButton(enabled: false))
            
            storyDetailDelegate.input.send(value: .isBookMarked(bookMarked: false))
            
            storyDetailDelegate.input.send(value: .hideLoading)
        }
    }
            
  }

