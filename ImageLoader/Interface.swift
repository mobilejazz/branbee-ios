import UIKit.UIImage
import Library
import ReactiveSwift

public struct ImageLoader {    
    public var loadFromURL: (URL) -> SignalProducer<UIImage?, Never>
    public let delete: (String) -> SignalProducer<Never, Never>
    public let get: (String) -> SignalProducer<String?, Never>
    public let isCached: (String) -> SignalProducer<Bool, Never>
    public let put: (String, String) -> SignalProducer<Never, Never>
    public init(
        loadFromURL: @escaping (URL) -> SignalProducer<UIImage?, Never> = { _ in .empty },
        delete: @escaping (String) -> SignalProducer<Never, Never> = { _ in .empty } ,
        get: @escaping (String) -> SignalProducer<String?, Never> = { _ in .empty },
        isCached: @escaping (String) -> SignalProducer<Bool, Never> = { _ in .empty } ,
        put: @escaping (String, String) -> SignalProducer<Never, Never> = { _,_ in .empty }
    ) {
        self.loadFromURL = loadFromURL
        self.delete = delete
        self.get = get
        self.isCached = isCached
        self.put = put
    }
}
