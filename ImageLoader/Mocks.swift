import ReactiveSwift
import UIKit.UIImage

extension ImageLoader {
    public static var empty: Self {
        Self.init()
    }
}

extension ImageLoader {
    public static var placeholder: Self {
        .init(
            loadFromURL: { _ in
                SignalProducer(value: UIImage(named: "pexels"))
            },
            delete: { _ in
                .none
            }) { _ in
                SignalProducer<String?, Never>(value: "key")
            } isCached: { path -> SignalProducer<Bool, Never> in
                .init(value: false)
            } put: { (key, path) -> SignalProducer<Never, Never> in
                .none
            }

    }
}

