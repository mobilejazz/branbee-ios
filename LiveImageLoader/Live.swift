import ReactiveSwift
import ImageLoader
import UIKit.UIImage
import Library
import SDWebImage
import BranbeeSDK
import core

extension ImageLoader {
    public static var live: Self {
        Self(loadFromURL: { photoURL in
            .future { promise in
                
                    if let image = SDImageCache.shared
                        .imageFromCache(forKey: photoURL.absoluteString) {
                        promise(.success(image))
                    }
                    
                    SDWebImageManager.shared.imageLoader.requestImage(
                        with: photoURL,
                        options: .retryFailed,
                        context: nil,
                        progress: nil) { (image, _, error, _) in
                        if let image = image {
                            SDImageCache.shared.store(image,
                                                      forKey: photoURL.absoluteString,
                                                      completion: nil)
                            promise(.success(image))
                        }
                        if let _ = error {
                            promise(.success(nil))
                        }
                    }
                }
        }, delete: { (key)  in
            .fireAndForget {
                imageCache.delete(key: key)
            }
        }, get: { key in
            .future { promise in
                promise(.success(imageCache.get(key: key)))
            }
        }, isCached: { (key) in
            .future { promise in
                promise(.success(imageCache.isCached(key: key)))
            }
        }, put: { (key, path) in
            .fireAndForget {
                imageCache.put(key: key, path: path)
            }
        }
        )
    }
}


private var imageCache: ImageCache { branbeeShared.imageCache }


class IOSUnauthorizedResolution: Harmony_kotlinUnauthorizedResolution {
    public func resolve() -> Bool {        
        return true
    }
}
